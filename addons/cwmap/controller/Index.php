<?php

namespace addons\cwmap\controller;

use think\addons\Controller;
use think\Config;
use think\Hook;

class Index extends Controller {

    protected $model = null;
    protected $searchFields = 'id,name';

    public function _initialize() {
        $this->model = new \app\admin\model\Company;
        parent::_initialize();
    }

    public function index() {
        $where['lat'] = ['>',0];
        //查看显示控制最小积分
        $cinfo = db('kpi_config')->where(['id'=>1])->find();
        if ($cinfo && isset($cinfo['min_score'])) {
            $where['score'] = ['>=',$cinfo['min_score']];
        }
        $data = $this->model->where($where)->select();
        $this->assign('data', json_encode($data));
        //新增显示服务站列表
        $stlist = db('service_station')->field('name,lat,lng,address,url')->select();
        $this->assign('stlist', json_encode($stlist));
        //获取统计信息
        $cinfo = db('front')->where(['id'=>1])->find();
        $this->assign('cinfo', json_encode($cinfo));

        $dconfig = get_addon_config('cwmap');

        // 语言检测
        $lang = strip_tags($this->request->langset());

        $site = Config::get("site");

        // 配置信息
        $config = [
            'site'           => array_intersect_key($site, array_flip(['name', 'cdnurl', 'version', 'timezone', 'languages'])),
            'upload'         => null,
            'modulename'     => 'addons',
            'controllername' => 'index',
            'actionname'     => 'index',
            'jsname'         => 'addons/cwmap',
            'moduleurl'      => '',
            'language'       => $lang
        ];
        $config = array_merge($config, $dconfig);
        $this->assign('config', $config);
        return view();
    }

    public function search() {
        if (request()->isAjax()) {
            $search = request()->get('keyword');
            $searcharr = is_array($this->searchFields) ? $this->searchFields : explode(',', $this->searchFields);

            $result = $this->model->where(implode("|", $searcharr), "LIKE", "%{$search}%")->select();
            return json_encode($result);
            //$this->success('',$result);
        }
    }

}
