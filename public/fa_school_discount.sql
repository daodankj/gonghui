/*
Navicat MySQL Data Transfer

Source Server         : 本地库
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : fastadmin

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2020-09-21 17:26:38
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for fa_school_discount
-- ----------------------------
DROP TABLE IF EXISTS `fa_school_discount`;
CREATE TABLE `fa_school_discount` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(5) unsigned NOT NULL DEFAULT '0',
  `user_id` int(5) unsigned NOT NULL COMMENT '会员',
  `image` varchar(100) NOT NULL DEFAULT '' COMMENT '申请文件',
  `add_time` int(5) unsigned NOT NULL COMMENT '上报时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '审核状态',
  `result` varchar(255) NOT NULL DEFAULT '' COMMENT '审核意见',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='上学优惠';

-- ----------------------------
-- Table structure for fa_site_discount
-- ----------------------------
DROP TABLE IF EXISTS `fa_site_discount`;
CREATE TABLE `fa_site_discount` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(5) unsigned NOT NULL DEFAULT '0',
  `user_id` int(5) unsigned NOT NULL COMMENT '会员',
  `image` varchar(100) NOT NULL DEFAULT '' COMMENT '申请文件',
  `add_time` int(5) unsigned NOT NULL COMMENT '上报时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '审核状态',
  `result` varchar(255) NOT NULL DEFAULT '' COMMENT '审核意见',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='场地优惠';

-- ----------------------------
-- Table structure for fa_skills_education
-- ----------------------------
DROP TABLE IF EXISTS `fa_skills_education`;
CREATE TABLE `fa_skills_education` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `company_id` int(5) unsigned NOT NULL DEFAULT '0',
  `user_id` int(5) unsigned NOT NULL COMMENT '会员',
  `image` varchar(100) NOT NULL DEFAULT '' COMMENT '申请文件',
  `add_time` int(5) unsigned NOT NULL COMMENT '上报时间',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '审核状态',
  `result` varchar(255) NOT NULL DEFAULT '' COMMENT '审核意见',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='技能学历培训';
SET FOREIGN_KEY_CHECKS=1;
