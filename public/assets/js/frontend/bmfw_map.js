define(['jquery', 'bootstrap', 'frontend', 'form', 'template','echarts', 'echarts-theme'], function ($, undefined, Frontend, Form, Template,Echarts, undefined) {
    var validatoroptions = {
        invalid: function (form, errors) {
            $.each(errors, function (i, j) {
                Layer.msg(j);
            });
        }
    };
    var Controller = {

        info: function () {
            $(".btnimg").click(function(){
                var url = $(this).attr('data-url');
                var title = $(this).attr('data-title');
                parent.Fast.api.open(url,title,{
                    area:['1200px','600px']
                });
            });
        },


        api: {

        }
    };
    return Controller;
});
