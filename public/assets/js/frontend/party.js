define(['jquery', 'bootstrap', 'frontend', 'form', 'template'], function ($, undefined, Frontend, Form, Template) {
    var validatoroptions = {
        invalid: function (form, errors) {
            $.each(errors, function (i, j) {
                Layer.msg(j);
            });
        }
    };
    var Controller = {
        index: function () {

        },
        //活动发布
        active: function () {
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg);
                window.location.href=res.url;//成功跳转
            },function(data,res){
                Layer.alert(res.msg);
            });

        },
        meeting: function () {
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg);
                window.location.href=res.url;//成功跳转
            },function(data,res){
                Layer.alert(res.msg);
            });

        },
        change: function () {
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg);
                window.location.href=res.url;//成功跳转
            },function(data,res){
                Layer.alert(res.msg);
            });

        },
        apply: function () {
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg);
                window.location.href=res.url;//成功跳转
            },function(data,res){
                Layer.alert(res.msg);
            });

        },
        member_change_apply: function () {
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg);
                window.location.href=res.url;//成功跳转
            },function(data,res){
                Layer.alert(res.msg);
            });

        },
        study: function () {
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg);
                window.location.href=res.url;//成功跳转
            },function(data,res){
                Layer.alert(res.msg);
            });

        },
        //党员创新工作室申报
        model_craftsman:function(){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },

        //新增功能
        warninglist:function(){
            $(".read").click(function(){
                var that = this;
                Layer.confirm('确认已读吗？', function (index) {
                    var id = $(that).attr('data-id');
                    layer.close(index);
                    var loadindex = Layer.load();
                    $.post({
                        url: '/index/party/warningred',
                        dataType: 'json',
                        data:{id:id},
                        cache: false,
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                var msg = ret.hasOwnProperty("msg") && ret.msg != "" ? ret.msg : "";
                                if (ret.code === 1) {
                                    Toastr.success(msg ? msg : '确认成功');
                                    layer.close(loadindex);
                                    window.location.reload();
                                } else {
                                    Toastr.error(msg ? msg : '确认失败');
                                    layer.close(loadindex);
                                }
                            } else {
                                Toastr.error(__('Unknown data format'));
                                layer.close(loadindex);
                            }
                        }, error: function () {
                            Toastr.error(__('Network error'));
                            layer.close(loadindex);
                        }
                    });
                });
            })
        },
        //成立申请
        partyapply:function(){
            $("#submit").attr('disabled',false);
            //Controller.api.bindevent();
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        //换届申请
        partychangeapply:function(){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        //改选申请
        partyupdateapply:function(){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        //指标上报
        kpiadd: function () {
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg);
                window.location.href=res.url;//成功跳转
            },function(data,res){
                Layer.alert(res.msg);
            });

        },
        //学习活动发布
        study_active: function () {
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg);
                window.location.href=res.url;//成功跳转
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        //新闻稿上报
        news: function () {
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg);
                window.location.href=res.url;//成功跳转
            },function(data,res){
                Layer.alert(res.msg);
            });
        },

        api: {

        }
    };
    return Controller;
});
