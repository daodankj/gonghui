define(['jquery', 'bootstrap', 'frontend', 'form', 'template'], function ($, undefined, Frontend, Form, Template) {
    var validatoroptions = {
        invalid: function (form, errors) {
            $.each(errors, function (i, j) {
                Layer.msg(j);
            });
        }
    };
    var Controller = {
        smyy: function () {
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg);
            },function(data,res){
                Layer.alert(res.msg);
            });

        },

        smyycar: function () {
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg);
            },function(data,res){
                Layer.alert(res.msg);
            });

        },
        
        search: function () {  
            $("#submit").attr('disabled',false);
            /*Form.api.bindevent($("#user-form"),function(data,res){
                window.location.href=data.url;//查询成功跳转到预约页面
            },function(data,res){
                Layer.alert(data.info,function(index){
                     window.location.href=data.url;//失败提示，跳转到首页
                });
            });*/
        },
        
        merchant:function(){
            Controller.api.bindevent();
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg);
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        api: {
            bindevent: function (value='') {
                require(['async!BMap3'], function () {
                    // 地图API功能
                    var map = new BMap.Map("allmap");

                    // 创建地址解析器实例
                    var myGeo = new BMap.Geocoder();
                    

                    function G(id) {
                        return document.getElementById(id);
                    }

                    var ac = new BMap.Autocomplete(//建立一个自动完成的对象
                            {"input": "address"
                                , "location": map
                            });
                    if (value) {
                        ac.setInputValue(value);
                    }
                    var myValue;
                    ac.addEventListener("onconfirm", function (e) {    //鼠标点击下拉列表后的事件
                        //console.log(e.item);
                        var _value = e.item.value;
                        myValue = _value.province + _value.city + _value.district + _value.street + _value.business;
                        G("searchResultPanel").innerHTML = "onconfirm<br />index = " + e.item.index + "<br />myValue = " + myValue;
                        setPlace();
                    });

                    
                    function setPlace() {
                        function myFun() {
                            var pp = local.getResults().getPoi(0).point;    //获取第一个智能搜索的结果
                            myGeo.getLocation(pp, function (rs) {
                                var addComp = rs.addressComponents;
                                $("#lng").val(pp.lng);
                                $("#lat").val(pp.lat);
                                //Layer.msg(__('Position update') + ' <br> ' + __('Longitude') + ' : ' + pp.lng + ' , ' + __('Latitude') + ' : ' + pp.lat);
                            });
                        }
                        var local = new BMap.LocalSearch(map, {//智能搜索
                            onSearchComplete: myFun
                        });
                        local.search(myValue);
                    }
                })

            }
        }
    };
    return Controller;
});