define(['jquery', 'bootstrap', 'frontend', 'form', 'template'], function ($, undefined, Frontend, Form, Template) {
    var validatoroptions = {
        invalid: function (form, errors) {
            $.each(errors, function (i, j) {
                Layer.msg(j);
            });
        }
    };
    var Controller = {
        index: function () {

        },
        warninglist:function(){
            $(".read").click(function(){
                var that = this;
                Layer.confirm('确认已读吗？', function (index) {
                    var id = $(that).attr('data-id');
                    layer.close(index);
                    var loadindex = Layer.load();
                    $.post({
                        url: '/index/admin/warningred',
                        dataType: 'json',
                        data:{id:id},
                        cache: false,
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                var msg = ret.hasOwnProperty("msg") && ret.msg != "" ? ret.msg : "";
                                if (ret.code === 1) {
                                    Toastr.success(msg ? msg : '确认成功');
                                    layer.close(loadindex);
                                    window.location.reload();
                                } else {
                                    Toastr.error(msg ? msg : '确认失败');
                                    layer.close(loadindex);
                                }
                            } else {
                                Toastr.error(__('Unknown data format'));
                                layer.close(loadindex);
                            }
                        }, error: function () {
                            Toastr.error(__('Network error'));
                            layer.close(loadindex);
                        }
                    });
                });
            })
        },
        //成立申请
        unionapply:function(){
            $("#submit").attr('disabled',false);
            //Controller.api.bindevent();
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        //换届申请
        unionchangeapply:function(){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        //改选申请
        unionupdateapply:function(){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        //文体活动
        unionactive:function(){
            //判断是否有指标未完成
            if (kpiInfo.status==1) {
                /*Layer.confirm('该任务考核指标尚未完成上报，是否去完成上报？',function(){
                    window.location.href=kpiInfo.url;//成功跳转首页面
                })*/
                Layer.alert('该任务考核指标尚未完成上报，是否去完成上报？',function () {
                    window.location.href=kpiInfo.url;//成功跳转首页面
                });
            }
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        capital: function() {
            //判断是否有指标未完成
            if (kpiInfo.status==1) {
                /*Layer.confirm('该任务考核指标尚未完成上报，是否去完成上报？',function(){
                    window.location.href=kpiInfo.url;//成功跳转首页面
                })*/
                Layer.alert('该任务考核指标尚未完成上报，是否去完成上报？',function () {
                    window.location.href=kpiInfo.url;//成功跳转首页面
                });
            }
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg);
                window.location.href=res.url;//成功跳转
            },function(data,res){
                Layer.alert(res.msg);
            });
            Controller.api.bindevent();
        },
        //指标上报
        kpiadd: function () {
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg);
                window.location.href=res.url;//成功跳转
            },function(data,res){
                Layer.alert(res.msg);
            });

        },
        company_info: function() {
            //判断是否有指标未完成
            if (kpiInfo.status==1) {
                Layer.confirm('该任务考核指标尚未完成上报，是否去完成上报？',function(){
                    window.location.href=kpiInfo.url;//成功跳转首页面
                })
            }
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg);
                window.location.href=res.url;//成功跳转
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        //议题讨论
        proposal_info:function(){
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
            $(".btn-embossed").click(function(){
                var status = $(this).attr('status');
                $("#status").val(status);
                $("#user-form").submit();
            })
        },
        //厂务信息发布
        factory_information_add:function(){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },

        api: {
            bindevent:function(){
                $("#savecg").click(function(){
                    var type = $(this).attr('data-type');
                    var url = '/index/index/saveCg/type/'+type;
                    var data = $("#user-form").serialize();
                    var loadindex = Layer.load();
                    $.post({
                        url: url,
                        dataType: 'json',
                        data:data,
                        cache: false,
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                var msg = ret.hasOwnProperty("msg") && ret.msg != "" ? ret.msg : "";
                                if (ret.code === 1) {
                                    Toastr.success(msg ? msg : '保存成功');
                                    layer.close(loadindex);
                                } else {
                                    Toastr.error(msg ? msg : '保存失败');
                                    layer.close(loadindex);
                                }
                            } else {
                                Toastr.error(__('Unknown data format'));
                                layer.close(loadindex);
                            }
                        }, error: function () {
                            Toastr.error(__('Network error'));
                            layer.close(loadindex);
                        }
                    });
                });
                $("#delCache").click(function(){
                    var type = $(this).attr('data-type');
                    var url = '/index/index/delCache/type/'+type;
                    Layer.confirm('确认删除？', function (index) {
                        layer.close(index);
                        var loadindex = Layer.load();
                        $.post({
                            url: url,
                            dataType: 'json',
                            cache: false,
                            success: function (ret) {
                                if (ret.hasOwnProperty("code")) {
                                    var msg = ret.hasOwnProperty("msg") && ret.msg != "" ? ret.msg : "";
                                    if (ret.code === 1) {
                                        Toastr.success(msg ? msg : '删除成功');
                                        //layer.close(loadindex);
                                        window.location.reload();
                                    } else {
                                        Toastr.error(msg ? msg : '删除失败');
                                        layer.close(loadindex);
                                    }
                                } else {
                                    Toastr.error(__('Unknown data format'));
                                    layer.close(loadindex);
                                }
                            }, error: function () {
                                Toastr.error(__('Network error'));
                                layer.close(loadindex);
                            }
                        });
                    })
                });
            }
        }
    };
    return Controller;
});
