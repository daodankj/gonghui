define(['jquery', 'bootstrap', 'frontend', 'form', 'template'], function ($, undefined, Frontend, Form, Template) {
    var validatoroptions = {
        invalid: function (form, errors) {
            $.each(errors, function (i, j) {
                Layer.msg(j);
            });
        }
    };
    var Controller = {
        index: function () {

        },
        //活动发布
        info: function () {
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                //Layer.alert(res.msg);
                //$("#qrimg").show();
                //window.location.href=res.url;//成功跳转
                var url = $("#qrimg").attr('data-url');
                var img = '<img width="300px" src="'+url+'" />';
                layer.open({
                  title: '微信群二维码',
                  content: '<p>报名成功！请扫描下面二维码加入微信群以便接受通知</p>'+img,
                  area: ['320px', '600px'], //宽高
                });
            },function(data,res){
                Layer.alert(res.msg);
            });

        },
        qiandao: function () {
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg);
                //window.location.href=res.url;//成功跳转
            },function(data,res){
                Layer.alert(res.msg);
            });

        },

        api: {

        }
    };
    return Controller;
});
