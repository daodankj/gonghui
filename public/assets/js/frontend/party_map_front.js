define(['jquery', 'bootstrap', 'frontend', 'form', 'template','echarts', 'echarts-theme'], function ($, undefined, Frontend, Form, Template,Echarts, undefined) {
    var validatoroptions = {
        invalid: function (form, errors) {
            $.each(errors, function (i, j) {
                Layer.msg(j);
            });
        }
    };
    var Controller = {

        index: function () {
          $(".btn-dialog").click(function(){
             var url = $(this).attr('data-url');
             parent.Fast.api.open(url,'企业VR');
          });
          $(".btn-dialog2").click(function(){
             var id = $(this).attr('data-id');
             var url = '/admin/company_score_log/list?company_id='+id;
             parent.Fast.api.open(url,'企业积分明细');
          });
          $(".btnimg").click(function(){
             var url = $(this).attr('data-url');
             var title = $(this).attr('data-title');
             parent.Fast.api.open(url,title);
          });
        },
         business_list:function(){
            $(".btnimg").click(function(){
                 var url = $(this).attr('data-url');
                 var title = $(this).attr('data-title');
                 parent.Fast.api.open(url,title);
            });
        },
        score_top: function(){
          Form.api.bindevent($("form[role=form]"));
          $(".btn").click(function(){
             var topic_id = $('#topic_id').val();
             var id = $('#company_id').val();
             id = id?id:'0';
             topic_id = topic_id?topic_id:'0';
             window.location.href='/index/party_map_front/score_top/company_id/'+id+'/topic_id/'+topic_id;
          })
          $(".openInfo").click(function(){
             var id = $(this).attr('data-id');
             parent.Fast.api.open('/index/party_map_front/index/id/'+id,'企业信息',{
                area:['1200px','600px']
            });
          });
        },

        api: {

        }
    };
    return Controller;
});
