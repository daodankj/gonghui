define(['jquery', 'bootstrap', 'frontend', 'form', 'template'], function ($, undefined, Frontend, Form, Template) {
    var validatoroptions = {
        invalid: function (form, errors) {
            $.each(errors, function (i, j) {
                Layer.msg(j);
            });
        }
    };
    var Controller = {
        index: function () {  
            $(".gotothird").click(function(){
                var url = $(this).attr('data-url');
                Layer.alert("即将跳转到第三方平台，本平台由慧创公司提供运营服务",function(index){
                     window.location.href = url;
                });
            })
        },
        zgyhzc:function(){
            $(".gotothird").click(function(){
                Layer.alert("即将跳转到第三方平台",function(index){
                     window.location.href='https://mall.joywon.cn/app/index.php?i=3&c=entry&m=ewei_shopv2&do=mobile&mid=1&ml=236';//成功跳转首页面
                });
                //return false;
            })
        },
        //登入
        login:function(){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        //企业绑定
        bindcompany: function () {
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                //console.log(res)
                Layer.alert(res.msg);
                window.location.href=res.url;//成功跳转首页面
                /*Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });*/
            },function(data,res){
                Layer.alert(res.msg);
            });

        },
        //成立申请
        unionapply:function(){
            $("#submit").attr('disabled',false);
            //Controller.api.bindevent();
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        //换届申请
        unionchangeapply:function(){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        //改选申请
        unionupdateapply:function(){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        //文体活动
        unionactive:function(){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        //职工维权劳资纠纷申请
        weiquan:function(){
            $(".submit").attr('disabled',false);
            Form.api.bindevent($(".user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        //困难职工上报
        kunnan:function(){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
            $(".charge").blur(function(){
                var value1 = parseInt($("#medical_charge").val()?$("#medical_charge").val():0);
                var value2 = parseInt($("#school_charge").val()?$("#school_charge").val():0);
                var value3 = parseInt($("#living_charge").val()?$("#living_charge").val():0);
                var value4 = parseInt($("#child_charge").val()?$("#child_charge").val():0);
                var value5 = parseInt($("#else_charge").val()?$("#else_charge").val():0);
                var value6 = parseInt($("#rent_charge").val()?$("#rent_charge").val():0);
                var value7 = parseInt($("#house_charge").val()?$("#house_charge").val():0);
                $("#total_charge").val(value1+value2+value3+value4+value5+value6+value7);
                $("#total_charge2").val(value1+value2+value3+value4+value5+value6+value7);
            });
            Controller.api.bindCg();
        },
        //欠薪求助申请
        arrears_help:function(){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
            $(".charge").blur(function(){
                var value1 = parseInt($("#medical_charge").val()?$("#medical_charge").val():0);
                var value2 = parseInt($("#school_charge").val()?$("#school_charge").val():0);
                var value3 = parseInt($("#living_charge").val()?$("#living_charge").val():0);
                var value4 = parseInt($("#child_charge").val()?$("#child_charge").val():0);
                var value5 = parseInt($("#else_charge").val()?$("#else_charge").val():0);
                var value6 = parseInt($("#rent_charge").val()?$("#rent_charge").val():0);
                var value7 = parseInt($("#house_charge").val()?$("#house_charge").val():0);
                $("#total_charge").val(value1+value2+value3+value4+value5+value6+value7);
                $("#total_charge2").val(value1+value2+value3+value4+value5+value6+value7);
            });
            Controller.api.bindCg();
        },
        //圆梦助学申请
        dream_apply:function(){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
            $(".charge").blur(function(){
                var value1 = parseInt($("#medical_charge").val()?$("#medical_charge").val():0);
                var value2 = parseInt($("#school_charge").val()?$("#school_charge").val():0);
                var value3 = parseInt($("#living_charge").val()?$("#living_charge").val():0);
                var value4 = parseInt($("#child_charge").val()?$("#child_charge").val():0);
                var value5 = parseInt($("#else_charge").val()?$("#else_charge").val():0);
                var value6 = parseInt($("#rent_charge").val()?$("#rent_charge").val():0);
                var value7 = parseInt($("#house_charge").val()?$("#house_charge").val():0);
                $("#total_charge").val(value1+value2+value3+value4+value5+value6+value7);
                $("#total_charge2").val(value1+value2+value3+value4+value5+value6+value7);
            });
        },
        //职工帮扶
        employees_assistance:function(){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        //企业人才服务--劳模工匠人才创新申报
        model_craftsman:function(){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        //企业人才服务--职工技能竞赛信息上报
        skills_competition:function(){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        //企业人才服务--技能学历培训
        skills_education:function(){
            $(".submit").attr('disabled',false);
            $("#nb_school_name").data("params", function (obj) {
                return {custom: {pid:0,type: $("#education2").val()=='大专'?2:1}};
            });
            $("#nb_major_name").data("params", function (obj) {
                return {custom: {pid: $("#nb_school_name").val()?$("#nb_school_name").val():11111}};
            });
            Form.api.bindevent($(".user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
            $(document).on("change", "#education2", function(){
                $("#nb_school_name").data("params", function (obj) {
                    return {custom: {pid:0,type: $("#education2").val()=='大专'?2:1}};
                });
                $('#nb_school_name').selectPageClear();
                $('#nb_major_name').selectPageClear();
            });
            $("#nb_school_name").data("eSelect", function(){
                $("#nb_major_name").data("params", function (obj) {
                    return {custom: {pid: $("#nb_school_name").val()}};
                });
                $('#nb_major_name').selectPageClear();
            });
        },
        //普惠职工--活动场地优惠
        site_discount:function(){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        //普惠职工--上学优惠
        school_discount:function(){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        //普惠职工--购物卡优惠
        shopcard_baoming:function(){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        send_file:function(){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg);
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        //个人信息
        information:function(){
            $(".unband").click(function(){
                Layer.confirm('确认解绑吗？', function (index) {
                    layer.close(index);
                    var loadindex = Layer.load();
                    $.post({
                        url: '/api/index/unbind',
                        dataType: 'json',
                        cache: false,
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                var msg = ret.hasOwnProperty("msg") && ret.msg != "" ? ret.msg : "";
                                if (ret.code === 1) {
                                    Toastr.success(msg ? msg : '解绑成功');
                                    layer.close(loadindex);
                                    window.location.reload();
                                } else {
                                    Toastr.error(msg ? msg : '解绑失败');
                                    layer.close(loadindex);
                                }
                            } else {
                                Toastr.error(__('Unknown data format'));
                                layer.close(loadindex);
                            }
                        }, error: function () {
                            Toastr.error(__('Network error'));
                            layer.close(loadindex);
                        }
                    });
                });       
            }) 
        },
        notice_list:function(){
            $(".read").click(function(){
                var that = this;
                Layer.confirm('确认已读吗？', function (index) {
                    var id = $(that).attr('data-id');
                    layer.close(index);
                    var loadindex = Layer.load();
                    $.post({
                        url: '/index/index/noticered',
                        dataType: 'json',
                        data:{id:id},
                        cache: false,
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                var msg = ret.hasOwnProperty("msg") && ret.msg != "" ? ret.msg : "";
                                if (ret.code === 1) {
                                    Toastr.success(msg ? msg : '确认成功');
                                    layer.close(loadindex);
                                    window.location.reload();
                                } else {
                                    Toastr.error(msg ? msg : '确认失败');
                                    layer.close(loadindex);
                                }
                            } else {
                                Toastr.error(__('Unknown data format'));
                                layer.close(loadindex);
                            }
                        }, error: function () {
                            Toastr.error(__('Network error'));
                            layer.close(loadindex);
                        }
                    });
                });       
            })
        },
        feedback(){
            /*$("#feedback").click(function(){
                Fast.api.open('/index/index/feedback_add', '意见反馈',{
                    callback:function(value){
                        
                    }
                });
            })*/
        },
        feedback_add:function(){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        //联谊报名
        lianyi:function(){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        //联谊报名修改
        lianyi_edit:function(){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        //议题征集
        proposal_add:function(){
            $("#submit").attr('disabled',false);
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        //议题讨论
        proposal_info:function(){
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                     //window.location.href=res.url;//成功跳转首页面
                     window.location.reload()
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
            Form.api.bindevent($("#user-form2"),function(data,res){
                Layer.alert(res.msg,function(index){
                     window.location.reload()
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
            $(".tp").click(function(){
                var status = $(this).attr('status');
                $("#status").val(status);
                $(".btn-embossed").hide();
                $("#submit"+status).show();
                Layer.open({
                    type:1,
                    title:"议题投票",
                    area:"80%",
                    content:$("#tpdiao")
                })
            });
            $("#cjtl").click(function(){
                var status = $(this).attr('status');
                Layer.open({
                    type:1,
                    title:"议题讨论",
                    area:"80%",
                    content:$("#tl")
                })
            })
        },
        //主席评议
        zxpy:function(){
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                    //window.location.href=res.url;//成功跳转首页面
                    window.location.reload()
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
            $(".submit").click(function (){
                var status = $(this).attr('status');
                $("#status").val(status);
                $("#user-form").submit();
            });
            $("#py").click(function(){
                Layer.open({
                    type:1,
                    title:"评议",
                    area:"80%",
                    content:$("#pydiv")
                })
            });
        },
        //建设评议
        jspy:function(){
            Form.api.bindevent($("#user-form"),function(data,res){
                Layer.alert(res.msg,function(index){
                    window.location.href=res.url;//成功跳转首页面
                });
            },function(data,res){
                Layer.alert(res.msg);
            });
        },
        api: {
            bindevent: function (value='') {
                require(['async!BMap3'], function () {
                    // 地图API功能
                    var map = new BMap.Map("allmap");

                    // 创建地址解析器实例
                    var myGeo = new BMap.Geocoder();
                    

                    function G(id) {
                        return document.getElementById(id);
                    }

                    var ac = new BMap.Autocomplete(//建立一个自动完成的对象
                            {"input": "address"
                                , "location": map
                            });
                    if (value) {
                        ac.setInputValue(value);
                    }
                    var myValue;
                    ac.addEventListener("onconfirm", function (e) {    //鼠标点击下拉列表后的事件
                        //console.log(e.item);
                        var _value = e.item.value;
                        myValue = _value.province + _value.city + _value.district + _value.street + _value.business;
                        G("searchResultPanel").innerHTML = "onconfirm<br />index = " + e.item.index + "<br />myValue = " + myValue;
                        setPlace();
                    });

                    
                    function setPlace() {
                        function myFun() {
                            var pp = local.getResults().getPoi(0).point;    //获取第一个智能搜索的结果
                            myGeo.getLocation(pp, function (rs) {
                                var addComp = rs.addressComponents;
                                $("#lng").val(pp.lng);
                                $("#lat").val(pp.lat);
                                //Layer.msg(__('Position update') + ' <br> ' + __('Longitude') + ' : ' + pp.lng + ' , ' + __('Latitude') + ' : ' + pp.lat);
                            });
                        }
                        var local = new BMap.LocalSearch(map, {//智能搜索
                            onSearchComplete: myFun
                        });
                        local.search(myValue);
                    }
                })

            },
            bindCg: function(){
                $("#savecg").click(function(){
                    var type = $(this).attr('data-type');
                    var url = '/index/index/saveCg/type/'+type;
                    var data = $("#user-form").serialize();
                    var loadindex = Layer.load();
                    $.post({
                        url: url,
                        dataType: 'json',
                        data:data,
                        cache: false,
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                var msg = ret.hasOwnProperty("msg") && ret.msg != "" ? ret.msg : "";
                                if (ret.code === 1) {
                                    Toastr.success(msg ? msg : '保存成功');
                                    layer.close(loadindex);
                                } else {
                                    Toastr.error(msg ? msg : '保存失败');
                                    layer.close(loadindex);
                                }
                            } else {
                                Toastr.error(__('Unknown data format'));
                                layer.close(loadindex);
                            }
                        }, error: function () {
                            Toastr.error(__('Network error'));
                            layer.close(loadindex);
                        }
                    });  
                });
                $("#delCache").click(function(){
                    var type = $(this).attr('data-type');
                    var url = '/index/index/delCache/type/'+type;
                    Layer.confirm('确认删除？', function (index) {
                        layer.close(index);
                        var loadindex = Layer.load();
                        $.post({
                            url: url,
                            dataType: 'json',
                            cache: false,
                            success: function (ret) {
                                if (ret.hasOwnProperty("code")) {
                                    var msg = ret.hasOwnProperty("msg") && ret.msg != "" ? ret.msg : "";
                                    if (ret.code === 1) {
                                        Toastr.success(msg ? msg : '删除成功');
                                        //layer.close(loadindex);
                                        window.location.reload();
                                    } else {
                                        Toastr.error(msg ? msg : '删除失败');
                                        layer.close(loadindex);
                                    }
                                } else {
                                    Toastr.error(__('Unknown data format'));
                                    layer.close(loadindex);
                                }
                            }, error: function () {
                                Toastr.error(__('Network error'));
                                layer.close(loadindex);
                            }
                        });
                    })  
                });
            }
        }
    };
    return Controller;
});