define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'lianyi/index' + location.search,
                    add_url: 'lianyi/add',
                    edit_url: 'lianyi/edit',
                    del_url: 'lianyi/del',
                    multi_url: 'lianyi/multi',
                    table: 'lianyi',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'active_name', title: __('参与活动'),defaultValue:""},
                        {field: 'status', title: __('审核状态'),searchList: {"0":'待审核',"1":'已通过',"2":'驳回修改'},formatter: Controller.api.formatter.status},
                        {field: 'company_id', title: __('Company_id'),visible:false,addClass: "selectpage", extend: "data-source='company/index'"},
                        {field: 'company_name', title: __('Company_id'),operate:false},
                        /*{field: 'user_id', title: __('User_id')},*/
                        {field: 'name', title: __('Name')},
                        {field: 'phone', title: __('Phone')},
                        {field: 'birthday', title: __('Birthday'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'age', title: __('Age')},
                        {field: 'sex', title: __('Sex'),searchList: {"单身男性":'单身男性',"单身女性":'单身女性'}},
                        {field: 'height', title: __('Height')},
                        {field: 'native_place', title: __('Native_place')},
                        {field: 'education', title: __('Education'),searchList: {"初中及以下":'初中及以下',"中专":'中专','高中':'高中','大专':'大专','本科':'本科','研究生':'研究生'}},
                        {field: 'income', title: __('Income'),searchList: {"3000至5000":'3000至5000',"5000至8000":'5000至8000','8000至10000':'8000至10000','10000以上':'10000以上'}},
                        {field: 'job', title: __('Job')},
                        {field: 'hobby', title: __('Hobby')},
                        {field: 'skills', title: __('Skills')},
                        {field: 'house', title: __('House'),searchList: {"有":'有',"无":'无'}},
                        {field: 'images', title: __('Images'), events: Table.api.events.image, formatter: Table.api.formatter.images},
                        {field: 'f_character', title: __('F_character')},
                        {field: 'f_job', title: __('F_job')},
                        {field: 'f_income', title: __('F_income'),searchList: {"3000至5000":'3000至5000',"5000至8000":'5000至8000','8000至10000':'8000至10000','10000以上':'10000以上'}},
                        {field: 'f_height', title: __('F_height')},
                        {field: 's_age', title: __('S_age'),searchList: {"20岁至25岁":'20岁至25岁',"25岁至30岁":'25岁至30岁','30岁至35岁':'30岁至35岁','35岁至40岁':'35岁至40岁','40岁至45岁':'40岁至45岁'}},
                        {field: 's_city', title: __('S_city')},
                        {field: 'f_house', title: __('F_house'),searchList: {"有":'有',"无":'无'}},
                        {field: 'add_time', title: __('Add_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                        buttons:[
                            {
                                name: 'check',
                                text: __('审核'),
                                icon: 'fa fa-caret-right',
                                classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                url: 'lianyi/check',
                                extend:'data-area=["400px","450px"]',
                                hidden:function(value,row){
                                    if(value.status == 0){
                                        return false;
                                    }else return true;
                                },
                                success:function(data,ret){
                                    $(".btn-refresh").trigger('click');
                                }
                            }
                        ], formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on("click", "#btn-sign", function () {
                var ids = Table.api.selectedids(table);
                layer.open({
                    title:'活动标记',
                    skin:'layui-layer-rim',
                    area:['450px', 'auto'],
                    
                    content: ' <div class="row" style="width: 420px;  margin-left:7px; margin-top:10px;">'
                        +'<div class="col-sm-12">'
                        +'<div class="input-group">'
                        +'<span class="input-group-addon"> 活动名称 :</span>'
                        +'<input id="active_name_input" type="text" class="form-control" placeholder="请输入活动名称">'
                        +'</div>'
                        +'</div>'
                        +'</div>'
                    ,
                    btn:['保存','取消'],
                    btn1: function (index,layero) {
                        var active_name = top.$("#active_name_input").val() || $("#active_name_input").val();
                        $.post({
                            url: 'lianyi/active_sign',
                            dataType: 'json',
                            data:{ids:ids.join(','),active_name:active_name},
                            cache: false,
                            success: function (ret) {
                                if (ret.hasOwnProperty("code")) {
                                    var msg = ret.hasOwnProperty("msg") && ret.msg != "" ? ret.msg : "";
                                    if (ret.code === 1) {
                                        Toastr.success(msg ? msg : '操作成功');
                                        layer.close(index);
                                        //Layer.closeAll();
                                        $(".btn-refresh").trigger("click");
                                    } else {
                                        Toastr.error(msg ? msg : '操作失败');
                                    }
                                } else {
                                    Toastr.error(__('Unknown data format'));
                                }
                            }, error: function () {
                                Toastr.error(__('Network error'));
                            }
                        });
                    },
                    btn2:function (index,layero) {
                         layer.close(index);
                    }               
                });
            })
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        check: function (){
            Controller.api.bindevent();
            $(".check").click(function(){
                var id = $("#c-id").val();
                var result = $("#c-result").val();
                var status = $(this).attr('data-status');
                /*if (!result) {
                    layer.alert('请输入意见');
                    return false;
                }*/
                var msg = status==1?'确认通过吗？':'确认驳回修改吗？'
                Layer.confirm(msg, function (index) {
                    layer.close(index);
                    var loadindex = Layer.load();
                    $(".check").addClass("disabled");
                    $.post({
                        url: '',
                        dataType: 'json',
                        data:{id:id,status:status,result:result},
                        cache: false,
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                var msg = ret.hasOwnProperty("msg") && ret.msg != "" ? ret.msg : "";
                                if (ret.code === 1) {
                                    parent.Toastr.success(msg ? msg : '审核成功');
                                    //Toastr.success(msg ? msg : '审核成功');
                                    //layer.close(loadindex);
                                    //window.location.reload();
                                    //Layer.closeAll();
                                    parent.$(".btn-refresh").trigger("click");
                                    var index = parent.Layer.getFrameIndex(window.name);
                                    parent.Layer.close(index);
                                } else {
                                    Toastr.error(msg ? msg : '审核失败');
                                    layer.close(loadindex);
                                }
                                $(".check").removeClass("disabled");
                            } else {
                                Toastr.error(__('Unknown data format'));
                                layer.close(loadindex);
                                $(".check").removeClass("disabled");
                            }
                        }, error: function () {
                            Toastr.error(__('Network error'));
                            layer.close(loadindex);
                            $(".check").removeClass("disabled");
                        }
                    });
                });       
            })
        }, 
        notice: function(){
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                status:function(value, row, index){
                    if (value==0) {
                        return '<font color="#FF9800">待审核</font>';
                    }else if(value==1){
                        return '<font color="green">已通过</font>';
                    }else{
                        return '<font color="red">驳回修改</font>';
                    }
                },
            }
        }
    };
    return Controller;
});