define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'company_score_log/index' + location.search,
                    //add_url: 'company_score_log/add',
                    //edit_url: 'company_score_log/edit',
                    //del_url: 'company_score_log/del',
                    //multi_url: 'company_score_log/multi',
                    table: 'company_score_log',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                //showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'),operate:false},
                        {field: 'topic_id', title: __('考核主题'),visible:false,addClass: "selectpage", extend: "data-source='topic/index'",defaultValue:topic_id},
                        {field: 'kpi_id', title: __('Kpi.name'),visible:false,addClass: "selectpage", extend: "data-source='kpi/index'",formatter: Controller.api.formatter.intro},
                        {field: 'kpi.name', title: __('Kpi.name'),operate:false,formatter: Controller.api.formatter.intro},
                        {field: 'company_id', title: __('企业'),visible:false,addClass: "selectpage", extend: "data-source='company/index'"},
                        {field: 'company.name', title: __('Company.name'),operate:false,formatter: Controller.api.formatter.intro},
                        /*{field: 'kpi_id', title: __('Kpi_id')},
                        {field: 'company_id', title: __('Company_id')},*/
                        {field: 'score', title: __('Score')},
                        {field: 'before', title: __('Before')},
                        {field: 'after', title: __('After')},
                        {field: 'memo', title: __('Memo')},
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        /*{field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}*/
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        list: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'company_score_log/list' + location.search,
                    table: 'company_score_log',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showExport: false,
                search:false,
                //commonSearch: false,
                showToggle: false,
                showColumns: false,
                columns: [
                    [
                        {field: 'topic_id', title: __('考核主题'),visible:false,addClass: "selectpage", extend: "data-source='topic/index'",defaultValue:topic_id},
                        {field: 'kpi.name', title: __('Kpi.name'),formatter: Controller.api.formatter.intro},
                        {field: 'company_id', title: __('企业'),visible:false},

                        {field: 'score', title: __('Score')},
                        /*{field: 'before', title: __('Before')},
                        {field: 'after', title: __('After')},
                        {field: 'memo', title: __('Memo')},*/
                        {field: 'createtime', title: __('Createtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        /*{field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}*/
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                intro: function (value, row, index) {
                    if (!value) {
                        return row.name;
                    }
                    var intro = value.substring(0,15);
                    return '<span data-toggle="tooltip" title="'+value+'">'+intro+'</span>';
                }
            }
        }
    };
    return Controller;
});
