define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cardshop_baoming/index' + location.search,
                    //add_url: 'cardshop_baoming/add',
                    //edit_url: 'cardshop_baoming/edit',
                    //del_url: 'cardshop_baoming/del',
                    //multi_url: 'cardshop_baoming/multi',
                    table: 'cardshop_baoming',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'cardshop_id', title: __('Cardshop_id'),visible:false, addClass: "selectpage", extend: "data-source='cardshop/index' data-field='name'"},
                        {field: 'active_id', title: __('Active_id'),visible:false, addClass: "selectpage", extend: "data-source='cardshop_active/index' data-field='name'"},
                        /*{field: 'user_id', title: __('User_id')},*/
                        {field: 'uname', title: __('Uname')},
                        {field: 'idcard', title: __('Idcard')},
                        {field: 'mobile', title: __('Mobile')},
                        {field: 'add_time', title: __('Add_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'status', title: '是否中签',searchList: {"0":'未中签',"1":'已中签'},formatter: Controller.api.formatter.status},
                        {field: 'card_no', title: __('Card_no')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                status:function(value, row, index){
                    if (value==0) {
                        return '<font color="#55B561">未中签</font>';
                    }else{
                        return '<font color="red">已中签</font>';
                    }
                },
            }
        }
    };
    return Controller;
});