define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'proposal/index' + location.search,
                    add_url: 'proposal/add',
                    edit_url: 'proposal/edit',
                    del_url: 'proposal/del',
                    multi_url: 'proposal/multi',
                    table: 'proposal',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'company_id', title: __('Company_id'),visible:false,addClass: "selectpage", extend: "data-source='company/index'"},
                        {field: 'company_name', title: __('Company_id'),operate:false},
                        {field: 'user_id', title: __('User_id'),visible:false,addClass: "selectpage", extend: "data-source='user/user/index' data-field='username'"},
                        {field: 'user_name', title: __('User_id'),operate:false},
                        {field: 'title', title: __('Title')},
                        {field: 'status', title: __('Status'),sortable: false,searchList: {"0":'待处理',"1":'已放弃',"2":'已确认',"3":'已转合理建议',"4":'已关闭'},formatter: Controller.api.formatter.status},
                        {field: 'vote_end_time', title: __('Vote_end_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'add_time', title: __('Add_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                        buttons:[
                            {
                                name: 'proposal_vote',
                                text: __('投票记录'),
                                icon: 'fa fa-eye',
                                classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                url: 'proposal_vote/index?proposal_id={row.id}',
                                extend:'data-area=["80%","100%"]',
                            },
                            {
                                name: 'proposal_pingjia',
                                text: __('讨论记录'),
                                icon: 'fa fa-list',
                                classname: 'btn btn-xs btn-success btn-magic btn-dialog',
                                url: 'proposal_vote/pingjia?proposal_id={row.id}',
                                extend:'data-area=["80%","100%"]',
                            },
                        ], formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        index2: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'proposal/index' + location.search,
                    table: 'proposal',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'company_id', title: __('Company_id'),visible:false,addClass: "selectpage", extend: "data-source='company/index'"},
                        {field: 'company_name', title: __('Company_id'),operate:false},
                        {field: 'user_id', title: __('User_id'),visible:false,addClass: "selectpage", extend: "data-source='user/user/index' data-field='username'"},
                        {field: 'user_name', title: __('User_id'),operate:false},
                        {field: 'title', title: __('Title')},
                        {field: 'status', title: __('Status'),sortable: false,searchList: {"0":'待处理',"1":'已放弃',"2":'已确认',"3":'已转合理建议',"4":'已关闭'},formatter: Controller.api.formatter.status},
                        {field: 'vote_end_time', title: __('Vote_end_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'add_time', title: __('Add_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                        buttons:[
                            {
                                name: 'proposal_vote',
                                text: __('投票记录'),
                                icon: 'fa fa-eye',
                                classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                url: 'proposal_vote/info?proposal_id={row.id}',
                                extend:'data-area=["80%","100%"]',
                            },
                            {
                                name: 'proposal_pingjia',
                                text: __('讨论记录'),
                                icon: 'fa fa-list',
                                classname: 'btn btn-xs btn-success btn-magic btn-dialog',
                                url: 'proposal_vote/pingjia?proposal_id={row.id}',
                                extend:'data-area=["80%","100%"]',
                            },
                        ], formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                status:function(value, row, index){
                    if (value==0) {
                        return '<font color="blue">待处理</font>';
                    }else if(value==1){
                        return '<font color="#999999">已放弃</font>';
                    }else if(value==2){
                        return '<font color="#FF9800">已确认</font>';
                    }else if(value==3){
                        return '<font color="green">已转合理建议</font>';
                    }else{
                        return '<font color="red">已关闭</font>';
                    }
                },
            }
        }
    };
    return Controller;
});