define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'jianse_pingyi/index' + location.search,
                    add_url: 'jianse_pingyi/add',
                    edit_url: 'jianse_pingyi/edit',
                    del_url: 'jianse_pingyi/del',
                    multi_url: 'jianse_pingyi/multi',
                    table: 'jianse_pingyi',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'topic_id', title: __('Topic_id'),visible: false,addClass: "selectpage", extend: "data-source='topic/index'"},
                        {field: 'company_id', title: __('Company_id'),visible: false,addClass: "selectpage", extend: "data-source='company/index'"},
                        {field: 'topic_name', title: __('Topic_id'),operate: false},
                        {field: 'company_name', title: __('Company_id'),operate: false},
                        {field: 'username', title: __('Username'),operate: false},
                        {field: 'user_id', title: __('User_id'),visible:false,addClass: "selectpage", extend: "data-source='user/user/index' data-field='username'"},
                        {field: 'ztgz', title: __('Ztgz'),searchList: {"1":'满意',"2":'不满意'},formatter: Controller.api.formatter.status},
                        {field: 'wthd', title: __('Wthd'),searchList: {"1":'满意',"2":'不满意'},formatter: Controller.api.formatter.status},
                        {field: 'zgbf', title: __('Zgbf'),searchList: {"1":'满意',"2":'不满意'},formatter: Controller.api.formatter.status},
                        {field: 'mzgz', title: __('Mzgz'),searchList: {"1":'满意',"2":'不满意'},formatter: Controller.api.formatter.status},
                        {field: 'ldgx', title: __('Ldgx'),searchList: {"1":'满意',"2":'不满意'},formatter: Controller.api.formatter.status},
                        {field: 'ghzz', title: __('Ghzz'),searchList: {"1":'满意',"2":'不满意'},formatter: Controller.api.formatter.status},
                        {field: 'add_time', title: __('Add_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                status:function(value, row, index){
                    if(value==1){
                        return '<font color="green">满意</font>';
                    }else{
                        return '<font color="red">不满意</font>';
                    }
                },
            }
        }
    };
    return Controller;
});