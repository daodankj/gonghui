define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'cardshop_active/index' + location.search,
                    add_url: 'cardshop_active/add',
                    edit_url: 'cardshop_active/edit',
                    del_url: 'cardshop_active/del',
                    multi_url: 'cardshop_active/multi',
                    table: 'cardshop_active',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name')},
                        {field: 'cardshop_id', title: __('Cardshop_id'),visible:false, addClass: "selectpage", extend: "data-source='cardshop/index' data-field='name'"},
                        {field: 'cardshop_name', title: __('Cardshop_id'),operate:false},
                        {field: 'num', title: __('Num')},
                        {field: 'starttime', title: __('Starttime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'endtime', title: __('Endtime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'status', title: __('Status'),searchList: {"0":'禁用',"1":'启用'},formatter:Table.api.formatter.toggle},
                        {field: 'rock_status', title: '是否摇号',searchList: {"0":'未摇号',"1":'已摇号'},formatter: Controller.api.formatter.rock_status},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                        buttons:[
                            {
                                name: 'rock',
                                text: __('开始摇号'),
                                icon: 'fa fa-caret-right',
                                classname: 'btn btn-xs btn-warning btn-magic btn-ajax',
                                url: 'cardshop_active/rock',
                                hidden:function(value,row){
                                    if(value.rock_status == 0){
                                        return false;
                                    }else return true;
                                },
                                confirm:'确认摇号吗？',
                                success:function(data,ret){
                                    $(".btn-refresh").trigger('click');
                                }
                            }
                        ], formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        intro: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                rock_status:function(value, row, index){
                    if (value==0) {
                        return '<font color="#55B561">未摇号</font>';
                    }else{
                        return '<font color="red">已摇号</font>';
                    }
                },
            }
        }
    };
    return Controller;
});