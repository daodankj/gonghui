define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'xfqyd/xfq/index' + location.search,
                    add_url: 'xfqyd/xfq/add',
                    edit_url: 'xfqyd/xfq/edit',
                    del_url: 'xfqyd/xfq/del',
                    multi_url: 'xfqyd/xfq/multi',
                    table: 'xfq',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'active_name', title: __('Active_id')},
                        {field: 'name', title: '券名称'},
                        {field: 'xfq_no', title: '券ID'},
                        {field: 'money', title: __('Money'), operate:'BETWEEN'},
                        {field: 'num', title: __('数量'), operate:false},
                        {field: 'add_time', title: __('Add_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'status', title: '是否摇号',searchList: {"0":'未摇号',"1":'已摇号'},formatter: Controller.api.formatter.status},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                        buttons:[
                            {
                                name: 'rock',
                                text: __('开始摇号'),
                                icon: 'fa fa-caret-right',
                                classname: 'btn btn-xs btn-warning btn-magic btn-ajax',
                                url: 'xfqyd/xfq/rock',
                                hidden:function(value,row){
                                    if(value.status == 0){
                                        return false;
                                    }else return true;
                                },
                                confirm:'确认摇号吗？',
                                success:function(data,ret){
                                    $(".btn-refresh").trigger('click');
                                }
                            }
                        ], formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                status:function(value, row, index){
                    if (value==0) {
                        return '<font color="#55B561">未摇号</font>';
                    }else{
                        return '<font color="red">已摇号</font>';
                    }
                },
            }
        }
    };
    return Controller;
});