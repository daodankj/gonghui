define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'warning_type/index' + location.search,
                    add_url: 'warning_type/add',
                    edit_url: 'warning_type/edit',
                    del_url: 'warning_type/del',
                    multi_url: 'warning_type/multi',
                    dragsort_url: 'ajax/weigh',
                    table: 'warning_type',
                }
            });

            var table = $("#table");
            //在表格内容渲染完成后回调的事件
            table.on('post-body.bs.table', function (e, json) {
                $("tbody tr[data-index]", this).each(function () {
                    if (parseInt($("td:eq(1)", this).text()) < 4) {
                        $("input[type=checkbox]", this).prop("disabled", true);
                    }
                });
            });

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'weigh',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name')},
                        {field: 'day', title: __('提前天数')},
                        {field: 'notice_content', title: __('短信通知内容'),operate:false,formatter: Controller.api.formatter.intro},
                        {field: 'status', title: __('Status'),searchList: {"0":'已禁用',"1":'已开启'},formatter:Table.api.formatter.toggle},
                        {field: 'weigh', title: __('排序')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: function (value, row, index) {
                                if(row.id < 4){
                                    var that = $.extend({}, this);
                                    var table = $(that.table).clone(true);
                                    $(table).data("operate-del", null);
                                    that.table = table;
                                    return Table.api.formatter.operate.call(that, value, row, index);
                                    //return '';
                                }
                                return Table.api.formatter.operate.call(this, value, row, index);
                            }}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                intro: function (value, row, index) {
                    var intro = value.substring(0,25);
                    return '<span data-toggle="tooltip" title="'+value+'">'+intro+'</span>';
                }
            }
        }
    };
    return Controller;
});