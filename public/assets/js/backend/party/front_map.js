define(['jquery', 'bootstrap', 'backend', 'table', 'form','echarts', 'echarts-theme'], function ($, undefined, Backend, Table, Form,Echarts, undefined) {

    var Controller = {
        index: function () {
            Controller.api.bindevent();
        },
        kpi_list: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'party/kpi_list/index' + location.search,
                    table: 'kpi_list',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'kpi_list.id',
                showExport: false,
                search:false,
                commonSearch: true,
                showToggle: false,
                showColumns: false,
                columns: [
                    [
                        /*{field: 'id', title: __('id')},*/
                        {field: 'kpi.topic_id', title: __('考核主题'),visible:false,addClass: "selectpage", extend: "data-source='party/topic/index'"},
                        {field: 'kpi.id', title: __('指标'),visible:false,operate:false,addClass: "selectpage", extend: "data-source='kpi/index'"},
                        {field: 'kpi.name', title: __('指标名称'),operate:false,formatter: Controller.api.formatter.intro},
                        {field: 'company_id', title: __('所属企业'),visible:false,operate:false,addClass: "selectpage", extend: "data-source='company/index'"},
                        /*{field: 'company_name', title: __('所属企业'),operate:false},*/
                        /*{field: 'user_name', title: __('上报人'),operate:false},*/
                        {field: 'intro', title: __('上报信息简介'),operate:false,formatter: Controller.api.formatter.intro},
                        {field: 'images', title: __('上传文件'),operate:false, events: Table.api.events.image, formatter: Controller.api.formatter.openWin},
                        {field: 'check_status', title: __('状态'),searchList: {"0":'待确认',"1":'已确认',"2":'已拒绝'},formatter: Controller.api.formatter.status},
                        {field: 'add_time', title: __('上报时间'), operate:'RANGE', addclass:'datetimerange'},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(".score").click(function(){
                 var url = $(this).attr('data-url');
                 var title = $(this).attr('data-title');
                 parent.Fast.api.open(url,title);
            });

            $(document).on('click', ".openwin", function () {
                 var url = $(this).attr('data-url');
                 var title = $(this).attr('data-title');
                 parent.Fast.api.open(url,title);
            });
            //绑定TAB事件
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // var options = table.bootstrapTable(tableOptions);
                var typeStr = $(this).attr("href").replace('#','');
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                options.queryParams = function (params) {
                    //console.log(params.filter);
                    if (!typeStr||typeStr==0) {
                        params.filter = JSON.stringify({});
                        params.op = JSON.stringify({});
                    }else{
                        params.filter = JSON.stringify({'kpi.type': typeStr});
                        params.op = JSON.stringify({'kpi.type': '='});
                    }
                    return params;
                };

                table.bootstrapTable('refresh', {});
                return false;
            });
        },

        party_study: function () {
            Table.api.init({
                extend: {
                    index_url: 'party_study/index' + location.search,
                    table: 'party_study',
                }
            });

            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {field: 'company_name', title: __('所属企业'),operate:false},
                        {field: 'company_id', title: __('所属企业'),visible:false,addClass: "selectpage", extend: "data-source='company/index'"},
                        {field: 'images', title: __('学习图片'), events: Table.api.events.image, formatter: Controller.api.formatter.openWin2,operate:false},
                        {field: 'intro', title: __('学习内容')},
                        {field: 'add_time', title: __('上报时间'), operate:'RANGE', addclass:'datetimerange'}
                    ]
                ]
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on('click', ".openwin", function () {
                 var url = $(this).attr('data-url');
                 var title = $(this).attr('data-title');
                 parent.Fast.api.open(url,title);
            });
        },

        user: function(){
            Table.api.init({
                extend: {
                    index_url: 'user/user/index',
                    multi_url: 'user/user/multi',
                    table: 'user',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'user.id',
                showExport: false,
                search:false,
                showToggle: false,
                showColumns: false,
                columns: [
                    [
                        {field: 'company_id', title: __('所属企业'),visible:false,addClass: "selectpage", extend: "data-source='company/index'"},
                        {field: 'username', title: __('Username'), operate: 'LIKE'},
                        {field: 'idcard', title: __('身份证'), operate: 'LIKE'},
                        {field: 'mobile', title: __('手机号码'), operate: 'LIKE'},
                        {field: 'is_dy', title: __('是否党员'),visible:false,searchList: {"0":'不是党员',"1":'是党员'},formatter:Table.api.formatter.toggle},
                        {field: 'status', title: __('Status'), formatter: Table.api.formatter.status, searchList: {normal: __('Normal'), hidden: __('Hidden')}},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },

        capital: function () {
          if (data.length<1) {
            $("#echart").html('暂无经费使用数据');
            return false;
          }
          var myChart = Echarts.init(document.getElementById('echart'), 'walden');
            var option = {
                title: {
                    left: 'center'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: '{a} <br/>{b} : {c} ({d}%)'
                },
                legend: {
                    orient: 'horizontal',//vertical
                    bottom: 0,
                    data: data,
                    textStyle: { //图例文字的样式
                        color: '#000',
                        fontSize: 14
                    },
                },
                series : [
                    {
                        name: '支出占比',
                        type: 'pie',    // 设置图表类型为饼图
                        radius: ['0','80'],
                        center: ['50%', '35%'],
                        data:data,
                        itemStyle: {
                            emphasis: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            },
                            normal:{
                                label:{
                                    show: true,
                                    formatter: '{b} : {c} ({d}%)' ,
                                    textStyle:{
                                        fontSize: 12
                                    },
                                },
                                labelLine :{show:true} ,
                                color:function(params) {
                                    //自定义颜色
                                    var colorList = [
                                            'purple', '#000000', 'indigo', '#FF8C00', '#FF0000', '#FE8463','blue','#26C0C0','#005ADF'
                                        ];
                                        return colorList[params.dataIndex]
                                }
                            }
                        }
                    }
                ]
            }
            myChart.setOption(option);
        },

        party_changeapply: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'party/party_changeapply/index' + location.search,
                    table: 'party_changeapply',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showExport: false,
                search:false,
                showToggle: false,
                showColumns: false,
                columns: [
                    [
                        {field: 'company_id', title: __('Company_id'),visible:false},
                        /*{field: 'company_name', title: __('Company_id'),operate:false},*/
                        {field: 'chairman', title: __('主席')},
                        {field: 'director_zz', title: __('组织委员')},
                        {field: 'director_wt', title: __('文体委员')},
                        {field: 'director_xc', title: __('宣传委员')},
                        {field: 'director_ld', title: __('劳动解调委员')},
                        {field: 'director_js', title: __('经审主任')},
                        {field: 'director_woman', title: __('女职工主任')},
                        {field: 'finance', title: __('财务委员')},
                        {field: 'add_time', title: __('申请时间'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'status', title: __('status'),searchList: {"0":'待审批',"1":'审批通过',"2":'审批拒绝'},formatter: Controller.api.formatter.status},

                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },

        party_updateapply: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'party/party_updateapply/index' + location.search,
                    table: 'party_updateapply',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showExport: false,
                search:false,
                showToggle: false,
                showColumns: false,
                columns: [
                    [
                        {field: 'company_id', title: __('Company_id'),visible:false},
                        /*{field: 'company_name', title: __('Company_id'),operate:false},*/
                        {field: 'chairman', title: __('主席')},
                        {field: 'director_zz', title: __('组织委员')},
                        {field: 'director_wt', title: __('文体委员')},
                        {field: 'director_xc', title: __('宣传委员')},
                        {field: 'director_ld', title: __('劳动解调委员')},
                        {field: 'director_js', title: __('经审主任')},
                        {field: 'director_woman', title: __('女职工主任')},
                        {field: 'finance', title: __('财务委员')},
                        {field: 'add_time', title: __('申请时间'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'status', title: __('status'),searchList: {"0":'待审批',"1":'审批通过',"2":'审批拒绝'},formatter: Controller.api.formatter.status},

                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },

        party_apply: function () {
            Table.api.init({
                extend: {
                    index_url: 'party/party_apply/index' + location.search,
                    table: 'party_apply',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showExport: false,
                search:false,
                showToggle: false,
                showColumns: false,
                columns: [
                    [
                        {field: 'company_id', title: __('Company_id'),visible:false},
                        /*{field: 'company_name', title: __('Company_id'),operate:false},*/
                        {field: 'legal_person', title: __('法人代表')},
                        {field: 'person_num', title: __('职工人数')},
                        {field: 'woman_num', title: __('女职工人数')},
                        {field: 'business_scope', title: __('经营范围')},
                        {field: 'nature', title: __('企业性质')},
                        {field: 'user_num', title: __('会员人数')},
                        {field: 'address', title: __('地址')},
                        {field: 'phone', title: __('联系电话')},
                        {field: 'chairman', title: __('主席')},
                        {field: 'director_woman', title: __('女职工主任')},
                        {field: 'finance', title: __('财务委员')},
                        {field: 'add_time', title: __('申请时间'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'status', title: __('status'),searchList: {"0":'待审批',"1":'审批通过',"2":'审批拒绝'},formatter: Controller.api.formatter.status},

                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },

        model_craftsman: function () {
            Table.api.init({
                extend: {
                    index_url: 'model_craftsman/index' + location.search,
                    table: 'model_craftsman',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showExport: false,
                search:false,
                showToggle: false,
                showColumns: false,
                columns: [
                    [
                        {field: 'company_id', title: __('企业'),visible:false},
                        {field: 'studio_name', title: __('工作室名称')},
                        {field: 'professional_field', title: __('专业领域')},
                        {field: 'studio_type', title: __('工作室类型')},
                        {field: 'user_num', title: __('成员总数')},
                        {field: 'leader_name', title: __('衔人姓名')},
                        {field: 'sex', title: __('性别'), searchList: {"男":__('男'),"女":__('女')}, formatter: Table.api.formatter.normal},
                        {field: 'birthday', title: __('出生日期'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'education', title: __('学历')},
                        {field: 'mingzu', title: __('民族')},
                        {field: 'political_outlook', title: __('政治面貌')},
                        {field: 'major', title: __('专业')},
                        {field: 'technical_title', title: __('技术职称')},
                        {field: 'phone', title: __('电话')},
                        {field: 'file', title: __('申请文件'),operate:false,formatter: Controller.api.formatter.thumb},
                        {field: 'status', title: __('status'),searchList: {"0":'待审批',"1":'审批通过',"2":'审批拒绝'},formatter: Controller.api.formatter.status},
                        {field: 'addtime', title: __('申请时间'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},

                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },

        skill_competition: function () {
            Table.api.init({
                extend: {
                    index_url: 'skills_competition/index' + location.search,
                    table: 'skills_competition',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showExport: false,
                search:false,
                showToggle: false,
                showColumns: false,
                columns: [
                    [
                        {field: 'company_id', title: __('所属企业'),visible:false},
                        {field: 'compet_time', title: __('竞赛时间'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'compet_item', title: __('竞赛项目')},
                        {field: 'users', title: __('参与人')},
                        {field: 'images', title: __('竞赛图片'), events: Table.api.events.image, formatter: Table.api.formatter.images},
                        {field: 'intro', title: __('竞赛总结')},
                        {field: 'add_time', title: __('上报时间'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'status', title: __('status'),searchList: {"0":'待审批',"1":'审批通过',"2":'审批拒绝'},formatter: Controller.api.formatter.status},

                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },

        skill_education: function () {
            Table.api.init({
                extend: {
                    index_url: 'skills_education/index' + location.search,
                    table: 'skills_education',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showExport: false,
                search:false,
                showToggle: false,
                showColumns: false,
                columns: [
                    [
                        {field: 'company_id', title: __('所属企业'),visible:false},
                        {field: 'name', title: __('姓名')},
                        {field: 'sex', title: __('性别'), searchList: {"男":__('男'),"女":__('女')}, formatter: Table.api.formatter.normal},
                        /*{field: 'age', title: __('Age')},*/
                        {field: 'idcard', title: __('身份证号码')},
                        {field: 'phone', title: __('电话')},
                        {field: 'education', title: __('学历')},
                        /*{field: 'school_name', title: __('School_name')},*/
                        {field: 'mingzu', title: __('民族')},
                        {field: 'major', title: __('拟报专业')},
                        /*{field: 'hk_location', title: __('Hk_location')},*/
                        {field: 'work_time', title: __('入职时间'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'work_station', title: __('所在岗位')},
                        {field: 'image', title: __('申请文件'),operate:false, events: Table.api.events.image, formatter: Controller.api.formatter.thumb},
                        {field: 'add_time', title: __('申请时间'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'status', title: __('Status'),searchList: {"0":'待审批',"1":'审批通过',"2":'审批拒绝'},formatter: Controller.api.formatter.status},

                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },

        weiquan: function () {
            Table.api.init({
                extend: {
                    index_url: 'weiquan/index' + location.search,
                    table: 'weiquan',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                showExport: false,
                search:false,
                showToggle: false,
                showColumns: false,
                columns: [
                    [
                        {field: 'company_id', title: __('所属企业'),visible:false},
                        {field: 'address', title: __('单位/住址')},
                        {field: 'name', title: __('姓名')},
                        {field: 'sex', title: __('性别'), searchList: {"男":__('男'),"女":__('女')}, formatter: Table.api.formatter.normal},

                        {field: 'idcard', title: __('身份证号码')},
                        {field: 'phone', title: __('电话')},
                        {field: 'num', title: __('涉及人数')},
                        {field: 'intro', title: __('要求解决事项')},
                        {field: 'shuqiu', title: __('申请人诉求')},
                        {field: 'add_time', title: __('上报时间'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'status', title: __('状态'),searchList: {"0":'待审批',"1":'已审批'},formatter: Controller.api.formatter.status},

                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },

        kunnan: function () {
            Table.api.init({
                extend: {
                    index_url: 'kunnan/index' + location.search,
                    table: 'kunnan',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showExport: false,
                search:false,
                showToggle: false,
                showColumns: false,
                columns: [
                    [
                        {field: 'company_id', title: __('所属企业'),visible:false},
                        {field: 's_name', title: __('本人姓名')},
                        {field: 's_age', title: __('本人年龄')},
                        {field: 's_company', title: __('本人单位')},
                        {field: 's_income', title: __('本人年收入')},
                        {field: 'image1', title: __('收入证明'),operate:false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'image2', title: __('申请书'),operate:false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'add_time', title: __('上报时间'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'status', title: __('状态'),searchList: {"0":'待审批',"1":'已审批'},formatter: Controller.api.formatter.status},

                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        arrears_help: function () {
            Table.api.init({
                extend: {
                    index_url: 'arrears_help/index' + location.search,
                    table: 'arrears_help',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showExport: false,
                search:false,
                showToggle: false,
                showColumns: false,
                columns: [
                    [
                        {field: 'company_id', title: __('所属企业'),visible:false},
                        {field: 's_name', title: __('本人姓名')},
                        {field: 's_age', title: __('本人年龄')},
                        {field: 's_company', title: __('本人单位')},
                        {field: 's_income', title: __('本人年收入')},
                        {field: 'image1', title: __('收入证明'),operate:false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'image2', title: __('申请书'),operate:false,events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'add_time', title: __('上报时间'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'status', title: __('状态'),searchList: {"0":'待审批',"1":'已审批'},formatter: Controller.api.formatter.status},

                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        employees_assistance: function () {
            Table.api.init({
                extend: {
                    index_url: 'employees_assistance/index' + location.search,
                    table: 'employees_assistance',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showExport: false,
                search:false,
                showToggle: false,
                showColumns: false,
                columns: [
                    [
                        {field: 'company_id', title: __('所属企业'),visible:false},
                        /*{field: 'company', title: __('单位名称')},*/
                        {field: 'name', title: __('姓名')},
                        {field: 'phone', title: __('电话')},
                        {field: 'idcard', title: __('身份证号码')},
                        {field: 'sex', title: __('性别'), searchList: {"男":__('男'),"女":__('女')}},
                        {field: 'image', title: __('申请文件'),operate:false,events: Table.api.events.image,formatter: Controller.api.formatter.thumb},
                        {field: 'add_time', title: __('申请时间'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'status', title: __('status'),searchList: {"0":'待审批',"1":'审批通过',"2":'审批拒绝'},formatter: Controller.api.formatter.status},

                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        kpi_warning: function () {
            Table.api.init({
                extend: {
                    index_url: 'party/kpi_warning/index' + location.search,
                    table: 'kpi_warning',
                }
            });

            var table = $("#table");
            var table2 = $("#table2");
            var table3 = $("#table3");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showExport: false,
                search:false,
                showToggle: false,
                showColumns: false,
                columns: [
                    [
                        {field: 'id', title: __('Id'),visible:false},
                        {field: 'kpi.name', title: __('指标名称'),operate:false,formatter: Controller.api.formatter.intro},
                        /*{field: 'company.name', title: __('Company.name'),operate:false},*/
                        {field: 'kpi_id', title: __('指标名称'),visible:false, addClass: "selectpage", extend: "data-source='kpi/index' data-field='name'"},
                        {field: 'company_id', title: __('所属企业'),visible:false, addClass: "selectpage", extend: "data-source='company/index'"},
                        {field: 'warning_type', title: __('预警类型'),operate:false,formatter: Controller.api.formatter.warning_type},
                        {field: 'notice', title: __('预警内容')},
                        {field: 'add_time', title: __('预警时间'), operate:'RANGE', addclass:'datetimerange'},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);

            table2.bootstrapTable({
                url: 'party/front_map/kpiinfo/type/1' + location.search,
                pk: 'id',
                sortName: 'id',
                showExport: false,
                search:false,
                commonSearch: false,
                showToggle: false,
                showColumns: false,
                pageSize:100,
                toolbar:'.toolbar2',
                columns: [
                    [
                        {field: 'id', title: __('Id'),visible:false},
                        {field: 'name', title: __('指标名称'),operate:false,formatter: Controller.api.formatter.intro},
                        {field: 'has_num', title: __('已上报次数'),operate:false},
                        {field: 'submit_num', title: __('要求上报次数'),operate:false},
                        {field: 'score', title: __('得分'),operate:false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table2);

            table3.bootstrapTable({
                url: 'party/front_map/kpiinfo/type/2' + location.search,
                pk: 'id',
                sortName: 'id',
                showExport: false,
                search:false,
                commonSearch: false,
                showToggle: false,
                showColumns: false,
                pageSize:100,
                toolbar:'.toolbar3',
                columns: [
                    [
                        {field: 'id', title: __('Id'),visible:false},
                        {field: 'name', title: __('指标名称'),operate:false,formatter: Controller.api.formatter.intro},
                        {field: 'has_num', title: __('已上报次数'),operate:false},
                        {field: 'submit_num', title: __('要求上报次数'),operate:false},
                        {field: 'score', title: __('得分'),operate:false}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table3);
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                thumb: function (value, row, index) {
                    if(!value){
                        return '';
                    }else if (value.indexOf("jp") > -1||value.indexOf("png") > -1) {
                        //var style = row.storage == 'upyun' ? '!/fwfh/120x90' : '';
                        //return '<a href="' + row.image + '" target="_blank"><img src="' + row.image + style + '" alt="" style="max-height:90px;max-width:120px"></a>';
                        return Table.api.formatter.images(value, row, index);
                    }else{
                        value = value === null ? '' : value.toString();
                        var arr = value.split(',');
                        var html = [];
                        $.each(arr, function (i, value) {
                            var info = '';
                            if (value.indexOf("xls") > -1) {
                                info = '<a class="btn-dialog" title="文件预览" href="https://view.officeapps.live.com/op/view.aspx?src=' + Fast.api.cdnurl(value,true) + '"><img src="https://tool.fastadmin.net/icon/xlx.png" height="30px" alt=""></a>';
                            } else if(value.indexOf("doc") > -1) {
                                info = '<a class="btn-dialog" title="文件预览" href="https://view.officeapps.live.com/op/view.aspx?src=' + Fast.api.cdnurl(value,true) + '"><img src="https://tool.fastadmin.net/icon/docx.png" height="30px" alt=""></a>';
                            }else if(value.indexOf("pdf") > -1){
                                info = '<a class="btn-dialog" title="文件预览" href="' + Fast.api.cdnurl(value,true) + '"><img src="https://tool.fastadmin.net/icon/pdf.png" height="30px" alt=""></a>';
                            }else{
                                info = '<a class="btn-dialog" title="文件预览" href="' + Fast.api.cdnurl(value,true) + '"><img src="https://tool.fastadmin.net/icon/file.png" height="30px" alt=""></a>';
                            }
                            html.push(info);
                        });
                        return html.join(' ');
                    }

                },
                intro: function (value, row, index) {
                    var intro = value.substring(0,15);
                    return '<span data-toggle="tooltip" title="'+value+'">'+intro+'</span>';
                },
                status:function(value, row, index){
                    if (value==0) {
                        return '<font color="#FF9800">待确认</font>';
                    }else if(value==1){
                        return '<font color="green">已通过</font>';
                    }else{
                        return '<font color="red">已拒绝</font>';
                    }
                },
                warning_type:function(value, row, index){
                    if (value==1) {
                        return '<font color="red">周预警</font>';
                    }else if(value==2){
                        return '<font color="#FF9800">月预警</font>';
                    }else if(value==3){
                        return '<font color="#000">季度预警</font>';
                    }else{
                        return value;
                    }
                },
                //指标上报记录弹框详情
                openWin: function (value, row, index) {
                    if(!value){
                        return '';
                    }else if (value.indexOf("jp") > -1||value.indexOf("png") > -1) {
                        //return Table.api.formatter.images(value, row, index);
                        value = value === null ? '' : value.toString();
                        var classname = 'openwin';
                        var arr = value.split(',');
                        var html = [];
                        $.each(arr, function (i, value) {
                            value = value ? value : '/assets/img/blank.gif';
                            html.push('<a href="javascript:void(0)" data-title="'+row.kpi.name+'" data-url="/admin/party/front_map/info/type/1/id/'+row.id+'" class="' + classname + '"><img width="35px" height="35px" src="' + Fast.api.cdnurl(value) + '" /></a>');
                        });
                        return html.join(' ');
                    }else{
                        value = value === null ? '' : value.toString();
                        var arr = value.split(',');
                        var html = [];
                        $.each(arr, function (i, value) {
                            var info = '';
                            if (value.indexOf("xls") > -1) {
                                info = '<a class="btn-dialog" title="文件预览" href="https://view.officeapps.live.com/op/view.aspx?src=' + Fast.api.cdnurl(value,true) + '" target="_blank"><img src="https://tool.fastadmin.net/icon/xlx.png" height="30px" alt=""></a>';
                            } else {
                                info = '<a class="btn-dialog" title="文件预览" href="https://view.officeapps.live.com/op/view.aspx?src=' + Fast.api.cdnurl(value,true) + '" target="_blank"><img src="https://tool.fastadmin.net/icon/docx.png" height="30px" alt=""></a>';
                            }
                            html.push(info);
                        });
                        return html.join(' ');
                    }

                },
                //文体活动弹框详情
                openWin2: function (value, row, index) {
                    if(!value){
                        return '';
                    }else{
                        value = value === null ? '' : value.toString();
                        var classname = 'openwin';
                        var arr = value.split(',');
                        var html = [];
                        $.each(arr, function (i, value) {
                            value = value ? value : '/assets/img/blank.gif';
                            html.push('<a href="javascript:void(0)" data-title="学习详情" data-url="/admin/party/front_map/info/type/2/id/'+row.id+'" class="' + classname + '"><img width="35px" height="35px" src="' + Fast.api.cdnurl(value) + '" /></a>');
                        });
                        return html.join(' ');
                    }

                },
            }
        }
    };
    return Controller;
});
