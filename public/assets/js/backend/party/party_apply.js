define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'party/party_apply/index' + location.search,
                    add_url: 'party/party_apply/add',
                    edit_url: 'party/party_apply/edit',
                    del_url: 'party/party_apply/del',
                    multi_url: 'party/party_apply/multi',
                    table: 'party_apply',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'status',
                sortOrder : 'asc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'),sortable: true},
                        {field: 'status', title: __('status'),sortable: true,searchList: {"0":'待审批',"1":'审批通过',"2":'审批拒绝'},formatter: Controller.api.formatter.status},
                        {field: 'company_id', title: __('Company_id'),visible:false},
                        {field: 'company_name', title: __('Company_id'),operate:false},
                        {field: 'legal_person', title: __('Legal_person')},
                        {field: 'person_num', title: __('Person_num')},
                        {field: 'woman_num', title: __('Woman_num')},
                        {field: 'business_scope', title: __('Business_scope')},
                        {field: 'nature', title: __('Nature')},
                        {field: 'user_num', title: __('User_num')},
                        {field: 'address', title: __('Address')},
                        {field: 'phone', title: __('Phone')},
                        {field: 'chairman', title: __('Chairman')},
                        /*{field: 'director_zz', title: __('Director_zz')},
                        {field: 'director_wt', title: __('Director_wt')},
                        {field: 'director_xc', title: __('Director_xc')},
                        {field: 'director_ld', title: __('Director_ld')},
                        {field: 'director_js', title: __('Director_js')},*/
                        {field: 'director_woman', title: __('Director_woman')},
                        {field: 'finance', title: __('Finance')},
                        {field: 'add_time', title: __('Add_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                        buttons:[
                            {
                                name: 'detail',
                                text: __('签署申请文档'),
                                icon: 'fa fa-magic',
                                classname: 'btn btn-xs btn-warning btn-magic btn-dialog',
                                url: 'party/party_apply/detail',
                                extend:'data-area=["600px","100%"]',
                            },
                            {
                                name: 'accept',
                                text: __('受理'),
                                icon: 'fa fa-hand-paper-o',
                                classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                url: 'party/party_apply/accept',
                                extend:'data-area=["400px","450px"]',
                                hidden:function(value,row){
                                    if(value.status == 0){
                                        return false;
                                    }else return true;
                                },
                                success:function(data,ret){
                                    $(".btn-refresh").trigger('click');
                                }
                            },
                            {
                                name: 'check',
                                text: __('审批'),
                                icon: 'fa fa-caret-right',
                                classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                url: 'party/party_apply/check',
                                extend:'data-area=["400px","450px"]',
                                hidden:function(value,row){
                                    if(value.status == 3){
                                        return false;
                                    }else return true;
                                },
                                success:function(data,ret){
                                    $(".btn-refresh").trigger('click');
                                }
                            }

                        ], formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        accept: function (){
            Controller.api.bindevent();
        },
        check: function (){
            Controller.api.bindevent();
            $(".check").click(function(){
                var id = $("#c-id").val();
                var result = $("#c-result").val();
                var status = $(this).attr('data-status');
                if (!result) {
                    layer.alert('请输入审核意见');
                    return false;
                }
                var msg = status==1?'确认审核通过吗？':'确认审核不通过吗？'
                Layer.confirm(msg, function (index) {
                    layer.close(index);
                    var loadindex = Layer.load();
                    $.post({
                        url: '',
                        dataType: 'json',
                        data:{id:id,status:status,result:result},
                        cache: false,
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                var msg = ret.hasOwnProperty("msg") && ret.msg != "" ? ret.msg : "";
                                if (ret.code === 1) {
                                    parent.Toastr.success(msg ? msg : '审核成功');
                                    //Toastr.success(msg ? msg : '审核成功');
                                    //layer.close(loadindex);
                                    //window.location.reload();
                                    //Layer.closeAll();
                                    parent.$(".btn-refresh").trigger("click");
                                    var index = parent.Layer.getFrameIndex(window.name);
                                    parent.Layer.close(index);
                                } else {
                                    Toastr.error(msg ? msg : '审核失败');
                                    layer.close(loadindex);
                                }
                            } else {
                                Toastr.error(__('Unknown data format'));
                                layer.close(loadindex);
                            }
                        }, error: function () {
                            Toastr.error(__('Network error'));
                            layer.close(loadindex);
                        }
                    });
                });       
            }) 
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                status:function(value, row, index){
                    if (value==0) {
                        return '<font color="red">待受理</font>';
                    }else if(value==1){
                        return '<font color="green">审批通过</font>';
                    }else if(value==3){
                        return '<font color="#FF9800">已受理,待审核</font>';
                    }else{
                        return '<font color="red">审批拒绝</font>';
                    }
                },
            }
        }
    };
    return Controller;
});