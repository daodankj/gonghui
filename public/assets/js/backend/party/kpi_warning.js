define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'party/kpi_warning/index' + location.search,
                    //add_url: 'party/kpi_warning/add',
                    //edit_url: 'party/kpi_warning/edit',
                    del_url: 'party/kpi_warning/del',
                    //multi_url: 'party/kpi_warning/multi',
                    table: 'party/kpi_warning',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'),visible:false},
                        {field: 'kpi.name', title: __('Kpi.name'),operate:false},
                        {field: 'company.name', title: __('Company.name'),operate:false},
                        {field: 'kpi_id', title: __('Kpi_id'),visible:false, addClass: "selectpage", extend: "data-source='kpi/index' data-field='name'"},
                        {field: 'company_id', title: __('Company_id'),visible:false, addClass: "selectpage", extend: "data-source='company/index'"},
                        {field: 'warning_type', title: __('Warning_type'),operate:false,formatter: Controller.api.formatter.warning_type},
                        {field: 'notice', title: __('Notice')},
                        {field: 'add_time', title: __('Add_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            //绑定TAB事件
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // var options = table.bootstrapTable(tableOptions);
                var typeStr = $(this).attr("href").replace('#','');
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                options.queryParams = function (params) {
                    //console.log(params.filter);
                    if (!typeStr||typeStr==0) {
                        params.filter = JSON.stringify({});
                        params.op = JSON.stringify({});
                    }else{
                        params.filter = JSON.stringify({warning_type: typeStr});
                        params.op = JSON.stringify({warning_type: '='});
                    }                
                    return params;
                };

                table.bootstrapTable('refresh', {});
                return false;
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                warning_type:function(value, row, index){
                    if (value==1) {
                        return '<font color="red">周预警</font>';
                    }else if(value==2){
                        return '<font color="#FF9800">月预警</font>';
                    }else if(value==3){
                        return '<font color="#000">季度预警</font>';
                    }else{
                        return value;
                    }
                },
            }
        }
    };
    return Controller;
});