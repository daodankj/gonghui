define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                custom:{普通: 'info', 重要:'danger'},
                extend: {
                    index_url: 'party/kpi/index' + location.search,
                    add_url: 'party/kpi/add',
                    edit_url: 'party/kpi/edit',
                    del_url: 'party/kpi/del',
                    multi_url: 'party/kpi/multi',
                    table: 'party_kpi',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'weigh',
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'topic_name', title: __('Topic_id'),operate:false},
                        {field: 'topic_id', title: __('考核主题'),visible:false,addClass: "selectpage", extend: "data-source='party/topic/index'",defaultValue:topic_id},
                        {field: 'type', title: __('指标类型'),visible:false,searchList: {"1":'阵地建设6000分',"2":'队伍管理2000分',"3":'活动开展2000分','4':'工会组织有活力（激励分）'}},
                        {field: 'name', title: __('Name'),formatter: Controller.api.formatter.intro},
                        {field: 'requirement', title: __('Requirement'),formatter: Controller.api.formatter.intro},
                        {field: 'grade', title: __('Grade'),searchList:{hot:'重要',index:'普通'},formatter: Table.api.formatter.flag},
                        {field: 'submit_type_name', title: __('Submit_type'),operate:false},
                        {field: 'submit_num', title: __('上报次数')},
                        {field: 'score_type', title: __('Score_type'),formatter: Controller.api.formatter.intro},
                        {field: 'score', title: __('Score')},
                        {field: 'end_time', title: __('End_time'), operate:'RANGE', addclass:'datetimerange'},
                        /*{field: 'warning_type', title: __('Warning_type')},*/
                        /*{field: 'add_time', title: __('Add_time'), operate:'RANGE', addclass:'datetimerange'},*/
                        {field: 'weigh', title: __('排序')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                        buttons:[
                            {
                                name: 'company',
                                text: __('完成情况'),
                                icon: 'fa fa-eye',
                                classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                url: 'party/kpi/showlist?kpi_id={row.id}',
                                extend:'data-area=["500px","100%"]',
                            },
                        ], formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        showlist:function(){
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'party/kpi/showlist' + location.search,
                    table: 'company',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('企业名称')},
                        {field: 'score', title: __('总积分')}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            //绑定TAB事件
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // var options = table.bootstrapTable(tableOptions);
                var typeStr = $(this).attr("href").replace('#','');
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                options.queryParams = function (params) {
                    //console.log(params.filter);
                    if (!typeStr||typeStr==0) {
                        params.status = ''
                    }else{
                        params.status = typeStr;
                    }                
                    return params;
                };

                table.bootstrapTable('refresh', {});
                return false;
            });
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
            $("#saveNew").click(function () {
                $("#c_save_new").val(1);
                Form.api.submit($("form[role=form]"),function () {
                    parent.Toastr.success( '操作成功');
                    parent.$(".btn-refresh").trigger("click");
                    var index = parent.Layer.getFrameIndex(window.name);
                    parent.Layer.close(index);
                });
            });
        },
        config: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                intro: function (value, row, index) {
                    if (!value) {
                        return '-';
                    }
                    var intro = value.substring(0,15);
                    return '<span data-toggle="tooltip" title="'+value+'">'+intro+'</span>';
                }
            }
        }
    };
    return Controller;
});