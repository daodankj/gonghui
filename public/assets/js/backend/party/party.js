define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'party/party/index' + location.search,
                    add_url: 'party/party/add',
                    edit_url: 'party/party/edit',
                    del_url: 'party/party/del',
                    import_url: 'party/party/import',
                    multi_url: 'party/party/multi',
                    table: 'party',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'party.id',
                showExport: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'company_id', title: __('Company_id'),visible:false,addClass: "selectpage", extend: "data-source='company/index'"},
                        {field: 'company_name', title: __('Company_id'),operate:false},
                        {field: 'type', title: __('等级')},
                        {field: 'score', title: __('Score'), operate: 'BETWEEN', sortable: true,formatter: Controller.api.formatter.opendialog},
                        {field: 'legal_person', title: __('Legal_person')},
                        /*{field: 'person_num', title: __('Person_num')},
                        {field: 'woman_num', title: __('Woman_num')},
                        {field: 'business_scope', title: __('Business_scope')},*/
                        {field: 'nature', title: __('Nature')},
                        /*{field: 'user_num', title: __('User_num')},*/
                        {field: 'address', title: __('Address')},
                        {field: 'phone', title: __('Phone')},
                        {field: 'chairman', title: __('Chairman')},
                        /*{field: 'director_zz', title: __('Director_zz')},
                        {field: 'director_wt', title: __('Director_wt')},
                        {field: 'director_xc', title: __('Director_xc')},
                        {field: 'director_ld', title: __('Director_ld')},
                        {field: 'director_js', title: __('Director_js')},
                        {field: 'director_woman', title: __('Director_woman')},*/
                        {field: 'finance', title: __('Finance')},
                        /*{field: 'add_time', title: __('Add_time'), operate:'RANGE', addclass:'datetimerange'},*/
                        /*{field: 'status', title: __('Status')},*/
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                        buttons:[
                            {
                                name: 'addscore',
                                text: __('加减积分'),
                                icon: 'fa fa-plus',
                                classname: 'btn btn-xs btn-success btn-magic btn-dialog',
                                url: 'party/party/addscore',
                                extend:'data-area=["600px","100%"]',
                            },
                            {
                                name: 'struct',
                                text: __('组织架构'),
                                icon: 'fa fa-magic',
                                classname: 'btn btn-xs btn-warning btn-magic btn-dialog',
                                url: 'party/party/struct',
                                extend:'data-area=["600px","100%"]',
                            },
                            {
                                name: 'user',
                                text: __('党员'),
                                icon: 'fa fa-male',
                                classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                url: 'user/user/index?is_dy=1&company_id={row.company_id}',
                                extend:'data-area=["800px","90%"]',
                            },
                        ], formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);

        },
        add: function () {
            Controller.api.bindevent();
            //企业选后查询企业信息
            $(document).on("change", "#c-company_id", function(){
                var company_id = $("#c-company_id").val();
                if (!company_id){
                    return false;
                }
                $.post({
                    url: 'ajax/getCompanyInfo/ids/'+company_id,
                    dataType: 'json',
                    cache: false,
                    success: function (ret) {
                        if(ret.info){
                            $("#c-person_num").val(ret.info.c_user_num);
                            $("#c-woman_num").val(ret.info.c_women_num);
                            $("#c-business_scope").val(ret.info.c_business);
                            $("#c-nature").val(ret.info.c_nature);
                            $("#c-user_num").val(ret.info.d_user_num);
                            $("#c-chairman").val(ret.info.u_chairman);
                        }else{
                            $("#c-person_num").val('');
                            $("#c-woman_num").val('');
                            $("#c-business_scope").val('');
                            $("#c-nature").val('');
                            $("#c-user_num").val('');
                            $("#c-chairman").val('');
                        }
                    }
                });
            });
        },
        edit: function () {
            Controller.api.bindevent();
        },
        addscore: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                opendialog: function (value, row, index) {
                    
                    return '<a title="积分明细" class="btn-dialog" href="party/party_score_log/index?company_id='+row.company_id+'">'+value+'</a>';
                }
            }
        }
    };
    return Controller;
});