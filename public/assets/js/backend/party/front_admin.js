define(['jquery', 'bootstrap', 'backend', 'table', 'form','echarts', 'echarts-theme'], function ($, undefined, Backend, Table, Form,Echarts, undefined) {

    var Controller = {
        index: function () {
            Controller.api.bindevent();
            $(".btn").click(function(){
                var id = $('#company_id').val();
                if (id) {
                    window.location.href='/admin/party/front_admin/cindex/id/'+id;
                }else{
                    Toastr.error('请先输入企业名称搜索并选择一个企业');
                }
            })
            $(".openInfo").click(function(){
                var id = $(this).attr('data-id');
                window.location.href='/admin/party/front_admin/cindex/id/'+id;
            });
            $(".openInfo2").click(function(){
                var id = $(this).attr('data-id');
                window.location.href='/admin/party/front_admin/admin_companylist/id/'+id;
            });
        },
        admin_companylist: function(){
            Controller.api.bindevent();
            $(".openInfo").click(function(){
                var id = $(this).attr('data-id');
                window.location.href='/admin/party/front_admin/cindex/id/'+id;
            });
        },
        cindex: function () {
            Controller.api.bindevent();
            $(".btnimg").click(function(){
                 var url = $(this).attr('data-url');
                 var title = $(this).attr('data-title');
                 //parent.Fast.api.open(url,title);
                 window.location.href=url;
            });
        },
        business_list:function(){
            $(".btnimg").click(function(){
                 var url = $(this).attr('data-url');
                 var title = $(this).attr('data-title');
                 //parent.Fast.api.open(url,title);
                 window.location.href=url;
            });
        },
        kpi_list: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'party/kpi_list/index' + location.search,
                    table: 'kpi_list',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'kpi_list.id',
                showExport: false,
                search:false,
                commonSearch: false,
                showToggle: false,
                showColumns: false,
                cardView:true,
                columns: [
                    [
                        /*{field: 'id', title: __('id')},*/
                        {field: 'kpi.id', title: __('指标'),visible:false,addClass: "selectpage", extend: "data-source='kpi/index'"},
                        {field: 'kpi.name', title: __('指标名称'),operate:false,formatter: Controller.api.formatter.intro},
                        {field: 'company_id', title: __('所属企业'),visible:false,addClass: "selectpage", extend: "data-source='company/index'"},
                        /*{field: 'company_name', title: __('所属企业'),operate:false},*/
                        /*{field: 'user_name', title: __('上报人'),operate:false},*/
                        {field: 'intro', title: __('上报信息简介'),operate:false,formatter: Controller.api.formatter.intro},
                        {field: 'images', title: __('上传文件'),operate:false, events: Table.api.events.image, formatter: Controller.api.formatter.openWin},
                        {field: 'check_status', title: __('审核状态'),searchList: {"0":'待审批',"1":'审批通过',"2":'审批拒绝'},formatter: Controller.api.formatter.status},
                        {field: 'add_time', title: __('上报时间'), operate:'RANGE', addclass:'datetimerange'},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on('click', ".openwin", function () {
                 var url = $(this).attr('data-url');
                 var title = $(this).attr('data-title');
                 //parent.Fast.api.open(url,title);
                 window.location.href=url;
            });
        },

        party_study: function () {
            Table.api.init({
                extend: {
                    index_url: 'party_study/index' + location.search,
                    table: 'party_study',
                }
            });

            var table = $("#table");
            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {field: 'company_name', title: __('所属企业'),operate:false},
                        {field: 'company_id', title: __('所属企业'),visible:false,addClass: "selectpage", extend: "data-source='company/index'"},
                        {field: 'images', title: __('学习图片'), events: Table.api.events.image, formatter: Controller.api.formatter.openWin2,operate:false},
                        {field: 'intro', title: __('学习内容')},
                        {field: 'add_time', title: __('上报时间'), operate:'RANGE', addclass:'datetimerange'}
                    ]
                ]
            });
            // 为表格绑定事件
            Table.api.bindevent(table);
            $(document).on('click', ".openwin", function () {
                var url = $(this).attr('data-url');
                var title = $(this).attr('data-title');
                parent.Fast.api.open(url,title);
            });
        },

        user: function(){
            Table.api.init({
                extend: {
                    index_url: 'user/user/index',
                    multi_url: 'user/user/multi',
                    table: 'user',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'user.id',
                showExport: false,
                search:false,
                showToggle: false,
                showColumns: false,
                cardView:true,
                columns: [
                    [
                        {field: 'company_id', title: __('所属企业'),visible:false,addClass: "selectpage", extend: "data-source='company/index'"},
                        {field: 'username', title: __('Username'), operate: 'LIKE'},
                        {field: 'idcard', title: __('身份证'), operate: 'LIKE'},
                        {field: 'mobile', title: __('手机号码'), operate: 'LIKE'},
                        {field: 'status', title: __('Status'), formatter: Table.api.formatter.status, searchList: {normal: __('Normal'), hidden: __('Hidden')}},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },


        party_changeapply: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'party/party_changeapply/index' + location.search,
                    table: 'party_changeapply',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showExport: false,
                search:false,
                showToggle: false,
                showColumns: false,
                cardView:true,
                columns: [
                    [
                        {field: 'company_id', title: __('Company_id'),visible:false},
                        /*{field: 'company_name', title: __('Company_id'),operate:false},*/
                        {field: 'chairman', title: __('主席')},
                        {field: 'director_zz', title: __('组织委员')},
                        {field: 'director_wt', title: __('文体委员')},
                        {field: 'director_xc', title: __('宣传委员')},
                        {field: 'director_ld', title: __('劳动解调委员')},
                        {field: 'director_js', title: __('经审主任')},
                        {field: 'director_woman', title: __('女职工主任')},
                        {field: 'finance', title: __('财务委员')},
                        {field: 'add_time', title: __('申请时间'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'status', title: __('status'),searchList: {"0":'待审批',"1":'审批通过',"2":'审批拒绝'},formatter: Controller.api.formatter.status},

                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },

        party_updateapply: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'party/party_updateapply/index' + location.search,
                    table: 'party_updateapply',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showExport: false,
                search:false,
                showToggle: false,
                showColumns: false,
                cardView:true,
                columns: [
                    [
                        {field: 'company_id', title: __('Company_id'),visible:false},
                        /*{field: 'company_name', title: __('Company_id'),operate:false},*/
                        {field: 'chairman', title: __('主席')},
                        {field: 'director_zz', title: __('组织委员')},
                        {field: 'director_wt', title: __('文体委员')},
                        {field: 'director_xc', title: __('宣传委员')},
                        {field: 'director_ld', title: __('劳动解调委员')},
                        {field: 'director_js', title: __('经审主任')},
                        {field: 'director_woman', title: __('女职工主任')},
                        {field: 'finance', title: __('财务委员')},
                        {field: 'add_time', title: __('申请时间'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'status', title: __('status'),searchList: {"0":'待审批',"1":'审批通过',"2":'审批拒绝'},formatter: Controller.api.formatter.status},

                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },

        party_apply: function () {
            Table.api.init({
                extend: {
                    index_url: 'party/party_apply/index' + location.search,
                    table: 'party_apply',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showExport: false,
                search:false,
                showToggle: false,
                showColumns: false,
                cardView:true,
                columns: [
                    [
                        {field: 'company_id', title: __('Company_id'),visible:false},
                        /*{field: 'company_name', title: __('Company_id'),operate:false},*/
                        {field: 'legal_person', title: __('法人代表')},
                        {field: 'person_num', title: __('职工人数')},
                        {field: 'woman_num', title: __('女职工人数')},
                        {field: 'business_scope', title: __('经营范围')},
                        {field: 'nature', title: __('企业性质')},
                        {field: 'user_num', title: __('会员人数')},
                        {field: 'address', title: __('地址')},
                        {field: 'phone', title: __('联系电话')},
                        {field: 'chairman', title: __('工会主席')},
                        {field: 'director_woman', title: __('女职工主任')},
                        {field: 'finance', title: __('财务委员')},
                        {field: 'add_time', title: __('申请时间'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'status', title: __('status'),searchList: {"0":'待审批',"1":'审批通过',"2":'审批拒绝'},formatter: Controller.api.formatter.status},

                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },

        kpi_warning: function () {
            Table.api.init({
                extend: {
                    index_url: 'party/kpi_warning/index' + location.search,
                    table: 'kpi_warning',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showExport: false,
                search:false,
                showToggle: false,
                showColumns: false,
                cardView:true,
                columns: [
                    [
                        {field: 'id', title: __('Id'),visible:false},
                        {field: 'kpi.name', title: __('指标名称'),operate:false,formatter: Controller.api.formatter.intro},
                        /*{field: 'company.name', title: __('Company.name'),operate:false},*/
                        {field: 'kpi_id', title: __('指标名称'),visible:false, addClass: "selectpage", extend: "data-source='party/kpi/index' data-field='name'"},
                        {field: 'company_id', title: __('所属企业'),visible:false, addClass: "selectpage", extend: "data-source='company/index'"},
                        {field: 'warning_type', title: __('预警类型'),operate:false,formatter: Controller.api.formatter.warning_type},
                        {field: 'notice', title: __('预警内容'),formatter: Controller.api.formatter.intro},
                        {field: 'add_time', title: __('预警时间'), operate:'RANGE', addclass:'datetimerange'},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
            //绑定TAB事件
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // var options = table.bootstrapTable(tableOptions);
                var typeStr = $(this).attr("href").replace('#','');
                var options = table.bootstrapTable('getOptions');
                options.pageNumber = 1;
                options.queryParams = function (params) {
                    //console.log(params.filter);
                    if (!typeStr||typeStr==0) {
                        params.filter = JSON.stringify({});
                        params.op = JSON.stringify({});
                    }else{
                        params.filter = JSON.stringify({warning_type: typeStr});
                        params.op = JSON.stringify({warning_type: '='});
                    }
                    return params;
                };

                table.bootstrapTable('refresh', {});
                return false;
            });
        },
        score_top: function(){
          $(".openInfo").click(function(){
             var id = $(this).attr('data-id');
             /*parent.Fast.api.open('/index/front/index/id/'+id,'企业信息',{
                area:['1200px','600px']
            });*/
            window.location.href='/admin/party/front_admin/cindex/id/'+id;
          });
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                thumb: function (value, row, index) {
                    if(!value){
                        return '';
                    }else if (value.indexOf("jp") > -1||value.indexOf("png") > -1) {
                        //var style = row.storage == 'upyun' ? '!/fwfh/120x90' : '';
                        //return '<a href="' + row.image + '" target="_blank"><img src="' + row.image + style + '" alt="" style="max-height:90px;max-width:120px"></a>';
                        return Table.api.formatter.images(value, row, index);
                    }else{
                        value = value === null ? '' : value.toString();
                        var arr = value.split(',');
                        var html = [];
                        $.each(arr, function (i, value) {
                            var info = '';
                            if (value.indexOf("xls") > -1) {
                                info = '<a class="btn-dialog" title="文件预览" href="https://view.officeapps.live.com/op/view.aspx?src=' + Fast.api.cdnurl(value,true) + '"><img src="https://tool.fastadmin.net/icon/xlx.png" height="30px" alt=""></a>';
                            } else if(value.indexOf("doc") > -1) {
                                info = '<a class="btn-dialog" title="文件预览" href="https://view.officeapps.live.com/op/view.aspx?src=' + Fast.api.cdnurl(value,true) + '"><img src="https://tool.fastadmin.net/icon/docx.png" height="30px" alt=""></a>';
                            }else if(value.indexOf("pdf") > -1){
                                info = '<a class="btn-dialog" title="文件预览" href="' + Fast.api.cdnurl(value,true) + '"><img src="https://tool.fastadmin.net/icon/pdf.png" height="30px" alt=""></a>';
                            }else{
                                info = '<a class="btn-dialog" title="文件预览" href="' + Fast.api.cdnurl(value,true) + '"><img src="https://tool.fastadmin.net/icon/file.png" height="30px" alt=""></a>';
                            }
                            html.push(info);
                        });
                        return html.join(' ');
                    }

                },
                intro: function (value, row, index) {
                    if(!value){
                        return '';
                    }
                    var intro = value.substring(0,15);
                    return '<span data-toggle="tooltip" title="'+value+'">'+intro+'</span>';
                },
                status:function(value, row, index){
                    if (value==0) {
                        return '<font color="#FF9800">待审批</font>';
                    }else if(value==1){
                        return '<font color="green">审批通过</font>';
                    }else{
                        return '<font color="red">审批拒绝</font>';
                    }
                },
                warning_type:function(value, row, index){
                    if (value==1) {
                        return '<font color="red">周预警</font>';
                    }else if(value==2){
                        return '<font color="#FF9800">月预警</font>';
                    }else if(value==3){
                        return '<font color="#000">季度预警</font>';
                    }else{
                        return value;
                    }
                },
                //指标上报记录弹框详情
                openWin: function (value, row, index) {
                    if(!value){
                        return '';
                    }else if (value.indexOf("jp") > -1||value.indexOf("png") > -1) {
                        //return Table.api.formatter.images(value, row, index);
                        value = value === null ? '' : value.toString();
                        var classname = 'openwin';
                        var arr = value.split(',');
                        var html = [];
                        $.each(arr, function (i, value) {
                            value = value ? value : '/assets/img/blank.gif';
                            html.push('<a href="javascript:void(0)" data-title="'+row.kpi.name+'" data-url="/admin/party/front_map/info/type/1/id/'+row.id+'" class="' + classname + '"><img width="35px" height="35px" src="' + Fast.api.cdnurl(value) + '" /></a>');
                        });
                        return html.join(' ');
                    }else{
                        value = value === null ? '' : value.toString();
                        var arr = value.split(',');
                        var html = [];
                        $.each(arr, function (i, value) {
                            var info = '';
                            if (value.indexOf("xls") > -1) {
                                info = '<a title="文件预览" href="https://view.officeapps.live.com/op/view.aspx?src=' + Fast.api.cdnurl(value,true) + '" target="_blank"><img src="https://tool.fastadmin.net/icon/xlx.png" height="30px" alt=""></a>';
                            } else {
                                info = '<a title="文件预览" href="https://view.officeapps.live.com/op/view.aspx?src=' + Fast.api.cdnurl(value,true) + '" target="_blank"><img src="https://tool.fastadmin.net/icon/docx.png" height="30px" alt=""></a>';
                            }
                            html.push(info);
                        });
                        return html.join(' ');
                    }

                },
                //文体活动弹框详情
                openWin2: function (value, row, index) {
                    if(!value){
                        return '';
                    }else{
                        value = value === null ? '' : value.toString();
                        var classname = 'openwin';
                        var arr = value.split(',');
                        var html = [];
                        $.each(arr, function (i, value) {
                            value = value ? value : '/assets/img/blank.gif';
                            html.push('<a href="javascript:void(0)" data-title="学习详情" data-url="/admin/party/front_map/info/type/2/id/'+row.id+'" class="' + classname + '"><img width="35px" height="35px" src="' + Fast.api.cdnurl(value) + '" /></a>');
                        });
                        return html.join(' ');
                    }

                },
            }
        }
    };
    return Controller;
});
