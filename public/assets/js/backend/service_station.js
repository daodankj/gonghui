define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'service_station/index' + location.search,
                    add_url: 'service_station/add',
                    edit_url: 'service_station/edit',
                    del_url: 'service_station/del',
                    multi_url: 'service_station/multi',
                    table: 'service_station',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name')},
                        {field: 'address', title: __('地址')},
                        {field: 'lat', title: __('纬度'), operate:'BETWEEN'},
                        {field: 'lng', title: __('经度'), operate:'BETWEEN'},
                        {field: 'url', title: __('VR地址'), formatter: Table.api.formatter.url},
                        //{field: 'intro', title: __('Intro')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});