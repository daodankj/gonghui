define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'party_news/index' + location.search,
                    add_url: 'party_news/add',
                    edit_url: 'party_news/edit',
                    del_url: 'party_news/del',
                    multi_url: 'party_news/multi',
                    table: 'party_news',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        /*{field: 'user_id', title: __('User_id')},*/
                        {field: 'company_id', title: __('Company_id'),visible:false,addClass: "selectpage", extend: "data-source='company/index'"},
                        {field: 'company.name', title: __('Company_id'),operate:false},
                        {field: 'title', title: __('Title')},
                        {field: 'content', title: __('Content'),operate:false,formatter: Controller.api.formatter.intro},
                        {field: 'images', title: __('Images'),operate:false, events: Table.api.events.image, formatter: Controller.api.formatter.file},
                        {field: 'add_time', title: __('Add_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                file: function (value, row, index) {
                    if(!value){
                        return '';
                    }else{
                        value = value === null ? '' : value.toString();
                        var arr = value.split(',');
                        var html = [];
                        $.each(arr, function (i, value) {
                            var info = '';
                            if (value.indexOf("jp") > -1||value.indexOf("png") > -1) {
                                value = value ? value : '/assets/img/blank.gif';
                                info = '<a href="javascript:void(0)"><img class="img-sm img-center" src="' + Fast.api.cdnurl(value) + '" /></a>';
                            }else if (value.indexOf("xls") > -1) {
                                info = '<a class="btn-dialog" title="文件预览" href="https://view.officeapps.live.com/op/view.aspx?src=' + Fast.api.cdnurl(value,true) + '"><img src="https://tool.fastadmin.net/icon/xlx.png" height="30px" alt=""></a>';
                            } else if(value.indexOf("doc") > -1) {
                                info = '<a class="btn-dialog" title="文件预览" href="https://view.officeapps.live.com/op/view.aspx?src=' + Fast.api.cdnurl(value,true) + '"><img src="https://tool.fastadmin.net/icon/docx.png" height="30px" alt=""></a>';
                            }else if(value.indexOf("pdf") > -1){
                                info = '<a class="btn-dialog" title="文件预览" href="' + Fast.api.cdnurl(value,true) + '"><img src="https://tool.fastadmin.net/icon/pdf.png" height="30px" alt=""></a>';
                            }else{
                                info = '<a class="btn-dialog" title="文件预览" href="' + Fast.api.cdnurl(value,true) + '"><img src="https://tool.fastadmin.net/icon/file.png" height="30px" alt=""></a>';
                            }
                            html.push(info);
                        });
                        return html.join(' ');
                    }

                },
                intro: function (value, row, index) {
                    var intro = value.substring(0,15);
                    return '<span data-toggle="tooltip" title="'+value+'">'+intro+'</span>';
                }
            }
        }
    };
    return Controller;
});
