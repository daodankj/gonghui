define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'company/index' + location.search,
                    add_url: 'company/add',
                    edit_url: 'company/edit',
                    del_url: 'company/del',
                    import_url: 'company/import',
                    multi_url: 'company/multi',
                    table: 'company',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'style', title: __('性质'), searchList: {"企业":__('企业'),"单位":__('单位'),"其他":__('其他')}, formatter: Table.api.formatter.normal},
                        {field: 'companyinfo.c_style', title: __('企业类型'), searchList: {"民营":__('民营'),"国企":__('国企'),"合资":__('合资'),"外资":__('外资'),"其他":__('其他')},},
                        {field: 'companyinfo.c_legal_person', title: __('法人')},
                        {field: 'name', title: __('Name')},
                        {field: 'short_name', title: __('简称')},
                        {field: 'lng', title: __('经度'),operate:false},
                        {field: 'lat', title: __('纬度'),operate:false},
                        /*{field: 'type', title: __('Type'),operate:false, searchList: {"D":__('D'),"C":__('C'),"B":__('B'),"A":__('A')}, formatter: Table.api.formatter.normal},
                        {field: 'score', title: __('积分'), operate: 'BETWEEN'},*/
                        /*{field: 'image', title: __('简介图片'), events: Table.api.events.image, formatter: Table.api.formatter.image, operate: false},*/
                        /*{field: 'add_time', title: __('Add_time'), operate:'RANGE', addclass:'datetimerange'},*/
                        {field: 'url', title: __('官网地址'), formatter: Table.api.formatter.url},
                        {field: 'status', title: __('是否投产'), searchList: {"0":'未投产',"1":'已投产'},formatter:Table.api.formatter.toggle},
                        {field: 'gh_map', title: __('是否在工会地图'), searchList: {"0":'不在',"1":'已在'}},
                        {field: 'dj_map', title: __('是否在党建地图'), searchList: {"0":'不在',"1":'已在'}},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                            buttons:[
                                {
                                    name: 'pingyi',
                                    text: __('评议设置'),
                                    icon: 'fa fa-info',
                                    classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                    url: 'company/pingyi',
                                    extend:'data-area=["70%","80%"]'
                                }
                            ],
                            formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        pingyi: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
