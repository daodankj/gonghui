define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'union_updateapply/index' + location.search,
                    add_url: 'union_updateapply/add',
                    edit_url: 'union_updateapply/edit',
                    del_url: 'union_updateapply/del',
                    multi_url: 'union_updateapply/multi',
                    table: 'union_updateapply',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'status',
                sortOrder : 'asc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'),sortable: true},
                        {field: 'status', title: __('status'),sortable: true,searchList: {"0":'待审批',"1":'审批通过',"2":'审批拒绝'},formatter: Controller.api.formatter.status},
                        {field: 'company_id', title: __('Company_id'),visible:false},
                        {field: 'company_name', title: __('Company_id'),operate:false},
                        {field: 'chairman', title: __('Chairman')},
                        {field: 'chairman_f', title: __('Chairman_f')},
                        {field: 'director_zz', title: __('Director_zz')},
                        {field: 'director_wt', title: __('Director_wt')},
                        {field: 'director_xc', title: __('Director_xc')},
                        {field: 'director_ld', title: __('Director_ld')},
                        {field: 'director_js', title: __('Director_js')},
                        {field: 'director_woman', title: __('Director_woman')},
                        {field: 'finance', title: __('Finance')},
                        {field: 'image', title: __('Image'),formatter: Controller.api.formatter.thumb, operate: false},
                        {field: 'add_time', title: __('Add_time'), operate:'RANGE', addclass:'datetimerange'},
                        
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, 
                        buttons:[
                            {
                                name: 'detail',
                                text: __('签署申请文档'),
                                icon: 'fa fa-magic',
                                classname: 'btn btn-xs btn-warning btn-magic btn-dialog',
                                url: 'union_updateapply/detail',
                                extend:'data-area=["600px","100%"]',
                            },
                            {
                                name: 'accept',
                                text: __('受理'),
                                icon: 'fa fa-hand-paper-o',
                                classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                url: 'union_updateapply/accept',
                                extend:'data-area=["400px","450px"]',
                                hidden:function(value,row){
                                    if(value.status == 0){
                                        return false;
                                    }else return true;
                                },
                                success:function(data,ret){
                                    $(".btn-refresh").trigger('click');
                                }
                            },
                            {
                                name: 'check',
                                text: __('审批'),
                                icon: 'fa fa-caret-right',
                                classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                url: 'union_updateapply/check',
                                extend:'data-area=["400px","450px"]',
                                hidden:function(value,row){
                                    if(value.status == 3){
                                        return false;
                                    }else return true;
                                },
                                success:function(data,ret){
                                    $(".btn-refresh").trigger('click');
                                }
                            }
                        ],formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        accept: function (){
            Controller.api.bindevent();
        },
        check: function (){
            Controller.api.bindevent();
            $(".check").click(function(){
                var id = $("#c-id").val();
                var result = $("#c-result").val();
                var status = $(this).attr('data-status');
                if (!result) {
                    layer.alert('请输入审核意见');
                    return false;
                }
                var msg = status==1?'确认审核通过吗？':'确认审核不通过吗？'
                Layer.confirm(msg, function (index) {
                    layer.close(index);
                    var loadindex = Layer.load();
                    $.post({
                        url: '',
                        dataType: 'json',
                        data:{id:id,status:status,result:result},
                        cache: false,
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                var msg = ret.hasOwnProperty("msg") && ret.msg != "" ? ret.msg : "";
                                if (ret.code === 1) {
                                    parent.Toastr.success(msg ? msg : '审核成功');
                                    //Toastr.success(msg ? msg : '审核成功');
                                    //layer.close(loadindex);
                                    //window.location.reload();
                                    //Layer.closeAll();
                                    parent.$(".btn-refresh").trigger("click");
                                    var index = parent.Layer.getFrameIndex(window.name);
                                    parent.Layer.close(index);
                                } else {
                                    Toastr.error(msg ? msg : '审核失败');
                                    layer.close(loadindex);
                                }
                            } else {
                                Toastr.error(__('Unknown data format'));
                                layer.close(loadindex);
                            }
                        }, error: function () {
                            Toastr.error(__('Network error'));
                            layer.close(loadindex);
                        }
                    });
                });       
            }) 
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                thumb: function (value, row, index) {
                    if (row.image.indexOf("jp") > -1||row.image.indexOf("png") > -1) {
                        var style = row.storage == 'upyun' ? '!/fwfh/120x90' : '';
                        return '<a href="' + row.image + '" target="_blank"><img src="' + row.image + style + '" alt="" style="max-height:90px;max-width:120px"></a>';
                    } else {
                        return '<a href="https://view.officeapps.live.com/op/view.aspx?src=https://dwgh.zqgx.gov.cn' + row.image + '" target="_blank"><img src="https://tool.fastadmin.net/icon/docx.png" height="30px" alt=""></a>';
                    }
                },
                status:function(value, row, index){
                    if (value==0) {
                        return '<font color="red">待受理</font>';
                    }else if(value==1){
                        return '<font color="green">审批通过</font>';
                    }else if(value==3){
                        return '<font color="#FF9800">已受理,待审核</font>';
                    }else{
                        return '<font color="red">审批拒绝</font>';
                    }
                },
            }
        }
    };
    return Controller;
});