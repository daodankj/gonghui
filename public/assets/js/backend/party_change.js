define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'party_change/index' + location.search,
                    add_url: 'party_change/add',
                    edit_url: 'party_change/edit',
                    del_url: 'party_change/del',
                    multi_url: 'party_change/multi',
                    table: 'party_change',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        /*{field: 'user_id', title: __('User_id')},*/
                        {field: 'company_name', title: __('Company_id'),operate:false},
                        {field: 'change_time', title: __('Change_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'term_office', title: __('Term_office')},
                        {field: 'type', title: __('Type')},
                        {field: 'yd_user_num', title: __('Yd_user_num')},
                        {field: 'sj_user_num', title: __('Sj_user_num')},
                        {field: 'image', title: __('Image'), events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'file', title: __('File')},
                        {field: 'intro', title: __('Intro')},
                        {field: 'add_time', title: __('Add_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});