define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'bmfw/index' + location.search,
                    add_url: 'bmfw/add',
                    edit_url: 'bmfw/edit',
                    del_url: 'bmfw/del',
                    multi_url: 'bmfw/multi',
                    table: 'bmfw',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name')},
                        {field: 'image', title: __('Image'), events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'address', title: __('Address')},
                        {field: 'lat', title: __('Lat'), operate:'BETWEEN'},
                        {field: 'lng', title: __('Lng'), operate:'BETWEEN'},
                        {field: 'add_time', title: __('Add_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, buttons:[
                            {
                                name: 'view',
                                text: __('查看设备'),
                                icon: 'fa fa-align-justify',
                                classname: 'btn btn-xs btn-info btn-magic btn-addtabs',
                                url: 'bmfw/device_list?ids={$row.id}',
                                //extend:'data-area=["400px","500px"]',
                            },
                            {
                                name: 'borrow',
                                text: __('图书借阅记录'),
                                icon: 'fa fa-align-justify',
                                classname: 'btn btn-xs btn-primary btn-addtabs',
                                url: 'bmfw/get_borrow_all',
                                //extend:'data-area=["400px","500px"]',
                            },
                            {
                                name: 'user',
                                text: __('图书用户列表'),
                                icon: 'fa fa-align-justify',
                                classname: 'btn btn-xs btn-warning btn-addtabs',
                                url: 'bmfw/get_user_all',
                                //extend:'data-area=["400px","500px"]',
                            }
                        ],formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        add_device_member: function () {
            Controller.api.bindevent();
        },
        device_list: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'bmfw/device_list' + location.search,
                    table: 'bmfw',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'deviceSerial',
                sortName: 'deviceSerial',
                showExport: false,
                search:false,
                commonSearch: false,
                showToggle: false,
                showColumns: false,
                cardView:false,
                pageSize:30,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'deviceSerial', title: __('设备序列号'),operate: false},
                        {field: 'deviceName', title: __('设备名称'),operate: false},
                        {field: 'deviceType', title: __('设备类型'), operate:false},
                        {field: 'status', title: __('在线状态'), operate:false,formatter: Controller.api.formatter.status},
                        {field: 'addTime', title: __('Add_time'), operate:false, addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, buttons:[
                                {
                                    name: 'channel',
                                    text: __('通道列表'),
                                    icon: 'fa fa-align-justify',
                                    classname: 'btn btn-xs btn-success btn-magic btn-dialog',
                                    url: 'bmfw/channel_list?fwId={$row.fwId}',
                                    //extend:'data-area=["400px","500px"]',
                                    hidden:function(value,row){
                                        if(value.deviceType == 'DS-7616NX-I3'){
                                            return false;
                                        }else return true;
                                    },
                                },
                                {
                                    name: 'event',
                                    text: __('告警事件'),
                                    icon: 'fa fa-list-ol',
                                    classname: 'btn btn-xs btn-primary btn-magic btn-dialog',
                                    url: 'bmfw/get_event_record?fwId={$row.fwId}',
                                    //extend:'data-area=["400px","500px"]',
                                    hidden:function(value,row){
                                        if(value.deviceType == 'DS-7616NX-I3'){
                                            return false;
                                        }else return true;
                                    },
                                },
                                {
                                    name: 'childList',
                                    text: __('子设备列表'),
                                    icon: 'fa fa-list-ol',
                                    classname: 'btn btn-xs btn-primary btn-magic btn-dialog',
                                    url: 'bmfw/child_list?fwId={$row.fwId}',
                                    //extend:'data-area=["400px","500px"]',
                                    hidden:function(value,row){
                                        if(value.deviceType == 'CS-A3-V200-WBG'){
                                            return false;
                                        }else return true;
                                    },
                                },
                                {
                                    name: 'live',
                                    text: __('查看监控'),
                                    icon: 'fa fa-eye',
                                    classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                    url: 'bmfw/viewLive?fwId={$row.fwId}',
                                    //extend:'data-area=["400px","500px"]',
                                    hidden:function(value,row){
                                        if(value.deviceType == 'DS-K1T670M'){
                                            return false;
                                        }else return true;
                                    },
                                },
                                {
                                    name: 'inout',
                                    text: __('出入记录'),
                                    icon: 'fa fa-eye',
                                    classname: 'btn btn-xs btn-success btn-magic btn-dialog',
                                    url: 'bmfw/get_in_out_list?fwId={$row.fwId}',
                                    //extend:'data-area=["400px","500px"]',
                                    hidden:function(value,row){
                                        if(value.deviceType == 'DS-K1T670M'){
                                            return false;
                                        }else return true;
                                    },
                                },
                                {
                                    name: 'member',
                                    text: __('人员列表'),
                                    icon: 'fa fa-list-ol',
                                    classname: 'btn btn-xs btn-primary btn-magic btn-dialog',
                                    url: 'bmfw/get_device_member_list?fwId={$row.fwId}',
                                    //extend:'data-area=["400px","500px"]',
                                    hidden:function(value,row){
                                        if(value.deviceType == 'DS-K1T670M'){
                                            return false;
                                        }else return true;
                                    },
                                }
                            ],formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        channel_list: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'bmfw/channel_list' + location.search,
                    table: 'bmfw',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'deviceSerial',
                sortName: 'deviceSerial',
                showExport: false,
                search:false,
                commonSearch: false,
                showToggle: false,
                showColumns: false,
                cardView:false,
                pageSize:30,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'deviceSerial', title: __('设备序列号'),operate: false},
                        //{field: 'deviceName', title: __('设备名称'),operate: false},
                        {field: 'channelNo', title: __('通道号'), operate:false},
                        {field: 'channelName', title: __('通道号名称'), operate:false},
                        {field: 'picUrl', title: __('图片'), events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'status', title: __('在线状态'), operate:false,formatter: Controller.api.formatter.status},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, buttons:[
                            {
                                    name: 'live',
                                    text: __('查看监控'),
                                    icon: 'fa fa-eye',
                                    classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                    url: 'bmfw/viewLive?fwId={$row.fwId}&channel={$row.channelNo}',
                                }
                            ],formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        child_list: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'bmfw/child_list' + location.search,
                    table: 'bmfw',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'deviceSerial',
                sortName: 'deviceSerial',
                showExport: false,
                search:false,
                commonSearch: false,
                showToggle: false,
                showColumns: false,
                cardView:false,
                pageSize:30,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'deviceSerial', title: __('设备序列号'),operate: false},
                        {field: 'deviceName', title: __('设备名称'),operate: false},
                        {field: 'model', title: __('设备型号'), operate:false},
                        {field: 'status', title: __('在线状态'), operate:false,formatter: Controller.api.formatter.status},
                        //{field: 'addTime', title: __('Add_time'), operate:false, addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, buttons:[],formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        get_borrow_all: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'bmfw/get_borrow_all' + location.search,
                    table: 'bmfw',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showExport: false,
                search:false,
                commonSearch: false,
                showToggle: false,
                showColumns: false,
                cardView:false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'bookname', title: __('图书名称'),operate: false},
                        {field: 'bookIdentifier', title: __('馆藏条码号'),operate: false},
                        {field: 'user', title: __('借阅人'), operate:false},
                        {field: 'day', title: __('借阅天数'), operate:false},
                        {field: 'renew', title: __('续借次数'), operate:false},
                        {field: 'state', title: __('状态'), operate:false,formatter: Controller.api.formatter.state},
                        {field: 'borrowDate', title: __('借阅日期'), operate:false, addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'returnDate', title: __('还书时间'), operate:false},
                        {field: 'dueDate', title: __('应还时间'), operate:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, buttons:[],formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        get_user_all: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'bmfw/get_user_all' + location.search,
                    table: 'bmfw',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showExport: false,
                search:false,
                commonSearch: false,
                showToggle: false,
                showColumns: false,
                cardView:false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'uid', title: __('读者证'),operate: false},
                        {field: 'name', title: __('姓名'),operate: false},
                        {field: 'rfid', title: __('读者卡号'), operate:false},
                        {field: 'type', title: __('身份类型'), operate:false},
                        {field: 'sex', title: __('性别'), operate:false},
                        {field: 'userState', title: __('状态'), operate:false},
                        {field: 'idCard', title: __('身份证号码'), operate:false},
                        {field: 'phone', title: __('电话号码'), operate:false},
                        {field: 'effectiveDate', title: __('有效日期'), operate:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, buttons:[],formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        get_in_out_list: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'bmfw/get_in_out_list' + location.search,
                    table: 'bmfw',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'deviceSerial',
                sortName: 'deviceSerial',
                showExport: false,
                search:false,
                commonSearch: true,
                showToggle: false,
                showColumns: false,
                cardView:false,
                pageSize:10,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'deviceSerial', title: __('设备序列号'),operate: false},
                        {field: 'deviceName', title: __('设备名称'),operate: false},
                        {field: 'name', title: __('用户名称'), operate:false},
                        {field: 'sex', title: __('性别'), operate:false},
                        {field: 'IDCardNo', title: __('身份证号'), operate: false},
                        {field: 'dateTime', title: __('出入时间'), operate:'RANGE', addclass:'datetimerange'},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        get_device_member_list: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'bmfw/get_device_member_list' + location.search,
                    add_url: 'bmfw/add_device_member' + location.search,
                    table: 'bmfw',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'deviceSerial',
                sortName: 'deviceSerial',
                showExport: false,
                search:false,
                commonSearch: false,
                showToggle: false,
                showColumns: false,
                cardView:false,
                pageSize:10,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'deviceSerial', title: __('设备序列号'),operate: false},
                        {field: 'name', title: __('用户名称'), operate:false},
                        {field: 'employeeNo',title: '工号',operate: false},
                        {field: 'gender', title: __('性别'), operate:false},
                        {field: 'beginTime', title: __('有效开始时间'), operate: false},
                        {field: 'endTime', title: __('有效结束时间'), operate:false},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        get_event_record: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'bmfw/get_event_record' + location.search,
                    table: 'bmfw',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'deviceSerial',
                sortName: 'deviceSerial',
                showExport: false,
                search:false,
                commonSearch: true,
                showToggle: false,
                showColumns: false,
                cardView:false,
                pageSize:10,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'deviceSerial', title: __('设备序列号'),operate: false},
                        {field: 'channel', title: __('通道号'), operate:false},
                        {field: 'behaviorEventType',title: '告警类型',operate: false},
                        {field: 'startTime', title: __('告警时间'), operate:'RANGE', addclass:'datetimerange'},
                        //{field: 'endTime', title: __('结束时间'), operate:false},
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        cache: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter:{
                status:function(value, row, index){
                    if(value==1){
                        return '<font color="green">在线</font>';
                    }else{
                        return '<font color="red">离线</font>';
                    }
                },
                state:function(value, row, index){
                    if(value==1){
                        return '<font color="red">未归还</font>';
                    }else{
                        return '<font color="green">已归还</font>';
                    }
                },
            }
        }
    };
    return Controller;
});