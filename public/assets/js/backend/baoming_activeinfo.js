define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'baoming_activeinfo/index' + location.search,
                    add_url: 'baoming_activeinfo/add',
                    //edit_url: 'baoming_activeinfo/edit',
                    del_url: 'baoming_activeinfo/del',
                    multi_url: 'baoming_activeinfo/multi',
                    table: 'baoming_activeinfo',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        /*{field: 'id', title: __('Id')},*/
                        {field: 'active_id', title: __('Active_id'),visible:false,addClass: "selectpage", extend: "data-source='baoming_active/index'"},
                        {field: 'baomingactive.name', title: __('Baomingactive.name'),operate:false},
                        {field: 'name', title: __('Name')},
                        {field: 'phone', title: __('Phone')},
                        {field: 'idcard', title: __('Idcard')},
                        {field: 'sex', title: __('Sex')},
                        {field: 'company_name', title: __('Company_name')},
                        {field: 'job', title: __('Job')},
                        {field: 'remark', title: __('Remark')},
                        {field: 'add_time', title: __('Add_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'status', title: __('是否签到'),searchList: {"0":'未签到',"1":'已签到'},visible:false},
                        {field: 'qd_time', title: __('签到时间'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
