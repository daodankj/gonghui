define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'zhuxi_pingyi/index' + location.search,
                    add_url: 'zhuxi_pingyi/add',
                    edit_url: 'zhuxi_pingyi/edit',
                    del_url: 'zhuxi_pingyi/del',
                    multi_url: 'zhuxi_pingyi/multi',
                    table: 'zhuxi_pingyi',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'topic_id', title: __('Topic_id'),visible: false,addClass: "selectpage", extend: "data-source='topic/index'"},
                        {field: 'company_id', title: __('Company_id'),visible: false,addClass: "selectpage", extend: "data-source='company/index'"},
                        {field: 'user_id', title: __('User_id'),visible:false,addClass: "selectpage", extend: "data-source='user/user/index' data-field='username'"},
                        {field: 'topic_name', title: __('Topic_id'),operate: false},
                        {field: 'company_name', title: __('Company_id'),operate: false},
                        {field: 'username', title: __('Username'),operate: false},
                        //{field: 'status', title: __('Status'),searchList: {"1":'满意',"2":'不满意'},formatter: Controller.api.formatter.status},
                        //{field: 'content', title: __('Content')},
                        {field: 'field1', title: __('总体工作满意度'),searchList: {"1":'满意',"2":'不满意'},formatter: Controller.api.formatter.status},
                        {field: 'field2', title: __('政治思想道德及廉洁自律'),searchList: {"1":'满意',"2":'不满意'},formatter: Controller.api.formatter.status},
                        {field: 'field3', title: __('工会内部管理规范有序'),searchList: {"1":'满意',"2":'不满意'},formatter: Controller.api.formatter.status},
                        {field: 'field4', title: __('关心职工热心解决职工困难'),searchList: {"1":'满意',"2":'不满意'},formatter: Controller.api.formatter.status},
                        {field: 'field5', title: __('依法维护职工合法权益'),searchList: {"1":'满意',"2":'不满意'},formatter: Controller.api.formatter.status},
                        {field: 'field6', title: __('民主参与企业管理'),searchList: {"1":'满意',"2":'不满意'},formatter: Controller.api.formatter.status},
                        {field: 'field7', title: __('开展职工喜闻乐见活动'),searchList: {"1":'满意',"2":'不满意'},formatter: Controller.api.formatter.status},
                        {field: 'add_time', title: __('Add_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                status:function(value, row, index){
                    if(value==1){
                        return '<font color="green">满意</font>';
                    }else{
                        return '<font color="red">不满意</font>';
                    }
                },
            }
        }
    };
    return Controller;
});