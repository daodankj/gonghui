define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'baoming_active/index' + location.search,
                    add_url: 'baoming_active/add',
                    edit_url: 'baoming_active/edit',
                    del_url: 'baoming_active/del',
                    multi_url: 'baoming_active/multi',
                    table: 'baoming_active',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name')},
                        {field: 'start_time', title: __('Start_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'end_time', title: __('End_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'user_num', title: __('最大参与人数'),operate:false},
                        {field: 'image', title: __('群二维码'), events: Table.api.events.image, formatter: Table.api.formatter.images,operate:false},
                        {field: 'status', title: __('Status'),searchList: {"0":'禁用',"1":'启用'},formatter:Table.api.formatter.toggle},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                        buttons:[
                            {
                                name: 'qrcode',
                                text: __('活动报名二维码'),
                                icon: 'fa fa-qrcode',
                                classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                url: 'baoming_active/qrcode',
                                extend:'data-area=["400px","450px"]'
                            },
                            {
                                name: 'qiandao',
                                text: __('签到二维码'),
                                icon: 'fa fa-qrcode',
                                classname: 'btn btn-xs btn-success btn-magic btn-dialog',
                                url: 'baoming_active/qiandao',
                                extend:'data-area=["400px","450px"]'
                            }
                        ], formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});
