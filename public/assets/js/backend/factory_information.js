define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'factory_information/index' + location.search,
                    add_url: 'factory_information/add',
                    edit_url: 'factory_information/edit',
                    del_url: 'factory_information/del',
                    multi_url: 'factory_information/multi',
                    table: 'factory_information',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'company_id', title: __('Company_id'),visible:false,addClass: "selectpage", extend: "data-source='company/index'"},
                        {field: 'company_name', title: __('Company_id'),operate:false},
                        {field: 'user_id', title: __('User_id'),visible:false,addClass: "selectpage", extend: "data-source='user/user/index' data-field='username'"},
                        {field: 'user_name', title: __('User_id'),operate:false},
                        {field: 'title', title: __('Title')},
                        {field: 'add_time', title: __('Add_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});