define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'dictionary/index' + location.search,
                    add_url: 'dictionary/add',
                    edit_url: 'dictionary/edit',
                    del_url: 'dictionary/del',
                    multi_url: 'dictionary/multi',
                    table: 'dictionary',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        /*{field: 'pid', title: __('Pid')},*/
                        {field: 'name', title: __('Name')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                            buttons:[
                                {
                                    name: 'child',
                                    text: __('子类'),
                                    icon: 'fa fa-sitemap',
                                    classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                    url: 'dictionary/index?pid={row.id}',
                                    extend:'data-area=["80%","100%"]',
                                    hidden:function(value,row){
                                        if(value.pid == 0){
                                            return false;
                                        }else return true;
                                    },
                                }

                            ],
                            formatter: Table.api.formatter.operate
                        }
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            }
        }
    };
    return Controller;
});