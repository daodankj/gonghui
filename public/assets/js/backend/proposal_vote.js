define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'proposal_vote/index' + location.search,
                    //add_url: 'proposal_vote/add',
                    //edit_url: 'proposal_vote/edit',
                    del_url: 'proposal_vote/del',
                    multi_url: 'proposal_vote/multi',
                    table: 'proposal_vote',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'proposal_id', title: __('提案'),visible:false,addClass: "selectpage", extend: "data-source='proposal/index' data-field='title'"},
                        {field: 'proposal_title', title: __('提案'),operate:false},
                        {field: 'user_id', title: __('用户'),visible:false,addClass: "selectpage", extend: "data-source='user/user/index' data-field='username'"},
                        {field: 'user_name', title: __('用户'),operate:false},
                        {field: 'status', title: __('Status'),sortable: false,searchList: {"0":'弃权',"1":'同意',"2":'反对'},formatter: Controller.api.formatter.status},
                        {field: 'add_time', title: __('投票时间'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        pingjia: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'proposal_vote/pingjia' + location.search,
                    del_url: 'proposal_vote/del',
                    multi_url: 'proposal_vote/multi',
                    table: 'proposal_vote',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'proposal_id', title: __('提案'),visible:false,addClass: "selectpage", extend: "data-source='proposal/index' data-field='title'"},
                        {field: 'proposal_title', title: __('提案'),operate:false},
                        {field: 'user_id', title: __('用户'),visible:false,addClass: "selectpage", extend: "data-source='user/user/index' data-field='username'"},
                        {field: 'user_name', title: __('用户'),operate:false},
                        {field: 'content', title: __('Content')},
                        {field: 'add_time', title: __('发布时间'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                status:function(value, row, index){
                    if (value==0) {
                        return '<font color="#999999">弃权</font>';
                    }else if(value==1){
                        return '<font color="green">同意</font>';
                    }else{
                        return '<font color="red">反对</font>';
                    }
                },
            }
        }
    };
    return Controller;
});