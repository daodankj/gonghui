define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'dream_apply/index' + location.search,
                    add_url: 'dream_apply/add',
                    edit_url: 'dream_apply/edit',
                    del_url: 'dream_apply/del',
                    multi_url: 'dream_apply/multi',
                    table: 'dream_apply',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'status',
                sortOrder : 'asc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'),sortable: true},
                        {field: 'status', title: __('状态'),sortable: true,searchList: {"0":'待审批',"1":'已审批'},formatter: Controller.api.formatter.status},
                        {field: 'company_name', title: __('所属企业'),operate:false},
                        {field: 'company_id', title: __('所属企业'),visible:false,addClass: "selectpage", extend: "data-source='company/index'"},
                        {field: 's_name', title: __('S_name')},
                        {field: 's_age', title: __('S_age')},
                        /*{field: 'phone', title: __('电话'),operate:false},*/
                        {field: 's_phone', title: __('电话')},
                        {field: 's_idcard', title: __('身份证号码')},
                        {field: 's_company', title: __('S_company')},
                        {field: 's_income', title: __('S_income')},
                        /*{field: 'w_name', title: __('W_name')},
                        {field: 'w_age', title: __('W_age')},
                        {field: 'w_company', title: __('W_company')},
                        {field: 'w_income', title: __('W_income')},*/
                        /*{field: 'live_squer', title: __('Live_squer')},
                        {field: 'is_signle', title: __('Is_signle'), searchList: {"是":__('是'),"否":__('否')}, formatter: Table.api.formatter.normal},
                        {field: 'is_welfare', title: __('Is_welfare'), searchList: {"是":__('是'),"否":__('否')}, formatter: Table.api.formatter.normal},
                        {field: 'is_company', title: __('Is_company'), searchList: {"是":__('是'),"否":__('否')}, formatter: Table.api.formatter.normal},
                        {field: 'is_economy', title: __('Is_economy'), searchList: {"是":__('是'),"否":__('否')}, formatter: Table.api.formatter.normal},
                        {field: 'is_lowrent', title: __('Is_lowrent'), searchList: {"是":__('是'),"否":__('否')}, formatter: Table.api.formatter.normal},
                        {field: 'is_mark', title: __('Is_mark'), searchList: {"是":__('是'),"否":__('否')}, formatter: Table.api.formatter.normal},
                        {field: 'live_start_date', title: __('Live_start_date'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'live_end_date', title: __('Live_end_date'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'village_squer', title: __('Village_squer')},
                        {field: 'is_private', title: __('Is_private'), searchList: {"是":__('是'),"否":__('否')}, formatter: Table.api.formatter.normal},
                        {field: 'privete_remark', title: __('Privete_remark')},
                        {field: 'is_public', title: __('Is_public'), searchList: {"是":__('是'),"否":__('否')}, formatter: Table.api.formatter.normal},
                        {field: 'public_remark', title: __('Public_remark')},
                        {field: 'family_info', title: __('Family_info')},
                        {field: 'gz_income', title: __('Gz_income')},
                        {field: 'zf_income', title: __('Zf_income')},
                        {field: 'village_income', title: __('Village_income')},
                        {field: 'tz_income', title: __('Tz_income')},
                        {field: 'else_income', title: __('Else_income')},
                        {field: 'total_income', title: __('Total_income')},
                        {field: 'medical_charge', title: __('Medical_charge')},
                        {field: 'school_charge', title: __('School_charge')},
                        {field: 'living_charge', title: __('Living_charge')},
                        {field: 'child_charge', title: __('Child_charge')},
                        {field: 'else_charge', title: __('Else_charge')},
                        {field: 'rent_charge', title: __('Rent_charge')},
                        {field: 'house_charge', title: __('House_charge')},
                        {field: 'total_charge', title: __('Total_charge')},
                        {field: 'intro', title: __('Intro')},
                        {field: 'needinfo', title: __('Needinfo')},*/
                        {field: 'image1', title: __('Image1'),events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'image2', title: __('Image2'),events: Table.api.events.image, formatter: Table.api.formatter.image},
                        {field: 'add_time', title: __('Add_time'), operate:'RANGE', addclass:'datetimerange'},
                        
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                        buttons:[
                            {
                                name: 'accept',
                                text: __('受理'),
                                icon: 'fa fa-hand-paper-o',
                                classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                url: 'dream_apply/accept',
                                extend:'data-area=["400px","450px"]',
                                hidden:function(value,row){
                                    if(value.status == 0){
                                        return false;
                                    }else return true;
                                },
                                success:function(data,ret){
                                    $(".btn-refresh").trigger('click');
                                }
                            },
                            {
                                name: 'check',
                                text: __('审批'),
                                icon: 'fa fa-caret-right',
                                classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                url: 'dream_apply/check',
                                extend:'data-area=["400px","450px"]',
                                hidden:function(value,row){
                                    if(value.status == 3){
                                        return false;
                                    }else return true;
                                },
                                success:function(data,ret){
                                    $(".btn-refresh").trigger('click');
                                }
                            }

                        ], formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        accept: function (){
            Controller.api.bindevent();
        },
        check: function (){
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                status:function(value, row, index){
                    if (value==0) {
                        return '<font color="red">待受理</font>';
                    }else if(value==1){
                        return '<font color="green">审批通过</font>';
                    }else if(value==3){
                        return '<font color="#FF9800">已受理,待审核</font>';
                    }else{
                        return '<font color="red">审批拒绝</font>';
                    }
                },
            }
        }
    };
    return Controller;
});