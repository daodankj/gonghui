define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'site_discount/index' + location.search,
                    add_url: 'site_discount/add',
                    edit_url: 'site_discount/edit',
                    del_url: 'site_discount/del',
                    multi_url: 'site_discount/multi',
                    table: 'site_discount',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'status',
                sortOrder : 'asc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'),sortable: true},
                        {field: 'status', title: __('Status'),sortable: true,searchList: {"0":'待审批',"1":'审批通过',"2":'审批拒绝'},formatter: Controller.api.formatter.status},
                        {field: 'company_name', title: __('所属企业'),operate:false},
                        {field: 'company_id', title: __('所属企业'),visible:false,addClass: "selectpage", extend: "data-source='company/index'"},
                        {field: 'user_name', title: __('User_id'),operate:false},

                        {field: 'use_time', title: __('Use_time')},
                        {field: 'intro', title: __('Intro')},
                        {field: 'user_num', title: __('User_num')},
                        {field: 'site_type', title: __('Site_type'), searchList: {"会议室":__('会议室'),"足球场":__('足球场'),"篮球场":__('篮球场')}, formatter: Table.api.formatter.normal},
                        {field: 'image', title: __('Image'),operate:false,formatter: Controller.api.formatter.thumb},
                        {field: 'add_time', title: __('Add_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},               
                        
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                        buttons:[
                            {
                                name: 'check',
                                text: __('审批'),
                                icon: 'fa fa-caret-right',
                                classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                url: 'site_discount/check',
                                extend:'data-area=["400px","450px"]',
                                hidden:function(value,row){
                                    if(value.status == 0){
                                        return false;
                                    }else return true;
                                },
                                success:function(data,ret){
                                    $(".btn-refresh").trigger('click');
                                }
                            }
                        ], formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        check: function (){
            Controller.api.bindevent();
            $(".check").click(function(){
                var id = $("#c-id").val();
                var result = $("#c-result").val();
                var result2 = $("#c-result2").val();
                var status = $(this).attr('data-status');
                /*if (!result) {
                    layer.alert('请输入审核意见');
                    return false;
                }*/
                var msg = status==1?'确认审核通过吗？':'确认审核不通过吗？'
                Layer.confirm(msg, function (index) {
                    layer.close(index);
                    var loadindex = Layer.load();
                    $.post({
                        url: '',
                        dataType: 'json',
                        data:{id:id,status:status,result:result,result2:result2},
                        cache: false,
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                var msg = ret.hasOwnProperty("msg") && ret.msg != "" ? ret.msg : "";
                                if (ret.code === 1) {
                                    parent.Toastr.success(msg ? msg : '审核成功');
                                    //Toastr.success(msg ? msg : '审核成功');
                                    //layer.close(loadindex);
                                    //window.location.reload();
                                    //Layer.closeAll();
                                    parent.$(".btn-refresh").trigger("click");
                                    var index = parent.Layer.getFrameIndex(window.name);
                                    parent.Layer.close(index);
                                } else {
                                    Toastr.error(msg ? msg : '审核失败');
                                    layer.close(loadindex);
                                }
                            } else {
                                Toastr.error(__('Unknown data format'));
                                layer.close(loadindex);
                            }
                        }, error: function () {
                            Toastr.error(__('Network error'));
                            layer.close(loadindex);
                        }
                    });
                });       
            }) 
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                thumb: function (value, row, index) {
                    if(!row.image){
                        return '';
                    }else if (row.image.indexOf("jp") > -1||row.image.indexOf("png") > -1) {
                        //var style = row.storage == 'upyun' ? '!/fwfh/120x90' : '';
                        //return '<a href="' + row.images + '" target="_blank"><img src="' + row.images + style + '" alt="" style="max-height:90px;max-width:120px"></a>';
                        return Table.api.formatter.images(value, row, index);
                    }else if (row.image.indexOf("doc") > -1) {
                        return '<a class="btn-dialog" title="文件预览" href="https://view.officeapps.live.com/op/view.aspx?src=https://dwgh.zqgx.gov.cn' + row.image + '" target="_blank"><img src="https://tool.fastadmin.net/icon/docx.png" height="30px" alt=""></a>';
                    } else {
                        return '<a class="btn-dialog" title="文件预览" href="https://view.officeapps.live.com/op/view.aspx?src=https://dwgh.zqgx.gov.cn' + row.image + '" target="_blank"><img src="https://tool.fastadmin.net/icon/xlx.png" height="30px" alt=""></a>';
                    }
                },
                intro: function (value, row, index) {
                    var intro = value.substring(0,15);
                    return '<span data-toggle="tooltip" title="'+value+'">'+intro+'</span>';
                },
                status:function(value, row, index){
                    if (value==0) {
                        return '<font color="#FF9800">待审批</font>';
                    }else if(value==1){
                        return '<font color="green">审批通过</font>';
                    }else{
                        return '<font color="red">审批拒绝</font>';
                    }
                },
            }
        }
    };
    return Controller;
});