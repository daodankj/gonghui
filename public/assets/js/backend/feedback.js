define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'feedback/index' + location.search,
                    //add_url: 'feedback/add',
                    //edit_url: 'feedback/edit',
                    //del_url: 'feedback/del',
                    //multi_url: 'feedback/multi',
                    table: 'feedback',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        /*{field: 'id', title: __('Id')},*/
                        {field: 'uname', title: __('Uname')},
                        {field: 'content', title: __('Content'),operate:false,formatter: Controller.api.formatter.intro},
                        {field: 'add_time', title: __('Add_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'rely_content', title: __('Rely_content'),operate:false,formatter: Controller.api.formatter.intro},
                        /*{field: 'rely_time', title: __('Rely_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},*/
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, 
                        buttons:[
                            {
                                name: 'replay',
                                text: __('回复'),
                                icon: 'fa fa-caret-right',
                                classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                url: 'feedback/replay',
                                extend:'data-area=["400px","450px"]',
                                success:function(data,ret){
                                    $(".btn-refresh").trigger('click');
                                }
                            }

                        ],formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        replay: function () {
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                intro: function (value, row, index) {
                    var intro = value.substring(0,25);
                    return '<span data-toggle="tooltip" title="'+value+'">'+intro+'</span>';
                }
            }
        }
    };
    return Controller;
});