define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'party_member_change/index' + location.search,
                    add_url: 'party_member_change/add',
                    edit_url: 'party_member_change/edit',
                    del_url: 'party_member_change/del',
                    multi_url: 'party_member_change/multi',
                    table: 'party_member_change',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        /*{field: 'user_id', title: __('User_id')},
                        {field: 'company_id', title: __('Company_id')},*/
                        {field: 'name', title: __('Name')},
                        {field: 'idcard', title: __('Idcard')},
                        {field: 'phone', title: __('Phone')},
                        {field: 'age', title: __('年龄')},
                        {field: 'mingz', title: __('Mingz')},
                        {field: 'now_party_name', title: __('Now_party_name')},
                        {field: 'to_party_name', title: __('To_party_name')},
                        {field: 'status', title: __('status'),searchList: {"0":'待受理',"1":'已受理',"2":'拒绝'},formatter: Controller.api.formatter.status},
                        {field: 'add_time', title: __('Add_time'), operate:'RANGE', addclass:'datetimerange'},
                        /*{field: 'result', title: __('Result')},*/
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,buttons:[

                            {
                                name: 'check',
                                text: __('受理'),
                                icon: 'fa fa-caret-right',
                                classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                url: 'party_member_change/check',
                                extend:'data-area=["400px","450px"]',
                                hidden:function(value,row){
                                    if(value.status == 0){
                                        return false;
                                    }else return true;
                                },
                                success:function(data,ret){
                                    $(".btn-refresh").trigger('click');
                                }
                            }

                        ], formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        check: function (){
            Controller.api.bindevent();
            $(".check").click(function(){
                var id = $("#c-id").val();
                var result = $("#c-result").val();
                var status = $(this).attr('data-status');
                if (!result) {
                    layer.alert('请输入受理意见');
                    return false;
                }
                var msg = status==1?'确认受理吗？':'确认拒绝吗？'
                Layer.confirm(msg, function (index) {
                    layer.close(index);
                    var loadindex = Layer.load();
                    $.post({
                        url: '',
                        dataType: 'json',
                        data:{id:id,status:status,result:result},
                        cache: false,
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                var msg = ret.hasOwnProperty("msg") && ret.msg != "" ? ret.msg : "";
                                if (ret.code === 1) {
                                    parent.Toastr.success(msg ? msg : '受理成功');
                                    //Toastr.success(msg ? msg : '审核成功');
                                    //layer.close(loadindex);
                                    //window.location.reload();
                                    //Layer.closeAll();
                                    parent.$(".btn-refresh").trigger("click");
                                    var index = parent.Layer.getFrameIndex(window.name);
                                    parent.Layer.close(index);
                                } else {
                                    Toastr.error(msg ? msg : '受理失败');
                                    layer.close(loadindex);
                                }
                            } else {
                                Toastr.error(__('Unknown data format'));
                                layer.close(loadindex);
                            }
                        }, error: function () {
                            Toastr.error(__('Network error'));
                            layer.close(loadindex);
                        }
                    });
                });
            })
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                status:function(value, row, index){
                    if (value==0) {
                        return '<font color="#FF9800">待审核</font>';
                    }else if(value==1){
                        return '<font color="green">已受理</font>';
                    }else{
                        return '<font color="red">拒绝</font>';
                    }
                },
            }
        }
    };
    return Controller;
});
