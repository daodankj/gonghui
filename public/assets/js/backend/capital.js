define(['jquery', 'bootstrap', 'backend', 'table','echarts', 'echarts-theme', 'form'], function ($, undefined, Backend, Table, Echarts, undefined, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'capital/index' + location.search,
                    //add_url: 'capital/add',
                    //edit_url: 'capital/edit',
                    //del_url: 'capital/del',
                    //multi_url: 'capital/multi',
                    table: 'capital',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                showExport: true,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'),visible:false},
                        {field: 'company.short_name', title: __('Company.name'),operate:false},
                        {field: 'company_id', title: __('Company_id'),visible:false,addClass: "selectpage", extend: "data-source='company/index'"},
                        /*{field: 'user_id', title: __('User_id')},*/
                        {field: 'total_money', title: __('Total_money'), operate:'BETWEEN',formatter: Controller.api.formatter.money},
                        {field: 'lastyear_balance', title: __('上期结余'), operate:'BETWEEN',formatter: Controller.api.formatter.money},
                        {field: 'thisyear_income', title: __('本期收入'), operate:'BETWEEN',formatter: Controller.api.formatter.money},
                        {field: 'thisyear_money', title: __('本期总支出'), operate:'BETWEEN',formatter: Controller.api.formatter.money},
                        {field: 'staff_activities_money', title: __('Staff_activities_money'), operate:'BETWEEN',formatter: Controller.api.formatter.money},
                        {field: 'business_money', title: __('Business_money'), operate:'BETWEEN',formatter: Controller.api.formatter.money},
                        {field: 'safeguard_rights_money', title: __('Safeguard_rights_money'), operate:'BETWEEN',formatter: Controller.api.formatter.money},
                        /*{field: 'administration_money', title: __('Administration_money'), operate:'BETWEEN',formatter: Controller.api.formatter.money},*/
                        {field: 'capital_money', title: __('Capital_money'), operate:'BETWEEN',formatter: Controller.api.formatter.money},
                        /*{field: 'subsidy_money', title: __('Subsidy_money'), operate:'BETWEEN',formatter: Controller.api.formatter.money},*/
                        /*{field: 'cause_money', title: __('Cause_money'), operate:'BETWEEN',formatter: Controller.api.formatter.money},*/
                        {field: 'other_money', title: __('Other_money'), operate:'BETWEEN',formatter: Controller.api.formatter.money},
                        {field: 'creaetime', title: __('Creaetime'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                        buttons:[
                            {
                                name: 'view',
                                text: __('视图'),
                                icon: 'fa fa-magic',
                                classname: 'btn btn-xs btn-warning btn-magic btn-dialog',
                                url: 'capital/view',
                                extend:'data-area=["800px","90%"]',
                            }
                        ], formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        view: function (){
            Controller.api.bindevent();
            var myChart = Echarts.init(document.getElementById('echart'), 'walden');
            var option = {
                title: {
                    left: 'center'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: '{a} <br/>{b} : {c} ({d}%)'
                },
                legend: {
                    orient: 'horizontal',//vertical
                    bottom: 0,
                    data: data,
                },
                series : [
                    {
                        name: '支出占比',
                        type: 'pie',    // 设置图表类型为饼图
                        selectedMode: 'single',
                        radius: ['0','50%'],
                        //center: ['50%', '50%'],
                        data:data,
                        itemStyle: {
                            emphasis: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            },
                            normal:{ 
                                label:{ 
                                    show: true, 
                                    formatter: '{b} : {c} ({d}%)' ,
                                    textStyle:{
                                        fontSize: 14
                                    },
                                    /*formatter: function (value) {
                                        let val = value.data.value;
                                        if (val >= 10000 && val < 10000000) {
                                          val = (val / 10000).toFixed(2) + "万";
                                        }
                                        return val;
                                    }*/
                                }, 
                                labelLine :{show:true} ,
                                color:function(params) {
                                    //自定义颜色
                                    var colorList = [          
                                            'purple', '#000000', 'indigo', '#FF8C00', '#FF0000', '#FE8463','blue','#26C0C0'
                                        ];
                                        return colorList[params.dataIndex]
                                }
                            } 
                        }
                    },
                    /*{
                        name: '支出占比',
                        type: 'pie',    // 设置图表类型为饼图
                        radius: ['40%','55%'],
                        //center: ['40%', '55%'],
                        data:datainfo,
                        itemStyle: {
                            emphasis: {
                                shadowBlur: 10,
                                shadowOffsetX: 0,
                                shadowColor: 'rgba(0, 0, 0, 0.5)'
                            },
                            normal:{ 
                                label:{ 
                                    show: true, 
                                    formatter: '{b} : {c} ({d}%)' ,
                                    textStyle:{
                                        fontSize: 14
                                    },
                                }, 
                                labelLine :{show:true} ,
                                
                            } 
                        }
                    }*/
                ]
            }
            myChart.setOption(option);
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                money: function (value, row, index) {
                    /*if(value>=10000){
                        return (value/10000)+"万";
                    }else{
                        return value;
                    }*/
                    return value;
                }
            }
        }
    };
    return Controller;
});