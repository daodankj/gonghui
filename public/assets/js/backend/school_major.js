define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'school_major/index' + location.search,
                    add_url: 'school_major/add',
                    addmajor_url: 'school_major/addmajor',
                    edit_url: 'school_major/edit',
                    del_url: 'school_major/del',
                    multi_url: 'school_major/multi',
                    table: 'school_major',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'type', title: __('Type'),formatter: Controller.api.formatter.type},
                        {field: 'name', title: __('Name')},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate, buttons:[
                            {
                                name: 'major',
                                text: __('专业'),
                                icon: 'fa fa-list',
                                classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                url: 'school_major/major?pid={row.id}',
                                extend:'data-area=["80%","90%"]'
                            }
                        ],formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        major: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'school_major/major' + location.search,
                    add_url: 'school_major/addmajor',
                    edit_url: 'school_major/editmajor',
                    del_url: 'school_major/del',
                    table: 'school_major',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'id',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id')},
                        {field: 'name', title: __('Name')},
                        {field: 'pid', title: __('学校'),visible:false},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        addmajor: function () {
             $("#c-pid").data("params", function (obj) {
                return {custom: {pid:0}};
            });
            Controller.api.bindevent();
        },
        editmajor: function () {
            $("#c-pid").data("params", function (obj) {
                return {custom: {pid:0}};
            });
            Controller.api.bindevent();
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                type:function(value, row, index){
                    if (value==1) {
                        return '<font color="#000000">专科</font>';
                    }else if(value==2){
                        return '<font color="green">本科</font>';
                    }else{
                        return '<font color="red"></font>';
                    }
                },
            }
        }
    };
    return Controller;
});