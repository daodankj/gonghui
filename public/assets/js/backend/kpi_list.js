define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'kpi_list/index' + location.search,
                    //add_url: 'kpi_list/add',
                    edit_url: 'kpi_list/edit',
                    del_url: 'kpi_list/del',
                    //multi_url: 'kpi_list/multi',
                    table: 'kpi_list',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'kpi_list.check_status',
                showExport: true,
                sortOrder : 'asc',
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'),sortable: true,operate:false},
                        {field: 'check_status', title: __('状态'),searchList: {"0":'待确认',"1":'已确认',"2":'已拒绝'},formatter: Controller.api.formatter.status,sortable: true},
                        {field: 'kpi.topic_id', title: __('考核主题'),visible:false,addClass: "selectpage", extend: "data-source='topic/index'"},
                        {field: 'kpi.id', title: __('Kpi_id'),visible:false,addClass: "selectpage", extend: "data-source='kpi/index'"},
                        {field: 'kpi.name', title: __('Kpi_id'),operate:false,formatter: Controller.api.formatter.intro},
                        {field: 'kpi.type', title: __('指标类型'),visible:false,searchList: {"1":'阵地建设6000分',"2":'队伍管理2000分',"3":'活动开展2000分','4':'工会组织有活力（激励分）'}},
                        {field: 'company_id', title: __('企业'),visible:false,addClass: "selectpage", extend: "data-source='company/index'"},
                        {field: 'company_name', title: __('Company_id'),operate:false},
                        {field: 'user_name', title: __('User_id'),operate:false},
                        {field: 'intro', title: __('Intro'),operate:false,formatter: Controller.api.formatter.intro},
                        {field: 'images', title: __('Images'),operate:false, events: Table.api.events.image, formatter: Controller.api.formatter.thumb},
                        
                        {field: 'add_time', title: __('Add_time'), operate:'RANGE', addclass:'datetimerange'},
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                        buttons:[
                            {
                                name: 'check',
                                text: __('确认'),
                                icon: 'fa fa-caret-right',
                                classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                url: 'kpi_list/check',
                                extend:'data-area=["400px","450px"]',
                                hidden:function(value,row){
                                    if(value.check_status == 0){
                                        return false;
                                    }else return true;
                                },
                                success:function(data,ret){
                                    $(".btn-refresh").trigger('click');
                                }
                            }
                        ], formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        check: function (){
            Controller.api.bindevent();
            $(".check").click(function(){
                var id = $("#c-id").val();
                var result = $("#c-result").val();
                var result2 = $("#c-result2").val();
                var notice = $("input[name='notice']:checked").val();
                var status = $(this).attr('data-status');
                /*if (!result) {
                    layer.alert('请输入意见');
                    return false;
                }*/
                var msg = status==1?'确认通过吗？':'确认不通过吗？'
                Layer.confirm(msg, function (index) {
                    layer.close(index);
                    var loadindex = Layer.load();
                    $(".check").addClass("disabled");
                    $.post({
                        url: '',
                        dataType: 'json',
                        data:{id:id,status:status,result:result,result2:result2,notice:notice},
                        cache: false,
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                var msg = ret.hasOwnProperty("msg") && ret.msg != "" ? ret.msg : "";
                                if (ret.code === 1) {
                                    parent.Toastr.success(msg ? msg : '审核成功');
                                    //Toastr.success(msg ? msg : '审核成功');
                                    //layer.close(loadindex);
                                    //window.location.reload();
                                    //Layer.closeAll();
                                    parent.$(".btn-refresh").trigger("click");
                                    var index = parent.Layer.getFrameIndex(window.name);
                                    parent.Layer.close(index);
                                } else {
                                    Toastr.error(msg ? msg : '审核失败');
                                    layer.close(loadindex);
                                }
                                $(".check").removeClass("disabled");
                            } else {
                                Toastr.error(__('Unknown data format'));
                                layer.close(loadindex);
                                $(".check").removeClass("disabled");
                            }
                        }, error: function () {
                            Toastr.error(__('Network error'));
                            layer.close(loadindex);
                            $(".check").removeClass("disabled");
                        }
                    });
                });       
            }) 
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                /*thumb: function (value, row, index) {
                    if(!row.images){
                        return '';
                    }else if (row.images.indexOf("jp") > -1||row.images.indexOf("png") > -1) {
                        //var style = row.storage == 'upyun' ? '!/fwfh/120x90' : '';
                        //return '<a href="' + row.images + '" target="_blank"><img src="' + row.images + style + '" alt="" style="max-height:90px;max-width:120px"></a>';
                        return Table.api.formatter.images(value, row, index);
                    } else if (row.images.indexOf("doc") > -1) {
                        return '<a class="btn-dialog" title="文件预览" href="https://view.officeapps.live.com/op/view.aspx?src=https://dwgh.zqgx.gov.cn' + row.images + '" target="_blank"><img src="https://tool.fastadmin.net/icon/docx.png" height="30px" alt=""></a>';
                    } else {
                        return '<a class="btn-dialog" title="文件预览" href="https://view.officeapps.live.com/op/view.aspx?src=https://dwgh.zqgx.gov.cn' + row.images + '" target="_blank"><img src="https://tool.fastadmin.net/icon/xlx.png" height="30px" alt=""></a>';
                    }
                },*/
                thumb: function (value, row, index) {
                    if(!value){
                        return '';
                    }else if (value.indexOf("jp") > -1||value.indexOf("png") > -1) {
                        //var style = row.storage == 'upyun' ? '!/fwfh/120x90' : '';
                        //return '<a href="' + row.image + '" target="_blank"><img src="' + row.image + style + '" alt="" style="max-height:90px;max-width:120px"></a>';
                        return Table.api.formatter.images(value, row, index);
                    }else{
                        value = value === null ? '' : value.toString();
                        var arr = value.split(',');
                        var html = [];
                        $.each(arr, function (i, value) {
                            var info = '';
                            if (value.indexOf("xls") > -1) {
                                info = '<a class="btn-dialog" title="文件预览" href="https://view.officeapps.live.com/op/view.aspx?src=' + Fast.api.cdnurl(value,true) + '"><img src="https://tool.fastadmin.net/icon/xlx.png" height="30px" alt=""></a>';
                            } else if(value.indexOf("doc") > -1) {
                                info = '<a class="btn-dialog" title="文件预览" href="https://view.officeapps.live.com/op/view.aspx?src=' + Fast.api.cdnurl(value,true) + '"><img src="https://tool.fastadmin.net/icon/docx.png" height="30px" alt=""></a>';
                            }else if(value.indexOf("pdf") > -1){
                            	info = '<a class="btn-dialog" title="文件预览" href="' + Fast.api.cdnurl(value,true) + '"><img src="https://tool.fastadmin.net/icon/pdf.png" height="30px" alt=""></a>';
                            }else{
                            	info = '<a class="btn-dialog" title="文件预览" href="' + Fast.api.cdnurl(value,true) + '"><img src="https://tool.fastadmin.net/icon/file.png" height="30px" alt=""></a>';
                            }
                            html.push(info);
                        });
                        return html.join(' ');
                    }
                    
                },
                intro: function (value, row, index) {
                    var intro = value.substring(0,15);
                    return '<span data-toggle="tooltip" title="'+value+'">'+intro+'</span>';
                },
                status:function(value, row, index){
                    if (value==0) {
                        return '<font color="#FF9800">待确认</font>';
                    }else if(value==1){
                        return '<font color="green">已确认</font>';
                    }else{
                        return '<font color="red">已拒绝</font>';
                    }
                },
            }
        }
    };
    return Controller;
});