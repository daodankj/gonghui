define(['jquery', 'bootstrap', 'backend', 'table', 'form'], function ($, undefined, Backend, Table, Form) {

    var Controller = {
        index: function () {
            // 初始化表格参数配置
            Table.api.init({
                extend: {
                    index_url: 'skills_competition/index' + location.search,
                    //add_url: 'skills_competition/add',
                    //edit_url: 'skills_competition/edit',
                    //del_url: 'skills_competition/del',
                    //multi_url: 'skills_competition/multi',
                    table: 'skills_competition',
                }
            });

            var table = $("#table");

            // 初始化表格
            table.bootstrapTable({
                url: $.fn.bootstrapTable.defaults.extend.index_url,
                pk: 'id',
                sortName: 'status',
                sortOrder : 'asc',
                showExport: false,
                columns: [
                    [
                        {checkbox: true},
                        {field: 'id', title: __('Id'),sortable: true},
                        {field: 'status', title: __('status'),sortable: true,searchList: {"0":'待审批',"1":'审批通过',"2":'审批拒绝'},formatter: Controller.api.formatter.status},
                        {field: 'company_name', title: __('所属企业'),operate:false},
                        {field: 'company_id', title: __('所属企业'),visible:false,addClass: "selectpage", extend: "data-source='company/index'"},
                        /*{field: 'compet_time', title: __('Compet_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        {field: 'compet_item', title: __('Compet_item')},*/
                        {field: 'users', title: __('联系人')},
                        {field: 'phone', title: __('联系电话')},
                        {field: 'images', title: __('Images'), events: Table.api.events.image, formatter: Table.api.formatter.images},
                        {field: 'intro', title: __('Intro'),formatter: Controller.api.formatter.intro},
                        {field: 'add_time', title: __('上报时间'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},
                        
                        /*{field: 'result', title: __('Result')},
                        {field: 'check_time', title: __('Check_time'), operate:'RANGE', addclass:'datetimerange', formatter: Table.api.formatter.datetime},*/
                        {field: 'operate', title: __('Operate'), table: table, events: Table.api.events.operate,
                        buttons:[
                            {
                                name: 'accept',
                                text: __('受理'),
                                icon: 'fa fa-hand-paper-o',
                                classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                url: 'skills_competition/accept',
                                extend:'data-area=["400px","450px"]',
                                hidden:function(value,row){
                                    if(value.status == 0){
                                        return false;
                                    }else return true;
                                },
                                success:function(data,ret){
                                    $(".btn-refresh").trigger('click');
                                }
                            },
                            {
                                name: 'check',
                                text: __('审批'),
                                icon: 'fa fa-caret-right',
                                classname: 'btn btn-xs btn-info btn-magic btn-dialog',
                                url: 'skills_competition/check',
                                extend:'data-area=["400px","450px"]',
                                hidden:function(value,row){
                                    if(value.status == 3){
                                        return false;
                                    }else return true;
                                },
                                success:function(data,ret){
                                    $(".btn-refresh").trigger('click');
                                }
                            }

                        ], formatter: Table.api.formatter.operate}
                    ]
                ]
            });

            // 为表格绑定事件
            Table.api.bindevent(table);
        },
        add: function () {
            Controller.api.bindevent();
        },
        edit: function () {
            Controller.api.bindevent();
        },
        accept: function (){
            Controller.api.bindevent();
        },
        check: function (){
            Controller.api.bindevent();
            $(".check").click(function(){
                var id = $("#c-id").val();
                var result = $("#c-result").val();
                var result2 = $("#c-result2").val();
                var status = $(this).attr('data-status');
                /*if (!result) {
                    layer.alert('请输入审核意见');
                    return false;
                }*/
                var msg = status==1?'确认审核通过吗？':'确认审核不通过吗？'
                Layer.confirm(msg, function (index) {
                    layer.close(index);
                    var loadindex = Layer.load();
                    $.post({
                        url: '',
                        dataType: 'json',
                        data:{id:id,status:status,result:result,result2:result2},
                        cache: false,
                        success: function (ret) {
                            if (ret.hasOwnProperty("code")) {
                                var msg = ret.hasOwnProperty("msg") && ret.msg != "" ? ret.msg : "";
                                if (ret.code === 1) {
                                    parent.Toastr.success(msg ? msg : '审核成功');
                                    //Toastr.success(msg ? msg : '审核成功');
                                    //layer.close(loadindex);
                                    //window.location.reload();
                                    //Layer.closeAll();
                                    parent.$(".btn-refresh").trigger("click");
                                    var index = parent.Layer.getFrameIndex(window.name);
                                    parent.Layer.close(index);
                                } else {
                                    Toastr.error(msg ? msg : '审核失败');
                                    layer.close(loadindex);
                                }
                            } else {
                                Toastr.error(__('Unknown data format'));
                                layer.close(loadindex);
                            }
                        }, error: function () {
                            Toastr.error(__('Network error'));
                            layer.close(loadindex);
                        }
                    });
                });       
            }) 
        },
        api: {
            bindevent: function () {
                Form.api.bindevent($("form[role=form]"));
            },
            formatter: {
                status:function(value, row, index){
                    if (value==0) {
                        return '<font color="red">待受理</font>';
                    }else if(value==1){
                        return '<font color="green">审批通过</font>';
                    }else if(value==3){
                        return '<font color="#FF9800">已受理,待审核</font>';
                    }else{
                        return '<font color="red">审批拒绝</font>';
                    }
                },
                intro: function (value, row, index) {
                    if (!value) {
                        return '-';
                    }
                    var intro = value.substring(0,15);
                    return '<span data-toggle="tooltip" title="'+value+'">'+intro+'</span>';
                }
            }
        }
    };
    return Controller;
});