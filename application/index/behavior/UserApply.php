<?php

namespace app\index\behavior;
use think\Db;
use addons\faqueue\library\QueueApi;

class UserApply
{
    /**
     *type标记审核对象(工会成立，改选等), obj_id对应id data对应申请对象信息
    */
    public function run(&$info)
    {
        /*switch ($info['type']) {
            case 1:
                $this->union_apply($info);
                break;
            case 2:
                $this->unionChange_apply($info);
                break;
            case 3:
                $this->unionUpdate_apply($info);
                break;
            case 4:
                $this->unionActive_apply($info);
                break;
            case 5:
                $this->weiquan_apply($info);
                break;
            case 6:
                $this->kunnan_apply($info);
                break;
            case 7:
                $this->arrears_help($info);
                break;
            case 8:
                $this->employees_assistance($info);
                break;
            case 9:
                $this->model_craftsman($info);
                break;
            case 10:
                $this->skills_competition($info);
                break;
            case 11:
                $this->skills_education($info);
                break;
            default:
                # code...
                break;
        }*/
        $msg_n = $this->getMsgInfo($info);
        if (!$msg_n) {
            return false;
        }
        //获取企业名称
        $cinfo = db('company')->where(['id'=>$info['data']['company_id']])->field('name,short_name')->find();
        $cname = $cinfo['short_name']??$cinfo['name'];
        $uname = db('user')->where(['id'=>$info['data']['user_id']])->value('username');
        $msg = '尊敬的'.$cname.'，'.$uname.'于'.date('Y-m-d',time()).'日'.$msg_n;
        $notice = [
            'user_id' => $info['data']['user_id'],
            'type'    => $info['type'],
            'obj_id'  => $info['obj_id'],
            'notice'  => $msg,
            'add_time' => time()
        ];
        db('user_notice')->insert($notice);

        //新增短信通知对应企业网格员
        $adminList = db('admin')->field('phone')->where('find_in_set('.$info['data']['company_id'].',company_ids)')->select();
        //短信通知
        $msg2 = '网格员您好，'.$cname.'，于'.date('Y-m-d',time()).'日'.$msg_n.'，需要您审核确认';
        foreach ($adminList as $key => $val) {
            if ($val['phone']) {
                QueueApi::smsNotice($val['phone'],$msg2);
            }
        }
    }

    private function getMsgInfo($info){
        $actionName = [
            '1' => '申请了工会成立',
            '2' => '申请了工会换届',
            '3' => '申请了工会改选',
            '4' => '上报了文体活动',
            '5' => '申请了劳资纠纷调解',
            '6' => '申请了困难职工扶持',
            '7' => '申请了欠薪求助',
            '8' => '申请了职工互助报障',
            '9' => '申请了劳模和工匠创新工作室',
            '10' => '上报了技能竞赛',
            '11' => '申请了技能和学历培训',
            '12' => '申请了活动场地优惠',
            '13' => '申请了学校学费优惠',
            '14' => '申请了圆梦助学',
        ];
        return isset($actionName[$info['type']])?$actionName[$info['type']]:'';
    }

    //1工会成立申请
    private function union_apply($info){
        
        $notice = [
            'user_id' => $info['data']['user_id'],
            'type'    => $info['type'],
            'obj_id'  => $info['obj_id'],
            'notice'  => '你有一个工会成立申请已提交审核',
            'add_time' => time()
        ];
        db('user_notice')->insert($notice);
    }

    //2工会换届申请
    private function unionChange_apply($info){
        $notice = [
            'user_id' => $info['data']['user_id'],
            'type'    => $info['type'],
            'obj_id'  => $info['obj_id'],
            'notice'  => '你有一个工会换届申请已提交审核',
            'add_time' => time()
        ];
        db('user_notice')->insert($notice);
    }

    //3工会改选申请
    private function unionUpdate_apply($info){
        
        $notice = [
            'user_id' => $info['data']['user_id'],
            'type'    => $info['type'],
            'obj_id'  => $info['obj_id'],
            'notice'  => '你有一个工会改选申请已提交审核',
            'add_time' => time()
        ];
        db('user_notice')->insert($notice);
    }

    //4文体活动上报
    private function unionActive_apply($info){
        
        $notice = [
            'user_id' => $info['data']['user_id'],
            'type'    => $info['type'],
            'obj_id'  => $info['obj_id'],
            'notice'  => '你有一个文体活动上报已提交审核',
            'add_time' => time()
        ];
        db('user_notice')->insert($notice);
    }

    //5维权劳资纠纷调解申请
    private function weiquan_apply($info){
        $notice = [
            'user_id' => $info['data']['user_id'],
            'type'    => $info['type'],
            'obj_id'  => $info['obj_id'],
            'notice'  => '你有一个劳资纠纷调解申请已提交审核',
            'add_time' => time()
        ];
        db('user_notice')->insert($notice);
    }

    //6困难职工扶持申请
    private function kunnan_apply($info){
        $notice = [
            'user_id' => $info['data']['user_id'],
            'type'    => $info['type'],
            'obj_id'  => $info['obj_id'],
            'notice'  => '你有一个困难职工扶持申请已提交审核',
            'add_time' => time()
        ];
        db('user_notice')->insert($notice);
        
    }

    //7欠薪求助申请
    private function arrears_help($info){
        $notice = [
            'user_id' => $info['data']['user_id'],
            'type'    => $info['type'],
            'obj_id'  => $info['obj_id'],
            'notice'  => '你有一个欠薪求助申请已提交审核',
            'add_time' => time()
        ];
        db('user_notice')->insert($notice);
    }

    //8职工互助报障申请
    private function employees_assistance($info){
        $notice = [
            'user_id' => $info['data']['user_id'],
            'type'    => $info['type'],
            'obj_id'  => $info['obj_id'],
            'notice'  => '你有一个职工互助报障申请已提交审核',
            'add_time' => time()
        ];
        db('user_notice')->insert($notice);
        
    }

    //9劳模和工匠创新申请
    private function model_craftsman($info){
        
        $notice = [
            'user_id' => $info['data']['user_id'],
            'type'    => $info['type'],
            'obj_id'  => $info['obj_id'],
            'notice'  => '你有一个劳模和工匠创新申请已提交审核',
            'add_time' => time()
        ];
        db('user_notice')->insert($notice);
    }

    //10技能竞赛上报
    private function skills_competition($info){
       $notice = [
            'user_id' => $info['data']['user_id'],
            'type'    => $info['type'],
            'obj_id'  => $info['obj_id'],
            'notice'  => '你有一个技能竞赛上报已提交审核',
            'add_time' => time()
        ];
        db('user_notice')->insert($notice);
        
    }

    //11技能和学历培训
    private function skills_education($info){
       $notice = [
            'user_id' => $info['data']['user_id'],
            'type'    => $info['type'],
            'obj_id'  => $info['obj_id'],
            'notice'  => '你有一个技能和学历培训申请已提交审核',
            'add_time' => time()
        ];
        db('user_notice')->insert($notice);
        
    }
}
