<?php

namespace app\index\behavior;
use think\Db;
use addons\faqueue\library\QueueApi;

class PartyApply
{
    /**
     *type标记审核对象(党成立，改选等), obj_id对应id data对应申请对象信息
    */
    public function run(&$info)
    {
        $msg_n = $this->getMsgInfo($info);
        if (!$msg_n) {
            return false;
        }
        //获取企业名称
        $cinfo = db('company')->where(['id'=>$info['data']['company_id']])->field('name,short_name')->find();
        $cname = $cinfo['short_name']??$cinfo['name'];
        $uname = db('user')->where(['id'=>$info['data']['user_id']])->value('username');
        $msg = '尊敬的'.$cname.'，'.$uname.'于'.date('Y-m-d',time()).'日'.$msg_n;
        $notice = [
            'user_id' => $info['data']['user_id'],
            'type'    => $info['type'],
            'obj_id'  => $info['obj_id'],
            'notice'  => $msg,
            'add_time' => time()
        ];
        db('user_notice')->insert($notice);

        //新增短信通知对应企业网格员
        $adminList = db('admin')->field('phone')->where('find_in_set('.$info['data']['company_id'].',company_ids)')->select();
        //短信通知
        $msg2 = '网格员您好，'.$cname.'，于'.date('Y-m-d',time()).'日'.$msg_n.'，需要您审核确认';
        foreach ($adminList as $key => $val) {
            if ($val['phone']) {
                QueueApi::smsNotice($val['phone'],$msg2);
            }
        }
    }

    private function getMsgInfo($info){
        $actionName = [
            '61' => '提交了党组织成立申请',
            '62' => '提交了党组织换届申请',
            '63' => '提交了党组织改选申请',
            '64' => '提交了入党申请',
            '65' => '提交了党员组织关系转出申请',
            '66' => '上报了学习活动',
            '67' => '申报了党员创新工作室',
        ];
        return isset($actionName[$info['type']])?$actionName[$info['type']]:'';
    }
}
