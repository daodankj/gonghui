<?php

namespace app\index\controller;

use app\common\controller\Frontend;
use think\Lang;
use think\Db;
/**
 * Ajax异步请求接口
 * @internal
 */
class Ajax extends Frontend
{

    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];
    protected $layout = '';

    /**
     * 加载语言包
     */
    public function lang()
    {
        header('Content-Type: application/javascript');
        $callback = $this->request->get('callback');
        $controllername = input("controllername");
        $this->loadlang($controllername);
        //强制输出JSON Object
        $result = jsonp(Lang::get(), 200, [], ['json_encode_param' => JSON_FORCE_OBJECT | JSON_UNESCAPED_UNICODE]);
        return $result;
    }
    
    /**
     * 上传文件
     */
    public function upload()
    {
        return action('api/common/upload');
    }

    /**
     * 读取省市区数据,联动列表
     */
    public function area()
    {
        $params = $this->request->get("row/a");
        if (!empty($params)) {
            $province = isset($params['province']) ? $params['province'] : '';
            $city = isset($params['city']) ? $params['city'] : null;
        } else {
            $province = $this->request->get('province');
            $city = $this->request->get('city');
        }
        $where = ['pid' => 0, 'level' => 1];
        $provincelist = null;
        if ($province !== '') {
            if ($province) {
                $where['pid'] = $province;
                $where['level'] = 2;
            }
            if ($city !== '') {
                if ($city) {
                    $where['pid'] = $city;
                    $where['level'] = 3;
                }
                $provincelist = Db::name('area')->where($where)->field('id as value,name')->select();
            }
        }
        $this->success('', null, $provincelist);
    }
    /**
     * 读取省市区数据,联动列表
     */
    public function disarea()
    {
        $params = $this->request->get("row/a");
        if (!empty($params)) {
            $province = isset($params['dis_province']) ? $params['dis_province'] : '';
            $city = isset($params['dis_city']) ? $params['dis_city'] : null;
        } else {
            $province = $this->request->get('dis_province');
            $city = $this->request->get('dis_city');
        }
        $where = ['pid' => 0, 'level' => 1];
        $provincelist = null;
        if ($province !== '') {
            if ($province) {
                $where['pid'] = $province;
                $where['level'] = 2;
            }
            if ($city !== '') {
                if ($city) {
                    $where['pid'] = $city;
                    $where['level'] = 3;
                }
                $provincelist = Db::name('area')->where($where)->field('id as value,name')->select();
            }
        }
        $this->success('', null, $provincelist);
    }
    //获取车牌前缀
    public function chepai(){
        $list = [
            ['value'=>'粤','name'=>'粤'],
            ['value'=>'湘','name'=>'湘'],
            ['value'=>'桂','name'=>'桂'],
            ['value'=>'京','name'=>'京'],
            ['value'=>'津','name'=>'津'],
            ['value'=>'渝','name'=>'渝'],
            ['value'=>'沪','name'=>'沪'],
            ['value'=>'冀','name'=>'冀'],
            ['value'=>'晋','name'=>'晋'],
            ['value'=>'辽','name'=>'辽'],
            ['value'=>'晋','name'=>'晋'],
            ['value'=>'吉','name'=>'吉'],
            ['value'=>'黑','name'=>'黑'],
            ['value'=>'苏','name'=>'苏'],
            ['value'=>'浙','name'=>'浙'],
            ['value'=>'皖','name'=>'皖'],
            ['value'=>'闽','name'=>'闽'],
            ['value'=>'赣','name'=>'赣'],
            ['value'=>'鲁','name'=>'鲁'],
            ['value'=>'豫','name'=>'豫'],
            ['value'=>'鄂','name'=>'鄂'],
            ['value'=>'琼','name'=>'琼'],
            ['value'=>'川','name'=>'川'],
            ['value'=>'贵','name'=>'贵'],
            ['value'=>'云','name'=>'云'],
            ['value'=>'陕','name'=>'陕'],
            ['value'=>'甘','name'=>'甘'],
            ['value'=>'青','name'=>'青'],
            ['value'=>'蒙','name'=>'蒙'],
            ['value'=>'宁','name'=>'宁'],
            ['value'=>'新','name'=>'新'],
            ['value'=>'藏','name'=>'藏'],
            ['value'=>'使','name'=>'使'],
            ['value'=>'领','name'=>'领'],
            ['value'=>'警','name'=>'警'],
            ['value'=>'学','name'=>'学'],
            ['value'=>'港','name'=>'港'],
            ['value'=>'澳','name'=>'澳']
        ];
        $this->success('', null, $list);
    }
}
