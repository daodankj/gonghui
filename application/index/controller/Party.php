<?php

namespace app\index\controller;

use app\common\controller\Frontend;
use think\Db;
use think\Session;
use fast\Random;
use think\Hook;
use think\Validate;
use app\common\model\User;

class Party extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = 'fk';
    protected $_user = null;


    public function _initialize()
    {
        parent::_initialize();
        $this->checkLogin();
    }

    protected function checkLogin(){
        $userId = session('userId');
        if (!$userId) {//未登入的则自动登入
            //获取小程序用户信息
            $idCardNumber = $this->request->get('idCardNumber');
            $uname = $this->request->get('uname');
            $mobile = $this->request->get('tel');
            if (!$idCardNumber||!$uname||!$mobile) {//未获取到认证信息提示错误
                echo '请从智慧大旺小程序进入';exit;
            }
            //判断是否已经注册过会员，没则注册
            $this->_user = User::get(['idcard' => $idCardNumber]);
            if (!$this->_user) {//不存在则注册
                $ip = request()->ip();
                $time = time();

                $data = [
                    'username' => $uname,
                    'password' => $mobile,
                    'idcard'    => $idCardNumber,
                    'mobile'   => $mobile,
                    'level'    => 1,
                    'score'    => 0,
                    'avatar'   => '',
                ];
                $params = array_merge($data, [
                    'nickname'  => $uname,
                    'salt'      => Random::alnum(),
                    'jointime'  => $time,
                    'joinip'    => $ip,
                    'logintime' => $time,
                    'loginip'   => $ip,
                    'prevtime'  => $time,
                    'status'    => 'normal'
                ]);
                $params['password'] = $this->getEncryptPassword($data['password'], $params['salt']);
                Db::startTrans();
                try {
                    $user = User::create($params, true);
                    //设置session
                    $userId = $user->id;
                    session('userId',$user->id);
                    Db::commit();
                } catch (Exception $e) {
                    Db::rollback();
                    echo $e->getMessage();exit;
                }
            }else{
                session('userId',$this->_user->id);
            }
        }
        if (!$this->_user) {
            $this->_user = User::get($userId);
        }
        //判断是否绑定企业
        $actionname = strtolower($this->request->action());
        if (!$this->_user['company_id']&&$actionname!='bindcompany') {
            $this->redirect('/index/bindcompany');
        }
    }
    /**
     * 获取密码加密后的字符串
     * @param string $password 密码
     * @param string $salt     密码盐
     * @return string
     */
    public function getEncryptPassword($password, $salt = '')
    {
        return md5(md5($password) . $salt);
    }

    //首页菜单页面
    public function index()
    {
        //获取预警数量
        $warning_num = db('party_kpi_warning')->where(['company_id'=>$this->_user->company_id,'read_time'=>0])->count();
        $this->view->assign('warning_num', $warning_num);
        $this->view->assign('is_party_admin', $this->_user->is_party_admin);
        $this->view->assign('title', '智慧党建');
        return $this->view->fetch();
    }

    //活动发布
    public function active($kpi_id=''){
        $kpi_id = intval($kpi_id);
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                'name'   => 'require',
                'type'   => 'require',
                'content'  => 'require|length:1,2000',
                /*'address'  => 'require',
                'start_time'=> 'require|date',
                'end_time'=> 'require|date',*/
                '__token__' => 'require|token'
            ];

            $msg = [
                'name'   => '活动主题必须',
                'type'   => '活动类型必须',
                'content.require'  => '活动简介必须',
                'content.length'  => '活动内容必须2000字以内',
                /*'address'  => '地址必须',
                'start_time.require'  => '开始时间必须',
                'start_time.date'  => '开始时间格式错误',
                'end_time.require'  => '截至时间必须',
                'end_time.date'  => '截至时间格式错误'*/
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['add_time'] = date('Y-m-d H:i:s');
            $res = db('party_active')->insert($data);
            if ($res) {
                if ($kpi_id) {//任务指标来的
                    $data['check_status'] = 1;
                    $data['kpi_id'] = $kpi_id;
                    $data['otherPage'] = 1;
                    Hook::listen("party_kpilist_check_successed", $data);
                }
                $this->success('发布成功','index');
            }else{
                $this->error('发布失败');
            }

        }

        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('title', '党日活动上报');
        return $this->view->fetch();
    }

    //会议发布
    public function meeting($kpi_id=''){
        $kpi_id = intval($kpi_id);
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                'name'   => 'require',
                'type'   => 'require',
                'title'   => 'require',
                'style'   => 'require',
                'intro'  => 'require|length:1,2000',
               /* 'notice'  => 'require',
                'start_time'=> 'require|date',
                'end_time'=> 'require|date',*/
                '__token__' => 'require|token'
            ];

            $msg = [
                'name'   => '会议名称必须',
                'type'   => '会议类型必须',
                'title'   => '会议主题必须',
                'style'   => '会议方式必须',
                'intro.require'  => '议题必须',
                'intro.length'  => '议题必须2000字以内',
               /* 'notice'  => '通知人必须',
                'start_time.require'  => '开始时间必须',
                'start_time.date'  => '开始时间格式错误',
                'end_time.require'  => '结束时间必须',
                'end_time.date'  => '结束时间格式错误'*/
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['add_time'] = date('Y-m-d H:i:s');
            $res = db('party_meeting')->insert($data);
            if ($res) {
                if ($kpi_id) {//任务指标来的
                    $data['check_status'] = 1;
                    $data['kpi_id'] = $kpi_id;
                    $data['otherPage'] = 1;
                    Hook::listen("party_kpilist_check_successed", $data);
                }
                $this->success('发布成功','index');
            }else{
                $this->error('发布失败');
            }

        }

        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('title', '三会一课上报');
        return $this->view->fetch();
    }


    //换届录入
    public function change(){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                'change_time'=> 'require|date',
                'term_office'   => 'require',
                'type'   => 'require',
                'intro'  => 'require|length:1,2000',
                'yd_user_num'  => 'require|number',
                'sj_user_num'  => 'require|number',
                '__token__' => 'require|token'
            ];

            $msg = [
                'change_time.require'  => '换届时间必须',
                'change_time.date'  => '换届时间格式错误',
                'term_office'   => '任期必须',
                'type'   => '选举方式必须',
                'intro.require'  => '会议记录必须',
                'intro.length'  => '会议记录必须2000字以内',
                'yd_user_num.require'  => '应到会场人数必须',
                'yd_user_num.number'  => '应到会场人数格式错误',
                'sj_user_num.require'  => '实际到场人数必须',
                'sj_user_num.number'  => '时间到场人数格式错误',

            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['add_time'] = date('Y-m-d H:i:s');
            $res = db('party_change')->insert($data);
            if ($res) {
                $this->success('发布成功','index');
            }else{
                $this->error('发布失败');
            }

        }

        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('title', '换届录入');
        return $this->view->fetch();
    }

    //党员申请
    public function apply(){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                'name'=> 'require',
                'idcard'   => 'require',
                'phone'   => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                'name'  => '姓名必须',
                'idcard'  => '身份证必须',
                'phone'   => '电话必须'

            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['add_time'] = date('Y-m-d H:i:s');
            $res = db('party_member')->insert($data);
            if ($res) {
                //监听申请事件
                $obj_id = db('party_member')->getLastInsID();
                $infodata = ['type'=>64,'obj_id'=>$obj_id,'data'=>$data];
                Hook::listen("party_apply", $infodata);
                $this->success('申请成功','index');
            }else{
                $this->error('申请失败');
            }

        }

        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('title', '入党申请');
        return $this->view->fetch();
    }

    //党员组织关系转出申请
    public function member_change_apply(){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                'name'=> 'require',
                'idcard'   => 'require',
                'phone'   => 'require',
                'age'     => 'require',
                'mingz'   => 'require',
                'now_party_name'     => 'require',
                'to_party_name'   => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                'name'  => '姓名必须',
                'idcard'  => '身份证必须',
                'phone'   => '电话必须',
                'age'     => '年龄必须',
                'mingz'   => '民族必须',
                'now_party_name'     => '现在的党组织名称必须',
                'to_party_name'   => '将要去的党组织名称必须',

            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['add_time'] = date('Y-m-d H:i:s');
            $res = db('party_member_change')->insert($data);
            if ($res) {
                //监听申请事件
                $obj_id = db('party_member')->getLastInsID();
                $infodata = ['type'=>65,'obj_id'=>$obj_id,'data'=>$data];
                Hook::listen("party_apply", $infodata);
                $this->success('申请成功','index');
            }else{
                $this->error('申请失败');
            }

        }
        $ystart = substr($this->_user->idcard,6,4);
        $yend = date('Y');

        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('username', $this->_user->username);
        $this->view->assign('phone', $this->_user->mobile);
        $this->view->assign('age', $yend-$ystart);
        $this->view->assign('idcard', $this->_user->idcard);
        $this->view->assign('title', '党员组织关系转出申请');
        return $this->view->fetch();
    }

    //学习计划上报
    public function study($kpi_id=''){
        $kpi_id = intval($kpi_id);
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                'images'   => 'require',
                'intro'  => 'require|length:1,150',
                '__token__' => 'require|token'
            ];

            $msg = [
                'images'   => '牌照必须',
                'intro.require'  => '学习计划说明必须',
                'intro.length'  => '学习计划说明必须150字以内'

            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['add_time'] = date('Y-m-d H:i:s');
            $res = db('party_study')->insert($data);
            if ($res) {
                //监听申请事件
                /*$obj_id = db('party_study')->getLastInsID();
                $infodata = ['type'=>7,'obj_id'=>$obj_id,'data'=>$data];
                Hook::listen("party_apply", $infodata);*/
                if ($kpi_id) {//任务指标来的
                    $data['check_status'] = 1;
                    $data['kpi_id'] = $kpi_id;
                    $data['otherPage'] = 1;
                    Hook::listen("party_kpilist_check_successed", $data);
                }
                $this->success('上报成功','index');
            }else{
                $this->error('上报失败');
            }

        }

        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('title', '学习计划上报');
        return $this->view->fetch();
    }


    //新增功能

    //党组织建设服务
    public function admin()
    {
        if (!$this->_user->is_admin) {//is_party_admin
            $this->error('您不是管理员，无法进入本页面');
        }
        //获取预警数量
        $warning_num = db('party_kpi_warning')->where(['company_id'=>$this->_user->company_id,'read_time'=>0])->count();
        $this->view->assign('warning_num', $warning_num);
        $this->view->assign('title', '党组织建设服务');
        return $this->view->fetch();
    }
    public function partyindex(){
        $this->view->assign('title', '党组织建设服务');
        return $this->view->fetch();
    }
    //1党组织成立申请
    public function partyapply(){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                'legal_person'   => 'require',
                'person_num'  => 'require|number',
                'woman_num'  => 'require|number',
                'business_scope'  => 'require',
                'nature'  => 'require',
                'user_num'  => 'require|number',
                'address'  => 'require',
                'phone'  => 'require',
                'chairman'  => 'require',
                //'director_woman'  => 'require',
                //'finance'  => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                'legal_person'   => '法人代表必须',
                'person_num'  => '职工人数必须',
                'woman_num'  => '女职工人数必须',
                'business_scope'  => '经营范围必须',
                'nature'  => '企业性质必须',
                'user_num'  => '会员人数必须',
                'address'  => '地址必须',
                'phone'  => '联系电话必须',
                'chairman'  => '支部书记必须',
                //'director_woman'  => '女职工主任必须',
                //'finance'  => '委员必须'
            ];

            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            //判断是否已经成立，是的话不能申请
            $unionInfo = db('party')->where(['company_id'=>$this->_user->company_id])->value('id');
            if ($unionInfo) {
                $this->error('该企业已经成立党组织，无需再次申请', null, ['token' => $this->request->token()]);
                return false;
            }

            unset($data['__token__']);
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['add_time'] = date('Y-m-d H:i:s');
            $res = db('party_apply')->insert($data);
            if ($res) {
                //监听申请事件
                $obj_id = db('party_apply')->getLastInsID();
                $infodata = ['type'=>61,'obj_id'=>$obj_id,'data'=>$data];
                Hook::listen("party_apply", $infodata);
                $this->success('申请成功','index');
            }else{
                $this->error('申请失败');
            }

        }

        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('title', '企业党组织成立申请');
        return $this->view->fetch();
    }

    //2换届申请
    public function partychangeapply(){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                'chairman'   => 'require',
                //'director_woman'  => 'require',
                //'finance'  => 'require',
                'image'  => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                'chairman'   => '支部书记必须',
                //'director_woman'  => '女职工主任必须',
                //'finance'  => '委员必须',
                'image'  => '申请书必须'
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['add_time'] = date('Y-m-d H:i:s');
            $res = db('party_changeapply')->insert($data);
            if ($res) {
                //监听申请事件
                $obj_id = db('party_changeapply')->getLastInsID();
                $infodata = ['type'=>62,'obj_id'=>$obj_id,'data'=>$data];
                Hook::listen("party_apply", $infodata);
                $this->success('申请成功','index');
            }else{
                $this->error('申请失败');
            }

        }
        //获取党信息
        $unionInfo = db('party')->where(['company_id'=>$this->_user->company_id])->find();
        $this->view->assign('union',$unionInfo);
        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('title', '企业党组织换届申请');
        return $this->view->fetch();
    }

    //3改选申请
    public function partyupdateapply(){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                'chairman'   => 'require',
                //'director_woman'  => 'require',
                //'finance'  => 'require',
                'image'  => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                'chairman'   => '支部书记必须',
                //'director_woman'  => '女职工主任必须',
                //'finance'  => '委员必须',
                'image'  => '申请书必须'
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['add_time'] = date('Y-m-d H:i:s');
            $res = db('party_updateapply')->insert($data);
            if ($res) {
                //监听申请事件
                $obj_id = db('party_updateapply')->getLastInsID();
                $infodata = ['type'=>63,'obj_id'=>$obj_id,'data'=>$data];
                Hook::listen("party_apply", $infodata);
                $this->success('申请成功','index');
            }else{
                $this->error('申请失败');
            }

        }
        //获取工会信息
        $unionInfo = db('party')->where(['company_id'=>$this->_user->company_id])->find();
        $this->view->assign('union',$unionInfo);
        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('title', '企业党组织改选申请');
        return $this->view->fetch();
    }

    //任务指标列表
    public function kpilist(){
        $unionInfo = db('party')->where(['company_id'=>$this->_user->company_id])->value('id');
        if (!$unionInfo) {
            return '<div style="font-size:31px;margin-top:50px;text-align:center;">您所在的企业还未成立党组织！暂时无法访问此页面！</div>';
        }

        $companyInfo = db('company')->where(['id'=>$this->_user->company_id])->field('name,short_name,type,score')->find();
        //获取党组织积分呵等级
        $partyInfo = db('party')->where(['company_id'=>$this->_user->company_id])->field('type,score,user_type')->find();
        if ($partyInfo) {
            $companyInfo['type'] = $partyInfo['type'];
            $companyInfo['score'] = $partyInfo['score'];
        }else{
            $companyInfo['type'] = 'D';
            $companyInfo['score'] = 0;
        }
        //获取任务指标,过滤已经完成的
        $nowtime = date('Y-m-d H:i:s');
        $list = [];
        $kpiList = db('party_kpi')->where(['end_time'=>['>',$nowtime]])->field('id,name,grade,score,submit_num,user_type,submit_type')->order('grade asc,weigh desc')->select();
        foreach ($kpiList as $key => &$val) {
            if($val['user_type']!=0 && $val['user_type']!=$partyInfo['user_type']){
                continue;
            }
            if($val['submit_type']==7||$val['submit_type']==8||$val['submit_type']==9){
                $submit_num = db('party_score_log')->where(['company_id'=>$this->_user->company_id,'kpi_id'=>$val['id']])->count();
                if ($submit_num<$val['submit_num']) {//未完成
                    $val['has_num'] = $submit_num;
                    array_push($list,$val);
                }
            }else{
                //查询完成了多少次
                $num = db('party_kpi_list')->where(['company_id'=>$this->_user->company_id,'kpi_id'=>$val['id'],'check_status'=>['<',2]])->count();
                if($num<$val['submit_num']){//未完成的
                    $val['has_num'] = $num;
                    array_push($list,$val);
                }
            }

        }
        //获取指标主题名称
        $topic_info = db('party_topic')->order('id desc')->find();

        $this->view->assign('companyInfo', $companyInfo);
        $this->view->assign('kpiList', $list);
        $this->view->assign('topic_info', $topic_info);
        $this->view->assign('title', '工作量化指标');
        return $this->view->fetch();
    }
    //主题详情
    public function topic_info($id=0){
        $id = intval($id);
        $topic_info = db('party_topic')->order('id desc')->find();
        $this->view->assign('topic_info', $topic_info);
        $this->view->assign('title', '考核主题说明');
        return $this->view->fetch();
    }

    //任务指标上报
    public function kpiadd($id){
        $id = intval($id);
        $kpiInfo = db('party_kpi')->where(['id'=>$id])->find();
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                'intro'  => 'require|length:1,150',
                '__token__' => 'require|token'
            ];

            $msg = [
                'intro.require'  => '上报简介必须',
                'intro.length'  => '上报简介必须150字以内',
            ];
            if ($kpiInfo['submit_type']!=1) {
                $rule['images'] = 'require';
                $msg['images'] = '图片或文件必须';
            }
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            //判断是否已经成立，是的话不能申请
            $partyInfo = db('party')->where(['company_id'=>$this->_user->company_id])->value('id');
            if (!$partyInfo) {
                $this->error('该企业还未成立党组织，请先申请成立党组织', null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            $data['kpi_id'] = $kpiInfo['id'];
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['add_time'] = date('Y-m-d H:i:s');
            $res = db('party_kpi_list')->insert($data);
            if ($res) {
                $this->success('上报成功','kpilist');
            }else{
                $this->error('上报失败');
            }

        }

        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('kpiInfo', $kpiInfo);
        $this->view->assign('title', '工作量化指标上报');
        switch ($kpiInfo['submit_type']) {
            case 1:
                $tmp = 'kpitmp1';
                break;
            case 2:
                $tmp = 'kpitmp2';
                break;
            case 3:
                $tmp = 'kpitmp3';
                break;
            /*case 4:
                $this->redirect('/index/admin/capital/kpi_id/'.$id);
                break;
            case 5:
                $this->redirect('/index/admin/company_info/kpi_id/'.$id);
                break;
            case 6:
                $this->redirect('/index/admin/unionactive/kpi_id/'.$id);
                break;*/
            case 7:
                $this->redirect('/index/party/study/kpi_id/'.$id);
                break;
            case 8:
                $this->redirect('/index/party/active/kpi_id/'.$id);
                break;
            case 9:
                $this->redirect('/index/party/meeting/kpi_id/'.$id);
                break;

            default:
                $tmp = 'kpitmp1';
                break;
        }
        return $this->view->fetch($tmp);
    }
    //积分明细
    public function score_list(){
        $list = db('party_score_log')->where(['company_id'=>$this->_user->company_id])->order('id desc')->limit(50)->select();
        $this->view->assign('list', $list);
        $this->view->assign('title', '积分明细');
        return $this->view->fetch();
    }

    //企业积分排行
    public function score_top(){
        $list  = db('party')->order('score desc')->limit(500)->field('company_id,type,score')->select();
        foreach ($list as $key=>&$val){
            $val['short_name'] = db('company')->where(['id'=>$val['company_id']])->value('short_name');
        }
        $this->view->assign('list', $list);
        $this->view->assign('title', '党积分排行');
        return $this->view->fetch();
    }

    //预警信息列表
    public function warninglist(){
        $list = db('party_kpi_warning')->where(['company_id'=>$this->_user->company_id])->order('id desc')->limit(20)->select();

        foreach ($list as $key => &$val) {
            $val['kpi_name'] = db('party_kpi')->where(['id'=>$val['kpi_id']])->value('name');
            if ($val['warning_type']==1) {
                $val['warning_name'] =  '<font color="red">周预警</font>';
                $val['day_num'] = 7;
            }elseif($val['warning_type']==2){
                $val['warning_name'] =  '<font color="#FF9800">月预警</font>';
                $val['day_num'] = 30;
            }elseif($val['warning_type']==3){
                $val['warning_name'] =  '<font color="black">季度预警</font>';
                $val['day_num'] = 90;
            }else{
                $info = db('party_warning_type')->where(['id'=>$val['warning_type']])->field('name,day')->find();
                $val['warning_name'] = $info['name']??'';
                $val['day_num'] = $info['day'];
            }
        }
        $this->view->assign('list', $list);
        $this->view->assign('title', '预警信息');
        return $this->view->fetch();
    }
    //预警信息确认
    public function warningred(){
        $id = $this->request->post('id');
        $rs = db('party_kpi_warning')->where(['id'=>$id])->update(['read_time'=>time()]);
        if ($rs) {
            $this->success('操作成功');
        }else{
            $this->error('操作失败');
        }
    }

    //指标上报记录
    public function kpi_submit_list(){
        $list = db('party_kpi_list')->where(['company_id'=>$this->_user->company_id])->field('id,kpi_id,intro,add_time,check_status')->order('id desc')->limit(50)->select();

        foreach ($list as $key => &$val) {
            $val['kpi_name'] = db('party_kpi')->where(['id'=>$val['kpi_id']])->value('name');
        }
        $this->view->assign('list', $list);
        $this->view->assign('title', '指标上报记录');
        return $this->view->fetch();
    }

    //组织架构
    public function struct(){
        $info = db('party')->where(['company_id'=>$this->_user->company_id])->find();
        if ($info){
            if ($info['s_add_time']){
                $stime = strtotime($info['s_add_time']);
                $etime = strtotime('+3 year',$stime);
                $info['difday'] = ceil(($etime-time())/(3600*24)).'天';
                //$info['difday']= date('Y-m-d',strtotime("+3 year",strtotime($partyInfo['s_add_time'])));
            }else{
                $info['difday'] = '没上传数据，系统无法预测下一届成立时间';
            }

        }


        $this->view->assign('info', $info);
        $this->view->assign('title', '党组织架构');
        return $this->view->fetch();
    }
    //普惠职工
    public function zgyhzc(){
        $this->view->assign('title', '普惠职工');
        return $this->view->fetch();
    }

    //党史学习专栏
    public function dsxxzl(){
        $this->view->assign('title', '党史学习专栏');
        return $this->view->fetch();
    }
    //党史资料
    public function dszl(){
        $this->view->assign('title', '党史资料');
        return $this->view->fetch();
    }

    //学习活动上报
    public function study_active(){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                'name'   => 'require',
                'type'   => 'require',
                'intro'  => 'require|length:1,150',
               'images'  => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                'name'   => '活动名称必须',
                'type'   => '活动类型必须',
                'intro.require'  => '活动简介必须',
                'intro.length'  => '活动简介必须150字以内',
                'images'  => '活动图片必须'
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['add_time'] = date('Y-m-d H:i:s');
            $res = db('party_study_active')->insert($data);
            if ($res) {
                //监听申请事件
                $obj_id = db('party_study_active')->getLastInsID();
                $infodata = ['type'=>66,'obj_id'=>$obj_id,'data'=>$data];
                Hook::listen("party_apply", $infodata);
                $this->success('上报成功','index');
            }else{
                $this->error('上报失败');
            }
        }
        $model = new \app\admin\model\party\PartyStudyActive;
        $this->view->assign("typeList", $model->getTypeList());
        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('title', '学习活动上报');
        return $this->view->fetch();
    }
    //新闻稿上报
    public function news(){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                'title' => 'require',
                'content'  => 'require|length:1,150',
                '__token__' => 'require|token'
            ];

            $msg = [
                'title.require'  => '标题必须',
                'content.require'  => '上报简介必须',
                'content.length'  => '上报简介必须150字以内',
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['add_time'] = date('Y-m-d H:i:s');
            $res = db('party_news')->insert($data);
            if ($res) {
                $this->success('上报成功','dsxxzl');
            }else{
                $this->error('上报失败');
            }

        }

        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('title', '新闻稿上报');
        return $this->view->fetch();
    }
    //党员创新工作室申报
    public function model_craftsman(){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                //'studio_name'   => 'require',
                'phone'  => 'require',
                //'file'  => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                /*'studio_name'   => '工作室名称必须',
                'createdate'  => '创建日期必须',
                'company'  => '所在单位必须',
                'named_unit'  => '命名单位必须',*/
                //'file'  => '申请文档必须',
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['addtime'] = time();
            $res = db('party_model_craftsman')->strict(false)->insert($data);
            if ($res) {
                //监听申请事件
                $obj_id = db('party_model_craftsman')->getLastInsID();
                $infodata = ['type'=>67,'obj_id'=>$obj_id,'data'=>$data];
                Hook::listen("party_apply", $infodata);
                $this->success('上报成功','index');
            }else{
                $this->error('上报失败');
            }

        }
        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('title', '党员创新工作室申报');
        return $this->view->fetch();
    }

    //删除草稿缓存
    public function delCg($type=""){
        if ($type) {
            $key = $type."_".$this->_user->id;
            cache($key,null);
        }
    }


}
