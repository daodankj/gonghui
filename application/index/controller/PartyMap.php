<?php
namespace app\index\controller;

use app\common\controller\Frontend;
use think\Config;
use think\Hook;

class PartyMap extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';
    protected $model = null;
    protected $searchFields = 'id,name';


    public function _initialize() {
        $this->model = new \app\admin\model\Company;
        parent::_initialize();
    }

    public function amap(){
        // 语言检测
        $lang = strip_tags($this->request->langset());
        // 配置信息
        $site = Config::get("site");
        $config = [
            'site' => array_intersect_key($site, array_flip(['name', 'cdnurl', 'version', 'timezone', 'languages'])),
            'upload'         => null,
            'modulename'     => 'index',
            'controllername' => 'map',
            'actionname'     => 'amap',
            'jsname'         => 'index/map',
            'moduleurl'      => '',
            'language'       => $lang
        ];
        $this->assign('config', $config);
        $this->view->assign('title', '大屏地图');
        return $this->view->fetch();
    }

    public function index(){
        $data = $this->model->alias('c')->join('party p','c.id=p.company_id')->field('c.*')->where(['lat'=>['>',0]])->select();
        $this->assign('data', json_encode($data));
        //获取统计信息
        $cinfo = db('front')->where(['id'=>1])->find();
        $this->assign('cinfo', json_encode($cinfo));

        $dconfig = get_addon_config('cwmap');

        // 语言检测
        $lang = strip_tags($this->request->langset());

        $site = Config::get("site");

        // 配置信息
        $config = [
            'site'           => array_intersect_key($site, array_flip(['name', 'cdnurl', 'version', 'timezone', 'languages'])),
            'upload'         => null,
            'modulename'     => 'addons',
            'controllername' => 'index',
            'actionname'     => 'index',
            'jsname'         => 'addons/cwmap',
            'moduleurl'      => '',
            'language'       => $lang
        ];
        $config = array_merge($config, $dconfig);
        $this->assign('config', $config);

        $this->view->assign('title', '大屏地图');
        return $this->view->fetch();
    }


    public function search() {
        if (request()->isAjax()) {
            $search = request()->get('keyword');
            $searcharr = is_array($this->searchFields) ? $this->searchFields : explode(',', $this->searchFields);

            $result = $this->model->where(implode("|", $searcharr), "LIKE", "%{$search}%")->select();
            return json_encode($result);
            //$this->success('',$result);
        }
    }

}
