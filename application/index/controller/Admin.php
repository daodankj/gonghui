<?php

namespace app\index\controller;

use app\common\controller\Frontend;
use think\Db;
use think\Session;
use fast\Random;
use think\Validate;
use think\Hook;
use app\common\model\User;
use addons\faqueue\library\QueueApi;

class Admin extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = 'fk';
    protected $_user = null;


    public function _initialize()
    {
        parent::_initialize();
        $this->checkLogin();
    }

    protected function checkLogin(){
        $userId = session('userId');
        if (!$userId) {
            //return  '请先登入';
            $this->error('请先登入');
        }
        $this->_user = User::get($userId);
        if (!$this->_user) {
            //return  '该用户不存在，请重新登入';
            $this->error('该用户不存在，请重新登入');
        }
        if (!$this->_user->is_admin) {
            //return  '您不是管理员，无法进入本页面';
            $this->error('您不是管理员，无法进入本页面');
        }
        //判断是否绑定企业
        if (!$this->_user['company_name']) {
            $this->redirect('/index/index/bindcompany');
        }
    }

    //首页菜单页面
    public function index()
    {
        //获取预警数量
        $warning_num = db('kpi_warning')->where(['company_id'=>$this->_user->company_id,'read_time'=>0])->count();


        $this->view->assign('title', '企业管理');
        $this->view->assign('warning_num', $warning_num);
        return $this->view->fetch();
    }
    //工会建设服务
    public function unindex2()
    {
        $this->view->assign('title', '工会建设服务');
        return $this->view->fetch();
    }
    //1工会申请
    public function unionapply(){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                'legal_person'   => 'require',
                'person_num'  => 'require|number',
                'woman_num'  => 'require|number',
                'business_scope'  => 'require',
                'nature'  => 'require',
                'user_num'  => 'require|number',
                'address'  => 'require',
                'phone'  => 'require',
                'chairman'  => 'require',
                'director_woman'  => 'require',
                'finance'  => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                'legal_person'   => '法人代表必须',
                'person_num'  => '职工人数必须',
                'woman_num'  => '女职工人数必须',
                'business_scope'  => '经营范围必须',
                'nature'  => '企业性质必须',
                'user_num'  => '会员人数必须',
                'address'  => '地址必须',
                'phone'  => '联系电话必须',
                'chairman'  => '工会主席必须',
                'director_woman'  => '女职工主任必须',
                'finance'  => '财务委员必须'
            ];

            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            //判断是否已经成立，是的话不能申请
            $unionInfo = db('union')->where(['company_id'=>$this->_user->company_id])->value('id');
            if ($unionInfo) {
                $this->error('该企业已经成立工会，无需再次申请', null, ['token' => $this->request->token()]);
                return false;
            }

            unset($data['__token__']);
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['add_time'] = date('Y-m-d H:i:s');
            $res = db('union_apply')->insert($data);
            if ($res) {
                //监听申请事件
                $obj_id = db('union_apply')->getLastInsID();
                $infodata = ['type'=>1,'obj_id'=>$obj_id,'data'=>$data];
                Hook::listen("user_apply", $infodata);
                $this->success('申请成功','index');
            }else{
                $this->error('申请失败');
            }

        }

        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('title', '企业工会成立申请');
        return $this->view->fetch();
    }

    //2工会换届申请
    public function unionchangeapply(){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                'chairman'   => 'require',
                'director_woman'  => 'require',
                'finance'  => 'require',
                'image'  => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                'chairman'   => '工会主席必须',
                'director_woman'  => '女职工主任必须',
                'finance'  => '财务委员必须',
                'image'  => '申请书必须'
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['add_time'] = date('Y-m-d H:i:s');
            $res = db('union_changeapply')->insert($data);
            if ($res) {
                //监听申请事件
                $obj_id = db('union_changeapply')->getLastInsID();
                $infodata = ['type'=>2,'obj_id'=>$obj_id,'data'=>$data];
                Hook::listen("user_apply", $infodata);
                $this->success('申请成功','index');
            }else{
                $this->error('申请失败');
            }

        }
        //获取工会信息
        $unionInfo = db('union')->where(['company_id'=>$this->_user->company_id])->find();
        $this->view->assign('union',$unionInfo);
        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('title', '企业工会换届申请');
        return $this->view->fetch();
    }

    //3工会改选申请
    public function unionupdateapply(){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                'chairman'   => 'require',
                'director_woman'  => 'require',
                'finance'  => 'require',
                'image'  => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                'chairman'   => '工会主席必须',
                'director_woman'  => '女职工主任必须',
                'finance'  => '财务委员必须',
                'image'  => '申请书必须'
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['add_time'] = date('Y-m-d H:i:s');
            $res = db('union_updateapply')->insert($data);
            if ($res) {
                //监听申请事件
                $obj_id = db('union_updateapply')->getLastInsID();
                $infodata = ['type'=>3,'obj_id'=>$obj_id,'data'=>$data];
                Hook::listen("user_apply", $infodata);
                $this->success('申请成功','index');
            }else{
                $this->error('申请失败');
            }

        }
        //获取工会信息
        $unionInfo = db('union')->where(['company_id'=>$this->_user->company_id])->find();
        $this->view->assign('union',$unionInfo);
        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('title', '企业工会改选申请');
        return $this->view->fetch();
    }

    //4文体活动上报
    public function unionactive($kpi_id=''){
        $kpi_id = intval($kpi_id);
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                'name'   => 'require',
                'type'   => 'require',
                'intro'  => 'require|length:1,150',
                'images'  => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                'name'   => '活动名称必须',
                'type'   => '活动类型必须',
                'intro.require'  => '活动简介必须',
                'intro.length'  => '活动简介必须150字以内',
                'images'  => '活动图片必须'
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['add_time'] = date('Y-m-d H:i:s');
            //新增指标低  alter table fa_union_active add kpi_id int(5) NOT NULL DEFAULT 0 COMMENT '所属指标';
            if ($kpi_id) {//任务指标来的
                $data['kpi_id'] = $kpi_id;
            }
            $res = db('union_active')->insert($data);
            if ($res) {
                //监听申请事件
                $obj_id = db('union_active')->getLastInsID();
                $infodata = ['type'=>4,'obj_id'=>$obj_id,'data'=>$data];
                Hook::listen("user_apply", $infodata);
                if ($kpi_id) {//任务指标来的
                    //插入到指标上报记录---新增2022-1-11
                    $listdata = [
                        'kpi_id'     => $kpi_id,
                        'company_id' => $this->_user->company_id,
                        'user_id'    => $this->_user->id,
                        'intro'      => $data['intro'],
                        'images'     => $data['images'],
                        'add_time'   => $data['add_time'],
                        'check_status'=>1
                    ];
                    db('kpi_list')->insert($listdata);
                    //-----------------end--------------
                    $data['check_status'] = 1;
                    $data['kpi_id'] = $kpi_id;
                    $data['otherPage'] = 1;
                    Hook::listen("apilist_check_successed", $data);
                }
                $this->success('上报成功','index');
            }else{
                $this->error('上报失败');
            }

        }
        //判断是否有相关指标未完成，有则提示是否跳转熬指标完成页面
        $kpiInfo = ['status'=>0,'url'=>''];
        if (!$kpi_id) {    
            $nowtime = date('Y-m-d H:i:s');
            $kpi_info = db('kpi')->where(['submit_type'=>6,'end_time'=>['>',$nowtime]])->field('id,submit_num,add_time')->order('id desc')->find();
            if ($kpi_info) {//有指标判断是否完成
                //$submit_num = db('kpi_list')->where(['company_id'=>$this->_user->company_id,'kpi_id'=>$kpi_info['id'],'check_status'=>['<',2]])->count();
                //$submit_num = db('union_active')->where(['company_id'=>$this->_user->company_id,'add_time'=>['>',$kpi_info['add_time']],'status'=>['<',2]])->count();
                $submit_num = db('company_score_log')->where(['company_id'=>$this->_user->company_id,'kpi_id'=>$kpi_info['id']])->count();
                if ($submit_num<$kpi_info['submit_num']) {//未完成
                    $kpiInfo['status'] = 1;
                    //$kpiInfo['url'] = url('kpiadd',['id'=>$kpi_info['id']]);
                    $kpiInfo['url'] = url('kpilist');
                }
            }
        }
        $this->view->assign("kpiInfo", $kpiInfo);
        

        $model = new \app\admin\model\UnionActive;
        $this->view->assign("typeList", $model->getTypeList());
        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('title', '文体活动上报');
        return $this->view->fetch();
    }
    //年度资金使用情况上报记录
    public function capital_list(){
        $list = db('capital')->where(['company_id'=>$this->_user->company_id])->order('id desc')->limit(50)->select();
        foreach ($list as $key => &$val) {
            $val['username'] = db('user')->where(['id'=>$val['user_id']])->value('username');
        }
        $this->view->assign('list', $list);
        $this->view->assign('title', '资金上报记录');
        return $this->view->fetch();
    }
    //年度资金使用情况上报
    public function capital($kpi_id=''){
        $kpi_id = intval($kpi_id);
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                'lastyear_balance'   => 'require',
                /*'thisyear_income'   => 'require',
                'staff_activities_money'   => 'require',
                'business_money'   => 'require',
                'safeguard_rights_money'   => 'require',
                'administration_money'   => 'require',
                'capital_money'   => 'require',
                'subsidy_money'   => 'require',
                'cause_money'   => 'require',
                'other_money'   => 'require',*/
                '__token__' => 'require|token'
            ];

            $msg = [
                'lastyear_balance'   => '去年经费结余必须',
                /*'thisyear_income'   => '今年经费收入必须',
                'staff_activities_money'   => '员工活动支出必须',
                'business_money'   => '业务支出必须',
                'safeguard_rights_money'   => '维权支出必须',
                'administration_money'   => '行政支出必须',
                'capital_money'   => '资本性支出必须',
                'subsidy_money'   => '补助下级支出必须',
                'cause_money'   => '事业支出必须',
                'other_money'   => '其他支出必须',*/
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            //收入合计
            $data['thisyear_income'] = $data['huifei_income']+$data['bokuan_income']+$data['huibo_help_income']+$data['zhuanxiang_help_income']+$data['other_help_income']+$data['administration_help_income']+$data['other_income'];//本期收入
            $data['total_money'] = $data['lastyear_balance']+$data['thisyear_income'];//本期总金额


            //支出合计
            $data['staff_activities_money'] = $data['staff_activities_education_money']+$data['staff_activities_wtactive_money']+$data['staff_activities_xcactive_money']+$data['staff_activities_other_money'];//职工活动支出

            $data['safeguard_rights_money'] = $data['safeguard_rights_xietiao_money']+$data['safeguard_rights_baohu_money']+$data['safeguard_rights_falv_money']+$data['safeguard_rights_kunnan_money']+$data['safeguard_rights_wenruan_money']+$data['safeguard_rights_other_money'];//维权支出

            $data['business_money'] = $data['business_peixun_money']+$data['business_meeting_money']+$data['business_waishi_money']+$data['business_zhuanxiang_money']+$data['business_other_money'];//业务支出

            $data['capital_money'] = $data['capital_work_money']+$data['capital_zhuanxiang_money']+$data['capital_jiaotong_money']+$data['capital_xiushang_money']+$data['capital_internate_money']+$data['capital_other_money'];//资本性支出

            $data['thisyear_money'] = $data['staff_activities_money']+$data['safeguard_rights_money']+$data['business_money']+$data['capital_money']+$data['other_money'];//合计支出

            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['creaetime'] = time();
            $res = db('capital')->insert($data);
            if ($res) {
                if ($kpi_id) {//任务指标来的
                    $data['check_status'] = 1;
                    $data['kpi_id'] = $kpi_id;
                    $data['otherPage'] = 1;
                    Hook::listen("apilist_check_successed", $data);
                }
                //删除草稿
                $this->delCg('capital');
                $this->success('上报成功','index');
            }else{
                $this->error('上报失败');
            }

        }
        //判断是否有相关指标未完成，有则提示是否跳转熬指标完成页面
        $kpiInfo = ['status'=>0,'url'=>''];
        if (!$kpi_id) {  
            $nowtime = date('Y-m-d H:i:s');
            $kpi_info = db('kpi')->where(['submit_type'=>4,'end_time'=>['>',$nowtime]])->field('id,submit_num,add_time')->order('id desc')->find();
            if ($kpi_info) {//有指标判断是否完成
                //$submit_num = db('capital')->where(['company_id'=>$this->_user->company_id,'creaetime'=>['>',strtotime($kpi_info['add_time'])]])->count();
                $submit_num = db('company_score_log')->where(['company_id'=>$this->_user->company_id,'kpi_id'=>$kpi_info['id']])->count();
                if ($submit_num<$kpi_info['submit_num']) {//未完成
                    $kpiInfo['status'] = 1;
                    //$kpiInfo['url'] = url('kpiadd',['id'=>$kpi_info['id']]);
                    $kpiInfo['url'] = url('kpilist');
                }
            }
        }
        $this->view->assign("kpiInfo", $kpiInfo);

        //判断当前是第几季度,上报的是上个季度的
        $month = date('m');
        if ($month<4) {
            $jdname = '第四季';
        }elseif($month<7){
            $jdname = '第一季';
        }elseif($month<10){
            $jdname = '第二季';
        }else{
            $jdname = '第三季';
        }

        //获取草稿信息
        $cacheKey = "capital_".$this->_user->id;
        $cacheData = cache($cacheKey);
        $this->view->assign('cacheData', $cacheData);
        
        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('jdname', $jdname);
        $this->view->assign('title', '年度工会使用情况上报');
        return $this->view->fetch();
    }

    //任务指标列表
    public function kpilist(){
        $unionInfo = db('union')->where(['company_id'=>$this->_user->company_id])->value('id');
        if (!$unionInfo) {
            return '<div style="font-size:31px;margin-top:50px;text-align:center;">您所在的企业还未成立工会！暂时无法访问此页面！</div>';
        }

        $companyInfo = db('company')->where(['id'=>$this->_user->company_id])->field('name,short_name,type,score')->find();
        //获取任务指标,过滤已经完成的
        $nowtime = date('Y-m-d H:i:s');
        $list = [];
        $kpiList = db('kpi')->where(['end_time'=>['>',$nowtime]])->field('id,name,grade,score,submit_num,submit_type,add_time')->order('grade asc,weigh desc')->select();
        foreach ($kpiList as $key => &$val) {
            if($val['submit_type']==6||$val['submit_type']==4){
                //$submit_num = db('union_active')->where(['company_id'=>$this->_user->company_id,'add_time'=>['>',$val['add_time']],'status'=>['<',2]])->count();
                $submit_num = db('company_score_log')->where(['company_id'=>$this->_user->company_id,'kpi_id'=>$val['id']])->count();
                if ($submit_num<$val['submit_num']) {//未完成
                    $val['has_num'] = $submit_num;
                    array_push($list,$val);
                }
            }else{
                //查询完成了多少次
                $num = db('kpi_list')->where(['company_id'=>$this->_user->company_id,'kpi_id'=>$val['id'],'check_status'=>['<',2]])->count();
                if($num<$val['submit_num']){//未完成的
                    $val['has_num'] = $num;
                    array_push($list,$val);
                }
            }

        }
        /*$nowtime = date('Y-m-d H:i:s');
                    $kpi_info = db('kpi')->where(['submit_type'=>6,'end_time'=>['>',$nowtime]])->field('id,submit_num,add_time')->order('id desc')->find();
                    if ($kpi_info) {//有指标判断是否完成
                        //$submit_num = db('kpi_list')->where(['company_id'=>$this->_user->company_id,'kpi_id'=>$kpi_info['id'],'check_status'=>['<',2]])->count();
                        $submit_num = db('union_active')->where(['company_id'=>$this->_user->company_id,'add_time'=>['>',$kpi_info['add_time']],'status'=>['<',2]])->count();
                        if ($submit_num<$kpi_info['submit_num']) {//未完成
                            $kpiInfo['status'] = 1;
                            //$kpiInfo['url'] = url('kpiadd',['id'=>$kpi_info['id']]);
                            $kpiInfo['url'] = url('kpilist');
                        }
                    }*/
        //获取指标主题名称
        $topic_info = db('topic')->order('id desc')->find();

        $this->view->assign('companyInfo', $companyInfo);
        $this->view->assign('kpiList', $list);
        $this->view->assign('topic_info', $topic_info);
        $this->view->assign('title', '工作量化指标');
        return $this->view->fetch();
    }
    //主题详情
    public function topic_info($id=0){
        $id = intval($id);
        $topic_info = db('topic')->order('id desc')->find();
        $this->view->assign('topic_info', $topic_info);
        $this->view->assign('title', '考核主题说明');
        return $this->view->fetch();
    }

    //任务指标上报
    public function kpiadd($id){
        $id = intval($id);
        $kpiInfo = db('kpi')->where(['id'=>$id])->find();
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                'intro'  => 'require|length:1,150',
                '__token__' => 'require|token'
            ];

            $msg = [
                'intro.require'  => '上报简介必须',
                'intro.length'  => '上报简介必须150字以内',
            ];
            if ($kpiInfo['submit_type']!=1) {
                $rule['images'] = 'require';
                $msg['images'] = '图片或文件必须';
            }
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            $data['kpi_id'] = $kpiInfo['id'];
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['add_time'] = date('Y-m-d H:i:s');
            $res = db('kpi_list')->insert($data);
            if ($res) {
                $this->success('上报成功','kpilist');
            }else{
                $this->error('上报失败');
            }

        }

        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('kpiInfo', $kpiInfo);
        $this->view->assign('title', '工作量化指标上报');
        switch ($kpiInfo['submit_type']) {
            case 1:
                $tmp = 'kpitmp1';
                break;
            case 2:
                $tmp = 'kpitmp2';
                break;
            case 3:
                $tmp = 'kpitmp3';
                break;
            case 4:
                $this->redirect('/index/admin/capital/kpi_id/'.$id);
                break;
            case 5:
                $this->redirect('/index/admin/company_info/kpi_id/'.$id);
                break;
            case 6:
                $this->redirect('/index/admin/unionactive/kpi_id/'.$id);
                break;

            default:
                $tmp = 'kpitmp1';
                break;
        }
        return $this->view->fetch($tmp);
    }
    //积分明细
    public function score_list(){
        $list = db('company_score_log')->where(['company_id'=>$this->_user->company_id])->order('id desc')->limit(50)->select();
        $this->view->assign('list', $list);
        $this->view->assign('title', '积分明细');
        return $this->view->fetch();
    }
  
    //预警信息列表
    public function warninglist(){
        $list = db('kpi_warning')->where(['company_id'=>$this->_user->company_id])->order('id desc')->limit(20)->select();

        foreach ($list as $key => &$val) {
            $val['kpi_name'] = db('kpi')->where(['id'=>$val['kpi_id']])->value('name');
            if ($val['warning_type']==1) {
                $val['warning_name'] =  '<font color="red">周预警</font>';
                $val['day_num'] = 7;
            }elseif($val['warning_type']==2){
                $val['warning_name'] =  '<font color="#FF9800">月预警</font>';
                $val['day_num'] = 30;
            }elseif($val['warning_type']==3){
                $val['warning_name'] =  '<font color="black">季度预警</font>';
                $val['day_num'] = 90;
            }else{
                $info = db('warning_type')->where(['id'=>$val['warning_type']])->field('name,day')->find();
                $val['warning_name'] = $info['name']??'';
                $val['day_num'] = $info['day'];
            }
        }
        $this->view->assign('list', $list);
        $this->view->assign('title', '预警信息');
        return $this->view->fetch();
    }
    //预警信息确认
    public function warningred(){
        $id = $this->request->post('id');
        $rs = db('kpi_warning')->where(['id'=>$id])->update(['read_time'=>time()]);
        if ($rs) {
            $this->success('操作成功');
        }else{
            $this->error('操作失败');
        }
    }

    //企业信息完善
    public function company_info($kpi_id=''){
        $kpi_id = intval($kpi_id);
        $info =  db('company_info')->where(['company_id'=>$this->_user->company_id])->find();
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                /*'c_nature'   => 'require',
                'c_style'   => 'require',
                'c_business'   => 'require',
                'c_annual_value'   => 'require',
                'c_build_time'   => 'require',
                'c_user_num'   => 'require',
                'c_man_num'   => 'require',
                'c_women_num'   => 'require',*/
                '__token__' => 'require|token'
            ];

            $msg = [
                /*'c_nature'   => '企业性质必须',
                'c_style'   => '企业类型必须',
                'c_business'   => '主营业务必须',
                'c_annual_value'   => '年产值必须',
                'c_build_time'   => '成立时间必须',
                'c_user_num'   => '员工人数必须',
                'c_man_num'   => '男员工人数',
                'c_women_num'   => '女员工人数'*/
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            if ($data['c_build_time']) {
                $data['c_build_time'] = strtotime($data['c_build_time']);
                $data['c_build_time'] = $data['c_build_time']<0?0:$data['c_build_time'];
            }
            if ($data['u_build_time']) {
                $data['u_build_time'] = strtotime($data['u_build_time']);
                $data['u_build_time'] = $data['u_build_time']<0?0:$data['u_build_time'];
            }
            if ($data['d_build_time']) {
                $data['d_build_time'] = strtotime($data['d_build_time']);
                $data['d_build_time'] = $data['d_build_time']<0?0:$data['d_build_time'];
            }
         
            
            unset($data['__token__']);
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            if ($info) {
                $res = db('company_info')->update($data);
            }else{
                $res = db('company_info')->insert($data);
            }
            
            if ($res) {
                if ($kpi_id) {//任务指标来的
                    $data['check_status'] = 1;
                    $data['kpi_id'] = $kpi_id;
                    $data['otherPage'] = 1;
                    Hook::listen("apilist_check_successed", $data);
                }
                $this->success('提交成功','index');
            }else{
                $this->error('提交失败');
            }

        }
        //判断是否有相关指标未完成，有则提示是否跳转熬指标完成页面
        $kpiInfo = ['status'=>0,'url'=>''];
        if (!$kpi_id) {  
            $nowtime = date('Y-m-d H:i:s');
            $kpi_info = db('kpi')->where(['submit_type'=>5,'end_time'=>['>',$nowtime]])->field('id,submit_num,add_time')->order('id desc')->find();
            if ($kpi_info) {//有指标判断是否完成
                $submit_num = db('capital')->where(['company_id'=>$this->_user->company_id])->count();
                if (!$submit_num) {//未完成
                    $kpiInfo['status'] = 1;
                    //$kpiInfo['url'] = url('kpiadd',['id'=>$kpi_info['id']]);
                    $kpiInfo['url'] = url('kpilist');
                }
            }
        }
        $this->view->assign("kpiInfo", $kpiInfo);


        $this->view->assign('info', $info);
        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('title', '企业信息完善');
        return $this->view->fetch();
    }

    //指标上报记录
    public function kpi_submit_list(){
        $list = db('kpi_list')->where(['company_id'=>$this->_user->company_id])->field('id,kpi_id,intro,add_time,check_status')->order('id desc')->limit(50)->select();

        foreach ($list as $key => &$val) {
            $val['kpi_name'] = db('kpi')->where(['id'=>$val['kpi_id']])->value('name');
        }
        $this->view->assign('list', $list);
        $this->view->assign('title', '指标上报记录');
        return $this->view->fetch();
    }


    //删除草稿缓存
    public function delCg($type=""){
        if ($type) {
            $key = $type."_".$this->_user->id;
            cache($key,null);
        }
    }

    //民主管理
    public function mzgl(){
        return $this->view->fetch();
    }

    //议题列表
    public function proposal_list(){
        $list = db('proposal')->where('company_id='.$this->_user->company_id)->order('add_time desc')->limit(100)->select();
        foreach ($list as $key => &$val) {
            $val['username'] = db('user')->where('id='.$val['user_id'])->value('username');
        }
        $this->view->assign('list', $list);
        return $this->view->fetch();
    }

    //议题详情
    public function proposal_info($id=0){
        $info = db('proposal')->where(['id'=>$id])->find();
        if ($this->request->isPost()) {
            if (!$info) {
                $this->error('议题获取失败');
            }
            $data = $this->request->post();
            $rule = [
                'vote_end_time' => 'require',
                'title'  => 'require',
                'content'    => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                'vote_end_time' => '讨论截至时间必须',
                'title'  => '提案名称必须',
                'content'     => '提案内容必须',
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            
            unset($data['__token__']);
            $data['vote_end_time'] = strtotime($data['vote_end_time']) + 3600*24 -1;
            $res = db('proposal')->where('id='.$id)->update($data);
            if ($res) {
                if (isset($data['status']) && $data['status']==3) {
                    //转合理化建议，发邮箱
                    $email = db('company_info')->where('company_id='.$info['company_id'])->value('u_chairman_email');
                    if ($email) {
                        $url = $this->request->domain().'/index/index/downloadProposal/id/'.$id;
                        $content = '<a href="'.$url.'">点击此处下载议题>></a>';
                        QueueApi::sendEmail($data['title'],$email,$content);
                    }
                }
                if ($data['status']==2) {
                    $msg = '请在“线上职工大会”处查看';
                }else{
                    $msg = '操作成功';
                }
                $this->success($msg,'proposal_list');
            }else{
                $this->error('操作失败');
            }

        }
        $info['vote_end_time'] = $info['vote_end_time']>0 ? date("Y-m-d",$info['vote_end_time']):'';
        $userInfo = db('user')->where('id='.$info['user_id'])->field('username,mobile')->find();
        $company_name = db('company')->where('id='.$info['company_id'])->value('name');
        $info['username'] = $userInfo['username'];
        $info['mobile'] = $userInfo['mobile'];
        $info['company_name'] = $company_name;
        $this->view->assign('info', $info);
        return $this->view->fetch();
    }

    //厂务信息发布
    public function factory_information_add(){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                'title'  => 'require',
                'content'    => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                'title'  => '信息标题必须',
                'content'     => '内容必须',
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            $data['company_id'] = $this->_user->company_id;
            $data['user_id'] = $this->_user->id;
            $data['add_time'] = time();
            $res = db('factory_information')->insert($data);
            if ($res) {
                $this->success('添加成功','mzgl');
            }else{
                $this->error('添加失败');
            }

        }
        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('datetime', date('Y年m月d日H:i'));
        return $this->view->fetch();
    }
}
