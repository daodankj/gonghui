<?php
namespace app\index\controller;

use app\common\controller\Frontend;


class PartyMapFront extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = 'fk';

    public function index($id=0){
        $info = db('company')->where('id='.$id)->field('id,name,short_name,url')->find();
        //企业详细信息
        $companyInfo = db('company_info')->where(['company_id'=>$info['id']])->find();
        if (!$companyInfo) {
            $companyInfo = [
                'c_build_time'=>'',
                'c_nature'=>'',
                'c_style'=>'',
                'c_business'=>'',
                'c_annual_value'=>'',
                'c_user_num'=>'',
                'u_build_time'=>'',
                'u_chairman'=>'',
                'u_user_num'=>'',
                'd_build_time'=>'',
                'd_secretary'=>'',
                'd_user_num'=>'',
                'd_nosuser_num'=>'',
                'd_characteristic'=>'',
                'c_image'=>''
            ];
        }else{
            $companyInfo['c_build_time'] = $companyInfo['c_build_time']==0?'':date('Y年m月',$companyInfo['c_build_time']);
            $companyInfo['u_build_time'] = $companyInfo['u_build_time']==0?'':date('Y年m月',$companyInfo['u_build_time']);
            $companyInfo['d_build_time'] = $companyInfo['d_build_time']==0?'':date('Y年m月',$companyInfo['d_build_time']);
        }
        $cinfo = array_merge($info,$companyInfo);
        $this->view->assign('cinfo', $cinfo);

        //查询资金上报信息
        //$row = db('capital')->where(['company_id'=>$info['id']])->order('creaetime desc')->find();
        $cmodel = new \app\admin\model\Capital;
        $row = $cmodel->where(['company_id'=>$info['id']])->order('creaetime desc')->find();
        if ($row) {
            /*$leave_money = $row->total_money-$row->staff_activities_money-$row->business_money-$row->safeguard_rights_money-$row->administration_money-$row->capital_money-$row->subsidy_money-$row->cause_money-$row->other_money;
            $data = [
                ['value'=>$row->staff_activities_money ,'name'=>'员工活动支出'],
                ['value'=>$row->business_money ,'name'=>'业务支出'],
                ['value'=>$row->safeguard_rights_money ,'name'=>'维权支出'],
                ['value'=>$row->administration_money ,'name'=>'行政支出'],
                ['value'=>$row->capital_money ,'name'=>'资本性支出'],
                ['value'=>$row->subsidy_money ,'name'=>'补助下级支出'],
                ['value'=>$row->cause_money ,'name'=>'事业支出'],
                ['value'=>$row->other_money ,'name'=>'其他支出'],
                ['value'=>$leave_money ,'name'=>'未支出金额'],
            ];*/
            $leave_money = $row->total_money-$row->thisyear_money;
            $data = [
                ['value'=>$row->staff_activities_money ,'name'=>'员工活动支出'],
                ['value'=>$row->business_money ,'name'=>'业务支出'],
                ['value'=>$row->safeguard_rights_money ,'name'=>'维权支出'],
                ['value'=>$row->capital_money ,'name'=>'资本性支出'],
                ['value'=>$row->other_money ,'name'=>'其他支出'],
                ['value'=>$leave_money ,'name'=>'结余金额'],
            ];
        }else{
            $data = [];
        }

        $this->assign('row',$row);
        $this->assign('data',$data);


        $this->view->assign('title', '企业详情');
        return $this->view->fetch();
    }

    //党组织信息  工会组织信息
    public function dinfo($id=1){
        $cinfo = db('front')->where(['id'=>1])->find();
        $this->assign('cinfo', $cinfo);
        $this->assign('id', $id);
        return $this->view->fetch();
    }

    //工会建设、职工帮扶、人才服务
    public function business_list($type=1,$company_id=0){
        if ($type==1) {
            $bgimg = '/assets/img/dyjs_bj.jpg';
        }elseif($type==2){
            $bgimg = '/assets/img/map_index_rcfw_bg.png';
        }else{
            $bgimg = '/assets/img/map_index_zg_bg.png';
        }
        //获取是否有换届申请
        $union_changeapply_num = db('party_changeapply')->where(['company_id'=>$company_id])->count();
        //是否有纠纷调解
        $weiquan_num = 0;//db('weiquan')->where(['company_id'=>$company_id])->count();
        //是否有欠薪求助
        $arrears_help_num = 0;//db('arrears_help')->where(['company_id'=>$company_id])->count();
        $numinfo = ['union_changeapply_num'=>$union_changeapply_num,'weiquan_num'=>$weiquan_num,'arrears_help_num'=>$arrears_help_num];

        //时间判断，5月1号到7月31号
        $month = date('m');
        if ($month>4&&$month<8) {
            $this->assign('showWord', 0);
        }else{
            $this->assign('showWord', 1);
        }
        //获取换届时间
        $partyInfo = db('party')->where('company_id='.$company_id)->field('s_add_time,change_num')->find();
        if(!$partyInfo['s_add_time']){
            $partyInfo['change_time'] = '没上传数据，系统无法预测下一届成立时间';
        }else{
            $partyInfo['change_time'] = date('Y-m-d',strtotime("+3 year",strtotime($partyInfo['s_add_time'])));
        }


        $this->assign('bgimg', $bgimg);
        $this->assign('type', $type);
        $this->assign('company_id', $company_id);
        $this->assign('numinfo', $numinfo);
        $this->assign('partyInfo', $partyInfo);
        return $this->view->fetch();
    }

    //企业积分排行
    public function score_top($company_id='',$topic_id=''){
        //更改按主题获取积分
        if (!$topic_id) {
            $topic_id = db('party_topic')->order('id desc')->value('id');
        }
        $where1 = ['topic_id'=>$topic_id];
        $where2 = [];
        if ($company_id) {
            $where1['company_id'] = $company_id;
            $where2['c.id'] = $company_id;
        }
        //$ids = db('party_score_log')->where($where1)->field('max(id)')->group('company_id')->column('max(id)');
        //$scoreLog = db('party_score_log')->where(['id'=>['in',$ids]])->field('company_id,after,union_type')->buildSql();
        //$list = db('company')->alias('c')->where($where2)->join([$scoreLog=> 'l'], 'c.id = l.company_id','left')->order('after desc')->select();
        $list = db('company')->alias('c')->join('party p', 'c.id = p.company_id','right')->field('c.id,c.short_name,c.name,p.score,p.type')->order('p.score desc')->select();
        /*if ($company_id) {
            $list  = db('company')->where(['id'=>$company_id])->order('score desc')->field('id,name,short_name,type,score')->select();
        }else{
            $list  = db('company')->order('score desc')->field('id,name,short_name,type,score')->select();
        }*/

        foreach ($list as $key => &$val) {
            if (!$val['short_name']) {
                $val['short_name'] = $val['name'];
            }
            /*if ($val['after']) {
                $val['score'] = $val['after'];
            }else{
                $val['score'] = 0;
            }
            if ($val['union_type']===null) {
                $val['type'] = 'D';
            }elseif($val['union_type']){
                $val['type'] = $val['union_type'];
            }else{
                $val['type'] = $val['type'];
            }*/
        }
        $this->view->assign('company_id', $company_id);
        $this->view->assign('topic_id', $topic_id);

        $this->view->assign('list', $list);
        $this->view->assign('title', '企业积分排行');
        return $this->view->fetch();
    }
    public function honour(){
        $this->view->assign('title', '获得荣誉');
        return $this->view->fetch();
    }

}

