<?php
namespace app\index\controller;

use app\common\controller\Frontend;
use think\Config;
use tool\YinshiApi;

class BmfwMap extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = 'fk';
    protected $model = null;
    protected $searchFields = 'id,name';


    public function _initialize() {
        $this->model = new \app\admin\model\Bmfw();
        parent::_initialize();
    }

    //地图
    public function index(){
        $data = $this->model->where(['lat'=>['>',0]])->select();
        $this->assign('data', json_encode($data));

        $dconfig = get_addon_config('cwmap');

        // 语言检测
        $lang = strip_tags($this->request->langset());

        $site = Config::get("site");

        // 配置信息
        $config = [
            'site'           => array_intersect_key($site, array_flip(['name', 'cdnurl', 'version', 'timezone', 'languages'])),
            'upload'         => null,
            'modulename'     => 'addons',
            'controllername' => 'index',
            'actionname'     => 'index',
            'jsname'         => 'addons/cwmap',
            'moduleurl'      => '',
            'language'       => $lang
        ];
        $config = array_merge($config, $dconfig);
        $this->assign('config', $config);

        $this->view->assign('title', '便民服务站地图');
        return $this->view->fetch();
    }

    //服务站点详情
    public function info($id){
        $data = $this->model->get($id);

        $this->assign('data',$data);
        $this->view->assign('title', '便民服务站点详情');
        return $this->view->fetch();
    }

    //获取设备列表
    public function getDeviceList($id){
        $data = $this->model->get($id);
        if (empty($data['ys_appkey']) || empty($data['ys_appsecret'])){
            exit('请先设置appKey和appSecret');
        }
        /*$cacheKey = 'devicelist_'.$data['ys_appkey'];
        $deviceList = cache($cacheKey);
        if (empty($deviceList)){
            $mApi = new YinshiApi($data['ys_appkey'],$data['ys_appsecret']);
            $deviceList = $mApi->getDeviceList();
            cache($cacheKey,$deviceList,1);
        }*/
        $mApi = new YinshiApi($data['ys_appkey'],$data['ys_appsecret']);
        $deviceList = $mApi->getDeviceList();
        if (empty($deviceList)){
            exit($mApi->errorMsg);
            //$this->error("$mApi->errorMsg");
        }
        $this->assign('deviceList',$deviceList["data"]);
        $this->view->assign('title', '设备列表');
        return $this->view->fetch();
    }

    public function search() {
        if (request()->isAjax()) {
            $search = request()->get('keyword');
            $searcharr = is_array($this->searchFields) ? $this->searchFields : explode(',', $this->searchFields);

            $result = $this->model->where(implode("|", $searcharr), "LIKE", "%{$search}%")->select();
            return json_encode($result);
            //$this->success('',$result);
        }
    }

}
