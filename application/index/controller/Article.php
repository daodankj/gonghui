<?php
namespace app\index\controller;

use app\common\controller\Frontend;


class Article extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = 'fk';

    public function index($type=''){
    	$model = db('article');
        if ($type) {
            $model->where(['type'=>$type]);
        }
        $list = $model->order('add_time desc')->limit(50)->select();

        $this->view->assign('list', $list);
        $this->view->assign('title', '文章列表');
        return $this->view->fetch();
    }

    public function info($id){
        if (!$id) {
            $this->error('该页面不存在');
        }
        $info = db('article')->where(['id'=>$id])->find();
        $this->view->assign('info', $info);
        $this->view->assign('title', '文章详情');
        return $this->view->fetch();
    }

}

