<?php

namespace app\index\controller;

use app\common\controller\Frontend;
use think\Db;
use think\Session;
use fast\Random;
use think\Validate;
use app\common\model\User;
use tool\Wechat;
use think\Hook;
use addons\faqueue\library\QueueApi;
use tool\Ygh;

class Index extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = 'fk';
    protected $_user = null;


    public function _initialize()
    {
        parent::_initialize();
        $actionname = strtolower($this->request->action());
        $white_action = ['login','wechatcallback','downloadproposal'];//免登入方法
        if (!in_array($actionname, $white_action)) {
            $this->checkLogin();
        }
    }

    protected function checkLogin(){
        $userId = session('userId');
        if (!$userId) {//未登入的则自动登入
            //新增粤工会自动登入
            $this->yghLogin();
            //获取小程序用户信息
            $idCardNumber = $this->request->get('idCardNumber');
            $uname = $this->request->get('uname');
            $mobile = $this->request->get('tel');
            if (!$idCardNumber||!$uname||!$mobile) {//未获取到认证信息提示错误
                //echo '请从智慧大旺小程序进入';exit;
                $this->redirect('login');
            }
            //判断是否已经注册过会员，没则注册
            $this->_user = User::get(['idcard' => $idCardNumber]);
            if (!$this->_user) {//不存在则注册
                $ip = request()->ip();
                $time = time();

                $data = [
                    'username' => $uname,
                    'password' => $mobile,
                    'idcard'    => $idCardNumber,
                    'mobile'   => $mobile,
                    'level'    => 1,
                    'score'    => 0,
                    'avatar'   => '',
                ];
                $params = array_merge($data, [
                    'nickname'  => $uname,
                    'salt'      => Random::alnum(),
                    'jointime'  => $time,
                    'joinip'    => $ip,
                    'logintime' => $time,
                    'loginip'   => $ip,
                    'prevtime'  => $time,
                    'status'    => 'normal'
                ]);
                $params['password'] = $this->getEncryptPassword($data['password'], $params['salt']);
                Db::startTrans();
                try {
                    $user = User::create($params, true);
                    //设置session
                    $userId = $user->id;
                    session('userId',$user->id);
                    Db::commit();
                } catch (Exception $e) {
                    Db::rollback();
                    echo $e->getMessage();exit;
                }
            }else{
                session('userId',$this->_user->id);
            }
        }
        if (!$this->_user) {
            $this->_user = User::get($userId);
        }
        //判断是否绑定企业
        $actionname = strtolower($this->request->action());
        if (!$this->_user['company_name']&&$actionname!='bindcompany') {
            $this->redirect('bindcompany');
        }
    }
    /**
     * 获取密码加密后的字符串
     * @param string $password 密码
     * @param string $salt     密码盐
     * @return string
     */
    public function getEncryptPassword($password, $salt = '')
    {
        return md5(md5($password) . $salt);
    }
    //登入页面
    public function login(){
        if ($this->request->isPost()) {
            //$this->error('0');//暂停登入
            $data = $this->request->post();

            $rule = [
                'name'   => 'require',
                'idcard'  => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                'name'   => '姓名必须',
                'idcard'  => '身份证必须'
            ];

            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            $this->_user = User::get(['username'=>$data['name'],'idcard' => $data['idcard']]);
            if ($this->_user) {
                session('userId',$this->_user->id);
                if (!$this->_user->openid && Session::get('openid')) {
                    $this->_user->openid = Session::get('openid');
                    $this->_user->save();
                }
                $this->success('登入成功','index');
            }else{
                $this->error('该用户不存在', null, ['token' => $this->request->token()]);
            }
        }
        //判断是否已经登入过
        $userId = session('userId');
        if ($userId) {
            $url = url('index');
            $this->redirect($url);
        }
        //新增微信自动登入
        $this->wechatLogin();
        $this->view->assign('title', '员工登入');
        return $this->view->fetch();
    }
    private function yghLogin(){
        $unique = $this->request->get('unique');
        $code = $this->request->get('code');
        /*echo 'unique：'.$unique;
        echo '<br/>';
        echo 'code：'.$code;exit;*/
        if ($unique&&$code) {//获取到认证信息才判断
            $Ygh = new Ygh();
            $token = $Ygh->getToken();
            if (isset($token['code'])&&$token['code']==8) {
                $uinfo = $Ygh->getInfo($token['data']['token'],$unique,$code);
                if (isset($uinfo['code'])&&$uinfo['code']==8) {
                    $info = $uinfo['data'];
                    if ($info['name']&&$info['mobile']&&$info['cardnum']) {
                        $this->redirect('/?idCardNumber='.$info['cardnum'].'&uname='.$info['name'].'&tel='.$info['mobile']);
                    }
                }else{
                    $this->error('用户信息获取失败'.$uinfo['data']['msg'],'login');
                }
            }else{
                $this->error('用户信息获取失败:'.$token['data']['msg'],'login');
            }
        }
    }
    private function wechatlogin(){
        //判断是否是微信浏览器
        $isWeixin = $this->isWeixin();
        if (!$isWeixin) {
            Session::delete('openid');
            return false;
        }
        if(Session::get('openid')){
            //判断该用户是否已经绑定过
            //$this->checkWxBind(Session::get('openid'));
            return;//如果已经获取过openid则不用在次获取了
        }
        $redirect_uri = url('index/index/wechatcallback','','',true);
        $wchate = new Wechat(['callback'=>$redirect_uri]);
        $this->redirect($wchate->getAuthorizeUrl());
    }
    //判断是否是微信浏览器
    private function isWeixin(){
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        if (strpos($user_agent, 'MicroMessenger') === false) {
            // 非微信浏览器
            return false;
        } else {
            // 微信浏览器，
            return true;
        }
    }
    public function wechatcallback(){
        $wchate = new Wechat();
        $userinfo = $wchate->getUserInfo();
        if (!$userinfo) {
            $this->error(__('操作失败'));
        }
        if (!$userinfo['openid']) {
            trace('用户数据:'.json_encode($userinfo),'error');
        }
        Session::set('openid',$userinfo['openid']);
        //判断该用户是否已经绑定过
        $this->checkWxBind($userinfo['openid'],true);
    }
    //判断微信是否已经绑定
    private function checkWxBind($openid='',$gologin=false){
        if (!$openid) {
            return;
        }
        $this->_user = User::get(['openid' => $openid]);
        if (!$this->_user) {
            if ($gologin) {
                $url = url('login');
                $this->redirect($url);
            }
            return false;
        }
        session('userId',$this->_user->id);
        //$this->success('微信自动登入成功','index');
        $url = url('index');
        $this->redirect($url);
    }
    //首页菜单页面
    public function index()
    {
        $warning_num = 0;
        if ($this->_user->is_admin) {
            //获取预警数量
            $warning_num = db('kpi_warning')->where(['company_id'=>$this->_user->company_id,'read_time'=>0])->count();
        }
        //电子书屋地址
        $sw_url = "https://ydmls.gxzhdw.cn/index/user/third/cardindex/1?idCardNumber={$this->_user->idcard}&uname={$this->_user->username}&tel={$this->_user->mobile}";
        //福利商城
        $shop_url = "https://flht.zqci.cn/index/index/auto_login?idCardNumber={$this->_user->idcard}&uname={$this->_user->username}&tel={$this->_user->mobile}";

        $this->view->assign('sw_url', $sw_url);
        $this->view->assign('shop_url', $shop_url);
        $this->view->assign('is_admin', $this->_user->is_admin);
        $this->view->assign('warning_num', $warning_num);
        $this->view->assign('title', '大旺工人');
        return $this->view->fetch();
    }
    //工人首页
    public function unindex()
    {
        $warning_num = 0;
        if ($this->_user->is_admin) {
            //获取预警数量
            $warning_num = db('kpi_warning')->where(['company_id'=>$this->_user->company_id,'read_time'=>0])->count();
        }
        $this->view->assign('is_admin', $this->_user->is_admin);
        $this->view->assign('warning_num', $warning_num);
        $this->view->assign('title', '大旺工人');
        return $this->view->fetch();
    }
    /*//工会建设服务
    public function unindex2()
    {
        $this->view->assign('title', '工会建设服务');
        return $this->view->fetch();
    }*/
    //职工帮扶
    public function zgbf(){
        $this->view->assign('title', '职工帮扶');
        return $this->view->fetch();
    }
    //企业人才服务
    public function qyrcfw(){
        $this->view->assign('title', '人才服务');
        return $this->view->fetch();
    }
    //普惠职工
    public function zgyhzc(){
        $this->view->assign('title', '普惠职工');
        return $this->view->fetch();
    }
    //zhaoping
    public function zhaoping(){
        $this->view->assign('title', '企业招聘信息');
        return $this->view->fetch();
    }
    //绑定企业
    public function bindcompany($step=1,$style="企业"){
        if ($this->request->isPost()) {
            $company_id = $this->request->post('company_id');
            if (!$company_id) {
                $this->error('请选择要绑定的企业');
            }
            $this->_user->company_id = $company_id;
            $this->_user->save();
            $this->success('绑定成功','index');
        }
        $companyList = [];
        /*if ($step==2) {
            $companyList = db('company')->where(['style'=>$style])->select();
        }*/
        $styleName = $style;
        if ($styleName=="其他") {
            $styleName = "其他社会组织";
        }
        $this->view->assign('companyList', $companyList);
        $this->view->assign('step', $step);
        $this->view->assign('style', $style);
        $this->view->assign('styleName', $styleName);
        $this->view->assign('title', '企业/单位/组织绑定');
        return $this->view->fetch();
    }

    //5职工维权劳资纠纷申请
    public function weiquan(){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                'name'   => 'require',
                /*'sex'  => 'require',
                'address'  => 'require',
                'idcard'  => 'require|length:18',
                'phone'  => 'require',
                'info'  => 'require',*/
                //'intro'  => 'require',
                //'shuqiu'  => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                'name'   => '姓名必须',
                /*'sex'  => '性别必须',
                'address'  => '单位必须',
                'idcard'  => '身份证必须',
                'idcard.length'  => '身份证格式错误',
                'phone'  => '电话必须',
                'info'  => '反映内容事项必须',*/
                //'intro'  => '要求解决事项必须',
                //'shuqiu'  => '申请人诉求必须',
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['add_time'] = date('Y-m-d H:i:s');
            $res = db('weiquan')->insert($data);
            if ($res) {
                //监听申请事件
                $obj_id = db('weiquan')->getLastInsID();
                $infodata = ['type'=>5,'obj_id'=>$obj_id,'data'=>$data];
                Hook::listen("user_apply", $infodata);
                $this->success('申请成功','index');
            }else{
                $this->error('申请失败');
            }

        }

        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('title', '劳资纠纷调解申请');
        return $this->view->fetch();
    }

    //6困难职工上报
    public function kunnan(){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                //'s_name'   => 'require',
                //'s_age'  => 'require',
                //'s_company'  => 'require',
                //'s_income'  => 'require',
                /*'w_name'   => 'require',
                'w_age'  => 'require',
                'w_company'  => 'require',
                'w_income'  => 'require',
                'live_squer'  => 'require',
                'live_start_date'  => 'require',
                'live_end_date'  => 'require',
                'village_squer'  => 'require',
                'live_squer'  => 'require',
                'family_info'  => 'require',
                'gz_income'  => 'require',
                'zf_income'  => 'require',*/
                //'intro'  => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                //'s_name'   => '本人姓名必须',
                //'s_age'  => '本人年龄必须',
                //'s_company'  => '本人单位必须',
                //'s_income'  => '本人年收入必须',
                /*'w_name'   => '配偶姓名必须',
                'w_age'  => '配偶年龄必须',
                'w_company'  => '配偶单位必须',
                'w_income'  => '配偶年收入必须',
                'live_squer'  => '常住地房面积必须',
                'live_start_date'  => '供房开始日期必须',
                'live_end_date'  => '供房结束日期必须',
                'village_squer'  => '农村宅基地面积必须',
                'family_info'  => '家庭成员必须',
                'gz_income'  => '工资收入必须',
                'zf_income'  => '政府补贴必须',*/
                //'intro'  => '困难情况描述必须',
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            //$data['needinfo'] = implode(',', $data['needinfo']);
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['add_time'] = date('Y-m-d H:i:s');
            $res = db('kunnan')->insert($data);
            if ($res) {
                //监听申请事件
                $obj_id = db('kunnan')->getLastInsID();
                $infodata = ['type'=>6,'obj_id'=>$obj_id,'data'=>$data];
                Hook::listen("user_apply", $infodata);

                //删除草稿
                $this->delCg('kunnan');
                $this->success('上报成功','index');
            }else{
                $this->error('上报失败');
            }

        }
        //获取草稿信息
        $cacheKey = "kunnan_".$this->_user->id;
        $cacheData = cache($cacheKey);
        $this->view->assign('cacheData', $cacheData);

        $this->view->assign('title', '困难职工帮扶申请');
        return $this->view->fetch();
    }
    //7欠薪求助申请
    public function arrears_help(){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                //'s_name'   => 'require',
                //'s_age'  => 'require',
                //'s_company'  => 'require',
                //'s_income'  => 'require',
                /*'w_name'   => 'require',
                'w_age'  => 'require',
                'w_company'  => 'require',
                'w_income'  => 'require',
                'live_squer'  => 'require',
                'live_start_date'  => 'require',
                'live_end_date'  => 'require',
                'village_squer'  => 'require',
                'live_squer'  => 'require',
                'family_info'  => 'require',
                'gz_income'  => 'require',
                'zf_income'  => 'require',*/
                //'intro'  => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                //'s_name'   => '本人姓名必须',
                //'s_age'  => '本人年龄必须',
                //'s_company'  => '本人单位必须',
                //'s_income'  => '本人年收入必须',
                /*'w_name'   => '配偶姓名必须',
                'w_age'  => '配偶年龄必须',
                'w_company'  => '配偶单位必须',
                'w_income'  => '配偶年收入必须',
                'live_squer'  => '常住地房面积必须',
                'live_start_date'  => '供房开始日期必须',
                'live_end_date'  => '供房结束日期必须',
                'village_squer'  => '农村宅基地面积必须',
                'family_info'  => '家庭成员必须',
                'gz_income'  => '工资收入必须',
                'zf_income'  => '政府补贴必须',*/
                //'intro'  => '困难情况描述必须',
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            //$data['needinfo'] = implode(',', $data['needinfo']);
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['add_time'] = date('Y-m-d H:i:s');
            $res = db('arrears_help')->insert($data);
            if ($res) {
                //监听申请事件
                $obj_id = db('arrears_help')->getLastInsID();
                $infodata = ['type'=>7,'obj_id'=>$obj_id,'data'=>$data];
                Hook::listen("user_apply", $infodata);

                //删除草稿
                $this->delCg('arrears_help');
                $this->success('申请成功','index');
            }else{
                $this->error('申请失败');
            }

        }
        //获取草稿信息
        $cacheKey = "arrears_help_".$this->_user->id;
        $cacheData = cache($cacheKey);
        $this->view->assign('cacheData', $cacheData);

        $this->view->assign('title', '欠薪求助申请');
        return $this->view->fetch();
    }
    //8职工帮扶--职工互助保障
    public function employees_assistance($type=''){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                /*'company'   => 'require',
                'name'  => 'require',
                'idcard'  => 'require|length:18',
                'phone'   => 'require',
                'sex'   => 'require',*/
                'image'  => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                /*'company'   => '单位名称必须',
                'name'  => '姓名必须',
                'idcard'  => '身份证号码必须',
                'idcard.length' => '身份证格式错误',
                'phone'  => '电话必须',
                'sex'  => '性别必须',*/
                'image'=>'文件必须'
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['add_time'] = time();
            $res = db('employees_assistance')->insert($data);
            if ($res) {
                //监听申请事件
                $obj_id = db('employees_assistance')->getLastInsID();
                $infodata = ['type'=>8,'obj_id'=>$obj_id,'data'=>$data];
                Hook::listen("user_apply", $infodata);
                $this->success('上报成功','zgbf');
            }else{
                $this->error('上报失败');
            }

        }
        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('type', $type);
        $this->view->assign('title', '职工互助保障');
        return $this->view->fetch();
    }

    //14圆梦助学申请
    public function dream_apply(){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                //'s_name'   => 'require',
                //'s_age'  => 'require',
                //'s_company'  => 'require',
                //'s_income'  => 'require',
                /*'w_name'   => 'require',
                'w_age'  => 'require',
                'w_company'  => 'require',
                'w_income'  => 'require',
                'live_squer'  => 'require',
                'live_start_date'  => 'require',
                'live_end_date'  => 'require',
                'village_squer'  => 'require',
                'live_squer'  => 'require',
                'family_info'  => 'require',
                'gz_income'  => 'require',
                'zf_income'  => 'require',*/
                //'intro'  => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                //'s_name'   => '本人姓名必须',
                //'s_age'  => '本人年龄必须',
                //'s_company'  => '本人单位必须',
                //'s_income'  => '本人年收入必须',
                /*'w_name'   => '配偶姓名必须',
                'w_age'  => '配偶年龄必须',
                'w_company'  => '配偶单位必须',
                'w_income'  => '配偶年收入必须',
                'live_squer'  => '常住地房面积必须',
                'live_start_date'  => '供房开始日期必须',
                'live_end_date'  => '供房结束日期必须',
                'village_squer'  => '农村宅基地面积必须',
                'family_info'  => '家庭成员必须',
                'gz_income'  => '工资收入必须',
                'zf_income'  => '政府补贴必须',*/
                //'intro'  => '困难情况描述必须',
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            //$data['needinfo'] = implode(',', $data['needinfo']);
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['add_time'] = date('Y-m-d H:i:s');
            $res = db('dream_apply')->insert($data);
            if ($res) {
                //监听申请事件
                $obj_id = db('dream_apply')->getLastInsID();
                $infodata = ['type'=>14,'obj_id'=>$obj_id,'data'=>$data];
                Hook::listen("user_apply", $infodata);

                $this->success('上报成功','index');
            }else{
                $this->error('上报失败');
            }

        }
       
        $cacheKey = "dream_apply_".$this->_user->id;
        $cacheData = cache($cacheKey);
        $this->view->assign('cacheData', $cacheData);

        $this->view->assign('title', '圆梦助学申请');
        return $this->view->fetch();
    } 

    //个人中心
    public function userinfo(){
        //待审核
        /*$num1 = db('union_apply')->where(['user_id'=>$this->_user->id,'status'=>0])->count();
        $num2 = db('union_changeapply')->where(['user_id'=>$this->_user->id,'status'=>0])->count();
        $num3 = db('union_updateapply')->where(['user_id'=>$this->_user->id,'status'=>0])->count();
        $num4 = db('weiquan')->where(['user_id'=>$this->_user->id,'status'=>0])->count();
        $num5 = db('kunnan')->where(['user_id'=>$this->_user->id,'status'=>0])->count();
        $num6 = db('union_active')->where(['user_id'=>$this->_user->id,'status'=>0])->count();
        //已完结
        $ynum1 = db('union_apply')->where(['user_id'=>$this->_user->id,'status'=>['gt',0]])->count();
        $ynum2 = db('union_changeapply')->where(['user_id'=>$this->_user->id,'status'=>['gt',0]])->count();
        $ynum3 = db('union_updateapply')->where(['user_id'=>$this->_user->id,'status'=>['gt',0]])->count();
        $ynum4 = db('weiquan')->where(['user_id'=>$this->_user->id,'status'=>['gt',0]])->count();
        $ynum5 = db('kunnan')->where(['user_id'=>$this->_user->id,'status'=>['gt',0]])->count();
        $ynum6 = db('union_active')->where(['user_id'=>$this->_user->id,'status'=>['gt',0]])->count();*/
        //待审核
        $dnum = db('user_notice')->where(['user_id'=>$this->_user->id,'status'=>0])->count();
        //待完结
        $ynum = db('user_notice')->where(['user_id'=>$this->_user->id,'status'=>['gt',0]])->count();
        //我的消息
        $noread_num = db('user_notice')->where(['user_id'=>$this->_user->id,'read_time'=>0])->count();
        $read_num = db('user_notice')->where(['user_id'=>$this->_user->id,'read_time'=>['gt',0]])->count();

        $this->view->assign('dnum',$dnum);
        $this->view->assign('ynum',$ynum);
        $this->view->assign('read_num',$read_num);
        $this->view->assign('noread_num',$noread_num);
        $this->view->assign('info', $this->_user->toArray());
        $this->view->assign('title', '我的');
        return $this->view->fetch();
    }
    //我的消息
    public function notice_list(){
        $list = db('user_notice')->where(['user_id'=>$this->_user->id])->order('id desc')->limit(50)->select();

        //全部标记已读
        db('user_notice')->where(['user_id'=>$this->_user->id])->update(['read_time'=>time()]);

        $this->view->assign('list', $list);
        $this->view->assign('title', '我的消息');
        return $this->view->fetch();
    }
    //我的申请事项
    public function apply_list(){
        $list = db('user_notice')->where(['user_id'=>$this->_user->id,'status'=>['gt',-1]])->order('id desc')->limit(50)->select();


        $this->view->assign('list', $list);
        $this->view->assign('title', '我的申请事项');
        return $this->view->fetch();
    }
    //预警信息确认
    public function noticered(){
        $id = $this->request->post('id');
        $rs = db('user_notice')->where(['id'=>$id])->update(['read_time'=>time()]);
        if ($rs) {
            $this->success('操作成功');
        }else{
            $this->error('操作失败');
        }
    }
    //基本信息
    public function information(){
        $this->view->assign('info', $this->_user->toArray());
        $this->view->assign('title', '我的基本信息');
        return $this->view->fetch();
    }
    //意见反馈列表
    public function feedback(){
        $list  = db('feedback')->where(['user_id'=>$this->_user->id])->order('id desc')->limit(50)->select();

        $this->view->assign('list', $list);
        $this->view->assign('title', '意见反馈');
        return $this->view->fetch();
    }
    public function feedback_add(){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                'content'   => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                'content'   => '内容必须'
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            $data['user_id'] = $this->_user->id;
            $data['uname'] = $this->_user->username;
            $data['add_time'] = time();
            $res = db('feedback')->insert($data);
            if ($res) {

                $this->success('提交成功','userinfo');
            }else{
                $this->error('提交失败');
            }

        }
        $this->view->assign('title', '意见反馈');
        return $this->view->fetch();
    }


    //企业积分排行
    public function score_top(){
        $list  = db('company')->where(['score'=>['>',0]])->order('score desc')->limit(500)->field('short_name,type,score')->select();

        $this->view->assign('list', $list);
        $this->view->assign('title', '企业积分排行');
        return $this->view->fetch();
    }


    //9企业人才服务--劳模工匠人才创新申报
    public function model_craftsman(){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                'studio_name'   => 'require',
                /*'createdate'  => 'require',
                'company'  => 'require',
                'named_unit'  => 'require',
                'professional_field'   => 'require',
                'nameddate'  => 'require',*/
                /*'studio_type'  => 'require',
                'user_num'  => 'require',
                'leader_name'  => 'require',
                'sex'  => 'require',
                'birthday'  => 'require',
                'education'  => 'require',
                'mingzu'  => 'require',
                'major'  => 'require',
                'technical_title'  => 'require',*/
                'phone'  => 'require',
                //'file'  => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                /*'studio_name'   => '工作室名称必须',
                'createdate'  => '创建日期必须',
                'company'  => '所在单位必须',
                'named_unit'  => '命名单位必须',*/
                //'file'  => '申请文档必须',
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['addtime'] = time();
            $res = db('model_craftsman')->insert($data);
            if ($res) {
                //监听申请事件
                $obj_id = db('model_craftsman')->getLastInsID();
                $infodata = ['type'=>9,'obj_id'=>$obj_id,'data'=>$data];
                Hook::listen("user_apply", $infodata);
                $this->success('上报成功','qyrcfw');
            }else{
                $this->error('上报失败');
            }

        }
        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('title', '劳模和工匠人才创新工作室申报');
        return $this->view->fetch();
    }
    //10企业人才服务--职工技能竞赛信息上报
    public function skills_competition(){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                /*'compet_time'   => 'require',
                'compet_item'  => 'require',
                'users'  => 'require',
                'images'  => 'require',
                'intro'   => 'require',*/
                '__token__' => 'require|token'
            ];

            $msg = [
                /*'compet_time'   => '竞赛时间必须',
                'compet_item'  => '竞赛项目必须',
                'users'  => '参与人必须',
                'images'  => '竞赛图标必须',
                'intro'  => '竞赛总结必须',*/
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            $data['compet_time'] = 0;//strtotime($data['compet_time']);
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['add_time'] = time();
            $res = db('skills_competition')->insert($data);
            if ($res) {
                //监听申请事件
                $obj_id = db('skills_competition')->getLastInsID();
                $infodata = ['type'=>10,'obj_id'=>$obj_id,'data'=>$data];
                Hook::listen("user_apply", $infodata);
                $this->success('上报成功','qyrcfw');
            }else{
                $this->error('上报失败');
            }

        }
        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('title', '职工技能竞赛信息上报');
        return $this->view->fetch();
    }

    //11企业人才服务--技能学历培训
    public function skills_education(){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                'name' => 'require',
                'idcard'  => 'require|length:18',
                //'image'  => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                'name' => '姓名必须',
                'idcard.length'  => '身份证格式错误',
                //'image'  => '文件必须'
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            //学历培训处理
            if ($data['type']==2) {
                //判断报名时间
                $education_start = strtotime(config('site.education_start'));
                $education_end   = strtotime(config('site.education_end'));
                if (time()<$education_start||time()>$education_end) {
                    //$msg = '抱歉，当前不在报名时间范围！<br/>报名时间：'.config('site.education_start').'至'.config('site.education_end');
                    $this->error('此功能暂未上线', null, ['token' => $this->request->token()]);
                }
                if ($data['nb_school_name']) {
                    $data['nb_school_name'] = $data['nb_school_name_text'];//db('school_major')->where('id='.$data['nb_school_name'])->value('name');
                }
                if ($data['major']) {
                    $data['major'] = $data['major_text'];//db('school_major')->where('id='.$data['nb_school_name'])->value('name');
                }
                //unset('nb_school_name_text');
                //unset('major_text');
            }else{
                //判断报名时间
                $education_start = strtotime(config('site.skills_start'));
                $education_end   = strtotime(config('site.skills_end'));
                if (time()<$education_start||time()>$education_end) {
                    //$msg = '抱歉，当前不在报名时间范围！<br/>报名时间：'.config('site.skills_start').'至'.config('site.skills_end');
                    $this->error('此功能暂未上线', null, ['token' => $this->request->token()]);
                }
            }
            unset($data['__token__']);
            $data['work_time'] = strtotime($data['work_time']);
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['add_time'] = time();
            $res = db('skills_education')->strict(false)->insert($data);
            if ($res) {
                //监听申请事件
                $obj_id = db('skills_education')->getLastInsID();
                $infodata = ['type'=>11,'obj_id'=>$obj_id,'data'=>$data];
                Hook::listen("user_apply", $infodata);
                if ($data['type']==2) {
                    $starttime = strtotime('-6 month' ,time());
                    $num = db('skills_education')->where('type=2 and status<>2 and id<='.$obj_id.' and add_time>'.$starttime)->count();
                    $msg = '报名成功<br/>报名时间：'.date('Y-m-d H:i:s').'<br/>当前排名：'.$num;
                    $this->success($msg,'education_list');
                }
                $this->success('上报成功','qyrcfw');
            }else{
                $this->error('上报失败');
            }

        }
        $this->view->assign('name', $this->_user->username);
        $this->view->assign('mobile', $this->_user->mobile);
        $this->view->assign('idcard', $this->_user->idcard);
        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('title', '技能和学历培训');
        return $this->view->fetch();
    }
    //学历提升报名记录
    public function education_list($type=2){
        $starttime = strtotime('-6 month' ,time());
        //自己报名的
        $mylist = db('skills_education')->where('type='.$type.' and status<>2 and user_id='.$this->_user->id.' and add_time>'.$starttime)->order('id desc')->select();
        $list = db('skills_education')->where('type='.$type.' and status<>2 and add_time>'.$starttime)->order('id asc')->limit(600)->select();
        foreach ($mylist as $key => &$val) {
            $val['num'] = db('skills_education')->where('type='.$type.' and status<>2 and id<='.$val['id'].' and add_time>'.$starttime)->count();
        }
        $title = $type==1?"技能提升报名排名":"学历提升报名排名";
        $this->view->assign('mylist',$mylist);
        $this->view->assign('list',$list);
        $this->view->assign('title', $title);
        return $this->view->fetch();
    }

    //12普惠职工--活动场地优惠
    public function site_discount(){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [

                'image'  => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                'image'  => '文件必须'
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['add_time'] = time();
            $res = db('site_discount')->insert($data);
            if ($res) {
                //监听申请事件
                $obj_id = db('site_discount')->getLastInsID();
                $infodata = ['type'=>12,'obj_id'=>$obj_id,'data'=>$data];
                Hook::listen("user_apply", $infodata);
                $this->success('上报成功','zgyhzc');
            }else{
                $this->error('上报失败');
            }

        }
        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('title', '活动场地优惠申请');
        return $this->view->fetch();
    }
    //场地优惠申请记录
    public function site_discount_list(){
        $list = db('site_discount')->where(['user_id'=>$this->_user->id])->order('id desc')->limit(50)->select();
        foreach ($list as $key=>&$val){
            $val['company_name'] = db('company')->where(['id'=>$val['company_id']])->value('name');
        }
        $this->view->assign('list',$list);
        $this->view->assign('title', '活动场地优惠申请记录');
        return $this->view->fetch();
    }

    //13普惠职工--上学优惠
    public function school_discount(){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [

                //'image'  => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                //'image'  => '文件必须'
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['add_time'] = time();
            $res = db('school_discount')->insert($data);
            if ($res) {
                //监听申请事件
                $obj_id = db('school_discount')->getLastInsID();
                $infodata = ['type'=>13,'obj_id'=>$obj_id,'data'=>$data];
                Hook::listen("user_apply", $infodata);
                $this->success('上报成功','zgyhzc');
            }else{
                $this->error('上报失败');
            }

        }
        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('title', '学校学费优惠申请');
        return $this->view->fetch();
    }
    //学费优惠申请记录
    public function school_discount_list(){
        $list = db('school_discount')->where(['user_id'=>$this->_user->id])->order('id desc')->limit(50)->select();
        foreach ($list as $key=>&$val){
            $val['company_name'] = db('company')->where(['id'=>$val['company_id']])->value('name');
        }
        $this->view->assign('list',$list);
        $this->view->assign('title', '学校学费优惠申请记录');
        return $this->view->fetch();
    }
    //14工会员工购物卡优惠
    public function shopcard_discount(){
        //获取活动信息
        $nowtime = time();
        $list = db('cardshop_active')->where(['status'=>1,'starttime'=>['<',$nowtime],'endtime'=>['>',$nowtime]])->order('id desc')->select();
        foreach($list as $key=>&$val){
            $val['shopname'] = db('cardshop')->where(['id'=>$val['cardshop_id']])->value('name');
        }
        $this->view->assign('list', $list);
        $this->view->assign('userinfo', $this->_user);
        $this->view->assign('title', '工会员工购物卡优惠');
        return $this->view->fetch();
    }
    public function shopcard_baoming($active_id=0){
        $active_id = intval($active_id);
        if ($this->request->isPost()) {
            $data = $this->request->post();
            $rule = [
                'active_id' => 'require',
                //'uname' => 'require',
                //'idcard'  => 'require|length:18',
                '__token__' => 'require|token'
            ];

            $msg = [
                'active_id' => '活动不存在'
                //'uname' => '姓名必须',
                //'idcard.length'  => '身份证格式错误'
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            //获取活动信息
            $info = db('cardshop_active')->where(['id'=>$data['active_id'],'status'=>1])->find();
            if (!$info) {
                $this->error('该活动已取消');
            }
            $nowtime = time();
            if ($info['starttime']>$nowtime) {
                $this->error('该活动还未开始');
            }elseif($info['endtime']<$nowtime){
                $this->error('该活动已结束');
            }
            //判断是否已经报名了
            $hascard = db('cardshop_baoming')->where(['active_id'=>$data['active_id'],'user_id'=>$this->_user->id])->find();
            if ($hascard) {
                $this->error('你已报名，无需在次报名');
            }
            //判断是否已经获取过该超市卡
            $cardinfo = db('cardshop_baoming')->where(['cardshop_id'=>$info['cardshop_id'],'user_id'=>$this->_user->id,'status'=>1])->find();
            if ($cardinfo) {
                $this->error('你已获取过本次超市优惠卡，无需在次报名获取');
            }
            //取消摇号，报名就可以获取
            //先判断数量是否还有剩余
            //获取卡信息
            $cardInfo = db('cardshop')->where(['id'=>$info['cardshop_id']])->find();
            if (!$cardInfo) {
                $this->error('该超市购物卡已下架！');
            }
            if (1>($cardInfo['total_num']-$cardInfo['use_num'])) {
                $this->error('折扣卡已申请完，申请失败');
            }
            $start_no = $cardInfo['start_no'];//开始派发卡号
            //获取该超市已经发放出去的最大卡号
            $max_card_no = db('cardshop_baoming')->where(['cardshop_id'=>$info['cardshop_id']])->order('card_no desc')->value('card_no');
            if ($max_card_no) {
                $start_no = $max_card_no+1;
            }
            $data['status'] = 1;
            $data['card_no'] = $start_no;
            Db::startTrans();
            unset($data['__token__']);
            $data['cardshop_id'] = $info['cardshop_id'];
            $data['user_id'] = $this->_user->id;
            $data['uname'] = $this->_user->username;
            $data['idcard'] = $this->_user->idcard;
            $data['mobile'] = $this->_user->mobile;
            $data['add_time'] = time();
            $res = db('cardshop_baoming')->insert($data);
            if ($res) {
                //监听申请事件
                /*$obj_id = db('cardshop_baoming')->getLastInsID();
                $infodata = ['type'=>18,'obj_id'=>$obj_id,'data'=>$data];
                Hook::listen("user_apply", $infodata);*/
                $rs2 = db('cardshop')->where(['id'=>$info['cardshop_id']])->update(['use_num'=>$cardInfo['use_num']+1]);
                if(!$rs2){
                    Db::rollback();
                    $this->error('报名失败！');
                }
                Db::commit();
                $this->success('报名成功','shopcard_discount');
            }else{
                Db::rollback();
                $this->error('报名失败');
            }
        }
        //获取活动信息
        $info = db('cardshop_active')->where(['status'=>1,'id'=>$active_id])->order('id desc')->find();
        $active_status = 0;//活动状态
        if ($info) {
            $nowtime = time();
            if ($info['starttime']>$nowtime) {
                $active_status = 1;//活动暂未开始
            }elseif($info['endtime']<$nowtime){
                $active_status = 2;//活动已结束
            }else{
                $active_status = 3;//活动进行中
            }
        }
        $info['active_status'] = $active_status;
        //报名须知
        $intro = cache('cardshopActiveIntro','');
        $this->view->assign('intro',$intro);

        $this->view->assign('info', $info);
        $this->view->assign('userinfo', $this->_user);
        $this->view->assign('title', '工会员工购物卡优惠');
        return $this->view->fetch();
    }
    //我的购物卡
    public function shopcard_list(){
        //润升百货倒闭，过滤掉
        $list = db('cardshop_baoming')->where(['user_id'=>$this->_user->id,'status'=>1,'cardshop_id'=>['<>',9]])->select();
        foreach ($list as $key => &$val) {
            $cardshop = db('cardshop')->where(['id'=>$val['cardshop_id']])->field('id,name,logo,bgimg')->find();
            $val['logo'] = $cardshop['logo']??'/assets/img/yiwangjia.png';
            $val['cardshop_name'] = $cardshop['name']??'益万家';
            $val['imagebg'] = $cardshop['bgimg']?$cardshop['bgimg']:'/assets/img/xfqcard1.png';
        }
        $this->view->assign('list', $list);
        //$this->view->assign('userinfo', $this->_user);
        $this->view->assign('title', '我的购物折扣卡');
        return $this->view->fetch();
    }
    //购物卡详情
    public function shopcard_info($id){
        $id = intval($id);
        $info = db('cardshop_baoming')->where(['id'=>$id])->find();
        $cardshop = db('cardshop')->where(['id'=>$info['cardshop_id']])->field('logo')->find();
        $info['logo'] = $cardshop['logo']??'/assets/img/ywj2.jpg';
        //生成条形码
        if ($info['card_no']) {
            $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
            $barcode = $generator->getBarcode($info['card_no'],$generator::TYPE_CODE_128,2,80);
            $barcode = base64_encode($barcode);
            $barcode = "data:image/png;base64,".$barcode;
        }else{
            $barcode = '';
        }


        $this->view->assign('info', $info);
        $this->view->assign('barcode', $barcode);
        //$this->view->assign('userinfo', $this->_user);
        $this->view->assign('title', '我的购物折扣卡');
        return $this->view->fetch();
    }

    public function noservice(){
        $this->view->assign('title', '服务暂未开通');
        return $this->view->fetch();
    }

    public function send_file($type=1){
        $type = intval($type);
        if ($this->request->isPost()) {
            $data = $this->request->post();
            $rule = [
                'email'  => 'require|email',
                '__token__' => 'require|token'
            ];

            $msg = [
                'email'   => '邮箱必须',
                'email.email'=>'邮箱格式错误'
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            $url = '';
            $content = '';
            if ($data['type']==1) {//劳模工匠创新申请模版
                $url = config('site.model_craftsman_tpl');
            }elseif ($data['type']==2) {//职工互助保障模版
                $url = config('site.employees_assistance_tpl');
            }elseif ($data['type']==3) {//学历培训申请模板
                $url = config('site.skills_education_tpl');
            }elseif ($data['type']==4) {//活动场地优惠申请
                $url = config('site.site_discount_tpl');
            }elseif ($data['type']==5) {//上学优惠申请模板
                $url = config('site.school_discount_tpl');
            }elseif ($data['type']==6) {//技能培训申请模板
                $url = config('site.skills_education_two_tpl');
            }elseif ($data['type']==7) {//困难帮扶申请模板
                $url = config('site.kunnan_tpl');
            }elseif ($data['type']==8) {//欠薪求助申请模板
                $url = config('site.arrears_help_tpl');
            }elseif ($data['type']==9){//资金上报（工会会计科目使用说明下载）
                $url = config('site.capital_tpl');
            }elseif ($data['type']==14){//圆梦助学
                $url = config('site.dream_apply_tpl');
            }elseif ($data['type']==61){//党员创新工作室申报
                $url = config('site.party_model_craftsman_tpl');
            }

            if (!$url) {
                $this->error('该模版文件未配置，发送失败');
            }
            $url = $this->request->domain().$url;
            $content = '<a href="'.$url.'">点击此处下载模版文件>></a>';
            $rs = QueueApi::sendEmail("模版文件下载",$data['email'],$content);
            if ($rs) {
                $this->success('邮件发送成功，请注意查收');
            }else{
                $this->error('发送失败');
            }
        }
        $this->view->assign('type', $type);
        $this->view->assign('title', '文件模版下载');
        return $this->view->fetch();
    }


    //保存草稿信息
    public function saveCg($type=''){
        if ($type && $this->request->isPost()) {
            $data = $this->request->post();
            unset($data['__token__']);
            if ($data) {
                $key = $type."_".$this->_user->id;
                cache($key,$data,24*3600);
                $this->success('草稿保存成功');
            }else{
                $this->error('无数据保存');
            }
        }else{
            $this->error('无数据保存');
        }
    }
    //删除缓存
    public function delCache($type=""){
        if ($type) {
            $key = $type."_".$this->_user->id;
            cache($key,null);
            $this->success('草稿删除成功');
        }else{
            $this->error('删除失败');
        }
    }
    //删除草稿缓存-内部调用
    public function delCg($type=""){
        if ($type) {
            $key = $type."_".$this->_user->id;
            cache($key,null);
        }
    }

    //操作说明
    public function kpi_intro($type=1,$id=0){
        if ($type==1) {
            $intro = db('kpi')->where(['id'=>$id])->value('intro');
        }else{
            $intro = db('party_kpi')->where(['id'=>$id])->value('intro');
        }
        $this->view->assign('intro', $intro);
        $this->view->assign('title', '操作说明');
        return $this->view->fetch();
    }

    //联谊报名
    public function lianyi_select(){
    	//判断是否报名过
        $id = db('lianyi')->where(['user_id'=>$this->_user->id])->value('id');
        if ($id) {
        	$url = url('lianyi_edit',['id'=>$id]);
            $this->redirect($url);
        }
        $this->view->assign('title', '单身职工交友');
        return $this->view->fetch();
    }
    public function lianyi($type=1){
        $type = intval($type);
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                'education'  => 'require',
                'income'    => 'require',
                'images'    => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                'education'  => '请选择学历',
                'income'     => '请选择收入',
                'images'    => '请上传生活照'
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            //判断是否报名过
            $bm = db('lianyi')->where(['user_id'=>$this->_user->id])->value('id');
            if ($bm) {
                $this->error('您已报名过，无需重复报名');
            }
            //默认值
            $data['hobby'] = $data['hobby']?$data['hobby']:'无';
            $data['skills'] = $data['skills']?$data['skills']:'无';
            $data['f_character'] = $data['f_character']?$data['f_character']:'无';
            $data['f_job'] = $data['f_job']?$data['f_job']:'无';
            $data['f_income'] = $data['f_income']?$data['f_income']:'无';
            $data['f_height'] = $data['f_height']?$data['f_height']:'无';
            $data['s_age'] = $data['s_age']?$data['s_age']:'无';
            $data['s_city'] = $data['s_city']?$data['s_city']:'无';

            unset($data['__token__']);
            $data['user_id'] = $this->_user->id;
            $data['company_id'] = $this->_user->company_id;
            $data['add_time'] = date('Y-m-d H:i:s');
            $res = db('lianyi')->insert($data);
            if ($res) {
                $this->success('上报成功','index');
            }else{
                $this->error('上报失败');
            }

        }
        //判断是否报名过
        $bm = db('lianyi')->where(['user_id'=>$this->_user->id])->value('id');
        if ($bm) {
            $this->redirect('lianyi_edit/id/'.$bm);
        }
        //先暂停男性报名
        if ($type==1) {
            $this->error('报名人数已满');
        }
        //判断年龄
        $idcard = $this->_user->idcard;
        $birthday = substr($idcard, 6,8);
        $birthday = date('Y-m-d',strtotime($birthday));
        $year = date('Y');
        $birth = substr($idcard, 6,4);
        $age = $year-$birth;
        $this->view->assign('name', $this->_user->username);
        $this->view->assign('mobile', $this->_user->mobile);
        $this->view->assign('birthday', $birthday);
        $this->view->assign('age', $age);
        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('type', $type);
        $this->view->assign('title', '单身职工交友报名');
        return $this->view->fetch();
    }

    public function lianyi_edit($id=0){
        $id = intval($id);
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                'education'  => 'require',
                'income'    => 'require',
                'images'    => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                'education'  => '请选择学历',
                'income'     => '请选择收入',
                'images'    => '请上传生活照'
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            //默认值
            $data['hobby'] = $data['hobby']?$data['hobby']:'无';
            $data['skills'] = $data['skills']?$data['skills']:'无';
            $data['f_character'] = $data['f_character']?$data['f_character']:'无';
            $data['f_job'] = $data['f_job']?$data['f_job']:'无';
            $data['f_income'] = $data['f_income']?$data['f_income']:'无';
            $data['f_height'] = $data['f_height']?$data['f_height']:'无';
            $data['s_age'] = $data['s_age']?$data['s_age']:'无';
            $data['s_city'] = $data['s_city']?$data['s_city']:'无';

            unset($data['__token__']);
            $data['status'] = 0;
            $res = db('lianyi')->where('id',$id)->update($data);
            if ($res) {
                $this->success('修改成功','index');
            }else{
                $this->error('修改失败');
            }

        }
        $info = db('lianyi')->where(['id'=>$id])->find();
        
        $this->view->assign('info', $info);
        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('title', '单身职工交友报名');
        return $this->view->fetch();
    }

    //民主管理
    public function mzgl(){
        return $this->view->fetch();
    }
    //议题征集
    public function proposal_add(){
        if ($this->request->isPost()) {
            $data = $this->request->post();

            $rule = [
                'title'  => 'require',
                'content'    => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                'title'  => '提案名称必须',
                'content'     => '提案内容必须',
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            $data['status'] = 0;
            $data['company_id'] = $this->_user->company_id;
            $data['user_id'] = $this->_user->id;
            $data['vote_end_time'] = 0;
            $data['add_time'] = time();
            $res = db('proposal')->insert($data);
            if ($res) {
                $this->success('待工会主席处理后，请在“线上职工大会”处查看','mzgl');
            }else{
                $this->error('添加失败');
            }

        }
        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('username', $this->_user->username);
        $this->view->assign('mobile', $this->_user->mobile);
        $this->view->assign('datetime', date('Y年m月d日H:i'));
        return $this->view->fetch();
    }

    //议题列表
    public function proposal_list(){
        $list = db('proposal')->where(['status'=>['gt',1],'company_id'=>$this->_user->company_id])->order('add_time desc')->limit(100)->select();
        foreach ($list as $key => &$val) {
            if ($val['vote_end_time']<time()) {
                $val['vote_status'] = 0;
            }else{
                $val['vote_status'] = 1;
            }
            //计算评论数量
            $val['plnum'] = db('proposal_vote')->where(['proposal_id'=>$val['id'],'type'=>2])->count();
        }
        $this->view->assign('list', $list);
        return $this->view->fetch();
    }

    //投票详情
    public function proposal_tplist($id,$status){
        $list = db('proposal_vote')->where(['proposal_id'=>$id,'type'=>1,'status'=>$status])->order('add_time desc')->select();
        foreach ($list as $key => &$val) {
            $val['username'] = db('user')->where('id='.$val['user_id'])->value('username');
        }
        $this->view->assign('list', $list);
        return $this->view->fetch();
    }

    //议题详情
    public function proposal_info($id=0){
        $info = db('proposal')->where(['id'=>$id])->find();
        if ($this->request->isPost()) {
            if (!$info) {
                $this->error('议题获取失败');
            }
            $data = $this->request->post();
            $rule = [
                'type'  => 'require',
                'status'    => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                'type'  => '参数不足',
                'status'     => '参数不足',
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            if (time()>$info['vote_end_time']) {
                $this->error('议题已结束，不能投票和讨论');
            }
            if ($data['type']==1) {//判断是否已投票
                $htp = db('proposal_vote')->where(['proposal_id'=>$id,'type'=>1,'user_id'=>$this->_user->id])->count();
                if ($htp>0) {
                    $this->error('您已经投过票，不能重复投票');
                }
            }
            unset($data['__token__']);
            $data['proposal_id'] = $id;
            $data['company_id'] = $this->_user->company_id;
            $data['user_id'] = $this->_user->id;
            $data['add_time'] = time();
            $res = db('proposal_vote')->insert($data);
            if ($res) {
                $this->success('操作成功','mzgl');
            }else{
                $this->error('操作失败');
            }

        }
        $info['username'] = db('user')->where('id='.$info['user_id'])->value('username');
        //统计同意和不同意数量
        $info['qq_num'] = db('proposal_vote')->where(['proposal_id'=>$id,'status'=>0,'type'=>1])->count();//弃权
        $info['agree_num'] = db('proposal_vote')->where(['proposal_id'=>$id,'status'=>1,'type'=>1])->count();
        $info['noagree_num'] = db('proposal_vote')->where(['proposal_id'=>$id,'status'=>2,'type'=>1])->count();
        //获取评论信息
        $list = db('proposal_vote')->where(['proposal_id'=>$id,'type'=>2])->order('add_time desc')->select();
        foreach ($list as $key => &$val) {
            $val['username'] = db('user')->where('id='.$val['user_id'])->value('username');
        }
        $info['vote_list'] = $list;
        $this->view->assign('info', $info);
        return $this->view->fetch();
    }

    //厂务信息列表
    public function factory_information_list(){
        $list = db('factory_information')->where(['company_id'=>$this->_user->company_id])->order('add_time desc')->limit(100)->select();
        $this->view->assign('list', $list);
        return $this->view->fetch();
    }

    //厂务信息详情
    public function factory_information_info($id){
        $info = db('factory_information')->where(['id'=>$id])->find();
        $this->view->assign('info', $info);
        return $this->view->fetch();
    }

    //议题文档下载
    public function downloadProposal($id){
        $info = db('proposal')->field('title,content')->where('id='.$id)->find();
        if (!$info) {
            $this->error('该文档不存在');
        }
        //打开缓冲区 
        ob_start(); 
        header("Cache-Control: public"); 
        Header("Content-type: application/octet-stream"); 
        Header("Accept-Ranges: bytes"); 
        
        $filename=$info['title'];
        //判断浏览器类型
        if (strpos($_SERVER["HTTP_USER_AGENT"],'MSIE')) { 
         header('Content-Disposition: attachment; filename='.$filename.'.doc'); 
        }else if (strpos($_SERVER["HTTP_USER_AGENT"],'Firefox')) { 
         Header('Content-Disposition: attachment; filename='.$filename.'.doc'); 
        } else { 
         header('Content-Disposition: attachment; filename='.$filename.'.doc'); 
        } 
         
        //不使用缓存
        header("Pragma:no-cache"); 
        //过期时间 
        header("Expires:0");
        echo '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns="http://www.w3.org/TR/REC-html40">
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
            <xml><w:WordDocument><w:View>Print</w:View></xml>
            </head><body>';
        echo '<h3 style="text-align: center">'.$info['title'].'</h3>';
        echo $info['content'];
        echo "</body></html>"; 
        //输出全部内容到浏览器 
        ob_end_flush();
    }

    //民主评议
    public function mzpy(){
        $this->view->assign('title', '民主评议');
        return $this->view->fetch();
    }

    //主席评议
    public function zxpy_bak(){
        //获取最近的主题
        $topId = db('topic')->order("id desc")->value("id");
        //判断是否已评议
        $isPy = db('zhuxi_pingyi')->where(['topic_id'=>$topId,'user_id'=>$this->_user->id])->count();
        if ($this->request->isPost()) {
            if (!$topId) {
                $this->error('暂无评议主题');
            }
            if ($isPy>0) {
                $this->error('您已经评议过，不能重复评议');
            }
            $data = $this->request->post();
            $rule = [
                'content'  => 'require',
                'status'    => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                'content'  => '请输入评议内容',
                'status'     => '请选择满意或不满意',
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            $data['topic_id'] = $topId;
            $data['company_id'] = $this->_user->company_id;
            $data['user_id'] = $this->_user->id;
            $data['username'] = $this->_user->username;
            $data['add_time'] = time();
            $res = db('zhuxi_pingyi')->insert($data);
            if ($res) {
                $this->success('操作成功','zxpy');
            }else{
                $this->error('操作失败');
            }
        }
        //获取评议简介
        $pyInfo = db('company')->where('id='.$this->_user->company_id)->field('pingyi_image,pingyi_intro')->find();
        $pyList = db('zhuxi_pingyi')->where('topic_id='.$topId.' and company_id='.$this->_user->company_id)->field('username,status,content,add_time')->order('id desc')->limit(300)->select();
        //获取评议总数和满意度
        $total = db('zhuxi_pingyi')->where('topic_id='.$topId.' and company_id='.$this->_user->company_id)->count();
        $totalMy = db('zhuxi_pingyi')->where('topic_id='.$topId.' and status=1 and company_id='.$this->_user->company_id)->count();
        $rate = $total==0?0:round($totalMy/$total,2)*100;
        $this->view->assign('isPy',$isPy);
        $this->view->assign('total',$total);
        $this->view->assign('rate',$rate);
        $this->view->assign('pyInfo', $pyInfo);
        $this->view->assign('pyList', $pyList);
        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('title', '本届工会主席评议');
        return $this->view->fetch();
    }

    //我的评议
    public function myzxpy(){
        //获取最近的主题
        $topId = db('topic')->order("id desc")->value("id");
        $pyList = db('zhuxi_pingyi')->where('topic_id='.$topId.' and company_id='.$this->_user->company_id.' and user_id='.$this->_user->id)->field('username,status,content,add_time')->order('id desc')->limit(300)->select();
        $this->view->assign('pyList', $pyList);
        $this->view->assign('title', '我的评议记录');
        return $this->view->fetch();
    }

    //建设评议
    public function jspy(){
        //获取最近的主题
        $topId = db('topic')->order("id desc")->value("id");
        //判断是否已评议
        $pyInfo = db('jianse_pingyi')->where(['topic_id'=>$topId,'user_id'=>$this->_user->id])->find();
        if ($this->request->isPost()) {
            if (!$topId) {
                $this->error('暂无评议主题');
            }
            if ($pyInfo) {
                $this->error('您已经评议过，不能重复评议');
            }
            $data = $this->request->post();
            $rule = [
                'ztgz'  => 'require',
                'wthd'    => 'require',
                'zgbf'    => 'require',
                'mzgz'    => 'require',
                'ldgx'    => 'require',
                'ghzz'    => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                'ztgz'  => '总体工作必须',
                'wthd'    => '文体活动必须',
                'zgbf'    => '职工帮扶必须',
                'mzgz'    => '民主工作必须',
                'ldgx'    => '劳动关系必须',
                'ghzz'    => '工会活动必须',
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            $data['topic_id'] = $topId;
            $data['company_id'] = $this->_user->company_id;
            $data['user_id'] = $this->_user->id;
            $data['username'] = $this->_user->username;
            $data['add_time'] = time();
            $res = db('jianse_pingyi')->insert($data);
            if ($res) {
                $this->success('操作成功','jspyinfo');
            }else{
                $this->error('操作失败');
            }
        }
        //获取评议总数和满意度
        $total = db('jianse_pingyi')->where('topic_id='.$topId.' and company_id='.$this->_user->company_id)->count();
        $totalMy = db('jianse_pingyi')->where('topic_id='.$topId.' and ztgz=1 and company_id='.$this->_user->company_id)->count();
        $rate = $total==0?0:round($totalMy/$total,2)*100;
        $this->view->assign('pyInfo',$pyInfo);
        $this->view->assign('total',$total);
        $this->view->assign('rate',$rate);
        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('title', '本届工会建设评议');
        return $this->view->fetch();
    }

    //建设评议详情
    public function jspyinfo(){
        //获取最近的主题
        $topId = db('topic')->order("id desc")->value("id");
        //获取评议总数和满意度
        $total = db('jianse_pingyi')->where('topic_id='.$topId.' and company_id='.$this->_user->company_id)->count();
        $total1 = db('jianse_pingyi')->where('topic_id='.$topId.' and ztgz=1 and company_id='.$this->_user->company_id)->count();
        $total2 = db('jianse_pingyi')->where('topic_id='.$topId.' and wthd=1 and company_id='.$this->_user->company_id)->count();
        $total3 = db('jianse_pingyi')->where('topic_id='.$topId.' and zgbf=1 and company_id='.$this->_user->company_id)->count();
        $total4 = db('jianse_pingyi')->where('topic_id='.$topId.' and mzgz=1 and company_id='.$this->_user->company_id)->count();
        $total5 = db('jianse_pingyi')->where('topic_id='.$topId.' and ldgx=1 and company_id='.$this->_user->company_id)->count();
        $total6 = db('jianse_pingyi')->where('topic_id='.$topId.' and ghzz=1 and company_id='.$this->_user->company_id)->count();
        $rate1 = $total==0?0:round($total1/$total,2)*100;
        $rate2 = $total==0?0:round($total2/$total,2)*100;
        $rate3 = $total==0?0:round($total3/$total,2)*100;
        $rate4 = $total==0?0:round($total4/$total,2)*100;
        $rate5 = $total==0?0:round($total5/$total,2)*100;
        $rate6 = $total==0?0:round($total6/$total,2)*100;
        $this->view->assign('total',$total);
        $this->view->assign('rate1',$rate1);
        $this->view->assign('rate2',$rate2);
        $this->view->assign('rate3',$rate3);
        $this->view->assign('rate4',$rate4);
        $this->view->assign('rate5',$rate5);
        $this->view->assign('rate6',$rate6);
        $this->view->assign('title', '满意度详情');
        return $this->view->fetch();
    }

    //主席评议
    public function zxpy(){
        //获取最近的主题
        $topId = db('topic')->order("id desc")->value("id");
        //判断是否已评议
        $pyInfo = db('zhuxi_pingyi')->where(['topic_id'=>$topId,'user_id'=>$this->_user->id])->find();
        if ($this->request->isPost()) {
            if (!$topId) {
                $this->error('暂无评议主题');
            }
            if ($pyInfo) {
                $this->error('您已经评议过，不能重复评议');
            }
            $data = $this->request->post();
            $rule = [
                'field1'  => 'require',
                'field2'    => 'require',
                'field3'    => 'require',
                'field4'    => 'require',
                'field5'    => 'require',
                'field6'    => 'require',
                'field7'    => 'require',
                '__token__' => 'require|token'
            ];

            $msg = [
                'field1'  => '政治思想道德及廉洁自律必须',
                'field2'    => '工会业务能力及组织协调沟通能力必须',
                'field3'    => '工会内部管理规范有序必须',
                'field4'    => '关心职工热心解决职工困难必须',
                'field5'    => '依法维护职工合法权益必须',
                'field6'    => '民主参与企业管理必须',
                'field7'    => '开展职工喜闻乐见活动必须',
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            unset($data['__token__']);
            $data['topic_id'] = $topId;
            $data['company_id'] = $this->_user->company_id;
            $data['user_id'] = $this->_user->id;
            $data['username'] = $this->_user->username;
            $data['add_time'] = time();
            $res = db('zhuxi_pingyi')->insert($data);
            if ($res) {
                $this->success('操作成功','zxpyinfo');
            }else{
                $this->error('操作失败');
            }
        }
        //获取评议总数和满意度
        $total = db('zhuxi_pingyi')->where('topic_id='.$topId.' and company_id='.$this->_user->company_id)->count();
        $totalMy = db('zhuxi_pingyi')->where('topic_id='.$topId.' and field1=1 and company_id='.$this->_user->company_id)->count();
        $rate = $total==0?0:round($totalMy/$total,2)*100;
        $this->view->assign('pyInfo',$pyInfo);
        $this->view->assign('total',$total);
        $this->view->assign('rate',$rate);
        $this->view->assign('company_name', $this->_user->company_name);
        $this->view->assign('title', '本届工会主席评议');
        return $this->view->fetch();
    }

    //建设评议详情
    public function zxpyinfo(){
        //获取最近的主题
        $topId = db('topic')->order("id desc")->value("id");
        //获取评议总数和满意度
        $total = db('zhuxi_pingyi')->where('topic_id='.$topId.' and company_id='.$this->_user->company_id)->count();
        $total1 = db('zhuxi_pingyi')->where('topic_id='.$topId.' and field1=1 and company_id='.$this->_user->company_id)->count();
        $total2 = db('zhuxi_pingyi')->where('topic_id='.$topId.' and field2=1 and company_id='.$this->_user->company_id)->count();
        $total3 = db('zhuxi_pingyi')->where('topic_id='.$topId.' and field3=1 and company_id='.$this->_user->company_id)->count();
        $total4 = db('zhuxi_pingyi')->where('topic_id='.$topId.' and field4=1 and company_id='.$this->_user->company_id)->count();
        $total5 = db('zhuxi_pingyi')->where('topic_id='.$topId.' and field5=1 and company_id='.$this->_user->company_id)->count();
        $total6 = db('zhuxi_pingyi')->where('topic_id='.$topId.' and field6=1 and company_id='.$this->_user->company_id)->count();
        $total7 = db('zhuxi_pingyi')->where('topic_id='.$topId.' and field7=1 and company_id='.$this->_user->company_id)->count();
        $rate1 = $total==0?0:round($total1/$total,2)*100;
        $rate2 = $total==0?0:round($total2/$total,2)*100;
        $rate3 = $total==0?0:round($total3/$total,2)*100;
        $rate4 = $total==0?0:round($total4/$total,2)*100;
        $rate5 = $total==0?0:round($total5/$total,2)*100;
        $rate6 = $total==0?0:round($total6/$total,2)*100;
        $rate7 = $total==0?0:round($total7/$total,2)*100;
        $this->view->assign('total',$total);
        $this->view->assign('rate1',$rate1);
        $this->view->assign('rate2',$rate2);
        $this->view->assign('rate3',$rate3);
        $this->view->assign('rate4',$rate4);
        $this->view->assign('rate5',$rate5);
        $this->view->assign('rate6',$rate6);
        $this->view->assign('rate7',$rate7);
        $this->view->assign('title', '满意度详情');
        return $this->view->fetch();
    }
}