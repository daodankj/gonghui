<?php
namespace app\index\controller;

use app\common\controller\Frontend;
use addons\faqueue\library\QueueApi;
use app\common\library\Email;
use think\Db;

class Test extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = '';

    public function test(){
        /*$word = new \COM("word.application") or die('can not start word');
        echo $word->Version;
        $word->Visible = 0;
        $word->Documents->open('D:\www\gonghui\public\uploads\1.doc');
        $test = $word->ActiveDocument->content->Text;
        echo $test;*/
    	//$rs = QueueApi::sendEmail("任务预警通知",'498129372@qq.com','测试');
        //$rs = QueueApi::smsNotice('18588730193','这是一条总工会测试短信，验证短信配置是否正常');
    	//$obj = \app\common\library\Email::instance();
          //$obj = new Email();
          //$rs = $obj->to('498129372@qq.com')->subject('验证码')->message('你的验证码是：3425')->send();

	   // print_r($rs);
    }
    //积分计算
    public function countType(){
        $scoreLog = db('company_score_log')->where(['union_type'=>''])->field('id,after,union_type')->select();
        //判断等级
        $kpiConfig = db('kpi_config')->where(['id'=>1])->find();
                
        foreach ($scoreLog as $key => &$val) {
            $score = $val['after'];
            if ($score>=$kpiConfig['A']) {
                $type = 'A';
            }elseif ($score>=$kpiConfig['B']) {
                $type = 'B';
            }elseif ($score>=$kpiConfig['C']) {
                $type = 'C';
            }elseif ($score>=$kpiConfig['D']) {
                $type = 'D';
            }else{
                $type = 'D';
            }
            db('company_score_log')->where(['id'=>$val['id']])->update(['union_type'=>$type]);
        }
    }
    //条形码
    public function barcode(){
        //html格式
        //$generator = new \Picqer\Barcode\BarcodeGeneratorHTML();
        //echo $generator->getBarcode('123456',$generator::TYPE_CODE_128,2,50,'red');
        //PNG
        $generator = new \Picqer\Barcode\BarcodeGeneratorPNG();
        $barcode = $generator->getBarcode('123456',$generator::TYPE_CODE_128);
        $barcode = base64_encode($barcode);
        echo  '<img src="data:image/png;base64,'.$barcode.'" />';
    }

    //地图
    public function map(){
        
    }

    public function getCompanyList(){
        db('union')->field('id,company_id,phone')->chunk(1, function ($items) {
                $items = collection($items)->toArray();
                foreach ($items as $index => $item) {
                    //判断是否已经完成了任务
                    /*$submit_num = db('kpi_list')->where(['kpi_id'=>1,'company_id'=>$item['id']])->count();
                    if ($submit_num>=1) {
                        continue;
                    }*/
                    echo $item['phone']."<br/>";
                }
            });
        return;
    }

    public function outport(){
        $db = Db::connect('mysql://root:@localhost:3306/fk#utf8');
        ob_end_clean(); 
        ob_start(); 
        $filename='人员信息列表'.date("YmdHis");
        header("Content-type:application/octet-stream");
        header("Accept-Ranges:bytes");
        header("Content-type:application/vnd.ms-excel");
        header("Content-Disposition:attachment;filename=".$filename.".xls");
        header("Pragma: no-cache");
        header("Expires: 0");
        echo '<table border=1>';
        echo '<tr height="30" align="center">';
        echo '<th>姓名</th>';
        echo '<th>电话</th>';
        echo '<th>身份证号码</th>';
        echo '<th>居住地址</th>';
        echo '<th>籍贯</th>';
        $db->table('fa_worker')->field('id,name,sex,mobile,idcard,address,faddress')->where('idcard >0 and YEAR (NOW()) - substring(idcard, 7, 4) between 17 and 120')->chunk(10000, function ($items) {
            $items = collection($items)->toArray();
            foreach ($items as $index => $item) {
                echo '<tr height="30" align="center">';
                echo '<th>'.$item['name'].'</th>';
                echo '<th>'.$item['mobile'].'</th>';
                echo "<th>'".$item['idcard'].'</th>';
                echo "<th>".$item['address'].'</th>';
                echo "<th>".$item['faddress'].'</th>';
            }
        });
        echo '</table>';
    }

}

