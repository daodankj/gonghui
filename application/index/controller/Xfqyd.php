<?php

namespace app\index\controller;

use app\common\controller\Frontend;
use think\Config;
use think\Cookie;
use think\Hook;
use think\Session;
use think\Validate;
use tool\Jssdk;
use think\Db;
use tool\CacheRedis;
/**
 * 消费券预约
 */
class Xfqyd extends Frontend
{
    protected $layout = 'fk';
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];
    public function _initialize()
    {
        parent::_initialize();
        $auth = $this->auth;
    }

    /**
     * 空的请求
     * @param $name
     * @return mixed
     */
    public function _empty($name)
    {
        
        return '请求地址不存在';
    }

    /**
     * 
     */
    public function index()
    {   
        $idCardNumber = $this->request->get('idCardNumber');
        $uname = $this->request->get('uname');
        $mobile = $this->request->get('tel');

        //获取菜单呵海报
        $image = db('xfq_config')->where(['id'=>1])->value('index_image');
        $menu_list = db('xfq_menu')->where(['status'=>1])->order('weigh desc')->select();
        foreach ($menu_list as $key => &$val) {
            if ($val['url']) {
                if ($uname) {
                    $val['url'].= '/uname/'.$uname;
                }
                if ($idCardNumber) {
                    $val['url'].= '/idCardNumber/'.$idCardNumber;
                }
                if ($mobile) {
                    $val['url'].= '/mobile/'.$mobile;
                }
            }
        }
        $this->view->assign('image', $image);
        $this->view->assign('menu_list', $menu_list);
        
    
        $this->view->assign('uname', $uname);
        $this->view->assign('idCardNumber', $idCardNumber);
        $this->view->assign('mobile', $mobile);
        $this->view->assign('title', '券享大旺');
        return $this->view->fetch();
    }

    public function sy()
    {
        $idCardNumber = $this->request->get('idCardNumber');
        $uname = $this->request->get('uname');
        $mobile = $this->request->get('tel');

        //获取菜单呵海报
        $image = db('xfq_config')->where(['id'=>1])->value('index_image');
        $menu_list = db('xfq_menu')->where(['status'=>1])->order('weigh desc')->select();
        foreach ($menu_list as $key => &$val) {
            if ($val['url']) {
                if ($uname) {
                    $val['url'].= '/uname/'.$uname;
                }
                if ($idCardNumber) {
                    $val['url'].= '/idCardNumber/'.$idCardNumber;
                }
                if ($mobile) {
                    $val['url'].= '/mobile/'.$mobile;
                }
            }
        }
        $this->view->assign('image', $image);
        $this->view->assign('menu_list', $menu_list);
        
    
        $this->view->assign('uname', $uname);
        $this->view->assign('idCardNumber', $idCardNumber);
        $this->view->assign('mobile', $mobile);
        $this->view->assign('title', '券享大旺');
        return $this->view->fetch('index');
    }
    
    //市民预约
    public function smyy($uname='',$idCardNumber='',$mobile=''){
        if ($this->request->isPost()) {
            $data  =  $this->request->post();
            if (!isset($data['mobile'])||!$data['mobile']) {
                $this->error('电话号码必须');
            }
            //判断预约活动是否结束
            $nowtime = time();
            $active = db('xfq_active')->where(['status'=>1])->order('id desc')->find();
            if (!$active||$active['starttime']>$nowtime||$active['endtime']<$nowtime) {
                $this->error('活动未开始或已结束,请留意报名时间');
            }
            //判断是否已经报名过
            $is_baoming = db('xfq_baoming')->where(['idcard'=>$data['idcard'],'active_id'=>$active['id']])->value('id');
            if ($is_baoming) {
                $this->error('已经报名过，无需重新报名');
            }
            //获取当前活动最大序号
            $maxNo = db('xfq_baoming')->where(['active_id'=>$active['id']])->order('no desc')->value('no');
            $sendlist = [
                'active_id'=>$active['id'],
                'uname'=>$data['uname'],
                'idcard'=>$data['idcard'],
                'mobile'  => $data['mobile'],
                'add_time'=>date('Y-m-d H:i:s'),
                'no'=>$maxNo+1
            ];
            try {
                $rs = db('xfq_baoming')->insert($sendlist);
            } catch (\Exception $e) {
                $this->error('报名失败,请重试');
            }
            //db('xfq_baoming1')->insert($sendlist);//新增一份总记录
            if (!$rs) {
                $this->error('报名失败,请稍后再试');
            }
            $this->success('报名成功,中签结果公布后可以在预约查询中查询中签结果');
            exit;
        }
        //获取最后一个活动
        $nowtime = time();
        $info = db('xfq_active')->where(['status'=>1])->order('id desc')->find();
        $active_status = 0;//活动状态
        if ($info) {
            if ($info['starttime']>$nowtime) {
                $active_status = 1;//活动暂未开始
            }elseif($info['endtime']<$nowtime){
                $active_status = 2;//活动已结束
            }else{
                $active_status = 3;//活动进行中
            }
            $info['active_status'] = $active_status;
        }
        
        $this->view->assign('info', $info);

        $this->view->assign('uname', $uname);
        $this->view->assign('idCardNumber', $idCardNumber);
        $this->view->assign('mobile', $mobile);
        $this->view->assign('title', '市民报名');
        return $this->view->fetch();
    }
    
	//市民中签详情
    public function sminfo($id=0,$idcard=''){
        $data  =  $this->request->post();
        $list = [];
        //获取最后一次活动
        $active_id = db('xfq_active')->where(['status'=>1])->value('id');
        if ($active_id) {
            $list = db('xfq_zhongqian')->where(['active_id'=>$active_id,'idcard'=>$data['idcard']])->select();
        }
        foreach ($list as $key => &$val) {
            $val['qname'] = db('xfq')->where(['id'=>$val['xfq_id']])->value('name');
            $val['aname'] = db('xfq_active')->where(['id'=>$val['active_id']])->value('name');
        }
        $this->view->assign('list',$list);
        $this->view->assign('title', '中签详情');
        return $this->view->fetch();
    }


    //二维码查询
    public function search($uname='',$idCardNumber='',$mobile=''){
        $this->view->assign('uname', $uname);
        $this->view->assign('idCardNumber', $idCardNumber);
        $this->view->assign('mobile', $mobile);
        $this->view->assign('title', '中签查询');
        return $this->view->fetch();
    }


    //商户信息采集
    public function merchant(){
        if ($this->request->isPost()) {
            $data  =  $this->request->post();
            if (!$data['name']) {
                $this->error('商户名称必须');
            }
			 if (!$data['enum']) {
                $this->error('社会信用统一代码必须');
            }
            if (!$data['ylcode']) {
                $this->error('银联商户编号必须');
            }
            if (!$data['lat']||!$data['lng']) {
                $this->error('请在地址下拉框中选择一个地址');
            }
            //判断该企业是否提交过
            $sub =  db('merchat')->where(['enum'=>$data['enum']])->value('id');
            if ($sub) {
                $this->error('该商户已经提交过，无需再提交');
            }
            unset($data['__token__']);
            $data['add_time'] = date('Y-m-d H:i:s');
			$info = db('companylist')->where(['enum'=>$data['enum']])->find();
			if($info){
                   $ret = db('merchat')->insert($data);
                   if($ret){
                    $this->success('提交成功');
                   }else {
                    $this->error('提交失败');
                   }
            }else {
                $this->error('没有查询到该企业信息');
            }
        
        }
       /* $idCardNumber = $this->request->get('idCardNumber');
        $uname = $this->request->get('uname');
        $mobile = $this->request->get('tel');


        $this->view->assign('uname', $uname);
        $this->view->assign('idCardNumber', $idCardNumber);
        $this->view->assign('mobile', $mobile);*/
        $this->view->assign('title', '商户信息采集');
        return $this->view->fetch();
    }
    //说明
    public function merchatinfo(){
        $this->view->assign('title', '肇庆高新区消费券商家报名信息表填写要点');
        return $this->view->fetch();
    }

    //单独购车券报名
    public function smyycar($uname='',$idCardNumber='',$mobile=''){
        if ($this->request->isPost()) {
            $data  =  $this->request->post();
            if (!isset($data['mobile'])||!$data['mobile']) {
                $this->error('电话号码必须');
            }
            //判断预约活动是否结束
            $nowtime = time();
            $active = db('xfq_active')->where(['id'=>14])->find();
            if (!$active||$active['starttime']>$nowtime||$active['endtime']<$nowtime) {
                $this->error('活动未开始或已结束,请留意报名时间');
            }
            //判断是否已经报名过
            $is_baoming = db('xfq_baoming')->where(['mobile'=>$data['mobile'],'active_id'=>$active['id']])->value('id');
            if ($is_baoming) {
                $this->error('已经报名过，无需重新报名');
            }
            //获取当前活动最大序号
            $maxNo = db('xfq_baoming')->where(['active_id'=>$active['id']])->order('no desc')->value('no');
            $sendlist = [
                'active_id'=>$active['id'],
                'uname'=>$data['uname'],
                'idcard'=>$data['idcard'],
                'mobile'  => $data['mobile'],
                'add_time'=>date('Y-m-d H:i:s'),
                'no'=>$maxNo+1
            ];
            $rs = db('xfq_baoming')->insert($sendlist);
            if (!$rs) {
                $this->error('报名失败,请稍后再试');
            }
            $this->success('报名成功,中签结果公布后可以在预约查询中查询中签结果');
            exit;
        }
        //获取最后一个活动
        $nowtime = time();
        $info = db('xfq_active')->where(['id'=>14])->order('id desc')->find();
        $active_status = 0;//活动状态
        if ($info) {
            if ($info['starttime']>$nowtime) {
                $active_status = 1;//活动暂未开始
            }elseif($info['endtime']<$nowtime){
                $active_status = 2;//活动已结束
            }else{
                $active_status = 3;//活动进行中
            }
            $info['active_status'] = $active_status;
        }
        
        $this->view->assign('info', $info);

        $this->view->assign('uname', $uname);
        $this->view->assign('idCardNumber', $idCardNumber);
        $this->view->assign('mobile', $mobile);
        $this->view->assign('title', '购车抵扣券报名');
        return $this->view->fetch();
    }
}
