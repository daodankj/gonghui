<?php

namespace app\index\controller;

use app\common\controller\Frontend;
use think\Db;
use think\Session;
use fast\Random;
use think\Validate;
use app\common\model\User;
use tool\Wechat;
use addons\faqueue\library\QueueApi;

class Active extends Frontend
{

    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = 'fk';
    protected $_user = null;


    public function _initialize()
    {
        parent::_initialize();
        $userId = session('userId');
        if ($userId) {
            $this->_user = User::get($userId);
        }
        //$this->wechatlogin();
    }
    /*private function wechatlogin(){
        //判断是否是微信浏览器
        $isWeixin = $this->isWeixin();
        if (!$isWeixin) {
            Session::delete('openid');
            return false;
        }
        if(Session::get('openid')){
            //判断该用户是否已经绑定过
            //$this->checkWxBind(Session::get('openid'));
            return;//如果已经获取过openid则不用在次获取了
        }
        $redirect_uri = url('wechatcallback','','',true);
        $wchate = new Wechat(['callback'=>$redirect_uri]);
        $this->redirect($wchate->getAuthorizeUrl());
    }
    //判断是否是微信浏览器
    private function isWeixin(){
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        if (strpos($user_agent, 'MicroMessenger') === false) {
            // 非微信浏览器
            return false;
        } else {
            // 微信浏览器，
            return true;
        }
    }
    public function wechatcallback(){
        $wchate = new Wechat();
        $userinfo = $wchate->getUserInfo();
        if (!$userinfo) {
            $this->error(__('操作失败'));
        }
        if (!$userinfo['openid']) {
            trace('用户数据:'.json_encode($userinfo),'error');
        }
        Session::set('openid',$userinfo['openid']);
        $url = url('index');
        $this->redirect($url);
    }*/

    //活动首页页面
    public function index()
    {


        //$this->view->assign('title', '大旺工人');
        //return $this->view->fetch();
    }

    public function info($id=0){
        if ($this->request->isPost()) {
            $data = $this->request->post();
            $rule = [
                'active_id' => 'require',
                'name' => 'require',
                'phone'  => 'require|length:11',
                '__token__' => 'require|token'
            ];

            $msg = [
                'active_id' => '活动不存在',
                'name' => '姓名必须',
                'idcard.length'  => '电话格式错误'
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }
            //获取活动信息
            $info = db('baoming_active')->where(['id'=>$data['active_id'],'status'=>1])->find();
            if (!$info) {
                $this->error('该活动已取消');
            }
            $nowtime = time();
            if ($info['start_time']>$nowtime) {
                $this->error('该活动还未开始');
            }elseif($info['end_time']<$nowtime){
                $this->error('该活动已结束');
            }
            //判断是否已经报名了
            $hascard = db('baoming_activeinfo')->where(['active_id'=>$data['active_id'],'phone'=>$data['phone']])->find();
            if ($hascard) {
                $this->error('你已报名，无需在次报名');
            }
            //判断人数是否已满
            $user_num = db('baoming_activeinfo')->where(['active_id'=>$data['active_id']])->count();
            if ($user_num>=$info['user_num']) {
                $this->error('报名人数已满');
            }
            unset($data['__token__']);
            $data['add_time'] = time();
            $res = db('baoming_activeinfo')->insert($data);
            if ($res) {
                //短信通知
                $msg = '恭喜您'.$data['name'].'，您参加的"'.$info['name'].'"活动已经报名成功！';
                QueueApi::smsNotice($data['phone'],$msg);
                $this->success('报名成功');
            }else{
                $this->error('报名失败');
            }
        }
        if (!$id) {
            $this->error('活动不存在');
        }
        $userinfo = [
            'name' => '',
            'phone' => '',
            'idcard' => ''
        ];
        if ($this->_user&&$this->_user->username) {
            $userinfo['name'] = $this->_user->username;
        }
        if ($this->_user&&$this->_user->mobile) {
            $userinfo['phone'] = $this->_user->mobile;
        }
        if ($this->_user&&$this->_user->idcard) {
            $userinfo['idcard'] = $this->_user->idcard;
        }
        $info = db('baoming_active')->where(['id'=>$id])->find();
        $info['field_type'] = explode(',', $info['field_type']);
        if (in_array('3', $info['field_type'])) {
            $info['idcard'] = 1;
        }else{
            $info['idcard'] = 0;
        }
        if (in_array('4', $info['field_type'])) {
            $info['sex'] = 1;
        }else{
            $info['sex'] = 0;
        }
        if (in_array('5', $info['field_type'])) {
            $info['company'] = 1;
        }else{
            $info['company'] = 0;
        }
        if (in_array('6', $info['field_type'])) {
            $info['job'] = 1;
        }else{
            $info['job'] = 0;
        }
        if (in_array('7', $info['field_type'])) {
            $info['remark'] = 1;
        }else{
            $info['remark'] = 0;
        }
        $active_status = 0;//活动状态
        if ($info) {
            $nowtime = time();
            if ($info['start_time']>$nowtime) {
                $active_status = 1;//活动暂未开始
            }elseif($info['end_time']<$nowtime){
                $active_status = 2;//活动已结束
            }else{
                $active_status = 3;//活动进行中
            }
        }
        $info['active_status'] = $active_status;

        $this->view->assign('info', $info);
        $this->view->assign('userinfo', $userinfo);
        $this->view->assign('title', '活动报名');
        return $this->view->fetch();
    }

    public function qiandao($id=0){
        if ($this->request->isPost()) {
            $data = $this->request->post();
            $rule = [
                'active_id' => 'require',
                //'name' => 'require',
                'phone'  => 'require|length:11',
                '__token__' => 'require|token'
            ];

            $msg = [
                'active_id' => '活动不存在',
                //'name' => '姓名必须',
                'idcard.length'  => '电话格式错误'
            ];
            $validate = new Validate($rule, $msg);
            $result = $validate->check($data);
            if (!$result) {
                $this->error(__($validate->getError()), null, ['token' => $this->request->token()]);
                return false;
            }

            //判断是否已经报名了
            $hascard = db('baoming_activeinfo')->where(['active_id'=>$data['active_id'],'phone'=>$data['phone']])->find();
            if (!$hascard) {
                $this->error('你未报名报名，无法签到');
            }
            if ($hascard['status']==1){
                $this->error('您已签到');
            }
            $res = db('baoming_activeinfo')->where(['active_id'=>$data['active_id'],'phone'=>$data['phone']])->update(['status'=>1,'qd_time'=>time()]);
            if ($res) {
                $this->success('签到成功');
            }else{
                $this->error('签到失败');
            }
        }
        $userinfo = [
            'id'   =>$id,
            'name' => '',
            'phone' => ''
        ];
        if ($this->_user&&$this->_user->username) {
            $userinfo['name'] = $this->_user->username;
        }
        if ($this->_user&&$this->_user->mobile) {
            $userinfo['phone'] = $this->_user->mobile;
        }
        if ($this->_user&&$this->_user->idcard) {
            $userinfo['idcard'] = $this->_user->idcard;
        }

        $this->view->assign('userinfo', $userinfo);
        $this->view->assign('title', '活动签到');
        return $this->view->fetch();
    }

}
