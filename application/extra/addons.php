<?php

return array (
  'autoload' => false,
  'hooks' => 
  array (
    'app_init' => 
    array (
      0 => 'log',
    ),
    'admin_login_init' => 
    array (
      0 => 'loginbg',
    ),
    'config_init' => 
    array (
      0 => 'third',
      1 => 'yktsms',
    ),
    'sms_send' => 
    array (
      0 => 'yktsms',
    ),
    'sms_notice' => 
    array (
      0 => 'yktsms',
    ),
    'sms_check' => 
    array (
      0 => 'yktsms',
    ),
  ),
  'route' => 
  array (
    '/qrcode$' => 'qrcode/index/index',
    '/qrcode/build$' => 'qrcode/index/build',
    '/third$' => 'third/index/index',
    '/third/connect/[:platform]' => 'third/index/connect',
    '/third/callback/[:platform]' => 'third/index/callback',
    '/third/bind/[:platform]' => 'third/index/bind',
    '/third/unbind/[:platform]' => 'third/index/unbind',
  ),
);