<?php

// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
// 应用行为扩展定义文件
return [
    // 应用结束
    'app_end'      => [
        'app\\admin\\behavior\\AdminLog',
    ],
    /*'apilist_check_successed' => [
    	'app\\admin\\behavior\\KpilistCheck',
    ],*/
    'apply_check' => ['app\\admin\\behavior\\ApplyCheck'],
    
    /*'party_kpilist_check_successed' => [
    	'app\\admin\\behavior\\PartyKpilistCheck',
    ],*/
    'party_apply_check' => ['app\\admin\\behavior\\PartyApplyCheck'],
];
