<?php

namespace app\admin\model;

use think\Model;


class Capital extends Model
{

    

    

    // 表名
    protected $name = 'capital';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'creaetime_text'
    ];
    

    



    public function getCreaetimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['creaetime']) ? $data['creaetime'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setCreaetimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }


    public function company()
    {
        return $this->belongsTo('Company', 'company_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
