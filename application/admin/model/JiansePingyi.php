<?php

namespace app\admin\model;

use think\Model;


class JiansePingyi extends Model
{

    

    

    // 表名
    protected $name = 'jianse_pingyi';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'add_time_text',
        'company_name',
        'topic_name'
    ];


    protected function getCompanyNameAttr($value,$row){
        $name = db('company')->where(['id'=>$row['company_id']])->value('name');
        return $name ? $name: '';
    }

    protected function getTopicNameAttr($value,$row){
        $name = db('topic')->where(['id'=>$row['topic_id']])->value('name');
        return $name ? $name: '';
    }

    public function getAddTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['add_time']) ? $data['add_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setAddTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }


}
