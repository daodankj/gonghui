<?php

namespace app\admin\model;

use think\Model;


class Kpi extends Model
{

    

    

    // 表名
    protected $name = 'kpi';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'topic_name',
        'submit_type_name',
        'type_text'
    ];

    public function getTypeList()
    {
        //return [1 => '阵地建设',2=>'队伍管理',3=>'活动开展',4=>'工会组织有活力'];
        $list = db('dictionary')->where(['pid'=>5])->field('id,name')->select();
        $list = array_column($list, 'name','id');

        return $list;
    }


    public function getTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['type']) ? $data['type'] : '');
        $list = $this->getTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    
    protected function getTopicNameAttr($value,$row){
        $name = db('topic')->where(['id'=>$row['topic_id']])->value('name');

        return $name ? $name : '';
    }

    protected function getSubmitTypeNameAttr($value,$row){
        $submitTypeData = ['1'=>'文字描述','2'=>'文字+图片','3'=>'文字+文件(word,excel)','4'=>'财务资金上报','5'=>'企业信息完善','6'=>'文体活动上报'];

        return isset($submitTypeData[$row['submit_type']])?$submitTypeData[$row['submit_type']]:'';
    }









}
