<?php

namespace app\admin\model;

use think\Model;


class EmployeesAssistance extends Model
{

    

    

    // 表名
    protected $name = 'employees_assistance';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'sex_text',
        'add_time_text',
        'check_time_text'
    ];
    

    
    public function getSexList()
    {
        return ['男' => __('男'), '女' => __('女')];
    }


    public function getSexTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['sex']) ? $data['sex'] : '');
        $list = $this->getSexList();
        return isset($list[$value]) ? $list[$value] : '男';
    }


    public function getAddTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['add_time']) ? $data['add_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getCheckTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['check_time']) ? $data['check_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setAddTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setCheckTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    public function getCompanyAttr($value,$data){
        $name = db('company')->where(['id'=>$data['company_id']])->value('name');
        return $name?$name:'';
    }

    public function getNameAttr($value,$data){
        $name = db('user')->where(['id'=>$data['user_id']])->value('username');
        return $name?$name:'';
    }

    public function getPhoneAttr($value,$data){
        $name = db('user')->where(['id'=>$data['user_id']])->value('mobile');
        return $name?$name:'';
    }

    public function getIdcardAttr($value,$data){
        $name = db('user')->where(['id'=>$data['user_id']])->value('idcard');
        return $name?$name:'';
    }

}
