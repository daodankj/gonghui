<?php

namespace app\admin\model;

use think\Model;


class KpiList extends Model
{

    

    

    // 表名
    protected $name = 'kpi_list';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'company_name',
        'user_name'
    ];

    protected function getCompanyNameAttr($value,$row){
        $name = db('company')->where(['id'=>$row['company_id']])->value('name');

        return $name ? $name: '';
    }
    protected function getUserNameAttr($value,$row){
        $name = db('user')->where(['id'=>$row['user_id']])->value('username');

        return $name ? $name: '';
    }
    
    public function kpi()
    {
        return $this->belongsTo('kpi', 'kpi_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
    







}
