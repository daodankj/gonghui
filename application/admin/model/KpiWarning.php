<?php

namespace app\admin\model;

use think\Model;


class KpiWarning extends Model
{

    

    

    // 表名
    protected $name = 'kpi_warning';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    

    







    public function kpi()
    {
        return $this->belongsTo('Kpi', 'kpi_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }


    public function company()
    {
        return $this->belongsTo('Company', 'company_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
}
