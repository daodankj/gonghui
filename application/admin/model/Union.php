<?php

namespace app\admin\model;

use think\Model;


class Union extends Model
{

    

    

    // 表名
    protected $name = 'union';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'company_name',
        'company_type'
    ];
    
    public function getTypeList()
    {
        return ['D' => __('D'), 'C' => __('C'), 'B' => __('B'), 'A' => __('A')];
    }
    protected function getCompanyNameAttr($value,$data)
    {
        $name = db('company')->where(['id'=>$data['company_id']])->value('name');
        return $name?$name:$data['company_id'];
    }

    protected function getCompanyTypeAttr($value,$data)
    {
        $name = db('company')->where(['id'=>$data['company_id']])->value('type');
        return $name?$name:$data['company_id'];
    }

    protected function getScoreAttr($value,$data){
        //更改按主题获取积分
        $topic_id = db('topic')->order('id desc')->value('id');
        $scoreLog = db('company_score_log')->where(['topic_id'=>$topic_id,'company_id'=>$data['company_id']])->field('after,union_type')->order('id desc')->find();
        if ($scoreLog) {
            $data['score'] = $scoreLog['after'];
            if ($scoreLog['union_type']) {
               $data['type'] = $scoreLog['union_type'];
            }
        }else{//不存在说明还没积分从0开始
            $data['score'] = 0;
            $data['type'] = 'D';
        }
        return $data['score'];
    }
    protected function getTypeAttr($value,$data){
        //更改按主题获取积分
        $topic_id = db('topic')->order('id desc')->value('id');
        $scoreLog = db('company_score_log')->where(['topic_id'=>$topic_id,'company_id'=>$data['company_id']])->field('after,union_type')->order('id desc')->find();
        if ($scoreLog) {
            if ($scoreLog['union_type']) {
               $data['type'] = $scoreLog['union_type'];
            }
        }else{//不存在说明还没积分从0开始
            $data['type'] = 'D';
        }
        return $data['type'];
    }

    //获取企业法人信息和地址
    protected function getLegalPersonAttr($value,$data){
        $name = db('company_info')->where(['company_id'=>$data['company_id']])->value('c_legal_person');
        return $name?$name:$value;
    }
    /*protected function getPhoneAttr($value,$data){
        $name = db('company_info')->where(['company_id'=>$data['company_id']])->value('c_legal_person_phone');
        return $name?$name:$value;
    }*/
    protected function getAddressAttr($value,$data){
        $name = db('company_info')->where(['company_id'=>$data['company_id']])->value('c_address');
        return $name?$name:$value;
    }

    public function company()
    {
        return $this->belongsTo('company', 'company_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
    

    







}
