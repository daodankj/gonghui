<?php

namespace app\admin\model;

use think\Model;


class Article extends Model
{





    // 表名
    protected $name = 'article';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'type_text'
    ];

    public function getTypeList()
    {
        return [5 => '劳资纠纷调解',6=>'困难职工帮扶',7=>'欠薪求助',8=>'职工互助报障',9=>'劳模和工匠人才创新工作室',11=>'学历提升',12=>'活动场地优惠',13=>'学校学费优惠',14=>'学习计划上报',15=>'党员创新工作室',16=>'民主参与指引'];
    }


    public function getTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['type']) ? $data['type'] : '');
        $list = $this->getTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }









}
