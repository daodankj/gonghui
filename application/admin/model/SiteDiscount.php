<?php

namespace app\admin\model;

use think\Model;


class SiteDiscount extends Model
{

    

    

    // 表名
    protected $name = 'site_discount';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'use_time_text',
        'site_type_text',
        'add_time_text',
        'check_time_text',
        'user_name',
        'company_name'
    ];
    

    
    public function getSiteTypeList()
    {
        return ['会议室' => __('会议室'), '足球场' => __('足球场'), '篮球场' => __('篮球场')];
    }


    public function getUseTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['use_time']) ? $data['use_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getSiteTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['site_type']) ? $data['site_type'] : '');
        $list = $this->getSiteTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getAddTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['add_time']) ? $data['add_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getCheckTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['check_time']) ? $data['check_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setUseTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setAddTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setCheckTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function getUserNameAttr($value,$data)
    {
        $name = db('user')->where(['id'=>$data['user_id']])->value('username');
        return $name?$name:$data['user_id'];
    }

    protected function getCompanyNameAttr($value,$row){
        $name = db('company')->where(['id'=>$row['company_id']])->value('name');

        return $name ? $name: '';
    }

}
