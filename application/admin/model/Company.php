<?php

namespace app\admin\model;

use think\Model;


class Company extends Model
{





    // 表名
    protected $name = 'company';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'type_text',
        'style_text',
        'gh_map',
        'dj_map'
    ];


    public function getTypeList()
    {
        return ['D' => __('D'), 'C' => __('C'), 'B' => __('B'), 'A' => __('A')];
    }
    public function getStyleList()
    {
        return ['企业' => __('企业'), '单位' => __('单位'), '其他' => __('其他')];
    }


    public function getTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['type']) ? $data['type'] : '');
        $list = $this->getTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function getStyleTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['style']) ? $data['style'] : '');
        $list = $this->getStyleList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    protected function getScoreAttr($value,$data){
        //更改按主题获取积分
        $topic_id = db('topic')->order('id desc')->value('id');
        $scoreLog = db('company_score_log')->where(['topic_id'=>$topic_id,'company_id'=>$data['id']])->field('after,union_type')->order('id desc')->find();
        if ($scoreLog) {
            $data['score'] = $scoreLog['after'];
            if ($scoreLog['union_type']) {
               $data['type'] = $scoreLog['union_type'];
            }
        }else{//不存在说明还没积分从0开始
            $data['score'] = 0;
            $data['type'] = 'D';
        }
        return $data['score'];
    }

    public function getGhMapAttr($value, $data){
        if($data['lat']>0&&$data['lng']>0){
            return '是';
        }else return '否';
    }
    public function getDjMapAttr($value, $data){
        $pid = db('party')->where(['company_id'=>$data['id']])->value('id');
        if($pid){
            return '是';
        }else{
            return '否';
        }
    }

    public function companyinfo()
    {
        //return $this->hasOne('CompanyInfo');
        return $this->belongsTo('CompanyInfo', 'id', 'company_id', [], 'LEFT')->setEagerlyType(0);
    }


}
