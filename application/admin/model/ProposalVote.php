<?php

namespace app\admin\model;

use think\Model;


class ProposalVote extends Model
{

    

    

    // 表名
    protected $name = 'proposal_vote';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'add_time_text',
        'user_name',
        'proposal_title'
    ];
    

    



    public function getAddTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['add_time']) ? $data['add_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setAddTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function getUserNameAttr($value,$data)
    {
        $name = db('user')->where(['id'=>$data['user_id']])->value('username');
        return $name?$name:$data['user_id'];
    }

    protected function getProposalTitleAttr($value,$data)
    {
        $name = db('proposal')->where(['id'=>$data['proposal_id']])->value('title');
        return $name?$name:$data['proposal_id'];
    }

}
