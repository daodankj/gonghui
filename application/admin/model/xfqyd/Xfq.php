<?php

namespace app\admin\model\xfqyd;

use think\Model;


class Xfq extends Model
{

    

    

    // 表名
    protected $name = 'xfq';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'active_name'
    ];
    
    public function getActiveNameAttr($value, $data)
    {
        $name = db('xfq_active')->where(['id'=>$data['active_id']])->value('name');
        return $name ? $name : $data['active_id'];
    }
    







}
