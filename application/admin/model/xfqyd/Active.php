<?php

namespace app\admin\model\xfqyd;

use think\Model;


class Active extends Model
{

    

    

    // 表名
    protected $name = 'xfq_active';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $starttime = false;
    protected $endtime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [

    ];
    

    protected function getStarttimeAttr($value){
        return date('Y-m-d H:i:s',$value);
    }
    protected function getEndtimeAttr($value){
        return date('Y-m-d H:i:s',$value);
    }

    protected function setStarttimeAttr($value){
        return strtotime($value);
    }
    protected function setEndtimeAttr($value){
        return strtotime($value);
    }



}
