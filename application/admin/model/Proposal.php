<?php

namespace app\admin\model;

use think\Model;


class Proposal extends Model
{

    

    

    // 表名
    protected $name = 'proposal';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'vote_end_time_text',
        'add_time_text',
        'company_name',
        'user_name'
    ];
    

    



    public function getVoteEndTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['vote_end_time']) ? $data['vote_end_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }


    public function getAddTimeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['add_time']) ? $data['add_time'] : '');
        return is_numeric($value) ? date("Y-m-d H:i:s", $value) : $value;
    }

    protected function setVoteEndTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function setAddTimeAttr($value)
    {
        return $value === '' ? null : ($value && !is_numeric($value) ? strtotime($value) : $value);
    }

    protected function getCompanyNameAttr($value,$row){
        $name = db('company')->where(['id'=>$row['company_id']])->value('name');

        return $name ? $name: '';
    }

    protected function getUserNameAttr($value,$data)
    {
        $name = db('user')->where(['id'=>$data['user_id']])->value('username');
        return $name?$name:$data['user_id'];
    }


}
