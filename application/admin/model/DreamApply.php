<?php

namespace app\admin\model;

use think\Model;


class DreamApply extends Model
{

    

    

    // 表名
    protected $name = 'dream_apply';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'is_signle_text',
        'is_welfare_text',
        'is_company_text',
        'is_economy_text',
        'is_lowrent_text',
        'is_mark_text',
        'is_private_text',
        'is_public_text',
        'company_name',
        'phone'
    ];
    

    
    public function getIsSignleList()
    {
        return ['是' => __('是'), '否' => __('否')];
    }

    public function getIsWelfareList()
    {
        return ['是' => __('是'), '否' => __('否')];
    }

    public function getIsCompanyList()
    {
        return ['是' => __('是'), '否' => __('否')];
    }

    public function getIsEconomyList()
    {
        return ['是' => __('是'), '否' => __('否')];
    }

    public function getIsLowrentList()
    {
        return ['是' => __('是'), '否' => __('否')];
    }

    public function getIsMarkList()
    {
        return ['是' => __('是'), '否' => __('否')];
    }

    public function getIsPrivateList()
    {
        return ['是' => __('是'), '否' => __('否')];
    }

    public function getIsPublicList()
    {
        return ['是' => __('是'), '否' => __('否')];
    }


    public function getIsSignleTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_signle']) ? $data['is_signle'] : '');
        $list = $this->getIsSignleList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIsWelfareTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_welfare']) ? $data['is_welfare'] : '');
        $list = $this->getIsWelfareList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIsCompanyTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_company']) ? $data['is_company'] : '');
        $list = $this->getIsCompanyList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIsEconomyTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_economy']) ? $data['is_economy'] : '');
        $list = $this->getIsEconomyList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIsLowrentTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_lowrent']) ? $data['is_lowrent'] : '');
        $list = $this->getIsLowrentList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIsMarkTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_mark']) ? $data['is_mark'] : '');
        $list = $this->getIsMarkList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIsPrivateTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_private']) ? $data['is_private'] : '');
        $list = $this->getIsPrivateList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    public function getIsPublicTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['is_public']) ? $data['is_public'] : '');
        $list = $this->getIsPublicList();
        return isset($list[$value]) ? $list[$value] : '';
    }


    protected function getCompanyNameAttr($value,$data)
    {
        $name = db('company')->where(['id'=>$data['company_id']])->value('name');
        return $name?$name:$data['company_id'];
    }

    protected function getPhoneAttr($value,$data)
        {
            $name = db('user')->where(['id'=>$data['user_id']])->value('mobile');
            return $name?$name:'';
        }

}
