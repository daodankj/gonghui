<?php

namespace app\admin\model\party;

use think\Model;


class PartyChangeapply extends Model
{

    

    

    // 表名
    protected $name = 'party_changeapply';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'company_name'
    ];
    

    protected function getCompanyNameAttr($value,$data)
    {
        $name = db('company')->where(['id'=>$data['company_id']])->value('name');
        return $name?$name:$data['company_id'];
    }







}
