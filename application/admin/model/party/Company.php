<?php

namespace app\admin\model\party;

use think\Model;


class Company extends Model
{

    

    

    // 表名
    protected $name = 'company';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'type_text',
        'style_text'
    ];
    

    
    public function getTypeList()
    {
        return ['D' => __('D'), 'C' => __('C'), 'B' => __('B'), 'A' => __('A')];
    }
    public function getStyleList()
    {
        return ['企业' => __('企业'), '单位' => __('单位'), '其他' => __('其他')];
    }


    public function getTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['type']) ? $data['type'] : '');
        $list = $this->getTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    public function getStyleTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['style']) ? $data['style'] : '');
        $list = $this->getStyleList();
        return isset($list[$value]) ? $list[$value] : '';
    }




}
