<?php

namespace app\admin\model\party;

use think\Model;


class PartyStudyActive extends Model
{





    // 表名
    protected $name = 'party_study_active';

    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'type_text',
        'company_name'
    ];



    public function getTypeList()
    {
        return ['学习活动' => __('学习活动'), '会议' => __('会议'),'其他活动' => __('其他活动')];
    }


    public function getTypeTextAttr($value, $data)
    {
        $value = $value ? $value : (isset($data['type']) ? $data['type'] : '');
        $list = $this->getTypeList();
        return isset($list[$value]) ? $list[$value] : '';
    }

    protected function getCompanyNameAttr($value,$data)
    {
        //$name = db('company')->where(['id'=>$data['company_id']])->value('name');
        //return $name?$name:$data['company_id'];
        return '';
    }

    public function company()
    {
        return $this->belongsTo('company', 'company_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }

}
