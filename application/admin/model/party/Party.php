<?php

namespace app\admin\model\party;

use think\Model;


class Party extends Model
{

    

    

    // 表名
    protected $name = 'party';
    
    // 自动写入时间戳字段
    protected $autoWriteTimestamp = false;

    // 定义时间戳字段名
    protected $createTime = false;
    protected $updateTime = false;
    protected $deleteTime = false;

    // 追加属性
    protected $append = [
        'company_name',
        'company_type'
    ];
    
    public function getTypeList()
    {
        return ['D' => __('D'), 'C' => __('C'), 'B' => __('B'), 'A' => __('A')];
    }
    protected function getCompanyNameAttr($value,$data)
    {
        $name = db('company')->where(['id'=>$data['company_id']])->value('name');
        return $name?$name:$data['company_id'];
    }

    protected function getCompanyTypeAttr($value,$data)
    {
        $name = db('company')->where(['id'=>$data['company_id']])->value('type');
        return $name?$name:$data['company_id'];
    }

    //获取企业法人信息和地址
    protected function getLegalPersonAttr($value,$data){
        $name = db('company_info')->where(['company_id'=>$data['company_id']])->value('c_legal_person');
        return $name?$name:$value;
    }
    /*protected function getPhoneAttr($value,$data){
        $name = db('company_info')->where(['company_id'=>$data['company_id']])->value('c_legal_person_phone');
        return $name?$name:$value;
    }*/
    protected function getAddressAttr($value,$data){
        $name = db('company_info')->where(['company_id'=>$data['company_id']])->value('c_address');
        return $name?$name:$value;
    }

    public function company()
    {
        return $this->belongsTo('company', 'company_id', 'id', [], 'LEFT')->setEagerlyType(0);
    }
    

    







}
