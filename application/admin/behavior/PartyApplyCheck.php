<?php

namespace app\admin\behavior;
use think\Db;
use addons\faqueue\library\QueueApi;

class PartyApplyCheck
{
    /**
     *type标记审核对象(当成立，改选等), status审核状态(1成功2拒绝)) data对应申请对象信息
    */
    public function run(&$info)
    {
        if (!$info['data']['company_id']) {
            return true;
        }

        //通知内容优先从消息内容中获取，如果没有在去组装(防止之前没有记录消息的情况)
        $msg = db('user_notice')->where(['type'=>$info['type'],'obj_id'=>$info['data']['id'],'status'=>['<',1]])->order('id asc')->value('notice');
        if (!$msg) {
            $msg = $this->getMsgInfo($info);
            if (!$msg) {
                return false;
            }
            //获取企业名称
            $cinfo = db('company')->where(['id'=>$info['data']['company_id']])->field('name,short_name')->find();
            $cname = $cinfo['short_name']??$cinfo['name'];
            $uname = db('user')->where(['id'=>$info['data']['user_id']])->value('username');
            if (isset($info['data']['add_time'])) {//命名不统一的坑爹
                $addtime = $info['data']['add_time'];
            }elseif (isset($info['data']['addtime'])) {
               $addtime = $info['data']['addtime'];
            }
            $addtime = is_numeric($addtime)?$addtime:strtotime($addtime);//格式不统一的坑爹
            $msg = '尊敬的'.$cname.'，'.$uname.'于'.date('Y-m-d',$addtime).'日'.$msg;
        }
        if ($info['status']==1) {//审核通过
            if ($info['type']==64){//入党申请
                $msg = $msg.'已受理。'.$info['data']['adminInfo'];
            }else{
                $msg = $msg.'已审批通过，请登陆平台查看详情。';
            }
        }elseif ($info['status']==3) {//受理通过
            $msg = $msg.'已受理成功，最终审批结果我们将会以短信通知您，请注意查收！';
        }else{
            if ($info['type']==64){//入党申请
                $msg = $msg.'已拒绝。'.$info['data']['adminInfo'];
            }else{
                $msg = $msg.'未审批通过！原因：'.$info['data']['result'].'。请登陆平台查看详情。';
            }
        }

        //更新代办状态
        db('user_notice')->where(['type'=>$info['type'],'obj_id'=>$info['data']['id']])->update(['status'=>-1]);
        $insert_data = [
            'user_id'=> $info['data']['user_id'],
            'type'   => $info['type'],
            'obj_id' => $info['data']['id'],
            'notice' => $msg,
            'status' =>  $info['data']['status'],
            'add_time'=> time(),
            'url'    => ''
        ];
        db('user_notice')->insert($insert_data);

        //通知所有管理员和申请人
        $adminList = db('user')->where(['company_id'=>$info['data']['company_id'],'is_admin'=>1])->whereOr(['id'=>$info['data']['user_id']])->field('mobile,openid')->select();
        //短信通知
        foreach ($adminList as $key => $val) {
            if ($val['mobile']) {
                QueueApi::smsNotice($val['mobile'],$msg);
            }
        }
        //微信通知
    }

    private function getMsgInfo($info){
        $actionName = [
            '61' => '提交了党组织成立申请',
            '62' => '提交了党组组换届申请',
            '63' => '提交了党组织改选申请',
            '64' => '提交了入党申请',
            '65' => '提交了党员组织关系转出申请',
            '66' => '上报了学习活动',
            '67' => '申报了党员创新工作室',
        ];
        return isset($actionName[$info['type']])?$actionName[$info['type']]:'';
    }
}
