<?php

namespace app\admin\behavior;
use think\Db;
use addons\faqueue\library\QueueApi;

class ApplyCheck
{
    /**
     *type标记审核对象(工会成立，改选等), status审核状态(1成功2拒绝)) data对应申请对象信息
    */
    public function run(&$info)
    {
        /*switch ($info['type']) {
            case 1:
                $this->union_apply($info);
                break;
            case 2:
                $this->unionChange_apply($info);
                break;
            case 3:
                $this->unionUpdate_apply($info);
                break;
            case 4:
                $this->unionActive_apply($info);
                break;
            case 5:
                $this->weiquan_apply($info);
                break;
            case 6:
                $this->kunnan_apply($info);
                break;
            case 7:
                $this->arrears_help($info);
                break;
            case 8:
                $this->employees_assistance($info);
                break;
            case 9:
                $this->model_craftsman($info);
                break;
            case 10:
                $this->skills_competition($info);
                break;
            case 11:
                $this->skills_education($info);
                break;
            default:
                # code...
                break;
        }*/
        if (!$info['data']['company_id']) {
            return true;
        }

        //通知内容优先从消息内容中获取，如果没有在去组装(防止之前没有记录消息的情况)
        $msg = db('user_notice')->where(['type'=>$info['type'],'obj_id'=>$info['data']['id'],'status'=>['<',1]])->order('id asc')->value('notice');
        if (!$msg) {
            $msg = $this->getMsgInfo($info);
            if (!$msg) {
                return false;
            }
            //获取企业名称
            $cinfo = db('company')->where(['id'=>$info['data']['company_id']])->field('name,short_name')->find();
            $cname = $cinfo['short_name']??$cinfo['name'];
            $uname = db('user')->where(['id'=>$info['data']['user_id']])->value('username');
            if (isset($info['data']['add_time'])) {//命名不统一的坑爹
                $addtime = $info['data']['add_time'];
            }elseif (isset($info['data']['addtime'])) {
               $addtime = $info['data']['addtime'];
            }
            $addtime = is_numeric($addtime)?$addtime:strtotime($addtime);//格式不统一的坑爹
            $msg = '尊敬的'.$cname.'，'.$uname.'于'.date('Y-m-d',$addtime).'日'.$msg;
        }

        if ($info['status']==1) {//审核通过
            $msg = $msg.'已审批通过，请登陆平台查看详情。';
        }elseif ($info['status']==3) {//受理通过
            $msg = $msg.'已受理成功，最终审批结果我们将会以短信通知您，请注意查收！';  
        }else{
            $msg = $msg.'未审批通过！原因：'.$info['data']['result'].'。请登陆平台查看详情。';
        }

        //更新代办状态
        db('user_notice')->where(['type'=>$info['type'],'obj_id'=>$info['data']['id']])->update(['status'=>-1]);
        $insert_data = [
            'user_id'=> $info['data']['user_id'],
            'type'   => $info['type'],
            'obj_id' => $info['data']['id'],
            'notice' => $msg,
            'status' =>  $info['data']['status'],
            'add_time'=> time(),
            'url'    => ''
        ];
        db('user_notice')->insert($insert_data);

        //通知所有管理员和申请人
        $adminList = db('user')->where(['company_id'=>$info['data']['company_id'],'is_admin'=>1])->whereOr(['id'=>$info['data']['user_id']])->field('id,mobile,openid')->select();
        //短信通知
        foreach ($adminList as $key => $val) {     
            if ($info['type']==11&&$info['data']['type']==2&&$info['status']==3&&$val['id']==$info['data']['user_id']) {//学历提示受理成功本人短信通知内容
                $msg = $info['data']['name'].'同学您好，您的报名信息资料已受理通过，请在24小时内联系肇庆高新区职工培训学院蔡老师提交具体报名资料，联系电话 0758-617908; 15629883311（微信同号）';
                QueueApi::smsNotice($info['data']['phone'],$msg);
            }else if ($val['mobile']) {
                QueueApi::smsNotice($val['mobile'],$msg);
            }
        }
        //微信通知
    }

    private function getMsgInfo($info){
        $actionName = [
            '1' => '申请了工会成立',
            '2' => '申请了工会换届',
            '3' => '申请了工会改选',
            '4' => '上报了文体活动',
            '5' => '申请了劳资纠纷调解',
            '6' => '申请了困难职工扶持',
            '7' => '申请了欠薪求助',
            '8' => '申请了职工互助报障',
            '9' => '申请了劳模和工匠创新工作室',
            '10' => '上报了技能竞赛',
            '11' => '申请了技能和学历培训',
            '12' => '申请了活动场地优惠',
            '13' => '申请了学校学费优惠',
            '14' => '申请了圆梦助学',
        ];
        return isset($actionName[$info['type']])?$actionName[$info['type']]:'';
    }

    //1工会成立申请
    private function union_apply($info){
        //短信通知
        $uinfo = db('user')->where(['id'=>$info['data']['user_id']])->field('mobile,openid')->find();
        if ($uinfo && $uinfo['mobile']) {
            if ($info['status']==1) {//通过
                $msg = '您的工会成立申请已经审核通过！请登入系统查看。';
            }else{
                $msg = '您的工会成立申请未通过审核！原因：'.$info['data']['result'];
            }
            QueueApi::smsNotice($uinfo['mobile'],$msg);
        }
        //微信通知


        //更新代办状态
        db('user_notice')->where(['type'=>1,'obj_id'=>$info['data']['id'],'status'=>0])->update(['status'=>-1]);
        $insert_data = [
            'user_id'=> $info['data']['user_id'],
            'type'   => 1,
            'obj_id' => $info['data']['id'],
            'notice' => $msg,
            'status' =>  $info['data']['status'],
            'add_time'=> time(),
            'url'    => ''
        ];
        db('user_notice')->insert($insert_data);
    }

    //2工会换届申请
    private function unionChange_apply($info){
        //短信通知
        $uinfo = db('user')->where(['id'=>$info['data']['user_id']])->field('mobile,openid')->find();
        if ($uinfo && $uinfo['mobile']) {
            if ($info['status']==1) {//通过
                $msg = '您的工会换届申请已经审核通过！请登入系统查看。';
            }else{
                $msg = '您的工会换届申请未通过审核！原因：'.$info['data']['result'];
            }
            QueueApi::smsNotice($uinfo['mobile'],$msg);
        }
        //微信通知
        

        //更新代办状态
        db('user_notice')->where(['type'=>2,'obj_id'=>$info['data']['id'],'status'=>0])->update(['status'=>-1]);
        $insert_data = [
            'user_id'=> $info['data']['user_id'],
            'type'   => 2,
            'obj_id' => $info['data']['id'],
            'notice' => $msg,
            'status' =>  $info['data']['status'],
            'add_time'=> time(),
            'url'    => ''
        ];
        db('user_notice')->insert($insert_data);
    }

    //3工会改选申请
    private function unionUpdate_apply($info){
        //短信通知
        $uinfo = db('user')->where(['id'=>$info['data']['user_id']])->field('mobile,openid')->find();
        if ($uinfo && $uinfo['mobile']) {
            if ($info['status']==1) {//通过
                $msg = '您的工会改选申请已经审核通过！请登入系统查看。';
            }else{
                $msg = '您的工会改选申请未通过审核！原因：'.$info['data']['result'];
            }
            QueueApi::smsNotice($uinfo['mobile'],$msg);
        }
        //微信通知
        

        //更新代办状态
        db('user_notice')->where(['type'=>3,'obj_id'=>$info['data']['id'],'status'=>0])->update(['status'=>-1]);
        $insert_data = [
            'user_id'=> $info['data']['user_id'],
            'type'   => 3,
            'obj_id' => $info['data']['id'],
            'notice' => $msg,
            'status' =>  $info['data']['status'],
            'add_time'=> time(),
            'url'    => ''
        ];
        db('user_notice')->insert($insert_data);
    }

    //4文体活动上报
    private function unionActive_apply($info){
        //短信通知
        $uinfo = db('user')->where(['id'=>$info['data']['user_id']])->field('mobile,openid')->find();
        if ($uinfo && $uinfo['mobile']) {
            if ($info['status']==1) {//通过
                $msg = '您的文体活动上报已经审核通过！请登入系统查看。';
            }else{
                $msg = '您的文体活动上报未通过审核！原因：'.$info['data']['result'];
            }
            QueueApi::smsNotice($uinfo['mobile'],$msg);
        }
        //微信通知
        

        //更新代办状态
        db('user_notice')->where(['type'=>4,'obj_id'=>$info['data']['id'],'status'=>0])->update(['status'=>-1]);
        $insert_data = [
            'user_id'=> $info['data']['user_id'],
            'type'   => 4,
            'obj_id' => $info['data']['id'],
            'notice' => $msg,
            'status' =>  $info['data']['status'],
            'add_time'=> time(),
            'url'    => ''
        ];
        db('user_notice')->insert($insert_data);
    }

    //5维权劳资纠纷调解申请
    private function weiquan_apply($info){
        //短信通知
        $uinfo = db('user')->where(['id'=>$info['data']['user_id']])->field('mobile,openid')->find();
        if ($uinfo && $uinfo['mobile']) {
            if ($info['status']==1) {//通过
                $msg = '您的维权劳资纠纷调解申请已经审核通过！请登入系统查看。';
            }else{
                $msg = '您的维权劳资纠纷调解申请未通过审核！原因：'.$info['data']['result'];
            }
            QueueApi::smsNotice($uinfo['mobile'],$msg);
        }
        //微信通知
        

        //更新代办状态
        db('user_notice')->where(['type'=>5,'obj_id'=>$info['data']['id'],'status'=>0])->update(['status'=>-1]);
        $insert_data = [
            'user_id'=> $info['data']['user_id'],
            'type'   => 5,
            'obj_id' => $info['data']['id'],
            'notice' => $msg,
            'status' =>  $info['data']['status'],
            'add_time'=> time(),
            'url'    => ''
        ];
        db('user_notice')->insert($insert_data);
    }

    //6困难职工扶持申请
    private function kunnan_apply($info){
        //短信通知
        $uinfo = db('user')->where(['id'=>$info['data']['user_id']])->field('mobile,openid')->find();
        if ($uinfo && $uinfo['mobile']) {
            if ($info['status']==1) {//通过
                $msg = '您的困难职工扶持申请已经审核通过！请登入系统查看。';
            }else{
                $msg = '您的困难职工扶持申请未通过审核！原因：'.$info['data']['result'];
            }
            QueueApi::smsNotice($uinfo['mobile'],$msg);
        }
        //微信通知
        

        //更新代办状态
        db('user_notice')->where(['type'=>6,'obj_id'=>$info['data']['id'],'status'=>0])->update(['status'=>-1]);
        $insert_data = [
            'user_id'=> $info['data']['user_id'],
            'type'   => 6,
            'obj_id' => $info['data']['id'],
            'notice' => $msg,
            'status' =>  $info['data']['status'],
            'add_time'=> time(),
            'url'    => ''
        ];
        db('user_notice')->insert($insert_data);
    }

    //7欠薪求助申请
    private function arrears_help($info){
        //短信通知
        $uinfo = db('user')->where(['id'=>$info['data']['user_id']])->field('mobile,openid')->find();
        if ($uinfo && $uinfo['mobile']) {
            if ($info['status']==1) {//通过
                $msg = '您的欠薪求助申请已经审核通过！请登入系统查看。';
            }else{
                $msg = '您的欠薪求助申请未通过审核！原因：'.$info['data']['result'];
            }
            QueueApi::smsNotice($uinfo['mobile'],$msg);
        }
        //微信通知
        

        //更新代办状态
        db('user_notice')->where(['type'=>7,'obj_id'=>$info['data']['id'],'status'=>0])->update(['status'=>-1]);
        $insert_data = [
            'user_id'=> $info['data']['user_id'],
            'type'   => 7,
            'obj_id' => $info['data']['id'],
            'notice' => $msg,
            'status' =>  $info['data']['status'],
            'add_time'=> time(),
            'url'    => ''
        ];
        db('user_notice')->insert($insert_data);
    }

    //8职工互助报障申请
    private function employees_assistance($info){
        //短信通知
        $uinfo = db('user')->where(['id'=>$info['data']['user_id']])->field('mobile,openid')->find();
        if ($uinfo && $uinfo['mobile']) {
            if ($info['status']==1) {//通过
                $msg = '您的职工互助报障申请已经审核通过！请登入系统查看。';
            }else{
                $msg = '您的职工互助报障申请未通过审核！原因：'.$info['data']['result'];
            }
            QueueApi::smsNotice($uinfo['mobile'],$msg);
        }
        //微信通知
        
        //更新代办状态
        db('user_notice')->where(['type'=>8,'obj_id'=>$info['data']['id'],'status'=>0])->update(['status'=>-1]);
        $insert_data = [
            'user_id'=> $info['data']['user_id'],
            'type'   => 8,
            'obj_id' => $info['data']['id'],
            'notice' => $msg,
            'status' =>  $info['data']['status'],
            'add_time'=> time(),
            'url'    => isset($info['data']['url'])?$info['data']['url']:''
        ];
        db('user_notice')->insert($insert_data);
    }

    //9劳模和工匠创新申请
    private function model_craftsman($info){
        //短信通知
        $uinfo = db('user')->where(['id'=>$info['data']['user_id']])->field('mobile,openid')->find();
        if ($uinfo && $uinfo['mobile']) {
            if ($info['status']==1) {//通过
                $msg = '您的劳模和工匠创新申请已经审核通过！请登入系统查看。';
            }else{
                $msg = '您的劳模和工匠创新申请未通过审核！原因：'.$info['data']['result'];
            }
            QueueApi::smsNotice($uinfo['mobile'],$msg);
        }
        //微信通知
        

        //更新代办状态
        db('user_notice')->where(['type'=>9,'obj_id'=>$info['data']['id'],'status'=>0])->update(['status'=>-1]);
        $insert_data = [
            'user_id'=> $info['data']['user_id'],
            'type'   => 9,
            'obj_id' => $info['data']['id'],
            'notice' => $msg,
            'status' =>  $info['data']['status'],
            'add_time'=> time(),
            'url'    => ''
        ];
        db('user_notice')->insert($insert_data);
    }

    //10技能竞赛上报
    private function skills_competition($info){
        //短信通知
        $uinfo = db('user')->where(['id'=>$info['data']['user_id']])->field('mobile,openid')->find();
        if ($uinfo && $uinfo['mobile']) {
            if ($info['status']==1) {//通过
                $msg = '您的技能竞赛上报已经审核通过！请登入系统查看。';
            }else{
                $msg = '您的技能竞赛上报未通过审核！原因：'.$info['data']['result'];
            }
            QueueApi::smsNotice($uinfo['mobile'],$msg);
        }
        //微信通知
        

        //更新代办状态
        db('user_notice')->where(['type'=>10,'obj_id'=>$info['data']['id'],'status'=>0])->update(['status'=>-1]);
        $insert_data = [
            'user_id'=> $info['data']['user_id'],
            'type'   => 10,
            'obj_id' => $info['data']['id'],
            'notice' => $msg,
            'status' =>  $info['data']['status'],
            'add_time'=> time(),
            'url'    => ''
        ];
        db('user_notice')->insert($insert_data);
    }

    //11技能和学历培训
    private function skills_education($info){
        //短信通知
        $uinfo = db('user')->where(['id'=>$info['data']['user_id']])->field('mobile,openid')->find();
        if ($uinfo && $uinfo['mobile']) {
            if ($info['status']==1) {//通过
                $msg = '您的技能和学历培训申请已经审核通过！请登入系统查看。';
            }else{
                $msg = '您的技能和学历培训申请未通过审核！原因：'.$info['data']['result'];
            }
            QueueApi::smsNotice($uinfo['mobile'],$msg);
        }
        //微信通知
        

        //更新代办状态
        db('user_notice')->where(['type'=>$info['type'],'obj_id'=>$info['data']['id'],'status'=>0])->update(['status'=>-1]);
        $insert_data = [
            'user_id'=> $info['data']['user_id'],
            'type'   => $info['type'],
            'obj_id' => $info['data']['id'],
            'notice' => $msg,
            'status' =>  $info['data']['status'],
            'add_time'=> time(),
            'url'    => ''
        ];
        db('user_notice')->insert($insert_data);
    }
}
