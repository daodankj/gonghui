<?php

namespace app\admin\behavior;
use \app\admin\model\CompanyScoreLog;
use think\Db;
use addons\faqueue\library\QueueApi;

class KpilistCheck
{
    public function run(&$kpilist)
    {
        if ($kpilist['check_status']==1) {//审核通过
        	//获取积分数
        	$kpiInfo = db('kpi')->where(['id'=>$kpilist['kpi_id']])->field('id,topic_id,name,score,submit_num')->find();
        	//$companyInfo = db('company')->where(['id'=>$kpilist['company_id']])->field('id,score')->find();
            $companyInfo = db('union')->where(['company_id'=>$kpilist['company_id']])->field('company_id as id,score')->find();
        	if ($kpiInfo && $companyInfo) {
                //短信通知
                if (isset($kpilist['notice']) && $kpilist['notice']==1) {
                    $uinfo = db('user')->where(['id'=>$kpilist['user_id']])->field('mobile,openid')->find();
                    if ($uinfo && $uinfo['mobile']) {
                        $msg = '您的任务指标('.$kpiInfo['name'].')上报已经通过审核！';
                        QueueApi::smsNotice($uinfo['mobile'],$msg);
                    }
                }
                
                //改任务有获得过几分的就不用再加了,说明已经完成过了
                $has_score = db('company_score_log')->where(['kpi_id' => $kpiInfo['id'],'company_id' => $companyInfo['id']])->find();
                if ($has_score) {
                    return;
                }
                if (!isset($kpilist['otherPage'])) {//特殊页面的,应为不在指标上报任务里面，不用判断次数（比如文体活动）
                    //还需要判断次数
                    $submit_num = db('kpi_list')->where(['kpi_id'=>$kpilist['kpi_id'],'company_id'=>$kpilist['company_id'],'check_status'=>1])->count();
                    if ($submit_num!=$kpiInfo['submit_num']) {
                        return;
                    }
                }
                //新变化，积分按主题累计
                $topic_id = $kpiInfo['topic_id'];
                if (!$topic_id) {
                    return;
                }
                //工会积分从积分记录里面获取，应为要按主题累计积分
                $scoreLog = db('company_score_log')->where(['company_id'=>$companyInfo['id'],'topic_id'=>$topic_id])->order('id desc')->value('after');
                if ($scoreLog) {
                    $companyInfo['score'] = $scoreLog;
                }else{//没有说明是主题，积分从0开始累积
                    $companyInfo['score'] = 0;
                }
        	    $score = $companyInfo['score']+$kpiInfo['score'];
                //判断等级
                $kpiConfig = db('kpi_config')->where(['id'=>1])->find();
                if ($score>=$kpiConfig['A']) {
                    $type = 'A';
                }elseif ($score>=$kpiConfig['B']) {
                    $type = 'B';
                }elseif ($score>=$kpiConfig['C']) {
                    $type = 'C';
                }elseif ($score>=$kpiConfig['D']) {
                    $type = 'D';
                }else{
                    $type = 'D';
                }
        		Db::startTrans();
        		$CompanyScoreLog = CompanyScoreLog::create(['kpi_id' => $kpiInfo['id'],'company_id' => $companyInfo['id'], 'score' => $kpiInfo['score'], 'before' => $companyInfo['score'], 'after' => $companyInfo['score']+$kpiInfo['score'], 'memo' => '任务指标完成','createtime'=>time(),'topic_id'=>$topic_id,'union_type'=>$type]);
        		if ($CompanyScoreLog->id) {
        			$rs1 = db('company')->where(['id'=>$kpilist['company_id']])->update(['type'=>$type,'score'=>$score]);
                    $rs2 = db('union')->where(['company_id'=>$kpilist['company_id']])->update(['type'=>$type,'score'=>$score]);
        			if ($rs1&&$rs2) {
        				Db::commit();
        			}else{
        				Db::rollback();
        			}
        		}else{
        			Db::rollback();
        		}
        	}
        	
        }else{
            //短信通知
            //判断是否需要通知
            if (isset($kpilist['notice']) && $kpilist['notice']==1) {
                $kpiInfo = db('kpi')->where(['id'=>$kpilist['kpi_id']])->field('id,name')->find();
                $uinfo = db('user')->where(['id'=>$kpilist['user_id']])->field('mobile,openid')->find();
                if ($uinfo && $uinfo['mobile']) {
                    $msg = '您的任务指标('.$kpiInfo['name'].')上报未通过审核！原因：'.$kpilist['remark'];
                    QueueApi::smsNotice($uinfo['mobile'],$msg);
                }
            }
            
        }
    }
}
