<?php

return [
    'User_id'       => '会员',
    'S_name'        => '学生姓名',
    'S_sex'         => '学生性别',
    'S_age'         => '学生年龄',
    'S_idcard_type' => '学生证件类型',
    'S_idcard'      => '学生证件号码',
    'S_class'       => '拟入年级',
    'S_techang'     => '特长',
    'F_name'        => '家长姓名',
    'F_age'         => '家长年龄',
    'F_relation'    => '与学生关系',
    'F_lacation'    => '籍贯',
    'F_company'     => '工作单位',
    'F_job'         => '职务',
    'F_idcard_type' => '家长证件类型',
    'F_idcard'      => '家长证件号码',
    'Phone'         => '联系方式',
    'Image'         => '申请文件',
    'Add_time'      => '上报时间',
    'Status'        => '审核状态',
    'Result'        => '审核意见',
    'Check_time'    => '审核时间'
];
