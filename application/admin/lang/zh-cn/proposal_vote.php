<?php

return [
    'Proposal_id' => '提案ID',
    'Type'        => '类型',
    'Status'      => '投票状态',
    'Content'     => '评论内容',
    'Add_time'    => '投票时间'
];
