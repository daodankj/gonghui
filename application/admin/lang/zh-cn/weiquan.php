<?php

return [
    'User_id'  => '申请人',
    'Sex'      => '性别',
    'Name'     => '姓名',
    'Address'  => '单位/地址',
    'Idcard'   => '身份证号码',
    'Phone'    => '联系电话',
    'Num'      => '涉及人数',
    'Info'     => '反映内容',
    'Intro'    => '要求解决事项',
    'Shuqiu'   => '申请人诉求',
    'Result'   => '处理结果',
    'Add_time' => '申请时间'
];
