<?php

return [
    'User_id'     => '会员',
    'Compet_time' => '竞赛时间',
    'Compet_item' => '竞赛项目',
    'Users'       => '参与人',
    'Images'      => '竞赛图片',
    'Intro'       => '竞赛总结',
    'Add_time'    => '上班时间',
    'Status'      => '审核状态',
    'Result'      => '审核意见',
    'Check_time'  => '审核时间'
];
