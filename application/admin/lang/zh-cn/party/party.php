<?php

return [
    'User_id'        => '申请人',
    'Company_id'     => '企业',
    'Legal_person'   => '法人代表',
    'Person_num'     => '职工人数',
    'Woman_num'      => '女职工人数',
    'Business_scope' => '经营范围',
    'Nature'         => '企业性质',
    'User_num'       => '党员人数',
    'Address'        => '地址',
    'Phone'          => '联系电话',
    'Chairman'       => '支部书记',
    'Director_zz'    => '组织委员',
    'Director_wt'    => '文体委员',
    'Director_xc'    => '宣传委员',
    'Director_ld'    => '劳动解调委员',
    'Director_js'    => '经审主任',
    'Director_woman' => '组织委员',
    'Finance'        => '委员',
    'Add_time'       => '成立时间',
    'Status'         => '状态'
];
