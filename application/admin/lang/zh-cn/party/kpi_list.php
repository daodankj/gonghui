<?php

return [
    'Kpi_id'     => '指标名称',
    'Company_id' => '所属企业',
    'User_id'    => '上报人',
    'Intro'      => '上报信息简介',
    'Images'     => '上传文件',
    'Add_time'   => '上报时间'
];
