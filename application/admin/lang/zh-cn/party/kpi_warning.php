<?php

return [
    'Kpi_id'       => '指标任务',
    'Company_id'   => '所属公司',
    'Warning_type' => '预警类型',
    'Notice'       => '预警提示',
    'Add_time'     => '预警时间',
    'Kpi.name'     => '工作指标名称',
    'Company.name' => '公司名称'
];
