<?php

return [
    'User_id'            => '上报人',
    'Company_id'         => '所属公司',
    'Studio_name'        => '工作室名称',
    'Createdate'         => '创建日期',
    'Company'            => '所在单位',
    'Named_unit'         => '命名单位',
    'Professional_field' => '专业领域',
    'Nameddate'          => '命名日期',
    'Studio_type'        => '工作室类型',
    'User_num'           => '成员总数',
    'Leader_name'        => '工作室领衔人姓名',
    'Sex'                => '性别',
    'Birthday'           => '出生日期',
    'Education'          => '学历',
    'Mingzu'             => '名族',
    'Political_outlook'  => '政治面貌',
    'Major'              => '专业',
    'Technical_title'    => '技术职称',
    'Phone'              => '电话',
    'File'               => '申请文档',
    'Addtime'            => '上报时间'
];
