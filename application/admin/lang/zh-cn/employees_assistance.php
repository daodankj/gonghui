<?php

return [
    'User_id'    => '上报人',
    'Company_id' => '所属公司',
    'Company'    => '单位名称',
    'Name'       => '姓名',
    'Phone'      => '电话',
    'Idcard'     => '身份证号码',
    'Sex'        => '性别',
    'Image'      => '文件',
    'Add_time'   => '申请时间',
    'Status'     => '审核状态',
    'Result'     => '审核意见',
    'Check_time' => '审核时间'
];
