<?php

return [
    'User_id'    => '上报人',
    'Company_id' => '所属企业',
    'Type'       => '活动类型',
    'Name'       => '活动名称',
    'Images'     => '活动图片',
    'Intro'      => '活动介绍',
    'Status'     => '状态',
    'Add_time'   => '上报时间'
];
