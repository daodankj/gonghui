<?php

return [
    'User_id'        => '发布人',
    'Company_id'     => '党组织',
    'Name'           => '姓名',
    'Idcard'         => '身份证号码',
    'Phone'          => '手机号码',
    'Mingz'          => '民族',
    'Now_party_name' => '接收组织关系地所在党委',
    'To_party_name'  => '将要去的党支部名称',
    'Status'         => '状态',
    'Add_time'       => '申请时间',
    'Result'         => '审核意见'
];
