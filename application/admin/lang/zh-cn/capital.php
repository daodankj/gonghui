<?php

return [
    'Company_id'             => '所属企业',
    'User_id'                => '上报人',
    'Total_money'            => '总资金',
    'Staff_activities_money' => '员工活动支出',
    'Business_money'         => '业务支出',
    'Safeguard_rights_money' => '维权支出',
    'Administration_money'   => '行政支出',
    'Capital_money'          => '资本性支出',
    'Subsidy_money'          => '补助下级支出',
    'Cause_money'            => '事业支出',
    'Other_money'            => '其他支出',
    'Creaetime'              => '上报时间',
    'Company.name'           => '公司名称'
];
