<?php

return [
    'User_id'    => '发布人',
    'Company_id' => '党组织',
    'Name'       => '活动主题',
    'Type'       => '活动类型',
    'Content'    => '活动内容',
    'Address'    => '地址',
    'Start_time' => '开始时间',
    'End_time'   => '截至时间',
    'Add_time'   => '发布时间'
];
