<?php

return [
    'User_id'    => '会员',
    'Use_time'   => '使用时间',
    'Intro'      => '开展活动内容',
    'User_num'   => '参与人数',
    'Site_type'  => '场地类型',
    'Image'      => '申请文件',
    'Add_time'   => '上报时间',
    'Status'     => '审核状态',
    'Result'     => '审核意见',
    'Check_time' => '审核时间'
];
