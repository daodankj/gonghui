<?php

return [
    'User_id'    => '发布人',
    'Company_id' => '党组织',
    'Title'      => '标题',
    'Content'    => '内容',
    'Images'     => '文件',
    'Add_time'   => '上报时间'
];
