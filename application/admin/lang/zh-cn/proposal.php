<?php

return [
    'Company_id'    => '所属公司',
    'User_id'       => '所属用户',
    'Title'         => '提案名称',
    'Content'       => '提案内容',
    'Status'        => '状态',
    'Vote_end_time' => '讨论截至时间',
    'Add_time'      => '添加时间'
];
