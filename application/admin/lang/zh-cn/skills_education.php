<?php

return [
    'User_id'      => '会员',
    'Name'         => '姓名',
    'Sex'          => '性别',
    'Age'          => '年龄',
    'Idcard'       => '身份证号码',
    'Phone'        => '电话',
    'Education'    => '学历',
    'School_name'  => '毕业学校',
    'Mingzu'       => '名族',
    'Major'        => '拟报专业',
    'Hk_location'  => '户口所在地',
    'Work_time'    => '入职时间',
    'Work_station' => '所在岗位',
    'Image'        => '申请文件',
    'Add_time'     => '上报时间',
    'Status'       => '审核状态',
    'Result'       => '审核意见',
    'Check_time'   => '审核时间'
];
