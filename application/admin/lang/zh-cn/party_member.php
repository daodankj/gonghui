<?php

return [
    'User_id'    => '发布人',
    'Company_id' => '党组织',
    'Name'       => '姓名',
    'Idcard'     => '身份证号码',
    'Phone'      => '手机号码',
    'Status'     => '状态',
    'Add_time'   => '申请时间'
];
