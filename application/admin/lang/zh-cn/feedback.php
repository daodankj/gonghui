<?php

return [
    'User_id'      => '会员',
    'Uname'        => '会员名称',
    'Content'      => '反馈内容',
    'Add_time'     => '反馈时间',
    'Rely_content' => '回复内容',
    'Rely_time'    => '回复时间'
];
