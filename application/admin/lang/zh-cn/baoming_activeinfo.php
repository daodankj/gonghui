<?php

return [
    'Active_id'          => '所属活动',
    'Name'               => '姓名',
    'Phone'              => '电话',
    'Idcard'             => '身份证号码',
    'Sex'                => '性别',
    'Company_name'       => '所属企业',
    'Job'                => '职务',
    'Remark'             => '备注',
    'Add_time'           => '报名时间',
    'Baomingactive.name' => '活动标题'
];
