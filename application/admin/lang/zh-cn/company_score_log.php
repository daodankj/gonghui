<?php

return [
    'Kpi_id'       => '任务id',
    'Company_id'   => '企业ID',
    'Score'        => '本次得分',
    'Before'       => '变更前积分',
    'After'        => '变更后积分',
    'Memo'         => '备注',
    'Createtime'   => '获得时间',
    'Kpi.name'     => '工作指标名称',
    'Company.name' => '公司名称'
];
