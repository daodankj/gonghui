<?php

return [
    'Topic_id'     => '考核主题',
    'Name'         => '工作指标名称',
    'Requirement'  => '指标要求',
    'Grade'        => '重要等级',
    'Submit_type'  => '上报类型',
    'Score_type'   => '评分方式',
    'Score'        => '得分',
    'End_time'     => '完成时间',
    'Warning_type' => '预警时间',
    'Add_time'     => '添加时间'
];
