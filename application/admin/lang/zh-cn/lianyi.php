<?php

return [
    'Company_id'   => '所属公司',
    'User_id'      => '用户',
    'Name'         => '姓名',
    'Phone'        => '电话',
    'Birthday'     => '生日',
    'Age'          => '年龄',
    'Sex'          => '性别',
    'Height'       => '身高',
    'Native_place' => '籍贯',
    'Education'    => '学历',
    'Income'       => '收入',
    'Job'          => '职业',
    'Hobby'        => '兴趣爱好',
    'Skills'       => '才艺',
    'House'        => '是否有房',
    'Images'       => '生活照',
    'F_character'  => '性格要求',
    'F_job'        => '职业要求',
    'F_income'     => '收入要求',
    'F_height'     => '身高要求',
    'S_age'        => '年龄要求',
    'S_city'       => '地区要求',
    'F_house'      => '住房要求',
    'Add_time'     => '报名时间'
];
