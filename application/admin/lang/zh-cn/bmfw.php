<?php

return [
    'Name'     => '名称',
    'Image'    => '图片地址',
    'Address'  => '地址',
    'Lat'      => '维度',
    'Lng'      => '经度',
    'Intro'    => '简介',
    'Add_time' => '创建时间'
];
