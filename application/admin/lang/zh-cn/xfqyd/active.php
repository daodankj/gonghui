<?php

return [
    'Name'      => '活动名称',
    'Totalnum'  => '总数量',
    'Num'       => '每人领取数量',
    'Starttime' => '开始时间',
    'Endtime'   => '结束时间',
    'Status'    => '状态'
];
