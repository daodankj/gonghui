<?php

return [
    'Name'   => '名称',
    'Intro'  => '说明',
    'Image'  => 'logo',
    'Url'    => '连接地址',
    'Status' => '状态'
];
