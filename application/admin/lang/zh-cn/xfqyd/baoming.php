<?php

return [
    'Active_id' => '活动id',
    'Xfq_id'    => '消费券id',
    'Uname'     => '姓名',
    'Idcard'    => '身份证号码',
    'Mobile'    => '电话号码',
    'Add_time'  => '预约时间'
];
