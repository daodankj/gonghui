<?php

return [
    'User_id'    => '发布人',
    'Company_id' => '党组织',
    'Name'       => '会议名称',
    'Title'      => '主题',
    'Type'       => '会议类型',
    'Style'      => '会议方式',
    'Intro'      => '议题',
    'Notice'     => '通知人员',
    'Start_time' => '开始时间',
    'End_time'   => '截至时间',
    'Add_time'   => '发布时间'
];
