<?php

return [
    'Topic_id'   => '所属主题',
    'Company_id' => '所属公司',
    'Username'   => '用户名称',
    'User_id'    => '用户',
    'Ztgz'       => '总体工作',
    'Wthd'       => '文体活动',
    'Zgbf'       => '职工帮扶',
    'Mzgz'       => '民主工作',
    'Ldgx'       => '劳动关系',
    'Ghzz'       => '工会组织',
    'Add_time'   => '评议时间'
];
