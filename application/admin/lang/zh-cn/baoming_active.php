<?php

return [
    'Name'       => '活动标题',
    'Content'    => '活动内容',
    'Start_time' => '开始时间',
    'End_time'   => '结束时间',
    'Field_type' => '报名必填字段'
];
