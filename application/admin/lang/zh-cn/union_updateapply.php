<?php

return [
    'User_id'        => '申报人',
    'Company_id'     => '企业',
    'Chairman'       => '工会主席',
    'Chairman_f'     => '工会副主席',
    'Director_zz'    => '组织委员',
    'Director_wt'    => '文体委员',
    'Director_xc'    => '宣传委员',
    'Director_ld'    => '劳动解调委员',
    'Director_js'    => '经审主任',
    'Director_woman' => '女职工主任',
    'Finance'        => '财务委员',
    'Image'          => '申请书',
    'Add_time'       => '申报时间'
];
