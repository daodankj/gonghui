<?php

return [
    'Topic_id'   => '所属主题',
    'Company_id' => '所属公司',
    'User_id'    => '用户',
    'Username'   => '用户名称',
    'Status'     => '评议',
    'Content'    => '评议内容',
    'Add_time'   => '评议时间'
];
