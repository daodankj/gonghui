<?php

return [
    'User_id'     => '发布人',
    'Company_id'  => '党组织',
    'Change_time' => '换届时间',
    'Term_office' => '任期',
    'Type'        => '选举方式',
    'Yd_user_num' => '应到会场人数',
    'Sj_user_num' => '实际到场人数',
    'Image'       => '现场图片',
    'File'        => '相关文件',
    'Intro'       => '会议记录',
    'Add_time'    => '添加时间'
];
