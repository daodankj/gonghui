<?php

return [
    'Name'      => '超市名称',
    'Start_no'  => '卡号开始序号',
    'End_no'    => '卡号结束序号',
    'Total_num' => '总数量',
    'Use_num'   => '已使用数量',
    'Add_time'  => '添加时间'
];
