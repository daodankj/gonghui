<?php

return [
    'Name'        => '活动名称',
    'Cardshop_id' => '所属超市',
    'Num'         => '派送数量',
    'Starttime'   => '开始时间',
    'Endtime'     => '结束时间',
    'Status'      => '状态'
];
