<?php

return [
    'Cardshop_id' => '所属超市',
    'Active_id'   => '活动id',
    'User_id'     => '会员',
    'Uname'       => '姓名',
    'Idcard'      => '身份证号码',
    'Mobile'      => '电话号码',
    'Add_time'    => '报名时间',
    'Card_no'     => '中签卡号'
];
