<?php

namespace app\admin\controller;

use app\common\controller\Backend;

/**
 * 前台展示
 *
 * @icon fa fa-circle-o
 */
class Front extends Backend
{
    
    /**
     * front模型对象
     * @var \app\admin\model\Front
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Front;
    }
    
    public function index(){
        $row = $this->model->get(1);
        if ($this->request->isPost()) {
            $info = $this->request->post('row/a');
            $row->num1 = $info['num1'];
            $row->num2 = $info['num2'];
            $row->num3 = $info['num3'];
            $row->num4 = $info['num4'];
            if ($row->save()) {
                $this->success('保存成功');
            }else{
                $this->error('保存失败');
            }
        }

        $this->view->assign('row',$row);
        return $this->view->fetch();
    }

    public function index2(){
        $row = $this->model->get(1);
        if ($this->request->isPost()) {
            $info = $this->request->post('row/a');
            $row->num5 = $info['num5'];
            $row->num6 = $info['num6'];
            $row->num7 = $info['num7'];
            $row->num8 = $info['num8'];
            if ($row->save()) {
                $this->success('保存成功');
            }else{
                $this->error('保存失败');
            }
        }

        $this->view->assign('row',$row);
        return $this->view->fetch();
    }

    public function index3(){
        $row = $this->model->get(1);
        if ($this->request->isPost()) {
            $info = $this->request->post('row/a');
            $row->num9 = $info['num9'];
            $row->num10 = $info['num10'];
            $row->charge_info = $info['charge_info'];
            $row->abcd_intro = $info['abcd_intro'];
            if ($row->save()) {
                $this->success('保存成功');
            }else{
                $this->error('保存失败');
            }
        }

        $this->view->assign('row',$row);
        return $this->view->fetch();
    }

    public function index4(){
        $row = $this->model->get(1);
        if ($this->request->isPost()) {
            $info = $this->request->post('row/a');
            $row->companyProNum = $info['companyProNum'];
            $row->unionCompanyNum = $info['unionCompanyNum'];
            $row->userNum = $info['userNum'];
            $row->activeNum = $info['activeNum'];
            $row->companyNum = $info['companyNum'];
            $row->toDoNum = $info['toDoNum'];
            $row->weiquanNum = $info['weiquanNum'];
            $row->weiquanPassNum = $info['weiquanPassNum'];
            $row->kunnanNum = $info['kunnanNum'];
            $row->kunnanPassNum = $info['kunnanPassNum'];
            $row->arrearsNum = $info['arrearsNum'];
            $row->arrearsPassNum = $info['arrearsPassNum'];
            if ($row->save()) {
                $this->success('保存成功');
            }else{
                $this->error('保存失败');
            }
        }

        $this->view->assign('row',$row);
        return $this->view->fetch();
    }
    

}
