<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Db;
/**
 * 购物卡报名活动
 *
 * @icon fa fa-circle-o
 */
class CardshopActive extends Backend
{
    
    /**
     * CardshopActive模型对象
     * @var \app\admin\model\CardshopActive
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\CardshopActive;

    }

    //报名须知
    public function intro(){
        if ($this->request->isPost()) {
            $intro = $this->request->post('intro');
            if (!$intro) {
                $this->error('内容不能为空');
            }
            cache('cardshopActiveIntro',$intro,0);
            $this->success('保存成功');
        }
        $intro = cache('cardshopActiveIntro','');
        $this->view->assign('intro',$intro);
        return $this->view->fetch();
    }
    
    public function rock($ids){
        $info = $this->model->get($ids);
        if ($info->rock_status==1) {
            $this->error('已经摇过号，不能再摇号！');
        }
        //获取卡信息
        $cardInfo = db('cardshop')->where(['id'=>$info->cardshop_id])->find();
        if (!$cardInfo) {
            $this->error('该超市购物卡已下架，摇号失败！');
        }
        $num = $info['num'];
        if ($info['num']>($cardInfo['total_num']-$cardInfo['use_num'])) {
            $this->error('剩余折扣卡不足，请修改派送数量在摇号！剩余卡：'.($cardInfo['total_num']-$cardInfo['use_num']));
        }
        $list  = db('cardshop_baoming')->where(['active_id'=>$info['id'],'status'=>0])->orderRaw('rand()')->limit($num)->select();
        if (!$list) {
            $this->error('无报名记录，摇号失败！');
        }
        $start_no = $cardInfo['start_no'];//开始派发卡号
        //获取该超市已经发放出去的最大卡号
        $max_card_no = db('cardshop_baoming')->where(['cardshop_id'=>$info->cardshop_id])->order('card_no desc')->value('card_no');
        if ($max_card_no) {
            $start_no = $max_card_no+1;
        }
        Db::startTrans();
        $i=0;
        foreach ($list as $key => $val) {
            db('cardshop_baoming')->where(['id'=>$val['id']])->update(['status'=>1,'card_no'=>$start_no]);
            $start_no++;
            $i++;
        }
        $info->rock_status=1;
        $rs1 = $info->save();//修改状态
        //修改已经派送数量
        $rs2 = db('cardshop')->where(['id'=>$info->cardshop_id])->update(['use_num'=>$cardInfo['use_num']+$i]);
        if($rs1&&$rs2){
            Db::commit();
            $this->success('摇号完成，成功派发'.$i);
        }else{
            Db::rollback();
            $this->error('摇号失败！');
        }
        
    }
    

}
