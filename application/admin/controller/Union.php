<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use think\Db;
use think\Exception;
use think\exception\PDOException;
use think\exception\ValidateException;
use \app\admin\model\CompanyScoreLog;

/**
 * 工会
 *
 * @icon fa fa-circle-o
 */
class Union extends Backend
{

    /**
     * Union模型对象
     * @var \app\admin\model\Union
     */
    protected $model = null;
    protected $searchFields = 'company.name,chairman';//默认搜索字段

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Union;
        $this->view->assign("typeList", $this->model->getTypeList());
    }

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $c_where = [];//企业权限过滤
            if (!$this->auth->isSuperAdmin()&&$this->auth->company_ids){
                $c_where = ['company_id'=>['in',$this->auth->company_ids]];
            }
            $total = $this->model
                ->with('company')
                ->where($where)
                ->where($c_where)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->with('company')
                ->where($where)
                ->where($c_where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                //判断该企业是否已经存在工会
                $info = $this->model->get(['company_id'=>$params['company_id']]);
                if ($info) {
                    $this->error('该企业已经成立工会，不能重复创建！');
                }
                $params = $this->preExcludeFields($params);

                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    if(!$params['add_time']){
                        unset($params['add_time']);
                    }
                    if(!$params['s_add_time']){
                        unset($params['s_add_time']);
                    }
                    $result = $this->model->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                //判断该企业是否已经存在工会
                $info = $this->model->get(['id'=>['neq',$ids],'company_id'=>$params['company_id']]);
                if ($info) {
                    $this->error('该企业已经成立工会，不能重复创建！');
                }
                $params = $this->preExcludeFields($params);
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }
                    if(!$params['add_time']){
                        unset($params['add_time']);
                    }
                    if(!$params['s_add_time']){
                        unset($params['s_add_time']);
                    }
                    $result = $row->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

    public function struct($ids){
        $info = $this->model->get($ids);
        $this->view->assign('info',$info);
        return $this->view->fetch();
    }

    public function addscore($ids){
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if (!$params['score']) {
                $this->error('请输入积分');
            }

            if (!$params['info']) {
                $this->error('请输入积分变动原因');
            }
            //获取最新考核主题id
            $topic_id = db('topic')->order('id desc')->value('id');
            if (!$topic_id) {
                $this->error('请先在考核管理栏目中添加考核主题');
            }
            //工会积分从积分记录里面获取，应为要按主题累计积分
            $scoreLog = db('company_score_log')->where(['company_id'=>$row->company_id,'topic_id'=>$topic_id])->order('id desc')->value('after');
            if ($scoreLog) {
                $row->score = $scoreLog;
            }else{//没有说明是主题，积分从0开始累积
                $row->score = 0;
            }
            $score = $row->score+$params['score'];
            //判断等级
            $kpiConfig = db('kpi_config')->where(['id'=>1])->find();
            if ($score>=$kpiConfig['A']) {
                $type = 'A';
            }elseif ($score>=$kpiConfig['B']) {
                $type = 'B';
            }elseif ($score>=$kpiConfig['C']) {
                $type = 'C';
            }elseif ($score>=$kpiConfig['D']) {
                $type = 'D';
            }else{
                $type = 'D';
            }

            Db::startTrans();
            $memo = '后台修改，原因：'.$params['info'].'(操作人：'.$this->auth->username.'，联系方式：'.$this->auth->phone.')';
            $CompanyScoreLog = CompanyScoreLog::create(['kpi_id' => 0,'company_id' => $row->company_id,'name' => $params['name'], 'score' => $params['score'], 'before' => $row->score, 'after' => $row->score+$params['score'], 'memo' => $memo,'createtime'=>time(),'topic_id'=>$topic_id,'union_type'=>$type]);
            if ($CompanyScoreLog->id) {
                $rs1 = db('company')->where(['id'=>$row->company_id])->update(['type'=>$type,'score'=>$score]);
                $rs2 = db('union')->where(['id'=>$ids])->update(['type'=>$type,'score'=>$score]);
                if ($rs1&&$rs2) {
                    Db::commit();
                    $this->success('操作成功');
                }else{
                    Db::rollback();
                    $this->error('操作失败');
                }
            }else{
                Db::rollback();
                $this->error('操作失败');
            }
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

    /**
     * 导入
     */
    public function import()
    {
        $file = $this->request->request('file');
        if (!$file) {
            $this->error(__('Parameter %s can not be empty', 'file'));
        }
        $filePath = ROOT_PATH . DS . 'public' . DS . $file;
        if (!is_file($filePath)) {
            $this->error(__('No results were found'));
        }
        //实例化reader
        $ext = pathinfo($filePath, PATHINFO_EXTENSION);
        if (!in_array($ext, ['csv', 'xls', 'xlsx'])) {
            $this->error(__('Unknown data format'));
        }
        if ($ext === 'csv') {
            $file = fopen($filePath, 'r');
            $filePath = tempnam(sys_get_temp_dir(), 'import_csv');
            $fp = fopen($filePath, "w");
            $n = 0;
            while ($line = fgets($file)) {
                $line = rtrim($line, "\n\r\0");
                $encoding = mb_detect_encoding($line, ['utf-8', 'gbk', 'latin1', 'big5']);
                if ($encoding != 'utf-8') {
                    $line = mb_convert_encoding($line, 'utf-8', $encoding);
                }
                if ($n == 0 || preg_match('/^".*"$/', $line)) {
                    fwrite($fp, $line . "\n");
                } else {
                    fwrite($fp, '"' . str_replace(['"', ','], ['""', '","'], $line) . "\"\n");
                }
                $n++;
            }
            fclose($file) || fclose($fp);

            $reader = new Csv();
        } elseif ($ext === 'xls') {
            $reader = new Xls();
        } else {
            $reader = new Xlsx();
        }


        //加载文件
        $insert = [];
        try {
            if (!$PHPExcel = $reader->load($filePath)) {
                $this->error(__('Unknown data format'));
            }
            $currentSheet = $PHPExcel->getSheet(0);  //读取文件中的第一个工作表
            $allColumn = $currentSheet->getHighestDataColumn(); //取得最大的列号
            $allRow = $currentSheet->getHighestRow(); //取得一共有多少行
            $maxColumnNumber = Coordinate::columnIndexFromString($allColumn);
            /*$fields = [];
            for ($currentRow = 1; $currentRow <= 1; $currentRow++) {
                for ($currentColumn = 1; $currentColumn <= $maxColumnNumber; $currentColumn++) {
                    $val = $currentSheet->getCellByColumnAndRow($currentColumn, $currentRow)->getValue();
                    $fields[] = $val;
                }
            }*/
            for ($currentRow = 2; $currentRow <= $allRow; $currentRow++) {
                $values = [];
                for ($currentColumn = 1; $currentColumn <= $maxColumnNumber; $currentColumn++) {
                    $val = $currentSheet->getCellByColumnAndRow($currentColumn, $currentRow)->getValue();
                    $values[] = is_null($val) ? '' : $val;
                }
                /*$row = [];
                $temp = array_combine($fields, $values);
                foreach ($temp as $k => $v) {
                    if (isset($fieldArr[$k]) && $k !== '') {
                        $row[$fieldArr[$k]] = $v;
                    }
                }
                if ($row) {
                    $insert[] = $row;
                }*/
                //----
                if ($values) {
                    $insert[] = $values;
                }
            }
        } catch (Exception $exception) {
            $this->error($exception->getMessage());
        }
        if (!$insert) {
            $this->error(__('No rows were updated'));
        }
        $insertData = [];
        $i = 0;
        try {
            foreach ($insert as $val) {
                $company_id = $this->getCompanyId($val[1]);
                if (!$company_id) {
                    continue;
                }
                $len = strlen($val[9]);
                if ($len!==11) {
                   $val[9]='';
                }
                $insertData[$i]['company_id'] = $company_id;
                $insertData[$i]['type'] = 'D';
                $insertData[$i]['legal_person'] = $val[10]?$val[10]:'';
                $insertData[$i]['person_num'] = $val[3]??0;
                $insertData[$i]['woman_num'] = $val[4]??0;
                $insertData[$i]['business_scope'] = $val[5]??'';
                $insertData[$i]['nature'] = $val[6]??'';
                $insertData[$i]['user_num'] = $val[7]??0;
                $insertData[$i]['address'] = '肇庆高新区';
                $insertData[$i]['phone'] = $val[9]??'';
                $insertData[$i]['chairman'] = $val[10]??'';
                $insertData[$i]['chairman_f'] = $val[11]??'';
                $insertData[$i]['director_zz'] = $val[12]??'';
                $insertData[$i]['director_wt'] = $val[13]??'';
                $insertData[$i]['director_xc'] = $val[14]??'';
                $insertData[$i]['director_ld'] = $val[15]??'';
                $insertData[$i]['director_js'] = $val[16]??'';
                $insertData[$i]['director_woman'] = $val[17]??'';
                $insertData[$i]['finance'] = $val[18]??'';
                if ($val[19]) {
                    $add_time = date('Y-m-d H:i:s',strtotime($val[19]));
                    $insertData[$i]['add_time'] = $add_time;
                }
                $i++;
            }
            $this->model->saveAll($insertData);
        } catch (PDOException $exception) {
            $msg = $exception->getMessage();
            if (preg_match("/.+Integrity constraint violation: 1062 Duplicate entry '(.+)' for key '(.+)'/is", $msg, $matches)) {
                $msg = "导入失败，包含【{$matches[1]}】的记录已存在";
            };
            $this->error($msg);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }

        $this->success();
    }

    //判断公司是否存在
    private function getCompanyId($name){
        if (!$name) {//名字空的不管
            return false;
        }
        $id = db('company')->where(['name'=>$name])->value('id');
        if (!$id) {//不存在
            return false;
        }
        //判断工会是否已经存在
        $is_create = db('union')->where(['company_id'=>$id])->value('id');
        if ($is_create) {
            return false;
        }

        return $id;
    }


    /**
     * 导入企业详情
     */
    public function importInfo()
    {
        $file = 'uploads/20201015/companyinfo.xls';
        if (!$file) {
            $this->error(__('Parameter %s can not be empty', 'file'));
        }
        $filePath = ROOT_PATH . DS . 'public' . DS . $file;
        if (!is_file($filePath)) {
            $this->error(__('No results were found'));
        }
        //实例化reader
        $ext = pathinfo($filePath, PATHINFO_EXTENSION);
        if (!in_array($ext, ['csv', 'xls', 'xlsx'])) {
            $this->error(__('Unknown data format'));
        }
        if ($ext === 'csv') {
            $file = fopen($filePath, 'r');
            $filePath = tempnam(sys_get_temp_dir(), 'import_csv');
            $fp = fopen($filePath, "w");
            $n = 0;
            while ($line = fgets($file)) {
                $line = rtrim($line, "\n\r\0");
                $encoding = mb_detect_encoding($line, ['utf-8', 'gbk', 'latin1', 'big5']);
                if ($encoding != 'utf-8') {
                    $line = mb_convert_encoding($line, 'utf-8', $encoding);
                }
                if ($n == 0 || preg_match('/^".*"$/', $line)) {
                    fwrite($fp, $line . "\n");
                } else {
                    fwrite($fp, '"' . str_replace(['"', ','], ['""', '","'], $line) . "\"\n");
                }
                $n++;
            }
            fclose($file) || fclose($fp);

            $reader = new Csv();
        } elseif ($ext === 'xls') {
            $reader = new Xls();
        } else {
            $reader = new Xlsx();
        }


        //加载文件
        $insert = [];
        try {
            if (!$PHPExcel = $reader->load($filePath)) {
                $this->error(__('Unknown data format'));
            }
            $currentSheet = $PHPExcel->getSheet(0);  //读取文件中的第一个工作表
            $allColumn = $currentSheet->getHighestDataColumn(); //取得最大的列号
            $allRow = $currentSheet->getHighestRow(); //取得一共有多少行
            $maxColumnNumber = Coordinate::columnIndexFromString($allColumn);
            /*$fields = [];
            for ($currentRow = 1; $currentRow <= 1; $currentRow++) {
                for ($currentColumn = 1; $currentColumn <= $maxColumnNumber; $currentColumn++) {
                    $val = $currentSheet->getCellByColumnAndRow($currentColumn, $currentRow)->getValue();
                    $fields[] = $val;
                }
            }*/
            for ($currentRow = 2; $currentRow <= $allRow; $currentRow++) {
                $values = [];
                for ($currentColumn = 1; $currentColumn <= $maxColumnNumber; $currentColumn++) {
                    $val = $currentSheet->getCellByColumnAndRow($currentColumn, $currentRow)->getValue();
                    $values[] = is_null($val) ? '' : $val;
                }
                /*$row = [];
                $temp = array_combine($fields, $values);
                foreach ($temp as $k => $v) {
                    if (isset($fieldArr[$k]) && $k !== '') {
                        $row[$fieldArr[$k]] = $v;
                    }
                }
                if ($row) {
                    $insert[] = $row;
                }*/
                //----
                if ($values) {
                    $insert[] = $values;
                }
            }
        } catch (Exception $exception) {
            echo $exception->getMessage();
            //$this->error($exception->getMessage());
        }
        if (!$insert) {
            $this->error(__('No rows were updated'));
        }
        $insertData = [];
        $companyIds = [];
        $i = 0;
        try {
            foreach ($insert as $val) {
                $company_id = $this->getCompanyIds($val[0]);
                if (!$company_id||in_array($company_id, $companyIds)) {
                    continue;
                }
                $companyIds[] = $company_id;
                $insertData[$i]['company_id'] = $company_id;
                $insertData[$i]['c_user_num'] = $val[1]?$val[1]:0;
                $insertData[$i]['c_women_num'] = $val[2]?$val[2]:0;
                $insertData[$i]['c_business'] = $val[3]??'';
                $insertData[$i]['c_nature'] = $val[4]??'';
                $insertData[$i]['u_user_num'] = $val[5]?$val[5]:0;
                $insertData[$i]['u_chairman'] = $val[6]??'';
                $insertData[$i]['u_build_time'] = $this->excelDateChange($val[7]);
                $insertData[$i]['d_secretary'] = $val[8]??'';
                $insertData[$i]['d_build_time'] = $this->excelDateChange($val[9]);
                $i++;
            }
            //dump($insertData);exit;
            //$this->model->saveAll($insertData);
            db('company_info')->insertAll($insertData);
        } catch (PDOException $exception) {
            $msg = $exception->getMessage();
            if (preg_match("/.+Integrity constraint violation: 1062 Duplicate entry '(.+)' for key '(.+)'/is", $msg, $matches)) {
                $msg = "导入失败，包含【{$matches[1]}】的记录已存在";
            };
            //$this->error($msg);
            echo $msg;
        } catch (\Exception $e) {
            //$this->error($e->getMessage());
            echo $e->getMessage();
        }
        return '导入成功'.$i;
        //$this->success();
    }

    //excel日期格式转换
    public function excelDateChange($val){
        if ($val) {
             $type  = gettype($val);
            if ($type=='string') {
                $num = strtotime($val);
            }else{
                $num = strtotime('1899-12-31')+$val*24*3600;
            }
        }else{
            $num = 0;
        };
        if ($num<0) {
            $num = 0;
        }
        return $num;
    }

    private function getCompanyIds($name){
        if (!$name) {//名字空的不管
            return false;
        }
        $id = db('company')->where(['name'=>$name])->value('id');
        if (!$id) {//不存在
            return false;
        }
        //判断工会是否已经存在
        $is_create = db('company_info')->where(['company_id'=>$id])->find();
        if ($is_create) {
            return false;
        }

        return $id;
    }


}
