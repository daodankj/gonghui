<?php

namespace app\admin\controller;

use app\common\controller\Backend;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class FrontMap extends Backend
{
    
    /**
     * Front模型对象
     * @var \app\admin\model\Front
     */
    protected $model = null;
    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    public function _initialize()
    {
        parent::_initialize();

    }
    
    public function index(){

        return $this->view->fetch();
    }
    
    public function kpi_list(){
        $id = $this->request->get('company_id',1);
        $info = db('company')->where(['id'=>$id])->field('id,name,short_name,score,type')->find();
        if ($info&&!$info['short_name']) {
            $info['short_name'] = $info['name'];
        }
        //更改按主题获取积分
        $topic_id = db('topic')->order('id desc')->value('id');
        $scoreLog = db('company_score_log')->where(['topic_id'=>$topic_id,'company_id'=>$id])->field('after,union_type')->order('id desc')->find();
        if ($scoreLog) {
            $info['score'] = $scoreLog['after'];
            if ($scoreLog['union_type']) {
               $info['type'] = $scoreLog['union_type'];
            }
        }else{//不存在说明还没积分从0开始
            $info['score'] = 0;
            $info['type'] = 'D';
        }
        $this->assign('info',$info);
        //获取考核类型
        $kpimodel = new \app\admin\model\Kpi;
        $this->view->assign("getTypeList", $kpimodel->getTypeList());
        return $this->view->fetch();
    }

    public function union_active(){
        return $this->view->fetch('index');
    }

    public function user(){
        return $this->view->fetch('index');
    }

    public function capital(){
        //查询资金上报信息
        $id = $this->request->get('company_id',1);
        $cmodel = new \app\admin\model\Capital;
        $row = $cmodel->where(['company_id'=>$id])->order('creaetime desc')->find();
        if ($row) {
            $leave_money = $row->total_money-$row->thisyear_money;
            $data = [
                ['value'=>$row->staff_activities_money ,'name'=>'职工活动支出'],
                ['value'=>$row->business_money ,'name'=>'业务支出'],
                ['value'=>$row->safeguard_rights_money ,'name'=>'维权支出'],
                ['value'=>$row->capital_money ,'name'=>'资本性支出'],
                ['value'=>$row->other_money ,'name'=>'其他支出'],
                ['value'=>$leave_money ,'name'=>'结余金额'],
            ];
        }else{
           $data = []; 
        }
        
        $this->assign('row',$row);
        $this->assign('data',$data);
        return $this->view->fetch();
    }
    //工会建设
    public function union_changeapply(){
        return $this->view->fetch('index');
    }

    public function union_updateapply(){
        return $this->view->fetch('index');
    }

    public function union_apply(){
        return $this->view->fetch('index');
    }
    //人才服务
    public function model_craftsman(){
        return $this->view->fetch('index');
    }

    public function skill_competition(){
        return $this->view->fetch('index');
    }

    public function skill_education(){
        return $this->view->fetch('index');
    }

    //职工帮扶
    public function weiquan(){
        return $this->view->fetch('index');
    }
    public function kunnan(){
        return $this->view->fetch('index');
    }
    public function arrears_help(){
        return $this->view->fetch('index');
    }
    public function employees_assistance(){
        return $this->view->fetch('index');
    }

    public function kpi_warning(){
        return $this->view->fetch();
    }

    public function info($type=1,$id=0){
        if ($type==1) {
            $info = db('kpi_list')->where(['id'=>$id])->find();
            $img = explode(',', $info['images']);
            $intro = $info['intro'];
        }else{
            $info = db('union_active')->where(['id'=>$id])->find();
            $img = explode(',', $info['images']);
            $intro = $info['intro'];
        }

        $this->assign('intro',$intro);
        $this->assign('img',$img);
        return $this->view->fetch();
    }

    //获取指标任务完成情况
    public function kpiinfo($type=1){
        $company_id = $this->request->get('company_id',1);
        //$type = $this->request->get('type',1);
        //获取任务指,最后一个指标主题
        $nowtime = date('Y-m-d H:i:s');
        $where['end_time'] = ['>',$nowtime];
        $dolist = [];//已完成
        $ndolist= [];//未完成
        $topic_id = db('topic')->order('id desc')->value('id');
        if ($topic_id) {
            $where = ['topic_id'=>$topic_id];
        }
        $kpiList = db('kpi')->where($where)->field('id,name,grade,score,submit_num')->order('grade asc,weigh desc')->select();
        foreach ($kpiList as $key => &$val) {
            //查询完成了多少次
            $num = db('kpi_list')->where(['company_id'=>$company_id,'kpi_id'=>$val['id'],'check_status'=>['<',2]])->count();
            if($num<$val['submit_num']){//未完成的
                $val['has_num'] = $num;
                array_push($ndolist,$val);
            }else{
                $val['has_num'] = $val['submit_num'];
                array_push($dolist,$val);
            }
        }
        if ($type==1) {
            $list = $dolist;
        }else{
            $list = $ndolist;
        }
        $total = count($list);

        $result = array("total" => $total, "rows" => $list);

        return json($result);
    }
}
