<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Config;

/**
 * 控制台
 *
 * @icon fa fa-dashboard
 * @remark 用于展示当前系统中的统计数据、统计报表及重要实时数据
 */
class Dashboard extends Backend
{

    /**
     * 查看
     */
    public function index()
    {
        $c_where = [];//企业权限过滤
        if ($this->auth->company_ids){
            $c_where = ['company_id'=>['in',$this->auth->company_ids]];
        }
        //加载指标上报记录只加载最后一个考核主题的
        $topic_id = db('topic')->order('id desc')->value('id');
        $topic_id||$topic_id=0;

        /*$hooks = config('addons.hooks');
        $uploadmode = isset($hooks['upload_config_init']) && $hooks['upload_config_init'] ? implode(',', $hooks['upload_config_init']) : 'local';
        $addonComposerCfg = ROOT_PATH . '/vendor/karsonzhang/fastadmin-addons/composer.json';
        Config::parse($addonComposerCfg, "json", "composer");
        $config = Config::get("composer");
        $addonVersion = isset($config['version']) ? $config['version'] : __('Unknown');*/
        //查询总数
        $totalInfo['union_apply'] = db('union_apply')->where($c_where)->count();
        $totalInfo['union_changeapply'] = db('union_changeapply')->where($c_where)->count();
        $totalInfo['union_updateapply'] = db('union_updateapply')->where($c_where)->count();
        $totalInfo['union_active'] = db('union_active')->where($c_where)->count();
        $totalInfo['kpi_list'] = db('kpi_list')->alias('l')->join('kpi k','l.kpi_id=k.id')->where('k.topic_id='.$topic_id)->where($c_where)->count();
        $totalInfo['model_craftsman'] = db('model_craftsman')->where($c_where)->count();
        $totalInfo['skills_competition'] = db('skills_competition')->where($c_where)->count();
        $totalInfo['skills_education'] = db('skills_education')->where($c_where)->count();
        $totalInfo['weiquan'] = db('weiquan')->where($c_where)->count();
        $totalInfo['kunnan'] = db('kunnan')->where($c_where)->count();
        $totalInfo['arrears_help'] = db('arrears_help')->where($c_where)->count();
        $totalInfo['employees_assistance'] = db('employees_assistance')->where($c_where)->count();
        $totalInfo['site_discount'] = db('site_discount')->where($c_where)->count();
        $totalInfo['school_discount'] = db('school_discount')->where($c_where)->count();
        $this->view->assign([
            'union_num'        => db('union')->count(),
            'active_num'       => db('union_active')->where($c_where)->where(['status'=>0])->count(),
            'user_num'       => db('user')->count(),
            'warning_num1'       => db('kpi_warning')->where(['warning_type'=>1])->count(),
            'warning_num2'       => db('kpi_warning')->where(['warning_type'=>2])->count(),
            'warning_num3'       => db('kpi_warning')->where(['warning_type'=>3])->count(),
            //'addonversion'       => $addonVersion,
            //'uploadmode'       => $uploadmode,
            'union_apply' => db('union_apply')->where($c_where)->where(['status'=>0])->count(),
            'union_changeapply' => db('union_changeapply')->where($c_where)->where(['status'=>0])->count(),
            'union_updateapply' => db('union_updateapply')->where($c_where)->where(['status'=>0])->count(),
            'kpi_list' => db('kpi_list')->alias('l')->join('kpi k','l.kpi_id=k.id')->where('k.topic_id='.$topic_id)->where($c_where)->where(['check_status'=>0])->count(),
            'model_craftsman' => db('model_craftsman')->where($c_where)->where(['status'=>0])->count(),
            'skills_competition' => db('skills_competition')->where($c_where)->where(['status'=>0])->count(),
            'skills_education' => db('skills_education')->where($c_where)->where(['status'=>0])->count(),
            'weiquan' => db('weiquan')->where($c_where)->where(['status'=>0])->count(),
            'kunnan' => db('kunnan')->where($c_where)->where(['status'=>0])->count(),
            'arrears_help' => db('arrears_help')->where($c_where)->where(['status'=>0])->count(),
            'employees_assistance' => db('employees_assistance')->where($c_where)->where(['status'=>0])->count(),
            'site_discount' => db('site_discount')->where($c_where)->where(['status'=>0])->count(),
            'school_discount' => db('school_discount')->where($c_where)->where(['status'=>0])->count(),
            'totalInfo' => $totalInfo
        ]);

        return $this->view->fetch();
    }

}
