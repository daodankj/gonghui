<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Hook;
/**
 * 欠薪求助
 *
 * @icon fa fa-circle-o
 */
class ArrearsHelp extends Backend
{
    
    /**
     * Kunnan模型对象
     * @var \app\admin\model\Kunnan
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\ArrearsHelp;
        $this->view->assign("isSignleList", $this->model->getIsSignleList());
        $this->view->assign("isWelfareList", $this->model->getIsWelfareList());
        $this->view->assign("isCompanyList", $this->model->getIsCompanyList());
        $this->view->assign("isEconomyList", $this->model->getIsEconomyList());
        $this->view->assign("isLowrentList", $this->model->getIsLowrentList());
        $this->view->assign("isMarkList", $this->model->getIsMarkList());
        $this->view->assign("isPrivateList", $this->model->getIsPrivateList());
        $this->view->assign("isPublicList", $this->model->getIsPublicList());
    }

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $c_where = [];//企业权限过滤
            if (!$this->auth->isSuperAdmin()&&$this->auth->company_ids){
                $c_where = ['company_id'=>['in',$this->auth->company_ids]];
            }
            $total = $this->model
                ->where($where)
                ->where($c_where)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->where($where)
                ->where($c_where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }
    //受理
    public function accept($ids){
        if ($this->request->isPost()) {
            $id = $this->request->post('id');
            $status = $this->request->post('status');
            $result = $this->request->post('result');
            if (!$result) {
                $result = $status==3?'情况已核实，给予受理':'情况不属实，不受理';
            }
            if (!$result) {
                $this->error('请输入审核意见');
            }
            $info = $this->model->get($id);
            if (!$info) {
                $this->error('该申请不存在，受理失败'.$id);
            }
            if ($info->status!=0) {
                $this->error('该申请已受理，不需再次受理');
            }
            $info->status = $status;
            $info->result = $result;
            $info->check_time = date('Y-m-d H:i:s');
            if ($info->save()) {
                //监听审核事件
                $infodata = ['status'=>$status,'type'=>7,'data'=>$info];
                Hook::listen("apply_check", $infodata);
                $this->success('受理成功');
            }else{
                $this->error('受理失败');
            }
            
        }
        $this->view->assign('id',$ids);
        return $this->view->fetch('union_changeapply/accept');
    }
    public function check($ids){
        if ($this->request->isPost()) {
            $id = $this->request->post('id');
            $result = $this->request->post('result');
            $status = $this->request->post('status');
            if (!$result) {
                //$result = $this->request->post('result2');
                $result = $status==1?'情况已核实，给予通过':'情况不属实，不通过';
            }
            if (!$result) {
                $this->error('请输入审核意见');
            }
            if (!$status) {
                $this->error('请选择审批结果');
            }
            $info = $this->model->get($id);
            if (!$info) {
                $this->error('该申请不存在，审核失败'.$id);
            }
            if ($info->status!=3) {
                $this->error('该申请还未受理，请先受理成功后再审核');
            }
            $info->status = $status;
            $info->result = $result;
            $info->check_time = date('Y-m-d H:i:s');
            if ($info->save()) {
                //监听审核事件
                $infodata = ['status'=>$status,'type'=>7,'data'=>$info];
                Hook::listen("apply_check", $infodata);
                $this->success('审核成功');
            }else{
                $this->error('审核失败');
            }
            
        }
        $info = $this->model->get($ids);
        $result = $info->result;
        $this->view->assign('result',$result);
        $this->view->assign('id',$ids);
        return $this->view->fetch();
    }
    

}
