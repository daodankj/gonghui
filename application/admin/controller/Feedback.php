<?php

namespace app\admin\controller;

use app\common\controller\Backend;

/**
 * 意见反馈
 *
 * @icon fa fa-circle-o
 */
class Feedback extends Backend
{
    
    /**
     * Feedback模型对象
     * @var \app\admin\model\Feedback
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Feedback;

    }
    
    
    public function replay($ids){
        if ($this->request->isPost()) {
            $id = $this->request->post('id');
            $rely_content = $this->request->post('rely_content');
            if (!$rely_content) {
                $this->error('请输入回复内容');
            }
            $info = $this->model->get($id);
            if (!$info) {
                $this->error('该反馈不存在，回复失败'.$id);
            }
            $info->rely_content = $rely_content;
            $info->rely_time = time();
            if ($info->save()) {
                $this->success('回复成功');
            }else{
                $this->error('回复失败');
            }
            
        }
        $info = $this->model->get($ids);
        $this->view->assign('info',$info);
        $this->view->assign('id',$ids);
        return $this->view->fetch();
    }
    

}
