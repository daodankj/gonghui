<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Hook;
/**
 * 场地优惠
 *
 * @icon fa fa-circle-o
 */
class SiteDiscount extends Backend
{
    
    /**
     * SiteDiscount模型对象
     * @var \app\admin\model\SiteDiscount
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\SiteDiscount;
        $this->view->assign("siteTypeList", $this->model->getSiteTypeList());
    }

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $c_where = [];//企业权限过滤
            if (!$this->auth->isSuperAdmin()&&$this->auth->company_ids){
                $c_where = ['company_id'=>['in',$this->auth->company_ids]];
            }
            $total = $this->model
                ->where($where)
                ->where($c_where)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->where($where)
                ->where($c_where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }
    
    public function check($ids){
        if ($this->request->isPost()) {
            $id = $this->request->post('id');
            $result = $this->request->post('result');
            $status = $this->request->post('status');
            if (!$result) {
                //$result = $this->request->post('result2');
                $result = $status==1?'情况已核实，给予通过':'情况不属实，不通过';
            }
            if (!$result) {
                $this->error('请输入审核意见');
            }
            if (!$status) {
                $this->error('请选择审批结果');
            }
            $info = $this->model->get($id);
            if (!$info) {
                $this->error('该申请不存在，审核失败'.$id);
            }
            $info->status = $status;
            $info->result = $result;
            $info->check_time = time();
            if ($info->save()) {
                //监听审核事件
                $infodata = ['status'=>$status,'type'=>12,'data'=>$info];
                Hook::listen("apply_check", $infodata);
                $this->success('审核成功');
            }else{
                $this->error('审核失败');
            }
            
        }
        $info = $this->model->get($ids);
        $result = $info->result;
        $this->view->assign('result',$result);
        $this->view->assign('id',$ids);
        return $this->view->fetch();
    }
    

}
