<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Hook;
/**
 * 指标上报记录
 *
 * @icon fa fa-circle-o
 */
class KpiList extends Backend
{

    /**
     * KpiList模型对象
     * @var \app\admin\model\KpiList
     */
    protected $model = null;
    protected $searchFields = 'kpi.name';//默认搜索字段
    protected $relationSearch=true;
    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\KpiList;

    }

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }

            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $c_where = [];//企业权限过滤
            if (!$this->auth->isSuperAdmin()&&$this->auth->company_ids){
                $c_where = ['company_id'=>['in',$this->auth->company_ids]];
            }
            //新增地图页面通过get搜索
            if ($this->request->get('company_id')) {
                $c_where = ['company_id'=>$this->request->get('company_id')];
            }
            //加载指标上报记录只加载最后一个考核主题的
            $filter = $this->request->get("filter", '');
            $filter = (array)json_decode($filter, true);
            $filter = $filter ? $filter : [];
            if (!isset($filter['kpi.topic_id'])||!$filter['kpi.topic_id']){
                $topic_id = db('topic')->order('id desc')->value('id');
                if ($topic_id) {
                    $c_where['kpi.topic_id'] = $topic_id;
                }
            }


            $total = $this->model
                ->with('kpi')
                ->where($where)
                ->where($c_where)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->with('kpi')
                ->where($where)
                ->where($c_where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }

    //审批
    public function check($ids){
        if ($this->request->isPost()) {
            $id = $this->request->post('id');
            $status = $this->request->post('status');
            $result = $this->request->post('result');
            $notice = $this->request->post('notice');
            if (!$result) {
                //$result = $this->request->post('result2');
                $result = $status==1?'情况已核实，给予通过':'情况不属实，不通过';
            }
            if (!$result) {
                $this->error('请输入审核意见');
            }
            $info = $this->model->get($id);
            if (!$info) {
                $this->error('该记录不存在，审核失败'.$id);
            }
            if ($info->check_status!=0) {
                $this->success('改记录已被审核，不用重新审核');
            }
            $info->check_status = $status;
            $info->remark = $result;
            $info->check_time = date('Y-m-d H:i:s');
            if ($info->save()) {
                //监听审核事件
                $info->notice = $notice;//是否短信通知
                Hook::listen("apilist_check_successed", $info);
                $this->success('审核成功');
            }else{
                $this->error('审核失败');
            }

        }
        $this->view->assign('id',$ids);
        return $this->view->fetch();
    }

}
