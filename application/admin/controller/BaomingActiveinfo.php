<?php

namespace app\admin\controller;

use app\common\controller\Backend;

/**
 * 报名记录
 *
 * @icon fa fa-circle-o
 */
class BaomingActiveinfo extends Backend
{
    
    /**
     * BaomingActiveinfo模型对象
     * @var \app\admin\model\BaomingActiveinfo
     */
    protected $model = null;
    protected $searchFields = 'name,phone';//默认搜索字段
    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\BaomingActiveinfo;

    }
    
    

    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                    ->with(['baomingactive'])
                    ->where($where)
                    ->order($sort, $order)
                    ->count();

            $list = $this->model
                    ->with(['baomingactive'])
                    ->where($where)
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();

            foreach ($list as &$row) {
                $row->idcard = "'".$row->idcard;
                $row->getRelation('baomingactive')->visible(['name']);
            }
            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }
}
