<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Config;
/**
 * 企业积分变动管理
 *
 * @icon fa fa-circle-o
 */
class CompanyScoreLog extends Backend
{
    
    /**
     * CompanyScoreLog模型对象
     * @var \app\admin\model\CompanyScoreLog
     */
    protected $noNeedLogin = ['list'];
    protected $noNeedRight = ['list'];
    protected $model = null;
    protected $searchFields = 'kpi.name,company.name';//默认搜索字段
    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\CompanyScoreLog;
    }
    
    
    

    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $c_where = [];//企业权限过滤
            if (!$this->auth->isSuperAdmin()&&$this->auth->company_ids){
                $c_where = ['company_id'=>['in',$this->auth->company_ids]];
            }
            $total = $this->model
                    ->with(['kpi','company'])
                    ->where($where)
                    ->where($c_where)
                    ->order($sort, $order)
                    ->count();

            $list = $this->model
                    ->with(['kpi','company'])
                    ->where($where)
                    ->where($c_where)
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();

            foreach ($list as $row) {
                
                $row->getRelation('kpi')->visible(['name']);
				$row->getRelation('company')->visible(['name']);
            }
            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        $topic_id = $this->request->get('topic_id');
        if (!$topic_id) {
            $topic_id = db('topic')->order('id desc')->value('id');
        }
        $this->view->assign("topic_id", $topic_id);
        return $this->view->fetch();
    }

    public function list(){
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                    ->with(['kpi','company'])
                    ->where($where)
                    ->order($sort, $order)
                    ->count();

            $list = $this->model
                    ->with(['kpi','company'])
                    ->where($where)
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();

            foreach ($list as $row) {
                
                $row->getRelation('kpi')->visible(['name']);
                $row->getRelation('company')->visible(['name']);
            }
            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        $topic_id = $this->request->get('topic_id');
        if (!$topic_id) {
            $topic_id = db('topic')->order('id desc')->value('id');
        }
        $this->view->assign("topic_id", $topic_id);
        return $this->view->fetch();
    }
}
