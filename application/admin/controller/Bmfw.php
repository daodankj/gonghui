<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Db;
use think\Exception;
use think\exception\PDOException;
use think\exception\ValidateException;
use tool\MiJiaApi;
use tool\YinshiApi;
use tool\TsgApi;

/**
 * 便民服务管理
 *
 * @icon fa fa-circle-o
 */
class Bmfw extends Backend
{
    
    /**
     * Bmfw模型对象
     * @var \app\admin\model\Bmfw
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Bmfw;

    }
    
    /**
     * 默认生成的控制器所继承的父类中有index/add/edit/del/multi五个基础方法、destroy/restore/recyclebin三个回收站方法
     * 因此在当前控制器中可不用编写增删改查的代码,除非需要自己控制这部分逻辑
     * 需要将application/admin/library/traits/Backend.php中对应的方法复制到当前控制器,然后进行修改
     */

    /**
     * @param $ids
     * @return string
     * @throws \think\Exception
     * @throws \think\exception\DbException
     */
    public function device_list($ids){
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            $data = $this->model->get($ids);
            if (empty($data['ys_appkey']) || empty($data['ys_appsecret'])){
                $this->error('请先设置appKey和appSecret');
            }
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $mApi = new YinshiApi($data['ys_appkey'],$data['ys_appsecret']);
            $res = $mApi->getDeviceList($offset,$limit);
            if ($mApi->errorMsg){
                $this->error($mApi->errorMsg);
            }
            $total = $res['page']['total'];
            $deviceList = $res['data'];
            foreach ($deviceList as &$val){
                $val['fwId'] = $ids;
                $val['addTime'] = ceil($val['addTime']/1000);
            }
            $result = array("total" => $total, "rows" => $deviceList);

            return json($result);
        }
        return $this->view->fetch();
    }

    //通道列表
    public function channel_list($fwId,$ids){
        $deviceSerial = $ids;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            $data = $this->model->get($fwId);
            if (empty($data['ys_appkey']) || empty($data['ys_appsecret'])){
                $this->error('请先设置appKey和appSecret');
            }
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $mApi = new YinshiApi($data['ys_appkey'],$data['ys_appsecret']);
            $res = $mApi->getDeviceChannelList($deviceSerial);
            if ($mApi->errorMsg){
                $this->error($mApi->errorMsg);
            }
            $total = count($res);
            $cList = $res;
            foreach ($cList as &$val){
                $val['fwId'] = $fwId;
            }
            $result = array("total" => $total, "rows" => $cList);

            return json($result);
        }
        return $this->view->fetch();
    }

    //子设备列表
    public function child_list($fwId,$ids){
        $deviceSerial = $ids;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            $data = $this->model->get($fwId);
            if (empty($data['ys_appkey']) || empty($data['ys_appsecret'])){
                $this->error('请先设置appKey和appSecret');
            }
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            $deviceList = [];
            $deviceIds = ['BE8436604-BE8247498','BE8436604-BE8247561','BE8436604-BE8386469','BE8436604-FT0219302','BE8436604-BE1530688','BE8436604-BE8386508','BE8436604-BE8386501','BE8436604-BE8386518','BE8436604-FT0219603','BE8436604-Q28710715','BE8436604-Q38058931','BE8436604-Q38058933','BE8436604-Q38058946','BE8436604-BE8386509'];
            //list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $mApi = new YinshiApi($data['ys_appkey'],$data['ys_appsecret']);
            //$res = $mApi->getChildDeviceList($deviceSerial,$offset,$limit);
            foreach ($deviceIds as $deviceId){
                $dInfo = $mApi->getDeviceInfo($deviceId);
                if (!empty($dInfo)){
                    $deviceList[] = $dInfo;
                }
            }
            $total = count($deviceList);
            $cList = $deviceList;
            foreach ($cList as &$val){
                $val['fwId'] = $fwId;
            }
            $result = array("total" => $total, "rows" => $cList);

            return json($result);
        }
        return $this->view->fetch('device_list');
    }

    //查看直播
    public function viewLive($fwId,$ids,$channel=1){
        $deviceSerial = $ids;
        $data = $this->model->get($fwId);
        if (empty($data['ys_appkey']) || empty($data['ys_appsecret'])){
            $this->error('请先设置appKey和appSecret');
        }
        $mApi = new YinshiApi($data['ys_appkey'],$data['ys_appsecret']);
        $url = $mApi->getLiveAddress($deviceSerial,$channel);
        header('Location:'.$url);
    }

    //获取图书借阅记录
    public function get_borrow_all(){
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $mApi = new TsgApi();
            $res = $mApi->getBorrowAll(1,$limit);
            if ($mApi->errorMsg){
                $this->error($mApi->errorMsg);
            }
            if ($res['code']!=200){
                $this->error($res['message']);
            }
            $total = $res['count'];
            $list = $res['jsonArray'];
            $result = array("total" => $total, "rows" => $list);
            return json($result);
        }
        return $this->view->fetch('device_list');
    }

    //获取图书借阅记录
    public function get_user_all(){
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $mApi = new TsgApi();
            $res = $mApi->getUsersAll(1,$limit);
            if ($mApi->errorMsg){
                $this->error($mApi->errorMsg);
            }
            if ($res['code']!=200){
                $this->error($res['message']);
            }
            $total = $res['count'];
            $list = $res['jsonArray'];
            $result = array("total" => $total, "rows" => $list);
            return json($result);
        }
        return $this->view->fetch('device_list');
    }
    
    public function cache($ids){
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $miJiaApi = new MiJiaApi($row['mi_user'],'');
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $miJiaApi->setCache($params['userId'],$params['ssecurity'],$params['deviceId'],$params['serviceToken']);
                $this->success();
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $miInfo = $miJiaApi->getCache();
        $this->view->assign("row", $miInfo);
        //$this->view->assign("miInfo", $miInfo);
        return $this->view->fetch();
    }

    //门禁出入记录
    public function get_in_out_list($fwId,$ids){
        $deviceSerial = $ids;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            $data = $this->model->get($fwId);
            if (empty($data['ys_appkey']) || empty($data['ys_appsecret'])){
                $this->error('请先设置appKey和appSecret');
            }
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams2();
            if (!empty($where['dateTime'])){
                $v = str_replace(' - ', ',', $where['dateTime']);
                $dateTime = array_slice(explode(',', $v), 0, 2);
                $startTime = $dateTime[0];
                $endTime = $dateTime[1];
            }else{
                $startTime = date('Y-m-d 00:00:00');
                $endTime = date('Y-m-d 23:59:59');
            }
            $mApi = new YinshiApi($data['ys_appkey'],$data['ys_appsecret']);
            $res = $mApi->getInOutList($deviceSerial,$startTime,$endTime,$offset,$limit);
            if ($mApi->errorMsg){
                $this->error($mApi->errorMsg);
            }
            $total = $res['totalMatches'];
            $cList = [];
            if (!empty($res['InfoList'])){
                foreach ($res['InfoList'] as $val){
                    $cList[] = [
                        'deviceSerial' => $deviceSerial,
                        'deviceName' => $val['deviceName'],
                        'majorEventType' => $val['majorEventType'],
                        'subEventType' => $val['subEventType'],
                        'dateTime' => $val['dateTime'],
                        'name' => $val['IDCardInfo']['name'],
                        'sex' => $val['IDCardInfo']['sex'],
                        'birth' => $val['IDCardInfo']['birth'],
                        'addr' => $val['IDCardInfo']['addr'],
                        'IDCardNo' => $val['IDCardInfo']['IDCardNo'],
                        'startDate' => $val['IDCardInfo']['startDate'],
                        'endDate' => $val['IDCardInfo']['endDate'],
                    ];
                }
            }
            $result = array("total" => $total, "rows" => $cList);
            return json($result);
        }
        return $this->view->fetch('device_list');
    }

    //获取门禁人员列表
    public function get_device_member_list($fwId,$ids){
        $deviceSerial = $ids;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            $data = $this->model->get($fwId);
            if (empty($data['ys_appkey']) || empty($data['ys_appsecret'])){
                $this->error('请先设置appKey和appSecret');
            }
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $mApi = new YinshiApi($data['ys_appkey'],$data['ys_appsecret']);
            $res = $mApi->getDeviceMemberList($deviceSerial,$offset,$limit);
            if ($mApi->errorMsg){
                $this->error($mApi->errorMsg);
            }
            $total = $res['totalMatches'];
            $cList = [];
            foreach ($res['UserInfo'] as $val){
                $cList[] = [
                    'deviceSerial' => $deviceSerial,
                    'name' => $val['name'],
                    'employeeNo' => $val['employeeNo'],
                    'gender' => $val['gender'],
                    'beginTime' => $val['Valid']['beginTime'],
                    'endTime' => $val['Valid']['endTime'],
                ];
            }
            $result = array("total" => $total, "rows" => $cList);

            return json($result);
        }
        return $this->view->fetch('member_list');
    }

    //添加门禁人员
    public function add_device_member($fwId,$ids)
    {
        $deviceSerial = $ids;
        if ($this->request->isPost()) {
            $data = $this->model->get($fwId);
            if (empty($data['ys_appkey']) || empty($data['ys_appsecret'])){
                $this->error('请先设置appKey和appSecret');
            }
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $mApi = new YinshiApi($data['ys_appkey'],$data['ys_appsecret']);
                //添加基础信息
                $mApi->addDeviceMember($deviceSerial,$params['employeeNo'],$params['name'],$params['gender'],$params['beginTime'],$params['endTime']);
                if ($mApi->errorMsg){
                    $this->error($mApi->errorMsg);
                }
                //添加扩展信息
                $mApi->addDeviceMemberExt($deviceSerial,$params['employeeNo'],$params['cardNo']);
                if ($mApi->errorMsg){
                    $this->error($mApi->errorMsg);
                }
                $this->success();
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    public function edit_device_member($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);

            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

    //告警事件列表
    public function get_event_record($fwId,$ids){
        $deviceSerial = $ids;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            $data = $this->model->get($fwId);
            if (empty($data['ys_appkey']) || empty($data['ys_appsecret'])){
                $this->error('请先设置appKey和appSecret');
            }
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams2();
            if (!empty($where['startTime'])){
                $v = str_replace(' - ', ',', $where['startTime']);
                $dateTime = array_slice(explode(',', $v), 0, 2);
                $startTime = $dateTime[0];
                $endTime = $dateTime[1];
            }else{
                $startTime = date('Y-m-d 00:00:00');
                $endTime = date('Y-m-d 23:59:59');
            }
            $mApi = new YinshiApi($data['ys_appkey'],$data['ys_appsecret']);
            $res = $mApi->getEventRecord($deviceSerial,$startTime,$endTime,$offset,$limit);
            if ($mApi->errorMsg){
                $this->error($mApi->errorMsg);
            }
            $total = $res['totalMatches'];
            $cList = [];
            if (!empty($res['Targets'])){
                foreach ($res['Targets'] as $val){
                    $cList[] = [
                        'deviceSerial' => $deviceSerial,
                        'channel' => $val['channel'],
                        'startTime' => $val['startTime'],
                        'endTime' => $val['endTime'],
                        'behaviorEventType' => $val['behavior']['behaviorEventType'],
                    ];
                }
            }
            $result = array("total" => $total, "rows" => $cList);

            return json($result);
        }
        return $this->view->fetch('device_list');
    }

    private function buildparams2(){
        $search = $this->request->get("search", '');
        $filter = $this->request->get("filter", '');
        $op = $this->request->get("op", '', 'trim');
        $sort = $this->request->get("sort", !empty($this->model) && $this->model->getPk() ? $this->model->getPk() : 'id');
        $order = $this->request->get("order", "DESC");
        $offset = $this->request->get("offset", 0);
        $limit = $this->request->get("limit", 0);
        $filter = (array)json_decode($filter, true);
        $op = (array)json_decode($op, true);
        $filter = $filter ? $filter : [];
        return [$filter, $sort, $order, $offset, $limit];
    }
}
