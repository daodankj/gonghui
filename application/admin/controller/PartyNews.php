<?php

namespace app\admin\controller;

use app\common\controller\Backend;

/**
 * 新闻稿上报
 *
 * @icon fa fa-circle-o
 */
class PartyNews extends Backend
{

    /**
     * PartyNews模型对象
     * @var \app\admin\model\PartyNews
     */
    protected $model = null;
    protected $searchFields = 'company.name,party_news.title';//默认搜索字段
    protected $relationSearch=true;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\PartyNews;

    }

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $c_where = [];//企业权限过滤
            if (!$this->auth->isSuperAdmin()&&$this->auth->company_ids){
                $c_where = ['company_id'=>['in',$this->auth->company_ids]];
            }
            $total = $this->model
                ->with('company')
                ->where($where)
                ->where($c_where)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->with('company')
                ->where($where)
                ->where($c_where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }


}
