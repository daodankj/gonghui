<?php

namespace app\admin\controller;

use app\common\controller\Backend;

/**
 * 提案投票管理
 *
 * @icon fa fa-circle-o
 */
class ProposalVote extends Backend
{
    
    /**
     * ProposalVote模型对象
     * @var \app\admin\model\ProposalVote
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\ProposalVote;

    }
    
    /**
     * 查看投票
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                ->where(['type'=>1])
                ->where($where)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->where(['type'=>1])
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }


    /**
     * 查看评价
     */
    public function pingjia()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                ->where(['type'=>2])
                ->where($where)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->where(['type'=>2])
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }
    
    public function info($proposal_id){
        $pModel = new \app\admin\model\Proposal;
        $info = $pModel->where(['id'=>$proposal_id])->find();
        $total0 = db('proposal_vote')->where(['proposal_id'=>$proposal_id,'type'=>1,'status'=>0])->count();
        $total1 = db('proposal_vote')->where(['proposal_id'=>$proposal_id,'type'=>1,'status'=>1])->count();
        $total2 = db('proposal_vote')->where(['proposal_id'=>$proposal_id,'type'=>1,'status'=>2])->count();
        $this->assign('info', $info);
        $this->assign('total0', $total0);
        $this->assign('total1', $total1);
        $this->assign('total2', $total2);
        return $this->view->fetch();
    }
}
