<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader\Xls;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use think\Db;
use think\Exception;
use think\exception\PDOException;
use think\exception\ValidateException;
/**
 * 企业管理
 *
 * @icon fa fa-circle-o
 */
class Company extends Backend
{

    /**
     * Company模型对象
     * @var \app\admin\model\Company
     */
    protected $noNeedLogin = ['index'];
    protected $noNeedRight = ['index'];
    protected $model = null;
    protected $searchFields = 'name,short_name';//默认搜索字段
    protected $relationSearch=true;
    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Company;
        $this->view->assign("typeList", $this->model->getTypeList());
        $this->view->assign("styleList", $this->model->getStyleList());
    }
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                if ($this->request->get('style')) {
                    $this->request->request(['custom'=>['style'=>$this->request->get('style')]]);
                }
                return $this->selectpage();
            }
            $e_where = [];//额外过滤
            $filter = json_decode($this->request->get('filter'),true);
            if(isset($filter['gh_map'])){
                if($filter['gh_map']==1){//在工会地图
                    $e_where = ['lat'=>['>',0],'lng'=>['>',0]];
                }else{
                    $e_where = ['lat'=>['<',1],'lng'=>['<',1]];
                }
                unset($filter['gh_map']);
            }
            if(isset($filter['dj_map'])){
                if($filter['dj_map']==1){//在党建地图
                    $e_where = ['lat'=>['>',0],'lng'=>['>',0]];
                    $pIds = db('party')->column('company_id');//已经成立的
                    $e_where['id'] = ['in',$pIds];
                }else{
                    $pIds = db('party')->column('company_id');//已经成立的
                    $e_where['id'] = ['not in',$pIds];
                }
                unset($filter['dj_map']);
            }
            $this->request->get(['filter'=>json_encode($filter)]);//重新设置变量
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $c_where = [];//企业权限过滤
            if (!$this->auth->isSuperAdmin()&&$this->auth->company_ids){
                $c_where = ['id'=>['in',$this->auth->company_ids]];
            }
            $total = $this->model
                ->with('companyinfo')
                ->where($where)
                ->where($c_where)
                ->where($e_where)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->with('companyinfo')
                ->where($where)
                ->where($c_where)
                ->where($e_where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }
    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        //获取详情
        $info = db('company_info')->where(['company_id'=>$ids])->find();

        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }
                    $params['upstring'] = getMillisecond();
                    $result = $row->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                //保存企业详情
                $params_info = $this->request->post("info/a");
                if ($params_info['c_build_time']) {
                    $params_info['c_build_time'] = strtotime($params_info['c_build_time']);
                    $params_info['c_build_time'] = $params_info['c_build_time']<0?0:$params_info['c_build_time'];
                }
                if ($params_info['u_build_time']) {
                    $params_info['u_build_time'] = strtotime($params_info['u_build_time']);
                    $params_info['u_build_time'] = $params_info['u_build_time']<0?0:$params_info['u_build_time'];
                }
                if ($params_info['d_build_time']) {
                    $params_info['d_build_time'] = strtotime($params_info['d_build_time']);
                    $params_info['d_build_time'] = $params_info['d_build_time']<0?0:$params_info['d_build_time'];
                }
                if ($info) {//已经存在则修改
                    $result = db('company_info')->where(['company_id'=>$ids])->update($params_info);
                }else{
                    $params_info['company_id'] = $ids;
                    $result = db('company_info')->insert($params_info);
                }

                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }


        $this->view->assign("info", $info);
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

    public function pingyi($ids=null){
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $result = false;
                Db::startTrans();
                try {
                    $result = $row->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }
    /**
     * 导入
     */
    public function import()
    {
        $file = $this->request->request('file');
        if (!$file) {
            $this->error(__('Parameter %s can not be empty', 'file'));
        }
        $filePath = ROOT_PATH . DS . 'public' . DS . $file;
        if (!is_file($filePath)) {
            $this->error(__('No results were found'));
        }
        //实例化reader
        $ext = pathinfo($filePath, PATHINFO_EXTENSION);
        if (!in_array($ext, ['csv', 'xls', 'xlsx'])) {
            $this->error(__('Unknown data format'));
        }
        if ($ext === 'csv') {
            $file = fopen($filePath, 'r');
            $filePath = tempnam(sys_get_temp_dir(), 'import_csv');
            $fp = fopen($filePath, "w");
            $n = 0;
            while ($line = fgets($file)) {
                $line = rtrim($line, "\n\r\0");
                $encoding = mb_detect_encoding($line, ['utf-8', 'gbk', 'latin1', 'big5']);
                if ($encoding != 'utf-8') {
                    $line = mb_convert_encoding($line, 'utf-8', $encoding);
                }
                if ($n == 0 || preg_match('/^".*"$/', $line)) {
                    fwrite($fp, $line . "\n");
                } else {
                    fwrite($fp, '"' . str_replace(['"', ','], ['""', '","'], $line) . "\"\n");
                }
                $n++;
            }
            fclose($file) || fclose($fp);

            $reader = new Csv();
        } elseif ($ext === 'xls') {
            $reader = new Xls();
        } else {
            $reader = new Xlsx();
        }


        //加载文件
        $insert = [];
        try {
            if (!$PHPExcel = $reader->load($filePath)) {
                $this->error(__('Unknown data format'));
            }
            $currentSheet = $PHPExcel->getSheet(0);  //读取文件中的第一个工作表
            $allColumn = $currentSheet->getHighestDataColumn(); //取得最大的列号
            $allRow = $currentSheet->getHighestRow(); //取得一共有多少行
            $maxColumnNumber = Coordinate::columnIndexFromString($allColumn);
            /*$fields = [];
            for ($currentRow = 1; $currentRow <= 1; $currentRow++) {
                for ($currentColumn = 1; $currentColumn <= $maxColumnNumber; $currentColumn++) {
                    $val = $currentSheet->getCellByColumnAndRow($currentColumn, $currentRow)->getValue();
                    $fields[] = $val;
                }
            }*/
            for ($currentRow = 2; $currentRow <= $allRow; $currentRow++) {
                $values = [];
                for ($currentColumn = 1; $currentColumn <= $maxColumnNumber; $currentColumn++) {
                    $val = $currentSheet->getCellByColumnAndRow($currentColumn, $currentRow)->getValue();
                    $values[] = is_null($val) ? '' : $val;
                }
                /*$row = [];
                $temp = array_combine($fields, $values);
                foreach ($temp as $k => $v) {
                    if (isset($fieldArr[$k]) && $k !== '') {
                        $row[$fieldArr[$k]] = $v;
                    }
                }
                if ($row) {
                    $insert[] = $row;
                }*/
                //----
                if ($values) {
                    $insert[] = $values;
                }
            }
        } catch (Exception $exception) {
            $this->error($exception->getMessage());
        }
        if (!$insert) {
            $this->error(__('No rows were updated'));
        }
        $insertData = [];
        $i = 0;
        try {
            foreach ($insert as $val) {
                if ($this->updateCompany($val)) {
                    continue;//如果存在则直接修改
                }
                if (!in_array($val[2], ['企业','单位','其他'])) {
                    $val[2] = '企业';
                }
                $val[3] = strtoupper($val[3]);
                if (!in_array($val[3], ['A','B','C','D'])) {
                    $val[3] = 'A';
                }
                $insertData[$i]['name'] = $val[0];
                $insertData[$i]['short_name'] = $val[1];
                $insertData[$i]['style'] = $val[2];
                $insertData[$i]['type'] = $val[3];
                $insertData[$i]['url'] = $val[4];
                $insertData[$i]['lat'] = $val[5]?$val[5]:0;
                $insertData[$i]['lng'] = $val[6]?$val[6]:0;
                $insertData[$i]['upstring'] = getMillisecond();
                $i++;
            }
            $this->model->saveAll($insertData);
        } catch (PDOException $exception) {
            $msg = $exception->getMessage();
            if (preg_match("/.+Integrity constraint violation: 1062 Duplicate entry '(.+)' for key '(.+)'/is", $msg, $matches)) {
                $msg = "导入失败，包含【{$matches[1]}】的记录已存在";
            };
            $this->error($msg);
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }

        $this->success();
    }

    //判断公司是否存在
    private function updateCompany($val){
        if (!$val[0]) {//名字空的不管
            return true;
        }
        $id = db('company')->where(['name'=>$val[0]])->value('id');
        if (!$id) {//不存在
            return false;
        }
        //更新记录
        if (!in_array($val[2], ['企业','单位','其他'])) {
            $val[2] = '企业';
        }
        $val[3] = strtoupper($val[3]);
        if (!in_array($val[3], ['A','B','C','D'])) {
            $val[3] = 'A';
        }
        $updateData['id'] = $id;
        $updateData['short_name'] = $val[1];
        $updateData['style'] = $val[2];
        $updateData['type'] = $val[3];
        $updateData['url'] = $val[4];
        $updateData['lat'] = $val[5]?$val[5]:0;
        $updateData['lng'] = $val[6]?$val[6]:0;
        $updateData['upstring'] = getMillisecond();
        db('company')->update($updateData);
        return true;
    }
}
