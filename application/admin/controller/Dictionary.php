<?php

namespace app\admin\controller;

use app\common\controller\Backend;

/**
 * 数据字典库
 *
 * @icon fa fa-circle-o
 */
class Dictionary extends Backend
{
    
    /**
     * Dictionary模型对象
     * @var \app\admin\model\Dictionary
     */
    protected $model = null;
    protected $searchFields = 'name';//默认搜索字段
    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Dictionary;

    }
    
    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                if ($this->request->get('pid')||$this->request->get('pid')==0) {
                    $this->request->request(['custom'=>['pid'=>$this->request->get('pid')]]);
                }
                return $this->selectpage();
            }
            
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            if($this->request->get('pid')){
                $c_where['pid'] = $this->request->get('pid');
            }else{
                $c_where['pid'] = 0;
            }
            
            $total = $this->model
                ->where($c_where)
                ->where($where)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->where($c_where)
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }

     
    

}
