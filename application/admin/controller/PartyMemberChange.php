<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Hook;

/**
 * 党员组织关系转出申请
 *
 * @icon fa fa-circle-o
 */
class PartyMemberChange extends Backend
{

    /**
     * PartyMemberChange模型对象
     * @var \app\admin\model\PartyMemberChange
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\PartyMemberChange;

    }

    public function check($ids){
        if ($this->request->isPost()) {
            $id = $this->request->post('id');
            $status = $this->request->post('status');
            $result = $this->request->post('result');
            if (!$result) {
                $this->error('请输入受理意见');
            }
            $info = $this->model->get($id);
            if (!$info) {
                $this->error('该申请不存在，受理失败'.$id);
            }
            $info->status = $status;
            $info->result = $result;
            //$info->check_time = date('Y-m-d H:i:s');
            if ($info->save()) {
                //监听审核事件
                //$info['adminInfo'] = '受理网格员：'.$this->auth->username.'，联系方式：'.$this->auth->phone;
                $infodata = ['status'=>$status,'type'=>65,'data'=>$info];
                Hook::listen("party_apply_check", $infodata);
                $this->success('受理成功');
            }else{
                $this->error('受理失败');
            }

        }
        $this->view->assign('id',$ids);
        return $this->view->fetch();
    }


}
