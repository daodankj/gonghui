<?php

namespace app\admin\controller;

use app\common\controller\Backend;

/**
 * 年度工会资金使用情况
 *
 * @icon fa fa-circle-o
 */
class Capital extends Backend
{
    
    /**
     * Capital模型对象
     * @var \app\admin\model\Capital
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Capital;

    }
    
    

    /**
     * 查看
     */
    public function index()
    {
        //当前是否为关联查询
        $this->relationSearch = true;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax())
        {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField'))
            {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $c_where = [];//企业权限过滤
            if (!$this->auth->isSuperAdmin()&&$this->auth->company_ids){
                $c_where = ['company_id'=>['in',$this->auth->company_ids]];
            }
            $total = $this->model
                    ->with(['company'])
                    ->where($where)
                    ->where($c_where)
                    ->order($sort, $order)
                    ->count();

            $list = $this->model
                    ->with(['company'])
                    ->where($where)
                    ->where($c_where)
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();

            foreach ($list as $row) {
                
                $row->getRelation('company')->visible(['short_name']);
            }
            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }

    //视图
    public function view($ids){
        
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        //$leave_money = $row->total_money-$row->staff_activities_money-$row->business_money-$row->safeguard_rights_money-$row->administration_money-$row->capital_money-$row->subsidy_money-$row->cause_money-$row->other_money;
        $leave_money = $row->total_money-$row->thisyear_money;
        $data = [
            ['value'=>$row->staff_activities_money ,'name'=>'职工活动支出'],
            ['value'=>$row->business_money ,'name'=>'业务支出'],
            ['value'=>$row->safeguard_rights_money ,'name'=>'维权支出'],
            //['value'=>$row->administration_money ,'name'=>'行政支出'],
            ['value'=>$row->capital_money ,'name'=>'资本性支出'],
            //['value'=>$row->subsidy_money ,'name'=>'补助下级支出'],
            //['value'=>$row->cause_money ,'name'=>'事业支出'],
            ['value'=>$row->other_money ,'name'=>'其他支出'],
            ['value'=>$leave_money ,'name'=>'结余金额'],
        ];
        //详细明细数据
        $datainfo  = [
            ['value'=>$row->staff_activities_education_money,'name'=>'职工教育费'],
            ['value'=>$row->staff_activities_wtactive_money,'name'=>'文体活动费'],
            ['value'=>$row->staff_activities_xcactive_money,'name'=>'宣传活动费'],
            ['value'=>$row->staff_activities_other_money,'name'=>'其他活动费'],

            ['value'=>$row->business_peixun_money,'name'=>'培训费'],
            ['value'=>$row->business_meeting_money,'name'=>'会议费'],
            ['value'=>$row->business_waishi_money,'name'=>'外事费'],
            ['value'=>$row->business_zhuanxiang_money,'name'=>'专项业务费'],
            ['value'=>$row->business_other_money,'name'=>'其他业务支出'],

            ['value'=>$row->safeguard_rights_xietiao_money,'name'=>'劳动关系协调费'],
            ['value'=>$row->safeguard_rights_baohu_money,'name'=>'劳动保护费'],
            ['value'=>$row->safeguard_rights_falv_money,'name'=>'法律援助费'],
            ['value'=>$row->safeguard_rights_kunnan_money,'name'=>'困难职工帮扶费'],
            ['value'=>$row->safeguard_rights_wenruan_money,'name'=>'送温暖费'],
            ['value'=>$row->safeguard_rights_other_money,'name'=>'其他维权支出'],

            
            ['value'=>$row->capital_work_money,'name'=>'办公设备购置'],
            ['value'=>$row->capital_zhuanxiang_money,'name'=>'专用设备购置'],
            ['value'=>$row->capital_jiaotong_money,'name'=>'交通工具购置'],
            ['value'=>$row->capital_xiushang_money,'name'=>'大型修缮'],
            ['value'=>$row->capital_internate_money,'name'=>'信息网络购建'],
            ['value'=>$row->capital_other_money,'name'=>'其他资本性支出'],

            ['value'=>$row->other_money ,'name'=>'其他支出'],
            ['value'=>$leave_money ,'name'=>'结余金额'],
        ];
        //$name = ['员工活动支出','业务支出','维权支出','行政支出','资本性支出','补助下级支出','事业支出','其他支出'];

        //$this->assign('name',$name);
        $this->assign('row',$row);
        $this->assign('data',$data);
        $this->assign('datainfo',$datainfo);
        return $this->view->fetch();
    }
}
