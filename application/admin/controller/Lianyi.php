<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Db;
use think\Exception;
use addons\faqueue\library\QueueApi;
/**
 * 职工联谊
 *
 * @icon fa fa-circle-o
 */
class Lianyi extends Backend
{
    
    /**
     * Lianyi模型对象
     * @var \app\admin\model\Lianyi
     */
    protected $model = null;
    protected $searchFields = 'name,sex,age,education';//默认搜索字段

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Lianyi;

    }
    
    public function active_sign(){
        if ($this->request->isPost()) {
            $ids = $this->request->post("ids");
            $active_name = $this->request->post("active_name");
            if (!$ids || !$active_name) {
                $this->error('参数不能为空');
            }
            $result = false;
            Db::startTrans();
            try {
                $result = $this->model->where('id','in',$ids)->update(['active_name'=>$active_name]);
                Db::commit();
            }catch (Exception $e) {
                Db::rollback();
                $this->error($e->getMessage());
            }
            if ($result !== false) {
                $this->success('设置成功');
            } else {
                $this->error('设置失败');
            }
        }else{
            $this->error('参数错误');
        }
    }
    
    //审批
    public function check($ids){
        if ($this->request->isPost()) {
            $id = $this->request->post('id');
            $status = $this->request->post('status');
            $result = $this->request->post('result');
           
            $info = $this->model->get($id);
            if (!$info) {
                $this->error('该记录不存在，审核失败'.$id);
            }
            /*if ($info->status!=0) {
                $this->success('改记录已被审核，不用重新审核');
            }*/
            $info->status = $status;
            $info->remark = $result;
            if ($info->save()) {
                //驳回短信通知
                if($status==2){
                    $msg = '驳回修改通知：您的职工联谊报名资料信息被驳回修改，原因：['.$result.']，请根据要求进入系统补全相关信息。';
                    QueueApi::smsNotice($info->phone,$msg);
                }
                $this->success('审核成功');
            }else{
                $this->error('审核失败');
            }

        }
        $this->view->assign('id',$ids);
        return $this->view->fetch();
    }

    //短信通知
    public function notice(){
        if ($this->request->isPost()) {
            //$ids = $this->request->post("ids");
            $active_name = $this->request->post("active_name");
            $content = $this->request->post("content");
            if (!$active_name || !$content) {
                $this->error('参数不能为空');
            }
            $notice_num = 0;
            $result = $this->model->where('active_name','=',$active_name)->select();
            foreach ($result as $key => $info) {
                QueueApi::smsNotice($info->phone,$content);
                $info->remark = $content;
                $info->save();
                $notice_num++;
            }
            
            if ($notice_num == 0) {
                $this->error('没有可发送的记录');
            } else {
                $this->success('发送成功，匹配记录数'.$notice_num.'条');
            }
        }

        return $this->view->fetch();
    }

}
