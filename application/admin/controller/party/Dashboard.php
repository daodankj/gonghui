<?php

namespace app\admin\controller\party;

use app\common\controller\Backend;
use think\Config;

/**
 * 控制台
 *
 * @icon fa fa-dashboard
 * @remark 用于展示当前系统中的统计数据、统计报表及重要实时数据
 */
class Dashboard extends Backend
{

    /**
     * 查看
     */
    public function index()
    {
        $c_where = [];//企业权限过滤
        if ($this->auth->company_ids){
            $c_where = ['company_id'=>['in',$this->auth->company_ids]];
        }
        //加载指标上报记录只加载最后一个考核主题的
        $topic_id = db('party_topic')->order('id desc')->value('id');
        $topic_id||$topic_id=0;
        //查询总数
        $totalInfo['party_apply'] = db('party_apply')->where($c_where)->count();
        $totalInfo['party_changeapply'] = db('party_changeapply')->where($c_where)->count();
        $totalInfo['party_updateapply'] = db('party_updateapply')->where($c_where)->count();
        $totalInfo['party_member'] = db('party_member')->where($c_where)->count();
        $totalInfo['party_member_change'] = db('party_member_change')->where($c_where)->count();
        $totalInfo['kpi_list'] = db('party_kpi_list')->alias('l')->join('party_kpi k','l.kpi_id=k.id')->where('k.topic_id='.$topic_id)->where($c_where)->count();
        $this->view->assign([
            'party_num'        => db('party')->count(),
            //'study_num'       => db('party_study')->where($c_where)->count(),
            //'user_num'       => db('user')->count(),
            'warning_num1'       => db('party_kpi_warning')->where(['warning_type'=>1])->count(),
            'warning_num2'       => db('party_kpi_warning')->where(['warning_type'=>2])->count(),
            'warning_num3'       => db('party_kpi_warning')->where(['warning_type'=>3])->count(),
            'party_apply' => db('party_apply')->where($c_where)->where(['status'=>0])->count(),
            'party_changeapply' => db('party_changeapply')->where($c_where)->where(['status'=>0])->count(),
            'party_updateapply' => db('party_updateapply')->where($c_where)->where(['status'=>0])->count(),
            'kpi_list' => db('party_kpi_list')->alias('l')->join('party_kpi k','l.kpi_id=k.id')->where('k.topic_id='.$topic_id)->where($c_where)->where(['check_status'=>0])->count(),
            'party_member' => db('party_member')->where($c_where)->where(['status'=>0])->count(),
            'party_member_change' => db('party_member_change')->where($c_where)->where(['status'=>0])->count(),
            'totalInfo' => $totalInfo
        ]);

        return $this->view->fetch();
    }

}
