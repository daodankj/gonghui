<?php

namespace app\admin\controller\party;

use app\common\controller\Backend;

/**
 * 预警时间设置
 *
 * @icon fa fa-circle-o
 */
class WarningType extends Backend
{
    
    /**
     * WarningType模型对象
     * @var \app\admin\model\WarningType
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\party\WarningType;

    }
    
    

}
