<?php

namespace app\admin\controller;

use app\common\controller\Backend;

/**
 * 
 *
 * @icon fa fa-circle-o
 */
class FrontAdmin extends Backend
{
    
    /**
     * Front模型对象
     * @var \app\admin\model\Front
     */
    protected $model = null;
    protected $noNeedLogin = '*';
    protected $noNeedRight = '*';
    protected $layout = 'frontadmin';
    public function _initialize()
    {
    
        parent::_initialize();
        if (!$this->auth->id) {
            $this->redirect('/admin/index/login?url=/admin/front_admin');
        }
    }
    //管理首页
    public function index(){
        $list  = db('company')->order('score desc')->limit(400)->field('id,short_name,type,score')->select();

        $this->view->assign('list', $list);
        //获取网格员列表
        $alist = [];
        $groupId = db('auth_group')->where(['name'=>'网格员'])->value('id');
        if ($groupId) {
            $adminIds = db('auth_group_access')->where(['group_id'=>$groupId])->field('uid')->select();
            $adminIds = array_column($adminIds, 'uid');
            $alist = db('admin')->where(['id'=>['in',$adminIds]])->field('id,nickname as username,company_ids,weigh')->order('weigh desc')->select();
            foreach ($alist as $key => &$val) {//计算绑定企业数和待办数量
                $val['company_num'] = count(explode(',', $val['company_ids']));
                $val['ntodo_num'] = 0;
                if ($val['company_ids']) {
                    $c_where = ['company_id'=>['in',$val['company_ids']]];
                    $val['ntodo_num']+=db('union_active')->where($c_where)->where(['status'=>0])->count();
                    $val['ntodo_num']+=db('union_apply')->where($c_where)->where(['status'=>0])->count();
                    $val['ntodo_num']+=db('union_changeapply')->where($c_where)->where(['status'=>0])->count();
                    $val['ntodo_num']+=db('union_updateapply')->where($c_where)->where(['status'=>0])->count();
                    $val['ntodo_num']+=db('kpi_list')->where($c_where)->where(['check_status'=>0])->count();
                    $val['ntodo_num']+=db('model_craftsman')->where($c_where)->where(['status'=>0])->count();
                    $val['ntodo_num']+=db('skills_competition')->where($c_where)->where(['status'=>0])->count();
                    $val['ntodo_num']+=db('skills_education')->where($c_where)->where(['status'=>0])->count();
                    $val['ntodo_num']+=db('weiquan')->where($c_where)->where(['status'=>0])->count();
                    $val['ntodo_num']+=db('kunnan')->where($c_where)->where(['status'=>0])->count();
                    $val['ntodo_num']+=db('arrears_help')->where($c_where)->where(['status'=>0])->count();
                    $val['ntodo_num']+=db('employees_assistance')->where($c_where)->where(['status'=>0])->count();
                    $val['ntodo_num']+=db('site_discount')->where($c_where)->where(['status'=>0])->count();
                    $val['ntodo_num']+=db('school_discount')->where($c_where)->where(['status'=>0])->count();
                }
            }
        }
        $this->view->assign('alist', $alist);

        $this->view->assign('title', '管理首页');
        return $this->view->fetch('admin_index');
    }
    //网格员绑定企业列表
    public function admin_companylist($id){
        $ainfo = db('admin')->where(['id'=>$id])->field('id,nickname as username,company_ids')->find();
        $list  = db('company')->where(['id'=>['in',$ainfo['company_ids']]])->order('score desc')->limit(100)->field('id,short_name,type,score')->select();
        $this->view->assign('list', $list);
        $this->view->assign('username', $ainfo['username']);
        $this->view->assign('title', '网格员绑定企业列表');
        return $this->view->fetch();
    }
    //企业信息首页
    public function cindex($id = ''){
        $info = db('company')->where('id='.$id)->field('id,name,short_name,url')->find();
    	//企业详细信息
        $companyInfo = db('company_info')->where(['company_id'=>$info['id']])->find();
        if (!$companyInfo) {
        	$companyInfo = [
        		'c_build_time'=>'',
        		'c_nature'=>'',
        		'c_style'=>'',
        		'c_business'=>'',
        		'c_annual_value'=>'',
        		'c_user_num'=>'',
        		'u_build_time'=>'',
        		'u_chairman'=>'',
        		'u_user_num'=>'',
        		'd_build_time'=>'',
        		'd_secretary'=>'',
        		'd_user_num'=>'',
        		'd_nosuser_num'=>'',
        		'd_characteristic'=>'',
                'c_image'=>''
        	];
        }else{
            $companyInfo['c_build_time'] = $companyInfo['c_build_time']==0?'':date('Y年m月',$companyInfo['c_build_time']);
            $companyInfo['u_build_time'] = $companyInfo['u_build_time']==0?'':date('Y年m月',$companyInfo['u_build_time']);
            $companyInfo['d_build_time'] = $companyInfo['d_build_time']==0?'':date('Y年m月',$companyInfo['d_build_time']);
        }
        $cinfo = array_merge($info,$companyInfo);
        $this->view->assign('cinfo', $cinfo);

        $this->view->assign('title', '企业信息');
        return $this->view->fetch('admin');
    }

    //党组织信息  工会组织信息
    public function dinfo($id=1){
        $cinfo = db('front')->where(['id'=>1])->find();
        $this->assign('cinfo', $cinfo);
        $this->assign('id', $id);
        if ($id==1) {
           $this->view->assign('title', '全区党组织信息');
        }else{
            $this->view->assign('title', '全区工会组织信息');
        }
        return $this->view->fetch();
    }
    
    public function kpi_list(){
        $id = $this->request->get('company_id',1);
        $info = db('company')->where(['id'=>$id])->field('id,name,short_name,score,type')->find();
        if ($info&&!$info['short_name']) {
            $info['short_name'] = $info['name'];
        }
        $this->assign('info',$info);

        $this->view->assign('title', '考核指标');
        return $this->view->fetch();
    }

    public function union_active(){
    	$this->view->assign('title', '文体活动');
        return $this->view->fetch('index');
    }

    public function user(){
        $this->view->assign('title', '会员管理');
        return $this->view->fetch('index');
    }

    public function capital(){
        //查询资金上报信息
        $id = $this->request->get('company_id',1);
        $cmodel = new \app\admin\model\Capital;
        $row = $cmodel->where(['company_id'=>$id])->order('creaetime desc')->find();
        if ($row) {
            $leave_money = $row->total_money-$row->thisyear_money;
            $data = [
                ['value'=>$row->staff_activities_money ,'name'=>'职工活动支出'],
                ['value'=>$row->business_money ,'name'=>'业务支出'],
                ['value'=>$row->safeguard_rights_money ,'name'=>'维权支出'],
                ['value'=>$row->capital_money ,'name'=>'资本性支出'],
                ['value'=>$row->other_money ,'name'=>'其他支出'],
                ['value'=>$leave_money ,'name'=>'结余金额'],
            ];
        }else{
           $data = []; 
        }
        
        $this->assign('row',$row);
        $this->assign('data',$data);
        $this->view->assign('title', '经费情况');
        return $this->view->fetch();
    }
    //工会建设
    public function union_changeapply(){
        $this->view->assign('title', '换届申请');
        return $this->view->fetch('index');
    }

    public function union_updateapply(){
        $this->view->assign('title', '改选申请');
        return $this->view->fetch('index');
    }

    public function union_apply(){
        $this->view->assign('title', '成立申请');
        return $this->view->fetch('index');
    }
    //人才服务
    public function model_craftsman(){
        $this->view->assign('title', '创新工作室申报');
        return $this->view->fetch('index');
    }

    public function skill_competition(){
        $this->view->assign('title', '职工技能竞赛');
        return $this->view->fetch('index');
    }

    public function skill_education(){
        $this->view->assign('title', '技能和学历培训');
        return $this->view->fetch('index');
    }

    //职工帮扶
    public function weiquan(){
        $this->view->assign('title', '劳资纠纷调解');
        return $this->view->fetch('index');
    }
    public function kunnan(){
        $this->view->assign('title', '困难职工帮扶');
        return $this->view->fetch('index');
    }
    public function arrears_help(){
        $this->view->assign('title', '欠薪求助');
        return $this->view->fetch('index');
    }
    public function employees_assistance(){
        $this->view->assign('title', '互助保障');
        return $this->view->fetch('index');
    }

    public function kpi_warning(){
        $this->view->assign('title', '任务预警');
        return $this->view->fetch('index');
    }

    public function info($type=1,$id=0){
        if ($type==1) {
            $info = db('kpi_list')->where(['id'=>$id])->find();
            $img = explode(',', $info['images']);
            $intro = $info['intro'];
        }else{
            $info = db('union_active')->where(['id'=>$id])->find();
            $img = explode(',', $info['images']);
            $intro = $info['intro'];
        }

        $this->assign('intro',$intro);
        $this->assign('img',$img);
        return $this->view->fetch();
    }

    //企业积分排行
    public function score_top(){
        $list  = db('company')->where(['score'=>['>',0]])->order('score desc')->limit(50)->field('id,short_name,type,score')->select();

        $this->view->assign('list', $list);
        $this->view->assign('title', '企业积分排行');
        return $this->view->fetch();
    }

    //工会建设、职工帮扶、人才服务
    public function business_list($type=1,$company_id=0){
        if ($type==1) {
            $bgimg = '/assets/img/wmap_index_gh_bg.png';
            $this->view->assign('title', '工会建设');
        }elseif($type==2){
            $bgimg = '/assets/img/wmap_index_rcfw_bg.png';
            $this->view->assign('title', '人才服务');
        }else{
            $bgimg = '/assets/img/wmap_index_zg_bg.png';
            $this->view->assign('title', '职工帮扶');
        }
        //获取是否有换届申请
        $union_changeapply_num = db('union_changeapply')->where(['company_id'=>$company_id])->count();
        //是否有纠纷调解
        $weiquan_num = db('weiquan')->where(['company_id'=>$company_id])->count();
        //是否有欠薪求助
        $arrears_help_num = db('arrears_help')->where(['company_id'=>$company_id])->count();
        $numinfo = ['union_changeapply_num'=>$union_changeapply_num,'weiquan_num'=>$weiquan_num,'arrears_help_num'=>$arrears_help_num];
        

        $this->assign('bgimg', $bgimg);
        $this->assign('type', $type);
        $this->assign('company_id', $company_id);
        $this->assign('numinfo', $numinfo);
        return $this->view->fetch();
    }
}
