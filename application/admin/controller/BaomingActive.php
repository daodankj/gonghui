<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Db;
use \think\Url;
/**
 * 报名活动
 *
 * @icon fa fa-circle-o
 */
class BaomingActive extends Backend
{

    /**
     * BaomingActive模型对象
     * @var \app\admin\model\BaomingActive
     */
    protected $model = null;
    protected $searchFields = 'name';//默认搜索字段
    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\BaomingActive;
        $fieldTypeData = ['3'=>'身份证','4'=>'性别','5'=>'所在企业','6'=>'职务','7'=>'备注'];
        $this->view->assign('fieldTypeData', $fieldTypeData);
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);

                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    $field_type = $this->request->post("field_type/a");
                    $params['field_type'] = implode(',', $field_type);
                    $result = $this->model->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }
                    $field_type = $this->request->post("field_type/a");
                    $params['field_type'] = implode(',', $field_type);
                    $result = $row->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }

    //二维码
    public function qrcode($ids=0){

        //$url = url('/index/active/info/id/'.$ids,'','',true);
        Url::root('/');
        $url = Url::build('/index/active/info/id/'.$ids,'','',true);

        $this->view->assign("url", $url);
        return $this->view->fetch();
    }

    public function qiandao($ids=0){

        //$url = url('/index/active/info/id/'.$ids,'','',true);
        Url::root('/');
        $url = Url::build('/index/active/qiandao/id/'.$ids,'','',true);

        $this->view->assign("url", $url);
        return $this->view->fetch();
    }

}
