<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Hook;
/**
 * 维权劳资纠纷
 *
 * @icon fa fa-circle-o
 */
class Weiquan extends Backend
{
    
    /**
     * Weiquan模型对象
     * @var \app\admin\model\Weiquan
     */
    protected $model = null;
    protected $noNeedLogin = 'detail';
    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Weiquan;
        $this->view->assign("sexList", $this->model->getSexList());
    }

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $c_where = [];//企业权限过滤
            if (!$this->auth->isSuperAdmin()&&$this->auth->company_ids){
                $c_where = ['company_id'=>['in',$this->auth->company_ids]];
            }
            $total = $this->model
                ->where($where)
                ->where($c_where)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->where($where)
                ->where($c_where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }
    //受理
    public function accept($ids){
        if ($this->request->isPost()) {
            $id = $this->request->post('id');
            $status = $this->request->post('status');
            $result = $this->request->post('result');
            if (!$result) {
                $result = $status==3?'情况已核实，给予受理':'情况不属实，不受理';
            }
            if (!$result) {
                $this->error('请输入审核意见');
            }
            $info = $this->model->get($id);
            if (!$info) {
                $this->error('该申请不存在，受理失败'.$id);
            }
            if ($info->status!=0) {
                $this->error('该申请已受理，不需再次受理');
            }
            $info->status = $status;
            $info->result = $result;
            $info->check_time = date('Y-m-d H:i:s');
            if ($info->save()) {
                //监听审核事件
                $infodata = ['status'=>$status,'type'=>5,'data'=>$info];
                Hook::listen("apply_check", $infodata);
                $this->success('受理成功');
            }else{
                $this->error('受理失败');
            }
            
        }
        $this->view->assign('id',$ids);
        return $this->view->fetch('union_changeapply/accept');
    }
    public function check($ids){
        if ($this->request->isPost()) {
            $id = $this->request->post('id');
            $result = $this->request->post('result');
            $status = $this->request->post('status');
            if (!$result) {
                $result = $status==1?'情况已核实，给予通过':'情况不属实，不通过';
            }
            if (!$result) {
                $this->error('请输入审核意见');
            }
            if (!$status) {
                $this->error('请选择审批结果');
            }
            $info = $this->model->get($id);
            if (!$info) {
                $this->error('该申请不存在，审核失败'.$id);
            }
            if ($info->status!=3) {
                $this->error('该申请还未受理，请先受理成功后再审核');
            }
            $info->status = $status;
            $info->result = $result;
            $info->check_time = date('Y-m-d H:i:s');
            if ($info->save()) {
                //监听审核事件
                $infodata = ['status'=>$status,'type'=>5,'data'=>$info];
                Hook::listen("apply_check", $infodata);
                $this->success('审核成功');
            }else{
                $this->error('审核失败');
            }
            
        }
        $info = $this->model->get($ids);
        $result = $info->result;
        $this->view->assign('result',$result);
        $this->view->assign('id',$ids);
        return $this->view->fetch();
    }
    
    //签署申请文档预览
    public function detail($ids){
        
        return $this->view->fetch();
    }
    //下载文档
    public function down(){
        $url = url('detail',['ids'=>1],'',true);
        $info = file_get_contents($url);
        $sprnstr="<!--startdownload-->";      
        $eprnstr="<!--enddownload-->";   
        $pos = strpos($info, $sprnstr);
        $pos2 = strpos($info, $eprnstr);
        $info = substr($info, $pos+20,$pos2-$pos-20);
        echo $info;
        //打开缓冲区 
        ob_start(); 
        header("Cache-Control: public"); 
        Header("Content-type: application/octet-stream"); 
        Header("Accept-Ranges: bytes"); 
        
        $filename = $filename='维权劳资纠纷'.date("YmdHis");
        //判断浏览器类型
        if (strpos($_SERVER["HTTP_USER_AGENT"],'MSIE')) { 
         header('Content-Disposition: attachment; filename=test.doc'); 
        }else if (strpos($_SERVER["HTTP_USER_AGENT"],'Firefox')) { 
         Header('Content-Disposition: attachment; filename=test.doc'); 
        } else { 
         header('Content-Disposition: attachment; filename=test.doc'); 
        } 
         
        //不使用缓存
        header("Pragma:no-cache"); 
        //过期时间 
        header("Expires:0"); 
        //输出全部内容到浏览器 
        ob_end_flush();
    }
    

}
