<?php

namespace app\admin\controller\xfqyd;

use app\common\controller\Backend;

/**
 * 消费券
 *
 * @icon fa fa-circle-o
 */
class Xfq extends Backend
{
    
    /**
     * Xfq模型对象
     * @var \app\admin\model\xfqyd\Xfq
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\xfqyd\Xfq;

    }
    
    public function rock($ids){
        $info = $this->model->get($ids);
        if ($info->status==1) {
            $this->error('已经摇过号，不能再摇号！');
        }
        $list  = db('xfq_baoming')->where(['active_id'=>$info['active_id']])->orderRaw('rand()')->limit($info['num'])->select();
        if (!$list) {
            $this->error('无报名记录，摇号失败！');
        }
        $no = 1;
        foreach ($list as $key => $val) {
            $data[$key] = [
                'active_id' => $val['active_id'],
                'xfq_id'    => $ids,
                'xfq_no'    => $info->xfq_no,
                'money'     => $info->money,
                'uname'     => $val['uname'],
                'idcard'    => $val['idcard'],
                'mobile'    => $val['mobile'],
                'add_time'  => date('Y-m-d H:i:s'),
                'no'=>$no
            ];
            $no++;
        }
        $res = db('xfq_zhongqian')->insertAll($data);
        if ($res) {
            $info->status = 1;
            $info->save();
            $this->success('摇号完成');
        }else{
            $this->error('摇号失败！');
        }
        
    }
    

}
