<?php

namespace app\admin\controller\xfqyd;

use app\common\controller\Backend;

/**
 * 中签记录
 *
 * @icon fa fa-circle-o
 */
class Zhongqian extends Backend
{
    
    /**
     * Zhongqian模型对象
     * @var \app\admin\model\xfqyd\Zhongqian
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\xfqyd\Zhongqian;

    }
    
    //导出
    public function export()
    {
        if ($this->request->isPost()) {
            set_time_limit(0);
            $search = $this->request->post('search');
            $ids = $this->request->post('ids');
            $filter = $this->request->post('filter');
            $op = $this->request->post('op');
            $columns = $this->request->post('columns');

            $whereIds = $ids == 'all' ? '1=1' : ['id' => ['in', explode(',', $ids)]];
            $this->request->get(['search' => $search, 'ids' => $ids, 'filter' => $filter, 'op' => $op]);
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();

            $line = 1;
            $list = [];
            ob_end_clean(); 
            ob_start(); 
            $filename='中签列表'.date("YmdHis");
            header("Content-type:application/octet-stream");
            header("Accept-Ranges:bytes");
            header("Content-type:application/vnd.ms-excel");
            header("Content-Disposition:attachment;filename=".$filename.".xls");
            header("Pragma: no-cache");
            header("Expires: 0");
            echo '<table border=1>';
            echo '<tr height="30" align="center">';
            echo '<th>活动名称</th>';
            echo '<th>姓名</th>';
            echo '<th>电话</th>';
            echo '<th>身份证号码</th>';
            echo '<th>券ID</th>';
            echo '<th>金额</th>';
            echo '</tr>';
            $this->model
                //->field($columns)
                ->where($where)
                ->where($whereIds)
                ->chunk(100, function ($items) use (&$list) {
                    $list = $items = collection($items)->toArray();
                    foreach ($items as $index => $item) {
                    	//$active_name = db('xfq_active')->where(['id'=>$item['active_id']])->value('name');
                    	$ckey = 'xfq_active_'.$item['active_id'];
                        $active_name = cache($ckey);
                        if (!$active_name) {
                            $active_name = db('xfq_active')->where(['id'=>$item['active_id']])->value('name');
                            cache($ckey,$active_name,3600);
                        }
                        echo '<tr height="30" align="center">';
                        echo '<th>'.$active_name.'</th>';
                        echo '<th>'.$item['uname'].'</th>';
                        echo '<th>'.$item['mobile'].'</th>';
                        echo "<th>'".$item['idcard'].'</th>';
                        echo "<th>'".$item['xfq_no'].'</th>';
                        echo '<th>'.$item['money'].'</th>';
                        echo '</tr>';
                    }
                });
            echo '</table>';
        }
    }
    

}
