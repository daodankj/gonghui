<?php

namespace app\admin\controller\xfqyd;

use app\common\controller\Backend;

/**
 * 首页菜单设置
 *
 * @icon fa fa-circle-o
 */
class XfqMenu extends Backend
{
    
    /**
     * XfqMenu模型对象
     * @var \app\admin\model\XfqMenu
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\xfqyd\XfqMenu;

    }
    
    public function imgset(){
        if ($this->request->isPost()) {
            $image = $this->request->post('image');
            if (!$image) {
                $this->error('图片不能为空');
            }
            $rs = db('xfq_config')->where(['id'=>1])->update(['index_image'=>$image]);
            if ($rs) {
                $this->success('保存成功');
            }else{
                $this->error('保存失败');
            }
         
        }
        $image = db('xfq_config')->where(['id'=>1])->value('index_image');
        $this->assign('image',$image);
        return $this->view->fetch();
    }
    

}
