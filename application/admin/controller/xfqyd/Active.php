<?php

namespace app\admin\controller\xfqyd;

use app\common\controller\Backend;
/**
 * 预约活动
 *
 * @icon fa fa-circle-o
 */
class Active extends Backend
{
    
    /**
     * Active模型对象
     * @var \app\admin\model\kzyd\Active
     */
    protected $model = null;

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\xfqyd\Active;

    }
    
    

}
