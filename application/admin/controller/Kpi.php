<?php

namespace app\admin\controller;

use app\common\controller\Backend;
use think\Db;
/**
 * 工作指标
 *
 * @icon fa fa-circle-o
 */
class Kpi extends Backend
{

    /**
     * Kpi模型对象
     * @var \app\admin\model\Kpi
     */
    protected $model = null;
    protected $searchFields = 'name';//默认搜索字段

    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\Kpi;

        $submitTypeData = ['1'=>'文字描述','2'=>'文字+图片','3'=>'文字+文件(word,excel)','4'=>'财务资金上报','5'=>'企业信息完善','6'=>'文体活动上报'];
        //$warningdata = ['1'=>'提前一周','2'=>'提前一个月','3'=>'提前一个季度'];
        $warningdata = [];
        //获取所有预警周期
        $warning_list = db('warning_type')->field('id,name')->where('status=1')->order('weigh desc')->select();
        foreach ($warning_list as $key => $val) {
            $warningdata[$val['id']] = $val['name'];
        }

        $this->view->assign('submitTypeData', $submitTypeData);
        $this->view->assign('warningdata', $warningdata);

        $this->view->assign("getTypeList", $this->model->getTypeList());
    }

    public function getSelectpage(){
        $list = db('kpi')->field('id,name')->select();

        return json($list);
    }

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $total = $this->model
                ->where($where)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->where($where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        $topic_id = $this->request->get('topic_id');
        if (!$topic_id) {
            $topic_id = db('topic')->order('id desc')->value('id');
        }
        $this->view->assign("topic_id", $topic_id);
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);

                if ($this->dataLimit && $this->dataLimitFieldAutoFill) {
                    $params[$this->dataLimitField] = $this->auth->id;
                }
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.add' : $name) : $this->modelValidate;
                        $this->model->validateFailException(true)->validate($validate);
                    }
                    $warning_type = $this->request->post("warning_type/a");
                    $params['warning_type'] = implode(',', $warning_type);
                    $result = $this->model->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were inserted'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = null)
    {
        $row = $this->model->get($ids);
        if (!$row) {
            $this->error(__('No Results were found'));
        }
        $adminIds = $this->getDataLimitAdminIds();
        if (is_array($adminIds)) {
            if (!in_array($row[$this->dataLimitField], $adminIds)) {
                $this->error(__('You have no permission'));
            }
        }
        if ($this->request->isPost()) {
            $params = $this->request->post("row/a");
            if ($params) {
                $params = $this->preExcludeFields($params);
                $result = false;
                Db::startTrans();
                try {
                    //是否采用模型验证
                    if ($this->modelValidate) {
                        $name = str_replace("\\model\\", "\\validate\\", get_class($this->model));
                        $validate = is_bool($this->modelValidate) ? ($this->modelSceneValidate ? $name . '.edit' : $name) : $this->modelValidate;
                        $row->validateFailException(true)->validate($validate);
                    }
                    $warning_type = $this->request->post("warning_type/a");
                    $params['warning_type'] = implode(',', $warning_type);
                    if ($params['save_new']==1){//另存为新指标，对应指标之前的上报记录全部重新自动上报一份
                        $row->id = null;
                        $row->warning = '';
                        $row->add_time = date('Y-m-d H:i:s');
                        $result = $row->isUpdate(false)->allowField(true)->save($params);
                        if (!$result) {
                            Db::rollback();
                            $this->error('操作失败');
                        }
                        $kpi_id = $row->id;
                        $kpi_list = model('kpi_list')->where(['kpi_id'=>$ids,'check_status'=>1])->select();
                        $newList = [];
                        foreach($kpi_list as $key=>$val){
                            unset($val['id']);
                            $val['kpi_id'] = $kpi_id;
                            $val['check_status'] = 0;
                            $val['remark'] = '';
                            $val['add_time'] = date('Y-m-d H:i:s');
                            $val['check_time'] = null;
                            $newList[] = $val;
                            $result = $val->isUpdate(false)->allowField(true)->save($val);
                        }
                        if ($result !== false) {
                            Db::commit();
                            $this->success('操作成功');
                        }else{
                            Db::rollback();
                            $this->error('操作失败');
                        }
                    }
                    $result = $row->allowField(true)->save($params);
                    Db::commit();
                } catch (ValidateException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (PDOException $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                } catch (Exception $e) {
                    Db::rollback();
                    $this->error($e->getMessage());
                }
                if ($result !== false) {
                    $this->success();
                } else {
                    $this->error(__('No rows were updated'));
                }
            }
            $this->error(__('Parameter %s can not be empty', ''));
        }
        $this->view->assign("row", $row);
        return $this->view->fetch();
    }


    //考核设置信息
    public function config(){
        $row = db('kpi_config')->where(['id'=>1])->find();
        if ($this->request->isPost()) {
            $info = $this->request->post('row/a');

            if (db('kpi_config')->where(['id'=>1])->update($info)) {
                $this->success('保存成功');
            }else{
                $this->error('保存失败');
            }
        }

        $this->view->assign('row',$row);
        return $this->view->fetch();
    }

    //获取指标完成情况
    public function showlist(){
        $this->model = new \app\admin\model\Company;
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            $kpi_id = $this->request->get('kpi_id');
            $status = $this->request->get('status',1);//1已完成 2未完成
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            if ($status==1) {//已完成
                $total = $this->model
                    ->where($where)
                    //->whereExists('select id from fa_kpi_list where company_id=fa_company.id and kpi_id='.$kpi_id)
                    ->whereExists('select id from fa_company_score_log where company_id=fa_company.id and kpi_id='.$kpi_id)
                    ->order($sort, $order)
                    ->count();

                $list = $this->model
                    ->where($where)
                    //->whereExists('select id from fa_kpi_list where company_id=fa_company.id and kpi_id='.$kpi_id)
                    ->whereExists('select id from fa_company_score_log where company_id=fa_company.id and kpi_id='.$kpi_id)
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();
            }else{//未完成
                $total = $this->model
                    ->where($where)
                    //->whereNotExists('select id from fa_kpi_list where company_id=fa_company.id and kpi_id='.$kpi_id)
                    ->whereNotExists('select id from fa_company_score_log where company_id=fa_company.id and kpi_id='.$kpi_id)
                    ->order($sort, $order)
                    ->count();

                $list = $this->model
                    ->where($where)
                    //->whereNotExists('select id from fa_kpi_list where company_id=fa_company.id and kpi_id='.$kpi_id)
                    ->whereNotExists('select id from fa_company_score_log where company_id=fa_company.id and kpi_id='.$kpi_id)
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();
            }


            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }

}
