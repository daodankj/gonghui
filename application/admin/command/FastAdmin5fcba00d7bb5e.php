<?php

namespace app\admin\command;

use app\common\controller\Backend;
use app\admin\model\party\Party;
use think\Db;
use think\Hook;
/**
 * 工会改选申请
 *
 * @icon fa fa-circle-o
 */
class FastAdmin5fcba00d7bb5e extends Backend
{
    
    /**
     * PartyUpdateapply模型对象
     * @var \app\admin\model\party\PartyUpdateapply
     */
    protected $model = null;
    protected $noNeedLogin = 'detail';
    public function _initialize()
    {
        parent::_initialize();
        $this->model = new \app\admin\model\party\PartyUpdateapply;

    }

    /**
     * 查看
     */
    public function index()
    {
        //设置过滤方法
        $this->request->filter(['strip_tags']);
        if ($this->request->isAjax()) {
            //如果发送的来源是Selectpage，则转发到Selectpage
            if ($this->request->request('keyField')) {
                return $this->selectpage();
            }
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            $c_where = [];//企业权限过滤
            if (!$this->auth->isSuperAdmin()&&$this->auth->company_ids){
                $c_where = ['company_id'=>['in',$this->auth->company_ids]];
            }
            $total = $this->model
                ->where($where)
                ->where($c_where)
                ->order($sort, $order)
                ->count();

            $list = $this->model
                ->where($where)
                ->where($c_where)
                ->order($sort, $order)
                ->limit($offset, $limit)
                ->select();

            $list = collection($list)->toArray();
            $result = array("total" => $total, "rows" => $list);

            return json($result);
        }
        return $this->view->fetch();
    }
    //受理
    public function accept($ids){
        if ($this->request->isPost()) {
            $id = $this->request->post('id');
            $status = $this->request->post('status');
            $result = $this->request->post('result');
            if (!$result) {
                $result = $status==3?'情况已核实，给予受理':'情况不属实，不受理';
            }
            if (!$result) {
                $this->error('请输入审核意见');
            }
            $info = $this->model->get($id);
            if (!$info) {
                $this->error('该申请不存在，受理失败'.$id);
            }
            if ($info->status!=0) {
                $this->error('改申请已受理，不需再次受理');
            }
            $info->status = $status;
            $info->result = $result;
            $info->check_time = date('Y-m-d H:i:s');
            if ($info->save()) {
                if ($status==2) {
                    //监听审核事件
                    $infodata = ['status'=>2,'type'=>63,'data'=>$info];
                    Hook::listen("apply_check", $infodata);
                    $this->success('受理成功');
                }
                //监听受理事件
                $infodata = ['status'=>3,'type'=>63,'data'=>$info];
                Hook::listen("apply_check", $infodata);
                $this->success('受理成功');
            }else{
                $this->error('受理失败');
            }
            
        }
        $this->view->assign('id',$ids);
        return $this->view->fetch('party_changeapply/accept');
    }
    //审批
    public function check($ids){
        if ($this->request->isPost()) {
            $id = $this->request->post('id');
            $status = $this->request->post('status');
            $result = $this->request->post('result');
            if (!$result) {
                $result = $status==1?'情况已核实，给予通过':'情况不属实，不通过';
            }
            if (!$result) {
                $this->error('请输入审核意见');
            }
            $info = $this->model->get($id);
            if (!$info) {
                $this->error('该申请不存在，审核失败'.$id);
            }
            if ($info->status!=3) {
                $this->error('该申请还未受理，请先受理成功后再审核');
            }
            $info->status = $status;
            $info->result = $result;
            $info->check_time = date('Y-m-d H:i:s');
            Db::startTrans();
            if ($info->save()) {
                if ($status==2) {
                    Db::commit();
                    //监听审核事件
                    $infodata = ['status'=>2,'type'=>63,'data'=>$info];
                    Hook::listen("apply_check", $infodata);
                    $this->success('审核成功');
                }
                //审核成功，修改工会信息
                $UnionModel = new \app\admin\\partymodel\Party;
                $Union = $UnionModel->get(['company_id'=>$info->company_id]);
                if (!$Union) {
                    Db::rollback();
                    $this->error('该企业还未成立党组织');
                }
                $Union->chairman = $info->chairman;
                $Union->chairman_f = $info->chairman_f;
                $Union->director_zz = $info->director_zz;
                $Union->director_wt = $info->director_wt;
                $Union->director_xc = $info->director_xc;
                $Union->director_ld = $info->director_ld;
                $Union->director_js = $info->director_js;
                $Union->director_woman = $info->director_woman;
                $Union->finance = $info->finance;
                if ($Union->save()) {
                    Db::commit();
                    //监听审核事件
                    $infodata = ['status'=>1,'type'=>63,'data'=>$info];
                    Hook::listen("apply_check", $infodata);
                    $this->success('审核成功');
                }else{
                    Db::rollback();
                    $this->error('审核失败');
                }
            }else{
                Db::rollback();
                $this->error('审核失败');
            }
            
        }
        $this->view->assign('id',$ids);
        return $this->view->fetch();
    }
    //签署申请文档预览
    public function detail($ids){
        $info = $this->model->get($ids);
        $info->add_time = date('Y年m月d日',strtotime($info->add_time));
        $this->view->assign('info',$info);
        return $this->view->fetch();
    }
    //下载文档
    public function down($id=''){
        //打开缓冲区 
        ob_start(); 
        header("Cache-Control: public"); 
        Header("Content-type: application/octet-stream"); 
        Header("Accept-Ranges: bytes"); 
        
        $filename = $filename='改选申请'.date("YmdHis");
        //判断浏览器类型
        if (strpos($_SERVER["HTTP_USER_AGENT"],'MSIE')) { 
         header('Content-Disposition: attachment; filename='.$filename.'.doc'); 
        }else if (strpos($_SERVER["HTTP_USER_AGENT"],'Firefox')) { 
         Header('Content-Disposition: attachment; filename='.$filename.'.doc'); 
        } else { 
         header('Content-Disposition: attachment; filename='.$filename.'.doc'); 
        } 
         
        //不使用缓存
        header("Pragma:no-cache"); 
        //过期时间 
        header("Expires:0"); 
        echo '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:w="urn:schemas-microsoft-com:office:word" xmlns="http://www.w3.org/TR/REC-html40">
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
            <xml><w:WordDocument><w:View>Print</w:View></xml>
            </head><body>';
            
        $url = url('detail',['ids'=>$id],'',true);
        $info = file_get_contents($url);
        $sprnstr="<!--startdownload-->";      
        $eprnstr="<!--enddownload-->";   
        $pos = strpos($info, $sprnstr);
        $pos2 = strpos($info, $eprnstr);
        $info = substr($info, $pos+20,$pos2-$pos-20);
        echo $info;
        echo "</body></html>"; 
        //输出全部内容到浏览器 
        ob_end_flush();
    }

}
