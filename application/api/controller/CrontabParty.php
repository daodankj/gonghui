<?php

namespace app\api\controller;

use app\common\controller\Api;
use addons\faqueue\library\QueueApi;
/**
 * 定时任务
 */
class CrontabParty extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];

    public function index(){

        return 'kpi';
    }


    //检测是否有等级升级通知
    public function score_check(){
        $config_info = db('kpi_config')->where('id=2')->find();
        if ($config_info['msg_time']) {
            return '已经检测并通知';//已经检测通知
        }
        if (time()>=strtotime($config_info['notice_time'])) {//到了通知时间
            $diff_score = $config_info['notice_score'];
            //先查询D企业
            $score = $config_info['C']-$diff_score;
            db('party')->field('company_id as id,phone')->where(['type'=>'D','score'=>['>=',$score]])
            ->chunk(100, function ($items) use (&$config_info) {
                $items = collection($items)->toArray();
                foreach ($items as $index => $item) {
                    //$msg = '您的企业距离上一级还相差不到'.$diff_score.'积分了，再加油一点就能升级了！';
                    $msg = $config_info['notice_content'];
                    //获取工会负责人手机号码,短信通知
                    if ($item['phone']) {
                        QueueApi::smsNotice($item['phone'],$msg);
                    }
                    //QueueApi::sendEmail("任务预警通知",'498129372@qq.com','测试');
                }
            });
            //先查询C企业
            $score = $config_info['B']-$diff_score;
            db('party')->field('company_id as id,phone')->where(['type'=>'C','score'=>['>=',$score]])
            ->chunk(100, function ($items) use (&$config_info) {
                $items = collection($items)->toArray();
                foreach ($items as $index => $item) {
                    //$msg = '您的企业距离上一级还相差不到'.$diff_score.'积分了，再加油一点就能升级了！';
                    $msg = $config_info['notice_content'];
                    //获取工会负责人手机号码,短信通知
                    if ($item['phone']) {
                        QueueApi::smsNotice($item['phone'],$msg);
                    }
                    //QueueApi::sendEmail("任务预警通知",'498129372@qq.com','测试');
                }
            });
            //先查询B企业
            $score = $config_info['A']-$diff_score;
            db('party')->field('company_id as id,phone')->where(['type'=>'B','score'=>['>=',$score]])
            ->chunk(100, function ($items) use (&$config_info) {
                $items = collection($items)->toArray();
                foreach ($items as $index => $item) {
                    //$msg = '您的企业距离上一级还相差不到'.$diff_score.'积分了，再加油一点就能升级了！';
                    $msg = $config_info['notice_content'];
                    //获取工会负责人手机号码,短信通知
                    if ($item['phone']) {
                        QueueApi::smsNotice($item['phone'],$msg);
                    }
                    //QueueApi::sendEmail("任务预警通知",'498129372@qq.com','测试');
                }
            });
            //比较检测过
            db('kpi_config')->where(['id'=>2])->update(['msg_time'=>time()]);
        }
        return '党升级通知执行完成'.date('Y-m-d H:i:s');
    }

    //预警检测重写，相同联系人只通知一次
    public function kpi_warning(){
        set_time_limit(0);
        //获取党
        $companyList = db('party')->field('id,company_id,phone')->select();
        if ($companyList) {
            //获取任务指标
            $nowtime = date('Y-m-d H:i:s');
            $kpiList = db('party_kpi')->where(['end_time'=>['>',$nowtime]])->field('id,name,grade,score,end_time,submit_num,warning_type,warning')->select();
            if (!$kpiList) {
                return '预警执行完成';
            }
            //获取所有的预警时间信息
            $warning_type_info = db('party_warning_type')->field('id,day,notice_content')->select();
            $warning_type_array = array_column($warning_type_info, null, 'id');
            foreach ($companyList as $kc => $item) {
                $item['notice_content'] = '';
                $item['num'] = 0;//未完成指标数
                foreach ($kpiList as $key => $kpiInfo) {
                    //判断是否已经完成指标
                    $submit_num = db('party_kpi_list')->where(['kpi_id'=>$kpiInfo['id'],'company_id'=>$item['company_id']])->count();
                    if ($submit_num>=$kpiInfo['submit_num']) {
                        continue;
                    }
                    $item['num']++;//未完成数+1
                    $warning_type = explode(',', $kpiInfo['warning_type']);//需要预警的时间
                    $warning = explode(',', $kpiInfo['warning']);//已经预警的时间
                    foreach ($warning_type as $k => $v) {//遍历预警时间
                        if (in_array($v, $warning)) {    //看是否已经预警过
                            continue;
                        }
                        //检测是否到了预警时间
                        if (!isset($warning_type_array[$v])||$warning_type_array[$v]['day']<1) {
                            continue;
                        }
                        //判断是时间是否到
                        $end_time = strtotime($kpiInfo['end_time']);
                        $pre_week_time = strtotime("-".$warning_type_array[$v]['day']." day",$end_time);//完成日前多少天
                        if (time()>=$pre_week_time) {//已到完成时间的规定天之内
                             $kpiInfo['notice_content'] = $warning_type_array[$v]['notice_content'];

                             //未完成任务的发出提示
                             $msg = '您当前还有{num}个指标任务未完成！请登入大旺工人查看并尽快完成！';
                             if (isset($kpiInfo['notice_content'])&&$kpiInfo['notice_content']) {
                                $msg = $kpiInfo['notice_content'];
                             }
                             $data = [
                                'kpi_id'     => $kpiInfo['id'],
                                'company_id' => $item['company_id'],
                                'warning_type' => $v,
                                'notice' => str_replace("{num}个",'',$msg),
                                'add_time' => date('Y-m-d H:i:s')
                             ];
                             db('party_kpi_warning')->insert($data);

                             $item['notice_content'] = $msg;
                             //已预警的类型
                             array_push($warning,$v);
                        }
                    }
                    //修改已预警的类型
                    $warning_str = implode(',', $warning);
                    db('party_kpi')->where(['id'=>$kpiInfo['id']])->update(['warning'=>$warning_str]);
                }
                //获取工会负责人手机号码,短信通知,多个指标只发一条短信
                if ($item['phone']&&$item['num']>0&&$item['notice_content']) {
                    $item['notice_content'] = str_replace("{num}",$item['num'],$item['notice_content']);
                    QueueApi::smsNotice($item['phone'],$item['notice_content']);
                }
            }
        }


        return '党预警执行完成'.date('Y-m-d H:i:s');

    }
}
