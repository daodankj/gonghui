<?php

namespace app\api\controller;

use app\common\controller\Api;
use app\common\model\User;
use tool\YinshiApi;
/**
 * 首页接口
 */
class Index extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];
    protected $_user = null;

    public function _initialize()
    {
        parent::_initialize();
        $userId = session('userId');
        if (!$userId) {
            $this->error('请先登入！');
        }
        $this->_user = User::get($userId);
        if (!$this->_user) {
            $this->error('帐号异常，请重新登入！');
        }
    }

    /**
     * 解绑
     *
     */
    public function unbind()
    {
        $notice = '';
        $this->_user->company_id = 0;
        if ($this->_user->is_admin==1) {
            $notice = '，并且管理员权限已去除';
        }
        $this->_user->is_admin = 0;
        if ($this->_user->save()) {
            $this->success('解绑成功'.$notice);
        }else{
            $this->error('解绑失败');
        }
        
    }

    public function test(){
        $mApi = new YinshiApi('36466f7a8c114c77acb89f8618ff05b9','d39e9f50e503395c56cd2f446d294dd5');
        $rs = $mApi->getDeviceMemberList('FV0106993',0,1);
        //$rs  = $mApi->getInOutList('FV0106993','2025-03-07 00:00:00','2025-03-07 23:59:59',0,10);
        $rs  = $mApi->getEventRecord('FV0443966','2025-03-07 00:00:00','2025-03-07 23:59:59',0,1);
        echo $mApi->errorMsg;
        print_r($rs);exit;
    }
}
