<?php

namespace app\api\controller;

use app\common\controller\Api;
use addons\faqueue\library\QueueApi;
/**
 * 定时任务
 */
class Crontab extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];

    public function index(){
        
        return 'kpi';
    }
    //$warningdata = ['1'=>'提前一周','2'=>'提前一个月','3'=>'提前一个季度'];
    /*public function kpi_warning(){
        set_time_limit(0);
        //获取任务指标
        $nowtime = date('Y-m-d H:i:s');
        $kpiList = db('kpi')->where(['end_time'=>['>',$nowtime]])->field('id,name,grade,score,end_time,submit_num,warning_type,warning')->select();
        //获取所有的预警时间信息
        $warning_type_info = db('warning_type')->field('id,day,notice_content')->select();
        $warning_type_array = array_column($warning_type_info, null, 'id');
        if ($kpiList) {
            foreach ($kpiList as $key => $val) {
                $warning_type = explode(',', $val['warning_type']);//需要预警的时间
                $warning = explode(',', $val['warning']);//已经预警的时间
                foreach ($warning_type as $k => $v) {//遍历预警时间
                    if (in_array($v, $warning)) {    //看是否已经预警过
                        continue;
                    }
                    //全部变成自定义
                    $rs = $this->custormTime($val,$v,$warning_type_array);
                    if ($rs) {
                        //修改已预警的类型
                        array_push($warning,$v);
                    }
                }
                $warning_str = implode(',', $warning);
                db('kpi')->where(['id'=>$val['id']])->update(['warning'=>$warning_str]);
            }
        }

        //顺便执行下积分升级通知检测
        //$this->score_check();

        return '预警执行完成';

    }*/

    //自定义预警时间
    private function custormTime($kpiInfo,$warning_type='',$warning_type_array=[]){
        if (!isset($warning_type_array[$warning_type])||$warning_type_array[$warning_type]['day']<1) {
            return false;
        }
        //判断是时间是否到
        $end_time = strtotime($kpiInfo['end_time']);
        $pre_week_time = strtotime("-".$warning_type_array[$warning_type]['day']." day",$end_time);//完成日前多少天
        if (time()>=$pre_week_time) {//已到完成时间的规定天之内
             $kpiInfo['notice_content'] = $warning_type_array[$warning_type]['notice_content'];
             $this->getCompanyList($kpiInfo,$warning_type);
             return true;
        } 
        return false;
    }

    //提前一周时间判断
    private function weekTime($kpiInfo){
        //判断是否到了一周前时间
        $end_time = strtotime($kpiInfo['end_time']);
        $pre_week_time = strtotime("-1 week",$end_time);//完成日前一周
        if (time()>=$pre_week_time) {//已到完成时间的一周之内
             $this->getCompanyList($kpiInfo,1);
             return true;
        } 
        return false;
    }

    //提前一个月时间判断
    private function monthTime($kpiInfo){
        //判断是否到了一周前时间
        $end_time = strtotime($kpiInfo['end_time']);
        $pre_week_time = strtotime("-1 month",$end_time);//完成日前一个月
        if (time()>=$pre_week_time) {//已到完成时间的一个月之内
             $this->getCompanyList($kpiInfo,2);
             return true;
        } 
        return false;
    }

    //提前一季度时间判断
    private function quarterTime($kpiInfo){
        //判断是否到了一周前时间
        $end_time = strtotime($kpiInfo['end_time']);
        $pre_week_time = strtotime("-3 month",$end_time);//完成日前3个月
        if (time()>=$pre_week_time) {//已到完成时间的3个月之内
             $this->getCompanyList($kpiInfo,3);
             return true;
        }
        return false; 
    }


    //查找没有完成任务的企业列表,并发出预警信息
    private function getCompanyList($kpiInfo,$warning_type=1){
        db('union')->field('id,company_id,phone')
            ->chunk(100, function ($items) use (&$kpiInfo,&$warning_type) {
                $items = collection($items)->toArray();
                foreach ($items as $index => $item) {
                    //判断是否已经完成了任务
                    //$over = db('company_score_log')->where(['kpi_id'=>$kpiInfo['id'],'company_id'=>$item['company_id']])->find();
                    $submit_num = db('kpi_list')->where(['kpi_id'=>$kpiInfo['id'],'company_id'=>$item['company_id']])->count();
                    if ($submit_num>=$kpiInfo['submit_num']) {
                        continue;
                    }
                    //未完成任务的发出提示
                    $msg = '您当前还有指标任务未完成！请登入系统查看并尽快完成哦！';
                    if (isset($kpiInfo['notice_content'])&&$kpiInfo['notice_content']) {
                        $msg = $kpiInfo['notice_content'];
                    }
                    $data = [
                        'kpi_id'     => $kpiInfo['id'],
                        'company_id' => $item['company_id'],
                        'warning_type' => $warning_type,
                        //'notice' => '您当前指标任务已完成'.$submit_num.'个,还差'.($kpiInfo['submit_num']-$submit_num).'个未完成！请尽快完成哦！',
                        'notice' => $msg,
                        'add_time' => date('Y-m-d H:i:s')
                    ];
                    db('kpi_warning')->insert($data);
                    //获取工会负责人手机号码,短信通知
                    if ($item['phone']) {
                        QueueApi::smsNotice($item['phone'],$data['notice']);
                    }
                    //QueueApi::sendEmail("任务预警通知",'498129372@qq.com','测试');
                }
            });
        return true;
    }



    //检测是否有等级升级通知
    public function score_check(){
        $config_info = db('kpi_config')->where('id=1')->find();
        if ($config_info['msg_time']) {
            return '已经检测并通知';//已经检测通知 
        }
        if (time()>=strtotime($config_info['notice_time'])) {//到了通知时间
            $diff_score = $config_info['notice_score'];
            //先查询D企业
            $score = $config_info['C']-$diff_score;
            db('union')->field('company_id as id,phone')->where(['type'=>'D','score'=>['>=',$score]])
            ->chunk(100, function ($items) use (&$config_info) {
                $items = collection($items)->toArray();
                foreach ($items as $index => $item) {
                    //$msg = '您的企业距离上一级还相差不到'.$diff_score.'积分了，再加油一点就能升级了！';
                    $msg = $config_info['notice_content'];
                    //获取工会负责人手机号码,短信通知
                    if ($item['phone']) {
                        QueueApi::smsNotice($item['phone'],$msg);
                    }
                    //QueueApi::sendEmail("任务预警通知",'498129372@qq.com','测试');
                }
            });
            //先查询C企业
            $score = $config_info['B']-$diff_score;
            db('union')->field('company_id as id,phone')->where(['type'=>'C','score'=>['>=',$score]])
            ->chunk(100, function ($items) use (&$config_info) {
                $items = collection($items)->toArray();
                foreach ($items as $index => $item) {
                    //$msg = '您的企业距离上一级还相差不到'.$diff_score.'积分了，再加油一点就能升级了！';
                    $msg = $config_info['notice_content'];
                    //获取工会负责人手机号码,短信通知
                    if ($item['phone']) {
                        QueueApi::smsNotice($item['phone'],$msg);
                    }
                    //QueueApi::sendEmail("任务预警通知",'498129372@qq.com','测试');
                }
            });
            //先查询B企业
            $score = $config_info['A']-$diff_score;
            db('union')->field('company_id as id,phone')->where(['type'=>'B','score'=>['>=',$score]])
            ->chunk(100, function ($items) use (&$config_info) {
                $items = collection($items)->toArray();
                foreach ($items as $index => $item) {
                    //$msg = '您的企业距离上一级还相差不到'.$diff_score.'积分了，再加油一点就能升级了！';
                    $msg = $config_info['notice_content'];
                    //获取工会负责人手机号码,短信通知
                    if ($item['phone']) {
                        QueueApi::smsNotice($item['phone'],$msg);
                    }
                    //QueueApi::sendEmail("任务预警通知",'498129372@qq.com','测试');
                }
            });
            //比较检测过
            db('kpi_config')->where(['id'=>1])->update(['msg_time'=>time()]);
        }
        return '升级通知执行完成';
    }

    //预警检测重写，相同联系人只通知一次
    public function kpi_warning(){
        set_time_limit(0);
        //获取工会
        $companyList = db('union')->field('id,company_id,phone')->select();
        if ($companyList) {
            //获取任务指标
            $nowtime = date('Y-m-d H:i:s');
            $kpiList = db('kpi')->where(['end_time'=>['>',$nowtime]])->field('id,name,grade,score,end_time,submit_num,warning_type,warning')->select();
            if (!$kpiList) {
                return '预警执行完成';
            }
            //获取所有的预警时间信息
            $warning_type_info = db('warning_type')->field('id,day,notice_content')->select();
            $warning_type_array = array_column($warning_type_info, null, 'id');
            foreach ($companyList as $kc => $item) {
                $item['notice_content'] = '';
                $item['num'] = 0;//未完成指标数
                foreach ($kpiList as $key => $kpiInfo) {
                    //判断是否已经完成指标
                    $submit_num = db('kpi_list')->where(['kpi_id'=>$kpiInfo['id'],'company_id'=>$item['company_id']])->count();
                    if ($submit_num>=$kpiInfo['submit_num']) {
                        continue;
                    }
                    $item['num']++;//未完成数+1
                    $warning_type = explode(',', $kpiInfo['warning_type']);//需要预警的时间
                    $warning = explode(',', $kpiInfo['warning']);//已经预警的时间
                    foreach ($warning_type as $k => $v) {//遍历预警时间
                        if (in_array($v, $warning)) {    //看是否已经预警过
                            continue;
                        }
                        //检测是否到了预警时间
                        if (!isset($warning_type_array[$v])||$warning_type_array[$v]['day']<1) {
                            continue;
                        }
                        //判断是时间是否到
                        $end_time = strtotime($kpiInfo['end_time']);
                        $pre_week_time = strtotime("-".$warning_type_array[$v]['day']." day",$end_time);//完成日前多少天
                        if (time()>=$pre_week_time) {//已到完成时间的规定天之内
                             $kpiInfo['notice_content'] = $warning_type_array[$v]['notice_content'];
                             
                             //未完成任务的发出提示
                             $msg = '您当前还有{num}个指标任务未完成！请登入大旺工人查看并尽快完成！';
                             if (isset($kpiInfo['notice_content'])&&$kpiInfo['notice_content']) {
                                $msg = $kpiInfo['notice_content'];
                             }
                             $data = [
                                'kpi_id'     => $kpiInfo['id'],
                                'company_id' => $item['company_id'],
                                'warning_type' => $v,
                                'notice' => str_replace("{num}个",'',$msg),
                                'add_time' => date('Y-m-d H:i:s')
                             ];
                             db('kpi_warning')->insert($data); 

                             $item['notice_content'] = $msg;
                             //已预警的类型
                             array_push($warning,$v);
                        }
                    }
                    //修改已预警的类型
                    $warning_str = implode(',', $warning);
                    db('kpi')->where(['id'=>$kpiInfo['id']])->update(['warning'=>$warning_str]);
                }
                //获取工会负责人手机号码,短信通知,多个指标只发一条短信
                if ($item['phone']&&$item['num']>0&&$item['notice_content']) {
                    $item['notice_content'] = str_replace("{num}",$item['num'],$item['notice_content']);
                    QueueApi::smsNotice($item['phone'],$item['notice_content']);
                }
            }
        }
        

        return '预警执行完成';

    }



    //通知学历提升待审核数量
    public function check_education(){
        $num = db('skills_education')->where('type=2 and status=3')->count();
        if ($num>0) {
            QueueApi::smsNotice('15629883311','当前学历提升申请还剩'.$num.'个待审核');
        }
        echo $num;
    }

    //截至讨论日期自动发送邮件
    public function proposal_eamil_send(){
        $num = 0;
        $list = db('proposal')->where(['status'=>2,'vote_end_time'=>['<',time()]])->field('id,company_id,title')->select();
        foreach ($list as $key => $val) {
            $num++;
            $rs = db('proposal')->where('id='.$val['id'])->update(['status'=>3]);
            $email = db('company_info')->where('company_id='.$val['company_id'])->value('u_chairman_email');
            if (!$rs || !$email) {
                continue;
            }
            $url = $this->request->domain().'/index/index/downloadProposal/id/'.$val['id'];
            $content = '<a href="'.$url.'">点击此处下载议题>></a>';
            QueueApi::sendEmail($val['title'],$email,$content);
        }
        echo $num;
    }
}
