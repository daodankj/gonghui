<?php

namespace app\api\controller;

use app\common\controller\Api;
use tool\Coordinate;
/**
 * 接口
 */
class Interfaceapi extends Api
{
    protected $noNeedLogin = ['*'];
    protected $noNeedRight = ['*'];


    //获取数据统计
    public function getData(){
    	//从自定义获取信息
    	$info = db('front')->where(['id'=>1])->find();
        //获取工会数量
        //$unionNum = db('union')->count();
        $unionNum = $info['unionCompanyNum'];
        //已有阵地
        //$companyNum = db('company')->count();
        $companyNum = $info['companyNum'];
        //已投产企业数量
        //$companyProNum = db('company')->where(['status'=>1])->count();
        $companyProNum = $info['companyProNum'];
        //活动数量
        //$activeNum = db('union_active')->where(['status'=>1])->count();
        $activeNum = $info['activeNum'];
        //维权申请待处理数量
        $wqNum = db('weiquan')->where(['status'=>0])->count();
        //困难申请待处理数量
        $knNum = db('kunnan')->where(['status'=>0])->count();
        //工会待办数量
        //$num1 = db('union_apply')->where(['status'=>0])->count();
        //$num2 = db('union_changeapply')->where(['status'=>0])->count();
        //$num3 = db('union_updateapply')->where(['status'=>0])->count();
        $toDoNum = $info['toDoNum'];
        //会员数量
        //$userNum = db('user')->count();
        $userNum = $info['userNum'];

        $data = [
            'companyProNum' =>$companyProNum,
            'unionNum' => $unionNum,
            'unionCompanyNum' => $unionNum,
            'companyNum' => $companyNum,
            'activeNum' => $activeNum,
            'wqNum' => $wqNum,
            'knNum' => $knNum,
            'toDoNum' => $toDoNum,//$num1+$num2+$num3+$wqNum+$knNum,
            'userNum' => $userNum
        ];
        $this->success('获取成功',$data);
    }
    //获取前台展示数据
    public function getFrontData(){
        $data = db('front')->where(['id'=>1])->find();
        unset($data['id']);
        $this->success('获取成功',$data);
    }

    //获取公司信息列表
    public function getCompanyList(){
        $index = $this->request->get('index',0);
        $limit = $this->request->get('limit',1000);
        if ($limit>1000) {
            $this->error('一次获取数量不能超过1000');
        }

        $where['upstring'] = ['>=',$index];
        $list = db('company')->field('id,name,short_name,type,url,lat,lng,image,upstring as indexKey')->where($where)->limit($limit)->order('upstring asc')->select();
 		foreach ($list as $key => &$val) {
 			//获取对应工会类型
 			$unionType = db('union')->where(['status'=>1,'company_id'=>$val['id']])->value('type');
 			if ($unionType) {
 				$val['unionType'] = $unionType;
 			}else $val['unionType'] = 'D';
            //坐标转换  百度坐标转换成火星坐标
            if ($val['lat']&&$val['lng']) {
                $point['x'] = $val['lng'];
                $point['y'] = $val['lat'];
                $coord = Coordinate::bd_gcj($point);
                //将火星坐标转换成大地坐标
                $coord = Coordinate::gcj_wgs($coord);
                $val['lng'] = $coord['x'];
                $val['lat']  = $coord['y'];
            }
            if ($val['image']) {
                $val['image'] = $this->request->domain().$val['image'];
            }
            //企业详细信息
            $companyInfo = db('company_info')->where(['company_id'=>$val['id']])->find();
            //查询资金上报信息
            $capitalInfo = db('capital')->where(['company_id'=>$val['id']])->order('creaetime desc')->find();
            $val['companyInfo'] =  $companyInfo;
            $val['capitalInfo'] =  $capitalInfo;
 		}
        $this->success('获取成功',$list);
    }
}
