<?php

namespace tool;

use \fast\Http;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;

class TsgApi{

    public $apiUrl = 'https://lib.dxrfid.cn';
	public $libraryId = '8545589886';
	public $secret = '863B8967A35F7177803888';
    public $account = 'ZQkfqzgh_acs';
    public $password = 'ZQkfqzgh_2025';
    public $organizationNo = '422';
	public $errorMsg = '';
	protected $cacheTime = 3600 * 24;
    protected $cacheKey = 'library_accessToken';

	public function __construct(){
		
	}

    //查询借还记录，当前在借书籍,有无超时归还
	public function getBorrowAll($page=1,$limit=10){
       $param = [
            'currentPage' => $page,
            'pageSize' => $limit,
       ];
       return $this->sendRequest('/api/libraryApi/getBorrowAll',$param);
    }

    //获取用户列表
    public function getUsersAll($page=1,$limit=10){
        $param = [
            'currentPage' => $page,
            'pageSize' => $limit,
       ];
       return $this->sendRequest('/api/libraryApi/getUsersALL',$param);
    }


    //普通表单请求
    private function sendRequest($url,$param){
       $accessToken = cache($this->cacheKey);
       if (empty($accessToken)) {
           $accessToken = $this->getAccessToken();
       }
       if (!$accessToken) {
           $this->errorMsg = 'get accessToken error';
            return [];
       }
       $client = new Client([
            'verify' => false
       ]);
       $headers = [
            'content-type' => 'application/json',
            'Authorization' => $accessToken
        ];
        $rs = $client->request('POST',$this->apiUrl.$url,['headers'=>$headers,'json'=>$param,'http_errors'=>false]);  
        if ($rs->getStatusCode() !== 200) {
            $this->errorMsg = "get request failed with status code: " . $rs->getStatusCode();
            return [];
        } 
        $content = $rs->getBody()->getContents();
        if (!$content){
            $this->errorMsg = 'get request error';
            return [];
        }
        $rs = json_decode($content,true);
        return $rs;
    }
	
    private function getAccessToken(){
        $param = [
            "clientId" => $this->libraryId,
            "clientSecret" => $this->secret,
            "acsId" => $this->account,
            "acsPassword" => $this->password,
            "organizationNo" => $this->organizationNo
        ];
        $client = new Client([
            'verify' => false
        ]);
        $headers = [
            'content-type' => 'application/json'
        ];
        $rs = $client->request('POST',$this->apiUrl."/api/libraryApi/getClient",['headers'=>$headers,'json'=>$param,'http_errors'=>false]);  
        if ($rs->getStatusCode() !== 200) {
            $this->errorMsg = "get getAccessToken failed with status code: " . $rs->getStatusCode();
            return [];
        } 
        $content = $rs->getBody()->getContents();
        if (!$content){
            $this->errorMsg = 'get getAccessToken error';
            return [];
        }
        $rs = json_decode($content,true);
        if (empty($rs["access_token"])){
            $this->errorMsg = 'get getAccessToken error:'.$rs['message'];
            return [];
        }
        cache($this->cacheKey,$rs["access_token"],$this->cacheTime);
        return $rs["access_token"];
    }
}