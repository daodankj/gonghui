<?php
/**
 * 荧石api相关操作类
 */
namespace tool;

use \fast\Http;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class YinshiApi{

    public $apiUrl = 'https://open.ys7.com';
	public $appKey = '';
	public $secret = '';
	public $errorMsg = '';
	protected $cacheTime = 3600 * 24 * 7 - 600;
    protected $cacheKey = '';

	public function __construct($appKey,$secret){
		$this->appKey = $appKey;
		$this->secret = $secret;
        $this->cacheKey = $appKey.'_accessToken';
	}

    //获取设备列表
	public function getDeviceList($page=0,$limit=10){
       $accessToken = cache($this->cacheKey);
       if (empty($accessToken)) {
           $accessToken = $this->getAccessToken();
       }
       if (empty($accessToken)){
           return [];
       }
       $param = [
            "accessToken" => $accessToken,
            "pageStart" => $page,
            "pageSize" => $limit
        ];
        $rs = Http::post("https://open.ys7.com/api/lapp/device/list",$param);       
        if (!$rs){
            $this->errorMsg = 'get device list error';
            return [];
        }
        $rs = json_decode($rs,true);
        if ($rs['code'] !="200"){
            $this->errorMsg = 'get device list error:'.$rs['msg'];
            return [];
        }
        return $rs;
    }


    //获取设备详情
    public function getDeviceInfo($deviceSerial){
        $accessToken = cache($this->cacheKey);
       if (empty($accessToken)) {
           $accessToken = $this->getAccessToken();
       }
       $param = [
            "accessToken" => $accessToken,
            "deviceSerial" => $deviceSerial
        ];
        $rs = Http::post("https://open.ys7.com/api/lapp/device/info",$param);       
        if (!$rs){
            $this->errorMsg = 'get device info error';
            return [];
        }
        $rs = json_decode($rs,true);
        if ($rs['code'] !="200"){
            $this->errorMsg = 'get device info error:'.$rs['msg'];
            return [];
        }
        return $rs["data"];
    }

    //获取设备状态
    public function getDeviceStatus($deviceSerial){
        $accessToken = cache($this->cacheKey);
       if (empty($accessToken)) {
           $accessToken = $this->getAccessToken();
       }
       $param = [
            "accessToken" => $accessToken,
            "deviceSerial" => $deviceSerial
        ];
        $rs = Http::post("https://open.ys7.com/api/lapp/device/status/get",$param);       
        if (!$rs){
            $this->errorMsg = 'get device status error';
            return [];
        }
        $rs = json_decode($rs,true);
        if ($rs['code'] !="200"){
            $this->errorMsg = 'get device status error:'.$rs['msg'];
            return [];
        }
        return $rs["data"];
    }

    //获取指定设备的通道信息
    public function getDeviceChannelList($deviceSerial){
        $param = [
            "deviceSerial" => $deviceSerial
        ];
        return $this->sendRequest('/api/lapp/device/camera/list',$param);
    }

    //获取播放地址
    public function getLiveAddress($deviceSerial,$channelNo=1){
        $accessToken = cache($this->cacheKey);
        if (empty($accessToken)) {
            $accessToken = $this->getAccessToken();
        }
        $url = "https://open.ys7.com/console/jssdk/pc.html?accessToken={$accessToken}&url=ezopen://open.ys7.com/{$deviceSerial}/{$channelNo}.hd.live&themeId=simple";
        return $url;
        /*$param = [
            "deviceSerial" => $deviceSerial,
            "channelNo" => $channelNo,
            "protocol" => 2,
            "expireTime" => 600,
            "supportH265" => 1
        ];
        return $this->sendRequest('/api/lapp/v2/live/address/get',$param);*/
    }

    //获取网关子设备列表
    public function getChildDeviceList($deviceSerial,$page=0,$limit=10){
        $param = [
            "currentPage" => $page,
            "pageSize" => $limit
        ];
        return $this->sendRequest2('/api/route/userdevicetob/v3/devices/childDevice/list',$param,$deviceSerial);
    }

    //普通表单请求
    private function sendRequest($url,$param){
        $accessToken = cache($this->cacheKey);
        if (empty($accessToken)) {
            $accessToken = $this->getAccessToken();
        }
        if (!$accessToken) {
            $this->errorMsg = 'get accessToken error';
            return [];
        }
        $param['accessToken'] = $accessToken;
        $rs = Http::post($this->apiUrl.$url,$param);
        if (!$rs){
            $this->errorMsg = 'request error';
            return [];
        }
        $rs = json_decode($rs,true);
        if ($rs['code'] !="200"){
            $this->errorMsg = 'get info error:'.$rs['msg'];
            return [];
        }
        return $rs["data"];
    }

    //json请求带请求头
    private function sendRequest2($url,$param,$deviceSerial){
        $accessToken = cache($this->cacheKey);
        if (empty($accessToken)) {
            $accessToken = $this->getAccessToken();
        }
        if (!$accessToken) {
            $this->errorMsg = 'get accessToken error';
            return [];
        }
        /*$header[CURLOPT_HTTPHEADER] = [
             "accessToken:".$accessToken,
             "deviceSerial:".$deviceSerial,
             "content-type:application/json"
         ];
         $rs = Http::post($this->apiUrl.$url,$param,$header);   */
        $client = new Client([
            'verify' => false
        ]);
        $headers = [
            'accessToken' => $accessToken,
            'deviceSerial' => $deviceSerial,
            'content-type' => 'application/json'
        ];
        try {
            $rs = $client->request('POST',$this->apiUrl.$url,['headers'=>$headers,'json'=>$param,'http_errors'=>false]);
        }catch (RequestException $e) {
            // 处理4xx或5xx错误
            $this->errorMsg =  'RequestException: ' . $e->getMessage();
            return [];
        } catch (TransferException $e) {
            // 处理传输错误
            $this->errorMsg =  'TransferException: ' . $e->getMessage();
            return [];
        } catch (Exception $e) {
            // 处理其他异常
            $this->errorMsg =  'Other Exception: ' . $e->getMessage();
            return [];
        }
        $rs = $rs->getBody()->getContents();
        if (!$rs){
            $this->errorMsg = 'request error';
            return [];
        }
        $rs = json_decode($rs,true);
        if ($rs['meta']['code'] !="200"){
            $this->errorMsg = 'get info error:'.$rs['meta']['message'];
            return [];
        }
        return $rs["data"];
    }

    //form请求带请求头
    private function sendRequest3($url,$param,$method = 'POST'){
        $accessToken = cache($this->cacheKey);
        if (empty($accessToken)) {
            $accessToken = $this->getAccessToken();
        }
        if (!$accessToken) {
            $this->errorMsg = 'get accessToken error';
            return [];
        }
        $client = new Client([
            'verify' => false
        ]);
        $headers = [
            'accessToken' => $accessToken,
            'content-type' => 'application/x-www-form-urlencoded'
        ];
        $postData = ['headers'=>$headers,'http_errors'=>false];
        if ($method == 'POST') {
            $postData['form_params'] = $param;
        }else{
            $postData['query'] = $param;
        }
        try {
            $rs = $client->request($method,$this->apiUrl.$url,$postData);
        }catch (RequestException $e) {
            // 处理4xx或5xx错误
            $this->errorMsg =  'RequestException: ' . $e->getMessage();
            return [];
        } catch (TransferException $e) {
            // 处理传输错误
            $this->errorMsg =  'TransferException: ' . $e->getMessage();
            return [];
        } catch (Exception $e) {
            // 处理其他异常
            $this->errorMsg =  'Other Exception: ' . $e->getMessage();
            return [];
        }
        $rs = $rs->getBody()->getContents();
        if (!$rs){
            $this->errorMsg = 'request error';
            return [];
        }
        $rs = json_decode($rs,true);
        if (!isset($rs['code'])) {
            $this->errorMsg = 'get info error:'.$rs['message'];
            return [];
        }
        if ($rs['code'] !="200"){
            $this->errorMsg = 'get info error:'.$rs['msg'];
            return [];
        }
        return $rs["data"]??[];
    }
	
    private function getAccessToken(){
        if (empty($this->appKey) || empty($this->secret)) {
            $this->errorMsg = 'appKey or secret empty';
            return [];
        }
        $param = [
            "appKey" => $this->appKey,
            "appSecret" => $this->secret
        ];
        $rs = Http::post("https://open.ys7.com/api/lapp/token/get",$param);
        if (!$rs){
            $this->errorMsg = 'get getAccessToken error';
            return [];
        }
        $rs = json_decode($rs,true);
        if ($rs['code'] !="200"){
            $this->errorMsg = 'get getAccessToken error:'.$rs['msg'];
            return [];
        }
        cache($this->cacheKey,$rs["data"]["accessToken"],$this->cacheTime);
        return $rs["data"]["accessToken"];
    }

    //
    public function addMember($param){
        return $this->sendRequest3('/api/v3/device/company/member/add',$param);
    }

    public function updateMember($param){
        return $this->sendRequest3('/api/v3/device/company/member/update',$param);
    }

    public function delMember($param){
        return $this->sendRequest3('/api/v3/device/company/member/delete',$param,'DELETE');
    }

    //
    public function addMemberExt($param){
        return $this->sendRequest3('/api/v3/device/company/member/ext/add',$param);
    }

    //
    public function updateMemberExt($param){
        return $this->sendRequest3('/api/v3/device/company/member/ext/update',$param);
    }

    public function getMember($param){
        return $this->sendRequest3('/api/v3/device/company/member/queryByNo',$param,'GET');
    }

    public function sendMemberToDevice($param){
        return $this->sendRequest3('/api/v3/device/company/member/info/send',$param,'POST');
    }

    public function sendBatchMemberToDevice($param){
        return $this->sendRequest3('/api/v3/device/company/member/info/batch/send',$param,'POST');
    }

    public function getEventList($deviceSerial,$param){
        $accessToken = cache($this->cacheKey);
        if (empty($accessToken)) {
            $accessToken = $this->getAccessToken();
        }
        if (!$accessToken) {
            $this->errorMsg = 'get accessToken error';
            return [];
        }
        $client = new Client([
            'verify' => false
        ]);
        $headers = [
            'accessToken' => $accessToken,
            'deviceSerial' => $deviceSerial
        ];
        $postData = ['headers'=>$headers,'http_errors'=>false];
        $method = 'GET';
        if ($method == 'POST') {
            $postData['form_params'] = $param;
        }else{
            $postData['query'] = $param;
        }
        try {
            $rs = $client->request($method,$this->apiUrl.'/api/service/devicekit/common/device/event',$postData);
        }catch (RequestException $e) {
            // 处理4xx或5xx错误
            $this->errorMsg =  'RequestException: ' . $e->getMessage();
            return [];
        } catch (TransferException $e) {
            // 处理传输错误
            $this->errorMsg =  'TransferException: ' . $e->getMessage();
            return [];
        } catch (Exception $e) {
            // 处理其他异常
            $this->errorMsg =  'Other Exception: ' . $e->getMessage();
            return [];
        }
        $rs = $rs->getBody()->getContents();
        if (!$rs){
            $this->errorMsg = 'request error';
            return [];
        }
        $rs = json_decode($rs,true);
        if (!isset($rs['meta']['code'])) {
            $this->errorMsg = 'get info error:'.$rs['meta']['message'];
            return [];
        }
        if ($rs['meta']['code'] !="200"){
            $this->errorMsg = 'get info error:'.$rs['meta']['message'];
            return [];
        }
        return $rs["data"];
    }

    public function elevatorInfo($param){
        $accessToken = cache($this->cacheKey);
        if (empty($accessToken)) {
            $accessToken = $this->getAccessToken();
        }
        if (!$accessToken) {
            $this->errorMsg = 'get accessToken error';
            return [];
        }
        $client = new Client([
            'verify' => false
        ]);
        $headers = $param;
        $headers['accessToken'] = $accessToken;
        $postData = ['headers'=>$headers,'http_errors'=>false];
        $method = 'GET';
        try {
            $rs = $client->request($method,$this->apiUrl.'/api/devicekit/elevator/sos/info/detail',$postData);
        }catch (RequestException $e) {
            // 处理4xx或5xx错误
            $this->errorMsg =  'RequestException: ' . $e->getMessage();
            return [];
        } catch (TransferException $e) {
            // 处理传输错误
            $this->errorMsg =  'TransferException: ' . $e->getMessage();
            return [];
        } catch (Exception $e) {
            // 处理其他异常
            $this->errorMsg =  'Other Exception: ' . $e->getMessage();
            return [];
        }
        $rs = $rs->getBody()->getContents();
        if (!$rs){
            $this->errorMsg = 'request error';
            return [];
        }
        $rs = json_decode($rs,true);
        if (!isset($rs['meta']['code'])) {
            $this->errorMsg = 'get info error:'.$rs['meta']['message'];
            return [];
        }
        if ($rs['meta']['code'] !="200"){
            $this->errorMsg = 'get info error:'.$rs['meta']['message'];
            return [];
        }
        return $rs["data"];
    }

    private function dateTimeIsoChange($dateTime,$t=1){
        $timezone = new \DateTimeZone('+08:00');
        $date = new \DateTime($dateTime, $timezone);
        if (1==$t){
            return $date->format('Y-m-d\TH:i:sP');
        }
        return $date->format('Y-m-d\TH:i:s');
    }
    public function getInOutList($deviceSerial,$startTime,$endTime,$start,$limit){
        $accessToken = cache($this->cacheKey);
        if (empty($accessToken)) {
            $accessToken = $this->getAccessToken();
        }
        if (!$accessToken) {
            $this->errorMsg = 'get accessToken error';
            return [];
        }
        $client = new Client([
            'verify' => false
        ]);
        $headers = [
            'EZO-AccessToken' => $accessToken,
            'EZO-DeviceSerial' => $deviceSerial,
            'EZO-Date' => date('Y-m-d H:i:s'),
            'Content-Type' => 'application/json'
        ];
        $param = [
            'IDCardInfoEventCond' => [
                'searchID' => '0d1610cd-f97d-4bb0-aa5a-35310fd37cf7',
                'searchResultPosition' => intval($start),
                'maxResults' => intval($limit),
                'startTime' => $this->dateTimeIsoChange($startTime),
                'endTime' => $this->dateTimeIsoChange($endTime),
                'majorEventType' => 0,
                'subEventType' => 0
            ]
        ];
        try {
            $rs = $client->request('POST',$this->apiUrl.'/api/hikvision/ISAPI/AccessControl/IDCardInfoEvent?format=json',['headers'=>$headers,'json'=>$param,'http_errors'=>false]);
        }catch (RequestException $e) {
            // 处理4xx或5xx错误
            $this->errorMsg =  'RequestException: ' . $e->getMessage();
            return [];
        } catch (TransferException $e) {
            // 处理传输错误
            $this->errorMsg =  'TransferException: ' . $e->getMessage();
            return [];
        } catch (Exception $e) {
            // 处理其他异常
            $this->errorMsg =  'Other Exception: ' . $e->getMessage();
            return [];
        }
        $rs = $rs->getBody()->getContents();
        if (!$rs){
            $this->errorMsg = 'request error';
            return [];
        }
        $rs = json_decode($rs,true);
        if (isset($rs['errorCode'])){
            $this->errorMsg = json_encode($rs);
            return [];
        }
        return $rs["IDCardInfoEvent"];
    }

    public function getDeviceMemberList($deviceSerial,$start,$limit){
        $accessToken = cache($this->cacheKey);
        if (empty($accessToken)) {
            $accessToken = $this->getAccessToken();
        }
        if (!$accessToken) {
            $this->errorMsg = 'get accessToken error';
            return [];
        }
        $client = new Client([
            'verify' => false
        ]);
        $headers = [
            'EZO-AccessToken' => $accessToken,
            'EZO-DeviceSerial' => $deviceSerial,
            'EZO-Date' => date('Y-m-d H:i:s'),
            'Content-Type' => 'application/json'
        ];
        $param = [
            'UserInfoSearchCond' => [
                'searchID' => '0d1610cd-f97d-4bb0-aa5a-35310fd37cf7',
                'searchResultPosition' => intval($start),
                'maxResults' => intval($limit),
                'subEventType' => 0
            ]
        ];
        try {
            $rs = $client->request('POST',$this->apiUrl.'/api/hikvision/ISAPI/AccessControl/UserInfo/Search?format=json',['headers'=>$headers,'json'=>$param,'http_errors'=>false]);
        }catch (RequestException $e) {
            // 处理4xx或5xx错误
            $this->errorMsg =  'RequestException: ' . $e->getMessage();
            return [];
        } catch (TransferException $e) {
            // 处理传输错误
            $this->errorMsg =  'TransferException: ' . $e->getMessage();
            return [];
        } catch (Exception $e) {
            // 处理其他异常
            $this->errorMsg =  'Other Exception: ' . $e->getMessage();
            return [];
        }
        $rs = $rs->getBody()->getContents();
        if (!$rs){
            $this->errorMsg = 'request error';
            return [];
        }
        $rs = json_decode($rs,true);
        if (isset($rs['errorCode'])){
            $this->errorMsg = json_encode($rs);
            return [];
        }
        return $rs['UserInfoSearch'];
    }

    public function addDeviceMember($deviceSerial,$employeeNo,$name,$gender,$beginTime,$endTime){
        $accessToken = cache($this->cacheKey);
        if (empty($accessToken)) {
            $accessToken = $this->getAccessToken();
        }
        if (!$accessToken) {
            $this->errorMsg = 'get accessToken error';
            return [];
        }
        $client = new Client([
            'verify' => false
        ]);
        $headers = [
            'EZO-AccessToken' => $accessToken,
            'EZO-DeviceSerial' => $deviceSerial,
            'EZO-Date' => date('Y-m-d H:i:s'),
            'Content-Type' => 'application/json'
        ];
        $param = [
            'UserInfo' => [
                'employeeNo' => $employeeNo,
                'name' => $name,
                'userType' => 'normal',
                'gender' => $gender,
                'localUIRight' => false,
                'maxOpenDoorTime' => 0,
                'RightPlan' => [['doorNo'=>1]],
                'Valid' => [
                    'enable' => true,
                    'beginTime' => $this->dateTimeIsoChange($beginTime,0),
                    'endTime' => $this->dateTimeIsoChange($endTime,0),
                    'timeType' => 'local'
                ],
                'floorNumbers'=>[],
                'callNumbers' =>[],
                'userVerifyMode' => ''
            ]
        ];
        try {
            $rs = $client->request('POST',$this->apiUrl.'/api/hikvision/ISAPI/AccessControl/UserInfo/Record?format=json',['headers'=>$headers,'json'=>$param,'http_errors'=>false]);
        }catch (RequestException $e) {
            // 处理4xx或5xx错误
            $this->errorMsg =  'RequestException: ' . $e->getMessage();
            return [];
        } catch (TransferException $e) {
            // 处理传输错误
            $this->errorMsg =  'TransferException: ' . $e->getMessage();
            return [];
        } catch (Exception $e) {
            // 处理其他异常
            $this->errorMsg =  'Other Exception: ' . $e->getMessage();
            return [];
        }
        $rs = $rs->getBody()->getContents();
        if (!$rs){
            $this->errorMsg = 'request error';
            return [];
        }
        $rs = json_decode($rs,true);
        if (isset($rs['errorCode'])){
            $this->errorMsg = json_encode($rs);
            return [];
        }
        return $rs;
    }

    public function addDeviceMemberExt($deviceSerial,$employeeNo,$cardNo){
        $accessToken = cache($this->cacheKey);
        if (empty($accessToken)) {
            $accessToken = $this->getAccessToken();
        }
        if (!$accessToken) {
            $this->errorMsg = 'get accessToken error';
            return [];
        }
        $client = new Client([
            'verify' => false
        ]);
        $headers = [
            'EZO-AccessToken' => $accessToken,
            'EZO-DeviceSerial' => $deviceSerial,
            'EZO-Date' => date('Y-m-d H:i:s'),
            'Content-Type' => 'application/json'
        ];
        $param = [
            'CardInfo' => [
                'employeeNo' => $employeeNo,//添加人员的工号
                'cardNo' => $cardNo,//身份证号
                'cardType' => 'normalCard',//卡的类型--普通卡
            ]
        ];
        try {
            $rs = $client->request('POST',$this->apiUrl.'/api/hikvision/ISAPI/AccessControl/CardInfo/Record?format=json',['headers'=>$headers,'json'=>$param,'http_errors'=>false]);
        }catch (RequestException $e) {
            // 处理4xx或5xx错误
            $this->errorMsg =  'RequestException: ' . $e->getMessage();
            return [];
        } catch (TransferException $e) {
            // 处理传输错误
            $this->errorMsg =  'TransferException: ' . $e->getMessage();
            return [];
        } catch (Exception $e) {
            // 处理其他异常
            $this->errorMsg =  'Other Exception: ' . $e->getMessage();
            return [];
        }
        $rs = $rs->getBody()->getContents();
        if (!$rs){
            $this->errorMsg = 'request error';
            return [];
        }
        $rs = json_decode($rs,true);
        if (isset($rs['errorCode'])){
            $this->errorMsg = json_encode($rs);
            return [];
        }
        return $rs;
    }

    public function getEventRecord($deviceSerial,$startTime,$endTime,$start,$limit){
        $accessToken = cache($this->cacheKey);
        if (empty($accessToken)) {
            $accessToken = $this->getAccessToken();
        }
        if (!$accessToken) {
            $this->errorMsg = 'get accessToken error';
            return [];
        }
        $client = new Client([
            'verify' => false
        ]);
        $headers = [
            'EZO-AccessToken' => $accessToken,
            'EZO-DeviceSerial' => $deviceSerial,
            'EZO-Date' => date('Y-m-d H:i:s'),
            'Content-Type' => 'application/json'
        ];
        $param = [
            'EventSearchDescription' => [
                'searchID' => 'EBCED88D-1D9D-4622-B033-330A1C0E613B',
                'searchResultPosition' => intval($start),
                'maxResults' => intval($limit),
                'timeSpanList' => [['startTime'=>$this->dateTimeIsoChange($startTime),'endTime'=>$this->dateTimeIsoChange($endTime)]],
                'type' => 'all',
                'channels'=>[1,2,3],
                'eventType' => 'all'
            ]
        ];
        try {
            $rs = $client->request('POST',$this->apiUrl.'/api/hikvision/ISAPI/ContentMgmt/eventRecordSearch?format=json',['headers'=>$headers,'json'=>$param,'http_errors'=>false]);
        }catch (RequestException $e) {
            // 处理4xx或5xx错误
            $this->errorMsg =  'RequestException: ' . $e->getMessage();
            return [];
        } catch (TransferException $e) {
            // 处理传输错误
            $this->errorMsg =  'TransferException: ' . $e->getMessage();
            return [];
        } catch (Exception $e) {
            // 处理其他异常
            $this->errorMsg =  'Other Exception: ' . $e->getMessage();
            return [];
        }
        $rs = $rs->getBody()->getContents();
        if (!$rs){
            $this->errorMsg = 'request error';
            return [];
        }
        $rs = json_decode($rs,true);
        if (isset($rs['errorCode'])){
            $this->errorMsg = json_encode($rs);
            return [];
        }
        return $rs['EventSearchResult'];
    }
}