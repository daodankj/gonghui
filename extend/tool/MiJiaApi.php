<?php

namespace tool;

use \fast\Http;
use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;

class MiJiaApi{

	public $user = '';
	public $password = '';
	public $errorMsg = '';
    protected $cacheKey = '';
	protected $cacheTime = 3600 * 24 * 30;

	public function __construct($user,$password){
		$this->user = $user;
		$this->password = $password;
        $this->cacheKey = $user.'_xmLoginInfo';
	}

    public function setCache($userId,$ssecurity,$deviceId,$serviceToken){
        $loginInfo = cache($this->cacheKey);
        $loginInfo['userId'] = $userId;
        $loginInfo['ssecurity'] = $ssecurity;
        $loginInfo['deviceId'] = $deviceId;
        $loginInfo['serviceToken'] = $serviceToken;
        cache($this->cacheKey,$loginInfo,$this->cacheTime);
    }

    public function getCache(){
        return cache($this->cacheKey);
    }

	public function getDeviceList(){
       $loginInfo = cache($this->cacheKey);
       if (empty($loginInfo)) {
           $loginInfo = $this->authLogin();
       }
       if (empty($loginInfo)) {
           return [];
       }
       $loginInfo['deviceId'] = empty($loginInfo['deviceId']) ? '' : $loginInfo['deviceId'];
       $header[CURLOPT_HTTPHEADER] = [
          "User-Agent:APP/com.xiaomi.mihome APPV/6.0.103 iosPassportSDK/3.9.0 iOS/14.4 miHSTS",
          "x-xiaomi-protocal-flag-cli:PROTOCAL-HTTP2",
          "Cookie:PassportDeviceId={$loginInfo['deviceId']};userId={$loginInfo['userId']};serviceToken={$loginInfo['serviceToken']};"
       ];
       $host = 'https://api.io.mi.com/app';
       $url = '/home/device_list';
       $nonce = $this->generateNonce();
       $data = '{"getVirtualModel": false, "getHuamiDevices": 0}';
       $signedNonce = $this->generateSignedNonce($loginInfo['ssecurity'],$nonce);
       $signature = $this->generateSignature($url,$signedNonce,$nonce,$data);
       $param = [
           '_nonce' => $nonce,
           'data' => $data,
           'signature' => $signature
       ];
        $rs = Http::post($host.$url,$param,$header);       
        if (!$rs){
        	$this->errorMsg = 'get device list error';
            return [];
        }
        $rs = json_decode($rs,true);
        if ($rs['code'] !=0){
        	$this->errorMsg = 'get device list error:'.$rs['message'];
            return [];
        }
        return $rs['result']['list'];
    }
	
    private function authLogin(){
        if (empty($this->user) || empty($this->password)) {
            $this->errorMsg = 'user or password empty';
            return [];
        }
        $info = $this->serviceLogin();
        if (empty($info)) {
        	$this->errorMsg = 'get serviceLogin error';
            return [];
        }
        $authUrl = 'https://account.xiaomi.com/pass/serviceLoginAuth2';
        $cookieJar = new CookieJar();
        // 创建一个GuzzleHttp客户端实例，并将cookieJar传递给它
        $client = new Client([
            'cookies' => $cookieJar,
            'verify' => false
        ]);
        // 设置请求头
        $headers = [
            'User-Agent' => 'APP/com.xiaomi.mihome APPV/6.0.103 iosPassportSDK/3.9.0 iOS/14.4 miHSTS',
        ];
        $param = [
            'qs'=>$info['qs'],
            'sid'=>$info['sid'],
            '_sign'=>$info['_sign'],
            'callback'=>$info['callback'],
            'user'=>$this->user,
            'hash'=>strtoupper(md5($this->password)),
            '_json'=>true
        ];
        $response = $client->post($authUrl,['form_params'=>$param,'headers'=>$headers]);
        if ($response->getStatusCode() !== 200) {
        	$this->errorMsg = 'Login failed with status code: '. $response->getStatusCode();
            return [];
        }
        $rs = $response->getBody()->getContents();
        if (!$rs){
        	$this->errorMsg = 'get authLogin error ';
            return [];
        }
        $ret = substr($rs,11);
        $ret = json_decode($ret,true);
        if ($ret['code'] != 0) {
        	$this->errorMsg = $ret['message'];
            return [];
        }
        //获取serviceToken
        if(empty($ret['location'])){
            //print_r($ret);
            $this->errorMsg = 'authLogin error:not get location';
            return [];
        }
        $dashboardResponse = $client->get($ret['location']);
        // 检查 GET 请求是否成功
        if ($dashboardResponse->getStatusCode() !== 200) {
        	$this->errorMsg = 'get location error with status code : '. $dashboardResponse->getStatusCode();
            return [];
        }
        // 获取最终的 cookies（包括登录后设置的和可能在重定向后更新的）
        $cookies = $cookieJar->toArray();
        // 打印 cookies（仅用于调试）
        //print_r($cookies);
        $ret['serviceToken'] = $this->getServiceTokenFromCookies($cookies);
        if (empty($ret['serviceToken'])) {
        	$this->errorMsg = 'get serviceToken error';
            return [];
        }
        if (!empty($ret['userId'])&&!empty($ret['ssecurity'])) {
        	$ret['deviceId'] = $this->generateNonce();
            cache($this->cacheKey,$ret,$this->cacheTime);
        }else{
        	$this->errorMsg = 'get authLogin error:userId or ssecurity empty';
            return [];
        }
        return $ret;
    }

    private function serviceLogin(){
        $loginUrl = 'https://account.xiaomi.com/pass/serviceLogin?sid=xiaomiio&_json=true';
        $header = ['User-Agent:APP/com.xiaomi.mihome APPV/6.0.103 iosPassportSDK/3.9.0 iOS/14.4 miHSTS'];
        $rs = Http::get($loginUrl,[],$header);
        if ($rs) {
            return json_decode(substr($rs, 11),true);
        }
        return [];
    }

    private function getServiceTokenFromCookies($cookies){
        $serviceToken = '';
        foreach($cookies as $val){
            if ('serviceToken' == $val['Name']) {
                $serviceToken = $val['Value'];
                break;
            }
        }
        return $serviceToken;
    }

    private function generateNonce($length=16) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $nonce = '';
        for ($i = 0; $i < $length; $i++) {
            $nonce .= $characters[rand(0, $charactersLength - 1)];
        }
        // Shuffle the nonce to ensure randomness
        $nonceArray = str_split($nonce);
        shuffle($nonceArray);
        return implode('', $nonceArray);
    }

    private function generateSignedNonce($securityToken, $nonce){
        // 解码 secret 和 nonce
        $decodedSecret = base64_decode($securityToken);
        $decodedNonce = base64_decode($nonce);

        // 连接解码后的 secret 和 nonce 并计算其 SHA-256 哈希值
        $concatenated = $decodedSecret . $decodedNonce;
        
        $hash = hash('sha256', $concatenated, true); // 第三个参数为 true 以获取原始二进制输出

        // 返回结果的 Base64 编码
        return base64_encode($hash);
    }

    private function generateSignature($url,$signedNonce,$nonce,$data){
        // 解码 signedNonce 以获取原始密钥
        $key = base64_decode($signedNonce);

        // 创建签名字符串
        $sign = $url . '&' . $signedNonce . '&' . $nonce . '&data=' . $data;

        // 计算 HMAC-SHA256 签名
        // 使用 hash_hmac 函数计算 HMAC，第三个参数 true 表示返回原始二进制数据
        $hmac = hash_hmac('sha256', $sign, $key, true);

        // 返回结果的 Base64 编码
        return base64_encode($hmac);
    }
}