alter table fa_company add pingyi_image varchar(255) not null default "" comment '评议图片介绍';
alter table fa_company add pingyi_intro text default null comment '评议文字介绍';

CREATE TABLE `fa_jianse_pingyi` (
    `id` int(5) NOT NULL AUTO_INCREMENT,
    `topic_id` int(5) NOT NULL DEFAULT '0' COMMENT '所属主题',
    `company_id` int(5) NOT NULL DEFAULT '0' COMMENT '所属公司',
    `username` varchar(255) NOT NULL DEFAULT '' COMMENT '用户名称',
    `user_id` int(5) NOT NULL DEFAULT '0' COMMENT '用户',
    `ztgz` tinyint(1) NOT NULL DEFAULT '0' COMMENT '总体工作',
    `wthd` tinyint(1) NOT NULL DEFAULT '0' COMMENT '文体活动',
    `zgbf` tinyint(1) NOT NULL DEFAULT '0' COMMENT '职工帮扶',
    `mzgz` tinyint(1) NOT NULL DEFAULT '0' COMMENT '民主工作',
    `ldgx` tinyint(1) NOT NULL DEFAULT '0' COMMENT '劳动关系',
    `ghzz` tinyint(1) NOT NULL DEFAULT '0' COMMENT '工会组织',
    `add_time` int(5) NOT NULL DEFAULT '0' COMMENT '评议时间',
    PRIMARY KEY (`id`),
    KEY `company_id` (`company_id`),
    KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='建设评议列表';

CREATE TABLE `fa_zhuxi_pingyi` (
   `id` int(5) NOT NULL AUTO_INCREMENT,
   `topic_id` int(5) NOT NULL DEFAULT '0' COMMENT '所属主题',
   `company_id` int(5) NOT NULL DEFAULT '0' COMMENT '所属公司',
   `user_id` int(5) NOT NULL DEFAULT '0' COMMENT '用户',
   `username` varchar(255) NOT NULL DEFAULT '' COMMENT '用户名称',
   `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '评议:1:满意 2:不满意',
   `content` varchar(2048) NOT NULL DEFAULT '' COMMENT '评议内容',
   `add_time` int(5) NOT NULL DEFAULT '0' COMMENT '评议时间',
   PRIMARY KEY (`id`),
   KEY `company_id` (`company_id`),
   KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COMMENT='主席评议列表';